# Locate hpctoolkit
# This module defines
# HPCTOOLKIT_INCLUDE_DIR, where to find the headers
# HPCTOOLKIT_LIBRARY hpctoolkit libraires
#

FIND_PATH(HPCTOOLKIT_INCLUDE_DIR hpctoolkit.h
    $ENV{HPCTOOLKIT_DIR}/include
    $ENV{HPCTOOLKIT_DIR}
    ~/Library/Frameworks
    /Library/Frameworks
    /usr/local/include
    /usr/include
    /soft/hpctoolkit/5.3.2 # lcrc blues@anl
    /usr/include/hpctoolkit # MT
    /usr/local/include/hpctoolkit #brew, manual
    /sw/include # Fink
    /opt/local/include # DarwinPorts
    /opt/local/include/hpctoolkit # DarwinPorts # MT
    /opt/csw/include # Blastwave
    /opt/include/hpctoolkit
    /usr/X11R6/include/hpctoolkit
)


find_library(HPCTOOLKIT_LIBRARY
             NAMES hpctoolkit
             PATHS /usr/lib
                  /usr/local/lib
                 ENV HPCTOOLKIT_DIR
                 ENV LD_LIBRARY_PATH
                 ENV LIBRARY_PATH
             PATH_SUFFIXES hpctoolkit
            )

SET(HPCTOOLKIT_FOUND 0)
IF(HPCTOOLKIT_LIBRARY AND HPCTOOLKIT_INCLUDE_DIR)
  SET(HPCTOOLKIT_FOUND 1)
ENDIF(HPCTOOLKIT_LIBRARY AND HPCTOOLKIT_INCLUDE_DIR)
