/*
 Copyright (c) 2015 UChicago Argonne, LLC

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>
#include "ipm.h"

/*
    Implements RK4 for a scalar problem using both IPM and raw C
*/
static void RKCompute(IPM_RArray scale,IPMK_ArrayNumeric Q,IPMK_ArrayNumeric W,IPMK_ArrayNumeric K,IPMK_Error *err)
{
  IPMK_ArrayLocalIter iter;
  for (IPMK_ArrayLocalIterInit(Q, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPM_Index i = IPMK_ArrayLocalIterToIndex(&iter);
    for (int j=0; j<IPM_ArrayGetLength((IPM_Array)Q,i,err); j++) {
      IPMK_ArrayNumericGet(K,i,err)[j] = IPMK_ArrayNumericGet(Q,i,err)[j] + *IPMK_RArrayGet(scale,err)*IPMK_ArrayNumericGet(W,i,err)[j];
    }
  }
}

static void RKCompute5(IPM_RArray scale,IPMK_ArrayNumeric Q,IPMK_ArrayNumeric k1,IPMK_ArrayNumeric k2,IPMK_ArrayNumeric k3,IPMK_ArrayNumeric k4,IPMK_Error *err)
{
  IPMK_ArrayLocalIter iter;
  for (IPMK_ArrayLocalIterInit(Q, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPM_Index i = IPMK_ArrayLocalIterToIndex(&iter);
    for (int j=0; j<IPM_ArrayGetLength((IPM_Array)Q,i,err); j++) {
      IPMK_ArrayNumericGet(Q,i,err)[j] = IPMK_ArrayNumericGet(Q,i,err)[j] + *IPMK_RArrayGet(scale,err)*(IPMK_ArrayNumericGet(k1,i,err)[j]+2*IPMK_ArrayNumericGet(k2,i,err)[j]+2*IPMK_ArrayNumericGet(k3,i,err)[j]+IPMK_ArrayNumericGet(k4,i,err)[j]);
    }
  }
}

/*
   This print isn't good since the various threads can print randomly
*/
static void kPrint(IPMK_ArrayNumeric Q,IPMK_Error *err)
{
  IPMK_ArrayLocalIter iter;
  for (IPMK_ArrayLocalIterInit(Q, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPM_Index i = IPMK_ArrayLocalIterToIndex(&iter);
    for (int j=0; j<IPM_ArrayGetLength((IPM_Array)Q,i,err); j++) {
      printf("%15.12e ",IPMK_ArrayNumericGet(Q,i,err)[j]);
    }
  }
  printf("\n");
}

void RK4Init(RK4 *rk,IPM_ArrayNumeric Q,IPM_Function rhs)
{
  IPM_Error err;

  rk->Q  = Q;
  // this code is wrong and needs to be changed once Clone() accepts -1 flag to indicate get lengths from parent
  int deflen = IPM_ArrayGetLength((IPM_Array)Q,0,&err);
  rk->k1 = IPM_ArrayNumericClone("k1", (IPM_Array)Q, deflen, 0.0, 0,&err);
  rk->k2 = IPM_ArrayNumericClone("k2", (IPM_Array)Q, deflen, 0.0, 0,&err);
  rk->k3 = IPM_ArrayNumericClone("k3", (IPM_Array)Q, deflen, 0.0, 0,&err);
  rk->k4 = IPM_ArrayNumericClone("k4", (IPM_Array)Q, deflen, 0.0, 0,&err);
  rk->W  = IPM_ArrayNumericClone("W", (IPM_Array)Q, deflen, 0.0, 0,&err);

  rk->scale = IPM_RArrayCreate("scale",1,&err);

  rk->myrhs     = rhs;
  rk->myCompute  = IPM_FunctionCreate(IPM_FUNCTION_DEFAULT,(void*)RKCompute,4,IPM_MEMORY_READ,IPM_MEMORY_READ,IPM_MEMORY_READ,IPM_MEMORY_WRITE,&err);
  rk->myCompute5 = IPM_FunctionCreate(IPM_FUNCTION_DEFAULT,(void*)RKCompute5,6,IPM_MEMORY_READ,IPM_MEMORY_WRITE,IPM_MEMORY_READ,IPM_MEMORY_READ,IPM_MEMORY_READ,IPM_MEMORY_READ,&err);
  rk->myPrint    = IPM_FunctionCreate(IPM_FUNCTION_DEFAULT,(void*)kPrint,1,IPM_MEMORY_READ,&err);

  rk->time      = 0.0;
  rk->dt        = .01;
  rk->FinalTime = 1.0;
  rk->tstep     =1;

}

#define EvaluateODERHS(rk,q,w,err) {rk->myrhs->array[0] = (IPM_Array)q; rk->myrhs->array[1] = (IPM_Array)w;  IPM_FunctionLaunch(rk->myrhs,err);}

void RK4Solve(RK4 *rk)
{
  IPM_Error err;


  while (rk->time<rk->FinalTime){

    if (rk->time+rk->dt>rk->FinalTime) rk->dt = rk->FinalTime - rk->time;

    EvaluateODERHS(rk,rk->Q,rk->k1,&err);

    *IPMK_RArrayGet(rk->scale,&err) = rk->dt/2.0;
    IPM_Launch(rk->myCompute, (IPM_Array)rk->scale,rk->Q, rk->k1, rk->W, &err) ;
    EvaluateODERHS(rk,rk->W,rk->k2,&err);

    IPM_Launch(rk->myCompute, (IPM_Array)rk->scale,rk->Q, rk->k2, rk->W, &err) ;
    EvaluateODERHS(rk,rk->W,rk->k3,&err);

    *IPMK_RArrayGet(rk->scale,&err) = rk->dt;
    IPM_Launch(rk->myCompute, (IPM_Array)rk->scale,rk->Q, rk->k3, rk->W, &err) ;
    EvaluateODERHS(rk,rk->W,rk->k4,&err);

    *IPMK_RArrayGet(rk->scale,&err) = rk->dt/6;
    IPM_Launch(rk->myCompute5, (IPM_Array)rk->scale, rk->Q, rk->k1, rk->k2, rk->k3, rk->k4,  &err) ;

    rk->time += rk->dt;
    rk->tstep++;
    printf("Time %15.12e ",rk->time);
    IPM_Launch(rk->myPrint, (IPM_Array)rk->Q, &err) ;
  }
}



