#include <unordered_map>
#include <vector>
#include <set>
#include <map>
#include <algorithm>
#include <utility>
#include <iostream>

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <assert.h>
#include <string.h>
#include <parmetis.h>
#include "ipmimpl.h"
#include "ipmutils.h"

using namespace std;

extern void IPMR_CommNumericAddWithContext(IPM_ArrayNumeric u, IPM_Error *err);

int gdebug, grank;
int IPMR_AbortOnError = 1;
int skip_parmetis = 0;

// Auxiliary array for comparison in sorting
static std::vector<global_int_t> *aux_cmp_vec = NULL;
static bool aux_cmp(global_int_t x, global_int_t y)
{
  return aux_cmp_vec->at(x) < aux_cmp_vec->at(y);
}

static bool compare_ipair_first(const std::pair<local_int_t, local_int_t>& x,
                         const std::pair<local_int_t, local_int_t>& y)
{
  return (x.first < y.first);
}

struct PackDS {
  int totpts; // Total number of unique points to be packed in this array
  int totlen; // Sum of lengths of these points
  int bufsize; // Storage size (in bytes) for packing the array and its clones
  std::vector<local_int_t> indices; // Of length 'totpts', storing local indices of these points
  std::vector<std::pair<local_int_t, local_int_t> > ipairs; // Of length 'totlen', storing pairs of {value, seqnum}
    // these points. Only meaningfull for index arrays. Used in
    // index rewritting. Suppose we are gonna pack 2 points p1 (s, t) and p2(u, v),
    // then we will store (s, 0) (t, 1) (u, 2) (v, 3) in ipairs[].

  // Map local indices to new local indices (counting from 0) in packing.
  std::unordered_map<local_int_t, local_int_t> imap;
  //std::map<local_int_t, local_int_t> imap;
  PackDS() { totpts = totlen = bufsize = 0; }
};

// Data structure for unpacking pieces of hierachical arrays
struct UnpackDS {
  // At layer l, we receive points with gids and put them at positions of lids.
  // Use this map to map gids to lids and also filter duplicated gids.
  std::unordered_map<global_int_t, local_int_t> gid2lid_map;

  // At layer l, from a certain process, we receive points with gids = (gid0, gid1, gid2, ...).
  // These points may already exist locally or not. Nevertheless, with gid2lid_map, we can get
  // their lids, e.g.,  (lid5, lid3, lid8, ...). Let us define vector lid2lid_map[] = {lid5, lid3, lid8}.
  // At layer l+1, we receive points with index values = (idx0, idx1, idx2, ...). Note that
  // these idx_i's are meant to point to data with gids = (gid0, gid1, gid2, ...) at layer l.
  // Therefore, we need to rewrite idx_i as lid2lid_map[idx_i].
  std::vector<local_int_t> lid2lid_map;

#ifdef IPM_CHECK
  std::unordered_map<global_int_t, local_int_t> gid2lid_map2;
#endif

};

/**
 * Constructor of array
 */
_p_IPM_Array::_p_IPM_Array(const char* name)
  : name(name)
{
  attr = 0;
  original = NULL;
  disp.push_back(0);
  layout = new _p_IPMR_Layout(); assert(layout);
  indexto = NULL;
}

/**
 * Clone an array
 */
_p_IPM_Array::_p_IPM_Array(const char* name, IPM_Array ref, int deflen, int temporary)
  : name(name), deflen(deflen)
{
  local_int_t nvisible = ref->layout->nvisible;
  disp.reserve(nvisible+1);
  attr = IPM_ARRAYATTR_ISCLONE;
  original = (ref->attr & IPM_ARRAYATTR_ISCLONE) ? ref->original: ref;

  if (deflen > 0) { // Fixed-length
    IPM_Index ofst = 0;
    for (IPM_Index i = 0; i < nvisible+1; i++) {
      disp.push_back(ofst);
      ofst += deflen;
    }
  } else if (deflen < 0) { // Hardclone. Entry lengths are copied from original
    attr |= IPM_ARRAYATTR_HARDCLONE;
    for (IPM_Index i = 0; i < nvisible+1; i++) {
      disp.push_back(ref->disp[i]); //TODO: using memcpy
    }
  } else { // Entry lengths are unknown
    disp.push_back(0);
  }

  attr |= IPM_ARRAYATTR_ISCLONE;
  this->layout = ref->layout;
  indexto = NULL;
  if (!temporary) layout->clones.push_back(this);
}

/**
 * Copy an array
 */
_p_IPM_Array::_p_IPM_Array(const char* name, IPM_Array ref, int temporary)
  : name(name), disp(ref->disp), layout(ref->layout)
{
  // Do not copy newdisp since we should only copy a fully committed array
  attr = ref->attr;
  original = (ref->attr & IPM_ARRAYATTR_ISCLONE) ? ref->original: ref;
  deflen = ref->deflen;
  attr |= IPM_ARRAYATTR_ISCLONE;
  this->layout = ref->layout;
  indexto = NULL;
  if (!temporary) layout->clones.push_back(this);
}

_p_IPM_Array::~_p_IPM_Array()
{
  if (layout) {
    int i = 0;
    for (; i < layout->clones.size(); i++) {
      if (this == layout->clones[i]) break;
    }
    if (i < layout->clones.size()) {
      layout->clones.erase(layout->clones.begin() + i);
    }

    if (layout->refcount > 1) {
      layout->refcount--;
    } else {
      delete layout;
    }
  }
}
_p_IPM_ArrayIndex::_p_IPM_ArrayIndex(const char* name, IPM_Array indexto)
  : _p_IPM_Array(name)
{
  type = IPM_ARRAY_INDEX;
  connect_depth = 1;
  this->indexto = indexto;
}

_p_IPM_ArrayIndex::_p_IPM_ArrayIndex(const char* name, IPM_Array ref, int deflen, local_int_t defval, int hops, int temporary)
  : _p_IPM_Array(name, ref, deflen, temporary), data(disp.back(), defval), defval(defval)
{
  type = IPM_ARRAY_INDEX;
  this->hops = hops;
  this->indexto = ref; // An index clone can only index into its original
  layout->refcount++; // Inc refcount when successfully cloned
}

_p_IPM_ArrayNumeric::_p_IPM_ArrayNumeric(const char* name)
  : _p_IPM_Array(name)
{
   type = IPM_ARRAY_NUMERIC;
   addctx = NULL;
}

_p_IPM_ArrayNumeric::_p_IPM_ArrayNumeric(const char* name, IPM_Array ref, int deflen, IPM_Real defval, int temporary)
  : _p_IPM_Array(name, ref, deflen, temporary), data(disp.back(), defval), defval(defval)
{
  type = IPM_ARRAY_NUMERIC;
  layout->refcount++; // Inc refcount when successfully cloned
  addctx = NULL;
}

_p_IPM_ArrayNumeric::_p_IPM_ArrayNumeric(const char* name, IPM_ArrayNumeric ref, int temporary)
  : _p_IPM_Array(name, (IPM_Array)ref, temporary), data(ref->data), defval(ref->defval)
{
  type = IPM_ARRAY_NUMERIC;
  layout->refcount++; // Inc refcount when successfully cloned
  addctx = NULL;
}

_p_IPM_RArray::_p_IPM_RArray(const char* name, int length)
  : _p_IPM_Array(name), data(length)
{
  type = IPM_ARRAY_REDUNDANT;
}

IPM_ArrayIndex IPM_ArrayIndexCreate(const char *name, IPM_Array indexto, IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  return new _p_IPM_ArrayIndex(name, indexto);
}

IPM_ArrayIndex IPM_ArrayIndexClone(const char *name, IPM_Array orig, int deflen, local_int_t defval, int temporary, int hops, IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  return new _p_IPM_ArrayIndex(name, orig, deflen, defval, hops, temporary);
}

IPM_ArrayNumeric IPM_ArrayNumericClone(const char *name, IPM_Array orig, int deflen, IPM_Real defval, int temporary, IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  return new _p_IPM_ArrayNumeric(name, orig, deflen, defval, temporary);
}

IPM_ArrayNumeric IPM_ArrayNumericCreate(const char *name, IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  return new _p_IPM_ArrayNumeric(name);
}

// x[i] = alpha*x[i] for all i
void IPM_ArrayNumericScale(IPM_ArrayNumeric x, IPM_Real alpha, IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  for (int i = 0; i < x->data.size(); i++) x->data[i] *= alpha;
}

/*
IPM_ArrayNumeric IPM_ArrayNumericCopy(const char *name, IPM_ArrayNumeric orig, int temporary, IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  return new _p_IPM_ArrayNumeric(name, orig, temporary);
}
*/

// y = x
void IPM_ArrayNumericCopy(IPM_ArrayNumeric x, IPM_ArrayNumeric y, IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  assert(y->data.size() == x->data.size());
  y->data = x->data;
}

// w(i) = x(i)*y(i) for all i
void IPM_ArrayNumericPointwiseMult(IPM_ArrayNumeric w, IPM_ArrayNumeric x, IPM_ArrayNumeric y, IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  assert(y->data.size() == x->data.size() && w->data.size() == x->data.size());
  for (int i = 0; i < w->data.size(); i++) w->data[i] = x->data[i] * y->data[i];
}

// x(i) = 1/x(i) for all i
void IPM_ArrayNumericReciprocal(IPM_ArrayNumeric x, IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  for (int i = 0; i < x->data.size(); i++) x->data[i] = 1.0/x->data[i];
}

/**
 * y = y + alpha*x
 */
void IPM_ArrayNumericAXPY(IPM_ArrayNumeric y, IPM_Real alpha, IPM_ArrayNumeric x, IPM_Error *err)
{
  for (int i = 0; i < y->data.size(); i++) y->data[i] += alpha*x->data[i];
}

/**
 * y = x + alpha*y
 */
void IPM_ArrayNumericAYPX(IPM_ArrayNumeric y, IPM_Real alpha, IPM_ArrayNumeric x, IPM_Error *err)
{
  for (int i = 0; i < y->data.size(); i++) y->data[i] = x->data[i] + alpha*y->data[i];
}

/**
 * y = alpha*x + beta*y
 */
void IPM_ArrayNumericAXPBY(IPM_ArrayNumeric y, IPM_Real alpha, IPM_Real beta, IPM_ArrayNumeric x, IPM_Error *err)
{
  if (alpha == 1.0) {
    if (beta == 1.0) {
      // y = x + y
      for (int i = 0; i < x->data.size(); i++) y->data[i] += x->data[i];
    } else {
      // y = x + beta*y
      for (int i = 0; i < x->data.size(); i++) y->data[i] = x->data[i] + beta*y->data[i];
    }
  } else {
    if (beta == 1.0) {
      // y = alpha*x + y
      for (int i = 0; i < x->data.size(); i++) y->data[i] += alpha*x->data[i];
    } else {
      // y = alpha*x + beta*y
      for (int i = 0; i < x->data.size(); i++) y->data[i] = alpha*x->data[i] + beta*y->data[i];
    }
  }
}

/**
 * Return inner product of x and y on local entries
 */
IPM_Real IPM_ArrayNumericInnerProduct(IPM_ArrayNumeric x, IPM_ArrayNumeric y, IPM_Error *err)
{
  IPM_Real sum = .0;
  for (int i = 0; i < x->layout->locals.size(); i++) {
    IPM_Index idx = x->layout->locals[i];
    sum += x->data[idx]*y->data[idx];
  }
  return sum;
}


void IPM_ArrayNumericSet(IPM_ArrayNumeric u, IPM_Real alpha, IPM_Error *err)
{
     u->data.assign(u->data.size(), alpha);
}

IPM_RArray IPM_RArrayCreate(const char *name, int length, IPM_Error * err)
{
  *err = IPM_ERROR_NONE;
  return new _p_IPM_RArray(name, length);
}

void IPM_ArrayIndexDestroy(IPM_ArrayIndex u, IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  delete u;
}

void IPM_ArrayNumericDestroy(IPM_ArrayNumeric u, IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  delete u;
}

void IPM_ArrayNumericSwap(IPM_ArrayNumeric u, IPM_ArrayNumeric v, IPM_Error *err)
{
*err = IPM_ERROR_NONE;
  u->data.swap(v->data);
}

void IPM_RArrayDestroy(IPM_RArray u, IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  delete u;
}

void IPM_ArrayDestroy(IPM_Array u, IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  IPM_ArrayIndex v1;
  IPM_ArrayNumeric v2;
  IPM_RArray v3;

  switch (u->type) {
    case IPM_ARRAY_INDEX:
      v1 = static_cast<IPM_ArrayIndex>(u);
      delete v1;
      break;
    case IPM_ARRAY_NUMERIC:
      v2 = static_cast<IPM_ArrayNumeric>(u);
      delete v2;
      break;
    case IPM_ARRAY_REDUNDANT:
      v3 = static_cast<IPM_RArray>(u);
      delete v3;
      break;
    default:
      *err = IPM_ERROR_WRONG_DATATYPE;
  }
}

void IPM_FunctionDestroy(IPM_Function func, IPM_Error *err)
{
  free(func);
  *err = IPM_ERROR_NONE;
}

void IPMK_ArrayLocalIterInit(void* v, IPMK_ArrayLocalIter *iter)
{
  IPM_Array u = (IPM_Array)v;
  iter->cur = 0;
  iter->end = u->layout->nlocal;
  iter->full = (u->layout->nlocal == u->layout->nvisible) ? 1 : 0;
  iter->locals = &(u->layout->locals[0]);
}

void IPMK_ArrayVisibleIterInit(void* v, IPMK_ArrayVisibleIter *iter)
{
  IPM_Array u = (IPM_Array)v;
  iter->cur = 0;
  iter->end = u->layout->nvisible;
}

// Get an entry of an array by its local index
IPMK_Index* IPMK_ArrayIndexGet(IPM_ArrayIndex u, IPMK_Index p, IPM_Error *err)
{
  assert(p >= 0);
  *err = IPM_ERROR_NONE;
  return &(u->data[u->disp[p]]);
}

IPM_Real * IPMK_ArrayNumericGet(IPM_ArrayNumeric u, IPMK_Index p, IPM_Error *err)
{
  assert(p >= 0);
  *err = IPM_ERROR_NONE;
  return &(u->data[u->disp[p]]);
}

IPM_Real * IPMK_RArrayGet(IPM_RArray u, IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  return &(u->data[0]);
}

// Get length of an entry by its local index
int IPM_ArrayGetLength(void *v, IPMK_Index p,IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  IPM_Array u = (IPM_Array)v;
  return u->disp[p+1] - u->disp[p];
}

// Provide these two interfaces when a user wants to access all visible data points
IPM_Index IPMK_ArrayIndexVisibleStart(IPM_ArrayIndex u,IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  return 0;
}
IPM_Index IPMK_ArrayIndexVisibleEnd(IPM_ArrayIndex u,IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  return u->layout->nvisible;
}

void IPM_LaunchWait(IPM_Function func,IPM_Error *err)
 {
  *err = IPM_ERROR_NONE;
 }

int IPM_ArrayTotalSize(IPM_Array u, IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  return u->layout->ntotal;
}

IPM_Index IPM_ArrayVisibleSize(IPM_Array u, IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  return u->layout->nvisible;
}

IPM_Index IPM_ArrayLocalSize(IPM_Array u, IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  return u->layout->nlocal;
}

IPM_Array IPMK_ArrayIndexTo(IPM_Array u, IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  assert(u->type == IPM_ARRAY_INDEX);
  return u->indexto;
}

global_int_t IPM_ArrayGetGid(IPM_Array u, IPMK_Index p, IPM_Error * err)
{
  return u->layout->gids[p];
}

void IPM_ArrayAttachContext(IPM_Array u, const char* name, void *ctxdata, void *freefunc, IPM_Error *err)
{
  typedef void (*ctx_freefunc_t)(void* ctxdata);

  *err = IPM_ERROR_NONE;
  std::string strname(name);
  std::map<std::string, std::pair<void*, void*> >::iterator iter = u->layout->ctxmap.find(strname);

  // Free the existing one first if any
  if (iter != u->layout->ctxmap.end()) {
    ((ctx_freefunc_t)(iter->second.second))(iter->second.first);
  }

  u->layout->ctxmap[strname] = std::pair<void*, void*>(ctxdata, freefunc);
}

// Runtime automatically fress context.
//void IPM_ArrayDetachContext()

void* IPM_ArrayLookupContext(IPM_Array u, const char* name, IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  std::string strname(name);
  std::map<std::string, std::pair<void*, void*> >::iterator iter = u->layout->ctxmap.find(strname);

  if (iter != u->layout->ctxmap.end()) {
    return iter->second.first;
  } else {
    return NULL;
  }
}

void IPM_ArrayIndexSetTop(IPM_ArrayIndex u, int connect_depth, IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  u->attr |=  IPM_ARRAYATTR_TOP;
  u->connect_depth = connect_depth;
}

void IPM_ArrayNumericPrint(IPMK_ArrayNumeric u, IPMK_Error *err)
{
  int rank, nproc;
  int ready = 1;
  IPMR_Layout layout = u->layout;
  MPI_Comm comm = u->layout->comm;
  int MY_MPI_TAG = 101;

  MPI_Comm_rank(comm, &rank);
  MPI_Comm_size(comm, &nproc);
  char *locally_owned = NULL;

  if (layout->nvisible > layout->nlocal) {
    locally_owned = (char *)calloc(layout->nvisible, sizeof(char));
    for (IPM_Index i = 0; i < layout->nlocal; i++) {
      IPM_Index idx = layout->locals[i];
      assert(idx < layout->nvisible);
      locally_owned[idx] = 1;
    }
  }

  if (rank > 0) MPI_Recv(&ready, 1, MPI_INT, rank-1, MY_MPI_TAG, comm, MPI_STATUS_IGNORE);

  printf("===On rank %d===\n", rank);
  // Print the array name in color
  printf("Numeric array \x1B[35m%s\x1B[0m has ntotal=%d, nvisible=%d, nlocal=%d,\nwith visible points in gid=(value, ...) format:\n",
    u->name.c_str(), layout->ntotal, layout->nvisible, layout->nlocal);

  for (IPM_Index p = 0; p < layout->nvisible; p++) {
    int len  = u->disp[p+1] - u->disp[p];
    IPM_Index disp = u->disp[p];
    global_int_t gid = layout->gids[p];
    // Use reference(=>) to differentiate points visible but not locally owned
    const char *refstr = locally_owned ? (locally_owned[p] ? "=" : "=>") : "=";
    for (int i = 0; i < len; i++) {
      IPM_Real val = u->data[disp+i];
      if (i == 0) printf("%d%s(%.2f", gid, refstr, val);
      else printf(", %.2f", val);
    }
    printf(") ");
  }
  printf("\n\n");
  fflush(stdout);
  free(locally_owned);

  if (rank < nproc - 1) MPI_Send(&ready, 1, MPI_INT, rank+1, MY_MPI_TAG, comm);

  *err = 0;
}

void IPM_ArrayIndexPrint(IPMK_ArrayIndex u, IPMK_Error *err)
{
  int rank, nproc;
  int ready = 1;
  IPMR_Layout layout_ind = u->layout; // Layout for index
  IPMR_Layout layout_val = u->indexto->layout; // Layout for value
  MPI_Comm comm = u->layout->comm;
  int MY_MPI_TAG = 100;
  char *locally_owned = NULL;

  MPI_Comm_rank(comm, &rank);
  MPI_Comm_size(comm, &nproc);

  if (layout_ind->nvisible > layout_ind->nlocal) {
    locally_owned = (char*)calloc(layout_ind->nvisible, sizeof(char));
    for (IPM_Index i = 0; i < layout_ind->nlocal; i++) {
      IPM_Index idx = layout_ind->locals[i];
      assert(idx < layout_ind->nvisible);
      locally_owned[idx] = 1;
    }
  }

  if (rank > 0) MPI_Recv(&ready, 1, MPI_INT, rank-1, MY_MPI_TAG, comm, MPI_STATUS_IGNORE);

  printf("===On rank %d===\n", rank);
  // Print the array name in color
  printf("Index array \x1B[35m%s\x1B[0m has ntotal=%d, nvisible=%d, nlocal=%d,\nwith visible points in gid=(gid, ...) format:\n",
    u->name.c_str(), layout_ind->ntotal, layout_ind->nvisible, layout_ind->nlocal);

  //for (IPM_Index p = 0; p < layout_ind->nvisible; p++) {
  for (IPM_Index p = 0; p < u->disp.size()-1; p++) {
    int len  = u->disp[p+1] - u->disp[p];
    IPM_Index disp = u->disp[p];
    IPM_Index gid = layout_ind->gids[p];
    const char *refstr = locally_owned ? (locally_owned[p] ? "=" : "=>") : "=";
    for (int i = 0; i < len; i++) {
      local_int_t idx = u->data[disp+i];
      IPM_Index gid_val = (idx == IPM_INDEX_NULL) ? IPM_INDEX_NULL : layout_val->gids[idx];
      if (i == 0) printf("%d%s(%d", gid, refstr, gid_val);
      else printf(", %d", gid_val);
    }
    printf(") ");
  }
  printf("\n\n");
  fflush(stdout);
  free(locally_owned);

  if (rank < nproc - 1) MPI_Send(&ready, 1, MPI_INT, rank+1, MY_MPI_TAG, comm);

  *err = 0;
}

void IPM_RArrayPrint(IPMK_RArray u, IPMK_Error *err)
{
  assert(u->type == IPM_ARRAY_REDUNDANT);

  int rank, nproc;

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &nproc);

  printf("===On rank %d===\n", rank);
  printf("RArray \x1B[35m%s\x1B[0m [%u] = ", u->name.c_str(), (unsigned)u->data.size());
  for (int i = 0; i < u->data.size(); i++) {
    IPM_Real val = u->data[i];
    if (i == 0) printf("{%.2f", val);
    else printf(", %.2f", val);
  }
  printf("}\n");
  *err = 0;
}

void IPM_ArrayPrint(IPM_Array u, IPMK_Error *err)
{
  switch (u->type) {
    case IPM_ARRAY_INDEX :
      IPM_ArrayIndexPrint((IPM_ArrayIndex)u, err); break;
    case IPM_ARRAY_NUMERIC :
      IPM_ArrayNumericPrint((IPM_ArrayNumeric)u, err); break;
    case IPM_ARRAY_REDUNDANT :
      IPM_RArrayPrint((IPM_RArray)u, err); break;
  }
}

IPM_Function ipm_arraynumericprint = NULL,ipm_arrayindexprint = NULL;

void IPM_Initialize(int *argc,char ***args,IPM_Error *err)
{
  MPI_Init(argc,args);
  MPI_Comm_rank(MPI_COMM_WORLD, &grank);

  if (sizeof(IPM_Index) != sizeof(idx_t)) {
    fprintf(stderr, "IPM_Index and ParMETIS idx_t must have the same size\n");
    abort();
  }

  if (sizeof(IPM_Real) != sizeof(real_t)) {
    fprintf(stderr, "IPM_Real and ParMETIS real_t must have the same size\n");
    abort();
  }

  ipm_arraynumericprint = IPM_FunctionCreate(IPM_FUNCTION_SEQUENTIAL, (void*)IPM_ArrayNumericPrint, 1, IPM_MEMORY_READ,err);
  ipm_arrayindexprint = IPM_FunctionCreate(IPM_FUNCTION_SEQUENTIAL, (void*)IPM_ArrayIndexPrint, 1, IPM_MEMORY_READ,err);
  *err = 0;
}

void IPM_Finalize(IPM_Error *err)
{
  free(ipm_arraynumericprint);
  free(ipm_arrayindexprint);
  *err = MPI_Finalize();
}


// Newly added data is stashed, and the indices returned are not supposed
// to be used immediately in IPMK_ArrayNumericGet
IPMK_Index IPMK_ArrayNumericAdd(IPM_ArrayNumeric u, int len, IPM_Real *values,IPM_Error *err)
{
  assert(len >= 0);

  // Push values to back of newdata
  u->newdata.insert(u->newdata.end(), values, values+len);

  // Also record the disp in newdisp
  local_int_t disp;
  if (u->newdisp.empty()) { disp = u->disp.back() + len; }
  else { disp = u->newdisp.back() + len; }

  u->newdisp.push_back(disp);

  // Returned indices are always based on data, instead of newdata
  return u->disp.size() + u->newdisp.size() - 2;
}

IPMK_Index IPMK_ArrayIndexAdd(IPM_ArrayIndex u, int len, IPMK_Index *values,IPM_Error *err)
{
  assert(len >= 0);

  // Push values to back of newdata
  u->newdata.insert(u->newdata.end(), values, values+len);

  // Also record the disp in newdisp
  local_int_t disp;
  if (u->newdisp.empty()) { disp = u->disp.back() + len; }
  else { disp = u->newdisp.back() + len; }

  u->newdisp.push_back(disp);

  // Returned indices are always based on data, instead of newdata
  return u->disp.size()  + u->newdisp.size() - 2;
}

void IPMK_ArrayIndexRemove(IPM_ArrayIndex u, IPMK_Index p, IPM_Error *err)
{
  if (u->attr & IPM_ARRAYATTR_ISCLONE) return;
  u->layout->trashbin.insert(p);
}

void IPMK_ArrayIndexChange(IPM_ArrayIndex u,IPMK_Index p, int len, local_int_t *values,IPM_Error *err)
{
  int oldlen = u->disp[p+1] - u->disp[p];
  assert(len == oldlen);
  memcpy(&(u->data[u->disp[p]]), values, sizeof(local_int_t)*len);
}

void IPMK_ArrayNumericChange(IPM_ArrayNumeric u, IPMK_Index p, int len, IPM_Real *values, IPM_Error *err)
{
  int oldlen = u->disp[p+1] - u->disp[p];
  assert(len == oldlen);
  memcpy(&(u->data[u->disp[p]]), values, sizeof(IPM_Real)*len);
}

void IPM_RArrayZero(IPM_RArray u, IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  for (int i = 0; i < u->data.size(); i++) { u->data[i] = 0.0; }
}

// Get gid of an entry by its local index.
global_int_t IPMR_ArrayGetGid(IPM_Array u, IPMK_Index p)
{
  return u->layout->gids[p];
}

void IPMR_RArrayReduce(IPM_RArray u, IPM_MemoryType ipm_op, IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  IPM_Real *buf = &u->data[0];
  int len = u->data.size();
  // TODO: How to decide communicator of a RArray?
  MPI_Op mpi_op;

  // TODO: use a table like ops_ipm2mpi[] to directly map the operations
  switch (ipm_op) {
    case IPM_MEMORY_ADD :
      mpi_op = MPI_SUM; break;
    case IPM_MEMORY_PROD :
      mpi_op = MPI_PROD; break;
    case IPM_MEMORY_MAX :
      mpi_op = MPI_MAX; break;
    case IPM_MEMORY_MIN :
      mpi_op = MPI_MIN; break;
    default:
      abort();
  }
  MPI_Allreduce(MPI_IN_PLACE, buf, len, REAL_DTYPE, mpi_op, MPI_COMM_WORLD);
}

//==============================================================================
// IPMR_FindSharedPoints:
//
// Given an array \p u, find out shared points and build the communication pattern.
// We know a point is shared if its gid appears on more than two processes.
// Assume range of gids of this array is [0, N). The basic algorithm is:
// Imagine the N points are owned blockly by processes. Each process sends its
// points' gids to their owners . The owner processes then get a complete
// picture of the points they own. They compute sharing info and send the info back.
// As a result, each process knows which points it has are shared and for each
// shared point, which processes share it.
//
// Suppose p2's block range is [lb, ub), and p1 has a point with gid in this range,
// in our code, p1 and p2 refer the point with rgid = gid - lb, which is called
// a rebased gid. rgid is useful when indexing local arrays.
//==============================================================================
void IPMR_FindSharedPoints(IPM_Array u, IPM_Error *err)
{
  int nprocs;
  int myrank;
  int MY_MPI_TAG = 99;
  int nvisible, ntotal;
  IPMR_Layout layout;
  MPI_Comm comm;
  MPI_Status status;

  // Stuff for sending rebased gids (rgids) to their owners
  int tgts = 0;
  int *sendto;
  int *sendbuf = NULL; // Use int since rgids are relatively small
  int *senddisp = NULL;
  MPI_Request *sendreqs = NULL;

  int srcs = 0;
  int tot_recvlen;
  int *recvbuf = NULL;
  int *recvfrom = NULL;
  int *recvdisp = NULL;
  MPI_Request *recvreqs = NULL;

  // Stuff for sending the sharing info (in interges) back
  int *new_sendbuf = NULL;
  int *new_senddisp = NULL;
  MPI_Request *new_sendreqs = NULL;

  int *new_recvbuf = NULL;
  int *new_recvdisp = NULL;
  MPI_Request *new_recvreqs = NULL;

  int accum;
  int myblocksize;

  *err = IPM_ERROR_NONE;

  //===========================================================================
  // Sort visible points by their gids to faciliate grouping gids. Also remember
  // their original local indices so that we can easily map them back.
  //===========================================================================
  assert(u->type == IPM_ARRAY_NUMERIC || u->type == IPM_ARRAY_INDEX);

  layout = u->layout;
  ntotal = layout->ntotal;
  nvisible= layout->nvisible;

  comm = layout->comm;
  MPI_Comm_size(comm, &nprocs);
  MPI_Comm_rank(comm, &myrank);
  myblocksize = BLOCK_SIZE(myrank, nprocs, ntotal);

  sendbuf = (int *)malloc(sizeof(int)*nvisible); assert(sendbuf);

  std::vector<local_int_t> lids(nvisible);
  std::vector<global_int_t> gids(nvisible);

  for (int i = 0; i < nvisible; i++) lids[i] = i;

  aux_cmp_vec = &(u->layout->gids);
  stable_sort(lids.begin(), lids.end(), aux_cmp);

  for (int i = 0; i < nvisible; i++) {
    gids[i] = u->layout->gids[lids[i]];
  }

  //===========================================================================
  // Group visible points by their destination processes. In other words,
  // calculate send info: tgts, sendto[], senddisp[], etc.
  //===========================================================================
  int currank = 0;
  int curidx = 0;
  IPM_Index curlow = BLOCK_LOW(currank, nprocs, ntotal);
  IPM_Index curhigh = BLOCK_HIGH(currank, nprocs, ntotal);
  int first = 1;
  int curlen = 16; // Arbitrary len to init sendto & senddisp
  sendto = (int *)malloc(sizeof(int)*curlen); assert(sendto);
  senddisp = (int *)malloc(sizeof(int)*(curlen+1)); assert(senddisp);

  while (curidx < nvisible) {
    if (curlow <= gids[curidx] && gids[curidx] <= curhigh) {
      if (first) { // Meet the first (smallest) gid for currank
        first = 0;
        if (tgts > curlen) {
          curlen *= 2; // Double the buffer
          int *tmp;
          tmp = (int *)realloc(sendto, sizeof(int)*curlen); assert(tmp);
          sendto = tmp;
          tmp = (int *)realloc(senddisp, sizeof(int)*(curlen+1)); assert(tmp);
          senddisp = tmp;
        }
        sendto[tgts] = currank;
        senddisp[tgts] = curidx;
        tgts++;
      }
      curidx++;
    } else { // gid is out of range of the currank
      first = 1;
      currank++;
      curlow = BLOCK_LOW(currank, nprocs, ntotal);
      curhigh = BLOCK_HIGH(currank, nprocs, ntotal);
    }
  }
  senddisp[tgts] = nvisible;

  // Rebase gids and store them in sendbuf
  for (int i = 0; i < tgts; i++) {
    IPM_Index base = BLOCK_LOW(sendto[i], nprocs, ntotal);
    for (int j = senddisp[i]; j < senddisp[i+1]; j++) {
      sendbuf[j] = gids[j] - base;
    }
  }
  sendreqs = (MPI_Request *)malloc(sizeof(MPI_Request)*tgts); assert(sendreqs);

  //===========================================================================
  // Compute the reverse receive info, in other words, compute srcs, recvfrom[],
  //  tot_recvlen, recvdisp[].
  //===========================================================================
  int *tmpv = (int *)calloc(nprocs*2, sizeof(int)); assert(tmpv);
  for (int i = 0; i < tgts; i++) {
    tmpv[2*sendto[i]] = 1;
    tmpv[2*sendto[i]+1] = senddisp[i+1] - senddisp[i];
  }
  MPI_Allreduce(MPI_IN_PLACE, tmpv, nprocs*2, MPI_INT, MPI_SUM, comm);
  srcs = tmpv[2*myrank];
  tot_recvlen = tmpv[2*myrank + 1];
  free(tmpv);

  recvbuf = (int *)malloc(sizeof(int)*tot_recvlen); assert(recvbuf);
  recvfrom = (int *)malloc(sizeof(int)*srcs); assert(recvfrom);
  recvdisp = (int *)malloc(sizeof(int)*(srcs+1)); assert(recvdisp);
  recvreqs = (MPI_Request *)malloc(sizeof(MPI_Request)*srcs); assert(recvreqs);

  // 1st round (point to point) communcation
  for (int i = 0; i < srcs; i++) {
    // Acutally received is something like recvlen. Will adjust it later.
    MPI_Irecv(&recvdisp[i+1], 1, MPI_INT, MPI_ANY_SOURCE, MY_MPI_TAG, comm, &recvreqs[i]);
  }

  for (int i = 0; i < tgts; i++) {
    int sendlen = senddisp[i+1] - senddisp[i];
    MPI_Send(&sendlen, 1, MPI_INT, sendto[i], MY_MPI_TAG, comm);
  }

  for (int i = 0; i < srcs; i++) {
    if (MPI_Wait(&recvreqs[i], &status) != MPI_SUCCESS) {
      fprintf(stderr, "MPI_Wait error!\n");
      MPI_Abort(comm, -1);
    }
    recvfrom[i] = status.MPI_SOURCE;
  }

  // Adjust recvdisp[] to CSR format
  recvdisp[0] = 0;
  for (int i = 1; i < srcs+1; i++) recvdisp[i] += recvdisp[i-1];
  assert(recvdisp[srcs] == tot_recvlen);

  //===========================================================================
  // Start isend/irecv of rgids
  //===========================================================================

  // Use a different MPI tag to seperate message matching.
  // Otherwise, MPI_Isend below may match with MPI_Irecv before this line.
  MY_MPI_TAG++; // 2nd round communcation
  for (int i = 0; i < srcs; i++) {
    int recvlen = recvdisp[i+1] - recvdisp[i];
    MPI_Irecv(&recvbuf[recvdisp[i]], recvlen, MPI_INT, recvfrom[i], MY_MPI_TAG, comm, &recvreqs[i]);
  }

  for (int i = 0; i < tgts; i++) {
    int sendlen = senddisp[i+1] - senddisp[i];
    MPI_Isend(&sendbuf[senddisp[i]], sendlen, MPI_INT, sendto[i], MY_MPI_TAG, comm, &sendreqs[i]);
  }

  //===========================================================================
  // Summarize info received, find out shared points and do bookkeeping.
  //
  // Scan received rgids, and count # of procs sharing them in nsharers[].
  // If nsharers[i] >=2, then i must be a shared point.
  //===========================================================================
  int *nsharers = (int *)calloc(myblocksize, sizeof(int)); assert(nsharers);
  for (int i = 0; i < srcs; i++) {
    int idx;
    MPI_Waitany(srcs, recvreqs, &idx, MPI_STATUS_IGNORE); assert(idx != MPI_UNDEFINED);
    int *rgids = &recvbuf[recvdisp[idx]]; // Rebased gids from rank idx
    for (int j = 0; j < recvdisp[idx+1] - recvdisp[idx]; j++) {
      nsharers[rgids[j]]++;
    }
  }

  MPI_Waitall(tgts, sendreqs, MPI_STATUSES_IGNORE);

  // Scan nsharers[], find out total number of shared points and who share them
  int shpts = 0; //# of shared points
  int sum_nsharers = 0; // sum of nsharers[] over shared points
  // If i is a shared point, seq[i] is the sequential order of it among shared points
  int *seq = (int *)malloc(myblocksize*sizeof(int)); assert(seq);
  for (int i = 0; i < myblocksize; i++) {
      if (nsharers[i] > 1) {
        seq[i] = shpts++;
        sum_nsharers += nsharers[i];
      }
  }

  // sharerbuf[disp[i]..disp[i+1]-1] stores ranks sharing the i-th shpt
  int *sharerbuf = (int *)malloc(sizeof(int)*sum_nsharers); assert(sharerbuf);
  int *disp = (int *)malloc(sizeof(int)*(shpts+1)); assert(disp);
  accum = 0;
  for (int i = 0; i < myblocksize; i++) {
    if (nsharers[i] > 1) {
      int curval = nsharers[i];
      disp[seq[i]] = accum;
      accum += curval;
    }
  }
  disp[shpts] = accum; // Set the last sentinel

  //===========================================================================
  // Fill sharerbuf[] and calculate shared points info per src proc
  //===========================================================================
  int *shpts_per_src = (int *)calloc(srcs, sizeof(int)); assert(shpts_per_src);
  int *sum_nsharers_per_src = (int *)calloc(srcs, sizeof(int)); assert(sum_nsharers_per_src);
  int *ip = (int*)calloc(shpts, sizeof(int)); assert(ip);
  for (int i = 0; i < srcs; i++) {
    int *rgids = &recvbuf[recvdisp[i]]; // Rebased gids from src i
    for (int j = 0; j < recvdisp[i+1] - recvdisp[i]; j++) {
      int rgid = rgids[j];
      if (nsharers[rgid] > 1) {
        sharerbuf[disp[seq[rgid]] + ip[seq[rgid]]++] = recvfrom[i];
        shpts_per_src[i]++;
        sum_nsharers_per_src[i] += nsharers[rgid];
      }
    }
  }
  free(ip);

  // Sort ranks per shpt, so we can easily find owners of shpts later
  for (int i = 0; i < shpts; i++) {
    sort(&sharerbuf[disp[i]], &sharerbuf[disp[i+1]]);
  }

  //===========================================================================
  // Push the sharing info back to src procs:
  //
  // Each src proc has sent me a list of rgids. I now tell the src proc
  // which gids are shared points, and for each shared point, which procs share it.
  // Suppose P1 has sent rgids (s0, s1, s2, s3, ...) to P2, and P2 found out
  // s0, s3 are shared points. P2 communicates with P1 by referring s0 and s3
  // with their offsets, i.e., 0, 3.
  //
  // Encoding format of sendbuf for each src proc:
  // int shpts -- number of shared points
  // int ofsts[shpts] -- offsets of shared points
  // int cnts[shpts] -- counts of procs sharing each point
  // int sharers[totshr] -- ranks of procs sharing each point
  //===========================================================================
  new_senddisp = (int *)malloc(sizeof(int)*(srcs+1)); assert(new_senddisp);
  new_sendreqs = (MPI_Request *)malloc(sizeof(MPI_Request)*srcs); assert(new_sendreqs);
  new_senddisp[0] = 0;
  for (int i = 1; i < srcs+1; i++) {
    new_senddisp[i] = new_senddisp[i-1]
                     + 1 // shpts
                     + shpts_per_src[i-1]*2 // ofsts & cnts
                     + sum_nsharers_per_src[i-1]; // sharers
  }

  new_recvdisp = (int *)malloc(sizeof(int)*(tgts+1)); assert(new_recvdisp);
  new_recvreqs = (MPI_Request *)malloc(sizeof(MPI_Request)*tgts); assert(new_recvreqs);

  MY_MPI_TAG++; // 3rd round communication

  new_recvdisp[0] = 0;
  for (int i = 0; i < tgts; i++) {
    MPI_Irecv(&new_recvdisp[i+1], 1, MPI_INT, sendto[i], MY_MPI_TAG, comm, &new_recvreqs[i]);
  }

  for (int i = 0; i < srcs; i++) {
    int sendlen = new_senddisp[i+1] - new_senddisp[i];
    MPI_Send(&sendlen, 1, MPI_INT, recvfrom[i], MY_MPI_TAG, comm);
  }

  MPI_Waitall(tgts, new_recvreqs, MPI_STATUSES_IGNORE);
  for (int i = 1; i < tgts+1; i++) { // Compute the correct form of displacement
    new_recvdisp[i] += new_recvdisp[i-1];
  }

  new_recvbuf = (int *)malloc(sizeof(int)*new_recvdisp[tgts]); assert(new_recvbuf);
  new_sendbuf = (int *)malloc(sizeof(int)*new_senddisp[srcs]); assert(new_sendbuf);

  MY_MPI_TAG++; // 4th round communcation

  for (int i = 0; i < tgts; i++) {
    int recvlen = new_recvdisp[i+1] - new_recvdisp[i];
    MPI_Irecv(new_recvbuf + new_recvdisp[i], recvlen, MPI_INT, sendto[i], MY_MPI_TAG, comm, &new_recvreqs[i]);
  }

  // Fill the new_sendbuf and start sending
  for (int i = 0; i < srcs; i++) {
    int sendlen = new_senddisp[i+1] - new_senddisp[i];
    int shpts = shpts_per_src[i];
    int *buf = &new_sendbuf[new_senddisp[i]];
    int *ofsts = buf + 1; // offsets pointer
    int *cnts = ofsts + shpts;
    int *sharers = cnts + shpts; // sharer pointer
    int *rgids = &recvbuf[recvdisp[i]]; // Rebased gids from src i

    buf[0] = shpts; // # of shared points
    for (int j = 0; j < recvdisp[i+1] - recvdisp[i]; j++) {
      int rgid = rgids[j];
      if (nsharers[rgid] > 1) {
        *ofsts++ = j; // Store offset of this shpt wrt i-th recvbuf
        *cnts++ = nsharers[rgid];
        for (int k = 0; k < nsharers[rgid]; k++) {
          int shr = sharerbuf[disp[seq[rgid]]+k];
          *sharers++ = shr;
        }
      }
    }
    MPI_Isend(new_sendbuf + new_senddisp[i], sendlen, MPI_INT, recvfrom[i], MY_MPI_TAG, comm, &new_sendreqs[i]);
  }

  MPI_Waitall(srcs, new_sendreqs, MPI_STATUSES_IGNORE);
  MPI_Waitall(tgts, new_recvreqs, MPI_STATUSES_IGNORE);

//#define IPM_DEBUG
#ifdef IPM_DEBUG
  printf("\nRank %d received this sharing info for array %s:\n", myrank, a->name);
  for (int i = 0; i < tgts; i++) {
    int *buf = &new_recvbuf[new_recvdisp[i]];
    int shpts = buf[0];
    int *ofsts = buf + 1;
    int *cnts = ofsts + shpts;
    int *sharers = cnts + shpts;
    int disp = 0;
    printf("From rank %d:\n", sendto[i]);
    printf("Shared point gids:");
    for (int j = 0; j < shpts; j++) {
      int ofst = ofsts[j];
      IPM_Index lid = lids[senddisp[i] + ofst];
      IPM_Index gid = a->layout->gids[lid];
      printf("%lld, ", gid);
    }
    printf("\n");

    printf("Sharer ranks:");
    int iter = 0;
    for (int j = 0; j < shpts; j++) {
      int cnt = cnts[j];
      for (int k = 0; k < cnt; k++) {
        printf("%lld, ", sharers[iter++]);
      }
      printf(" / ");
    }
    printf("\n");
  }
#endif

  //===========================================================================
  // Scan new_recvbuf, find out shpts owned by me, and shpts owned by others.
  // Then, build commmunication patterns with that. Also, mark shpts owned by
  // others so we can easily construct layout->locals without hurting locality.
  //===========================================================================
  std::vector<std::pair<local_int_t, local_int_t> > ipairs; // <rank, lid> pairs
  std::vector<bool> is_alien(nvisible, false); // Is this point owned by others?
  int naliens = 0; // How many such alien points

  for (int i = 0; i < tgts; i++) {
    int *buf = &new_recvbuf[new_recvdisp[i]];
    int shpts = buf[0];
    int *ofsts = buf + 1;
    int *cnts = ofsts + shpts;
    int *sharers = cnts + shpts;
    int disp = 0;
    for (int j = 0; j < shpts; j++) {
      local_int_t lid = lids[senddisp[i] + ofsts[j]];
      if (sharers[disp] == myrank) { // Smallest rank owns a shpt
        for (int k = 1; k < cnts[j]; k++) { // k starts from 1 to exclude myself
          assert(sharers[disp+k] != myrank);
          ipairs.push_back(std::make_pair(sharers[disp+k], lid)); // {rank, shpt local id}
        }
      } else {
        ipairs.push_back(std::make_pair(sharers[disp], lid)); // {rank, shpt local id}
        is_alien[lid] = 1; // for layout->locals purpose
        naliens++;
      }
      disp += cnts[j];
    }
  }

  //===========================================================================
  // Sort ipairs[] according to ranks of neighbors
  //
  // Points in ipairs[] are already ordered according to their gids on a per
  // neighbor basis. We must use stable sorts to maintain this order, in order
  // to have a consistent view of the shpts between me and neighbors. Note we
  // have to provide compare_ipair_first. Without it, C++ will compare first and
  // if equal then second, which is not what we want.
  //===========================================================================

  std::stable_sort(ipairs.begin(), ipairs.end(), compare_ipair_first);

  //===========================================================================
  // Scan ipairs[] to find out shpts per nbr, i.e., build layout->{nbrs, lids, disp}
  //===========================================================================
  layout->nbrs.clear();
  layout->lids.clear();
  layout->disp.clear();

  layout->disp.push_back(0);
  int prev_nbr = MPI_PROC_NULL;
  for (int i = 0; i < ipairs.size(); i++) {
    if (ipairs[i].first != prev_nbr) {
      prev_nbr = ipairs[i].first;
      layout->nbrs.push_back(ipairs[i].first);
      layout->disp.push_back(1);
    } else {
      layout->disp.back()++;
    }
    layout->lids.push_back(ipairs[i].second);
  }

  for (int i = 1; i < layout->disp.size(); i++) layout->disp[i] += layout->disp[i-1];

  // nowers = # of nbrs with ranks less  than myrank
  std::vector<int>::iterator low = std::lower_bound(layout->nbrs.begin(), layout->nbrs.end(), myrank);
  layout->nowners = low - layout->nbrs.begin();

  //===========================================================================
  // Fill up layout->{locals, nlocal} with help of is_alien[]. We
  // maintain order in a->data[] to preserve locality existing in a->data[].
  //===========================================================================
  layout->locals.clear();
  layout->nlocal = nvisible - naliens;
  layout->locals.reserve(layout->nlocal);
  for (int i = 0; i < nvisible; i++) { if (!is_alien[i]) layout->locals.push_back(i); }

  //===========================================================================
  // Free buffers
  //===========================================================================
  free(sendto);
  free(sendbuf);
  free(senddisp);
  free(sendreqs);

  free(recvfrom);
  free(recvbuf);
  free(recvdisp);
  free(recvreqs);

  free(new_sendbuf);
  free(new_senddisp );
  free(new_sendreqs);

  free(new_recvbuf);
  free(new_recvdisp);
  free(new_recvreqs);

  free(shpts_per_src);
  free(sum_nsharers_per_src);

  free(nsharers);
  free(seq);
  free(sharerbuf);
  free(disp);
}

/**
 * Renumer Gids of data points so that they are in a contiguous range.
 * Side effect: ntotal is updated.
 */
void IPMR_RenumberGids(IPM_Array u, IPM_Error *err)
{
  int myrank;
  int MY_MPI_TAG = 299;
  IPMR_Layout layout = u->layout;
  MPI_Comm comm = layout->comm;
  local_int_t nlocal = layout->nlocal;
  global_int_t mystart, ntotal;
  global_int_t curgid;

  /* Re-compute gids of data points owned by me
    */
  MPI_Comm_rank(comm, &myrank);
  MPI_Exscan(&nlocal, &mystart, 1, GINT_MPI_DTYPE, MPI_SUM, comm);
  MPI_Allreduce(&nlocal, &ntotal, 1, GINT_MPI_DTYPE, MPI_SUM, comm);
  layout->ntotal = ntotal;
  if (myrank == 0) mystart = 0; // On rank 0, mystart is undefined after exscan
  curgid = mystart;
  for (local_int_t i = 0; i < nlocal; i++, curgid++) {
    local_int_t lid = layout->locals[i];
    u->layout->gids[lid] = curgid;
  }

  /* Send new gids to sharers, recv new gids from owners
   */
  std::vector<global_int_t> buf(layout->disp.back());
  std::vector<MPI_Request> reqs(layout->nbrs.size());

  for (local_int_t i = layout->disp[layout->nowners]; i < layout->disp.back(); i++) { buf[i] = layout->gids[layout->lids[i]]; }

  for (int i = 0; i < layout->nowners; i++) { // Recv gids from owners
    MPI_Irecv(&buf[layout->disp[i]], layout->disp[i+1] - layout->disp[i], GINT_MPI_DTYPE, layout->nbrs[i], MY_MPI_TAG, comm, &reqs[i]);
  }

  for (int i = layout->nowners; i < layout->nbrs.size(); i++) { // Send gids to sharers
    MPI_Isend(&buf[layout->disp[i]], layout->disp[i+1] - layout->disp[i], GINT_MPI_DTYPE, layout->nbrs[i], MY_MPI_TAG, comm, &reqs[i]);
  }

  MPI_Waitall(layout->nbrs.size(), &reqs[0], MPI_STATUSES_IGNORE);

  /* Set gids of data points visible to me but owned by others
    */
  for (local_int_t i = 0; i < layout->disp[layout->nowners]; i++) {
    layout->gids[layout->lids[i]] = buf[i];
  }

}

/**
 * Pack array(s) according to a given distribution
 *
 * Given an array \p top, we want to send entries of the array to \p tgts
 * processes.  Specifically, for the i-th process, we want to send ptdisp[i+1] -
 * ptdisp[i] entries, whose indices are stored at ptlist[ptdisp[i]..ptdisp[i+1]).
 *
 * This routine packs these entries and all values they reference recursively,
 * and stores them in the output argument \p sendbuf. For the i-th
 * target, its data is stored at *sendbuf[senddisp[i]..senddisp[i+1]). In other
 * words, the buffer length in bytes for the i-th process is senddisp[i+1] - senddisp[i]
 *
 * The caller is responsible for freeing sendbuf.
 *
 *
 * Note: sendto[] contains ranks of target processes. rewrite indicates if the
 * routine is called by IPMK_CommIndexWrite. They are solely for debugging.
 *
 */
void IPMR_ArrayPack(IPM_ArrayIndex top, int tgts, const int *sendto,
  const local_int_t* ptlist, const local_int_t* ptdisp,
  char **sendbuf, int *senddisp, int rewrite, int *err)
{
  //============================================================================
  // Discover the array hierarchy along with their clones below the top
  //============================================================================
  int top_is_index_clone = (top->type == IPM_ARRAY_INDEX) && (top->attr & IPM_ARRAYATTR_ISCLONE);
  int n = 0; // depth of the array hierarchy
  IPM_Array cur = top;
  while (cur) { n++; cur = cur->indexto; }
  cur = top;

  std::vector<IPM_Array> arrays(n);
  std::vector<std::vector<IPM_Array> > clones(n); // each level may have multiple clones
  std::vector<std::vector<IPM_ArrayIndex> > indexclones(n); // index clones at each layer

  for (int l = n -1; l >= 0; l--) { arrays[l] = cur; cur = cur->indexto; }

  // Find out clones at each level
  for (int l = n -1; l >= 0; l--) {
    if (l == n -1 && top_is_index_clone) continue; // itself is a clone, so skip it.
    IPMR_Layout layout = arrays[l]->layout;
    for (int i = 0; i < layout->clones.size(); i++) {
      IPM_Array clone = layout->clones[i];
      assert(clone->attr & IPM_ARRAYATTR_ISCLONE);
      if (clone != top) {
        if (clone->type == IPM_ARRAY_INDEX) indexclones[l].push_back((IPM_ArrayIndex)clone);
        clones[l].push_back(clone);
      }
    }
  }

  //============================================================================
  // For each target, compute all layers' totpts, totlen, indices and ipairs
  //============================================================================
  std::vector<PackDS*> layers_for_tgts(tgts);
  PackDS *all_layers = new PackDS[n*tgts];
  for (int i = 0; i < tgts; i++) { layers_for_tgts[i] = &all_layers[n*i]; }

  senddisp[0] = 0;
  for (int i = 0; i < tgts; i++) {
    int base = ptdisp[i];
    PackDS *layers = layers_for_tgts[i];

    // Set up top layer's totpts, totlen and indices to jump-start the filling loop
    layers[n-1].totpts = ptdisp[i+1]-ptdisp[i];
    layers[n-1].totlen = 0;
    layers[n-1].indices.resize(layers[n-1].totpts);
    for (int j = 0; j < layers[n-1].totpts; j++) {
      IPM_Index idx = ptlist[base+j];
      layers[n-1].totlen += IPM_ArrayGetLength(arrays[n-1], idx, err);
      layers[n-1].indices[j] = idx;
    }

    // Fill in lower (index) layer's totpts, totlen and indices
    for (int l = n-1; l > 0; l--) {
      assert(arrays[l]->type == IPM_ARRAY_INDEX);
      IPM_ArrayIndex u = static_cast<IPM_ArrayIndex>(arrays[l]);
      layers[l].ipairs.resize(layers[l].totlen);

      // Find out which entries are accessed at layer l-1 by reading layer l's values at indices.
      // Put layer l's values in ipairs -- a vector of pairs of {val, seqnum}. val is an index to
      // access layer l-1 and seqnum is a sequential counter. Sorting ipairs by val lets us easily
      // filter duplicates and get unique indices accessing layer l-1, while seqnum les us
      // easily renumber layer l's values in the later packing stage.
      {
        local_int_t seqnum = 0;
        for (int j = 0; j < layers[l].totpts; j++) {
          local_int_t idx = layers[l].indices[j];
          int len = IPM_ArrayGetLength(u, idx, err);
          local_int_t *dat = (IPM_Index *)IPMK_ArrayIndexGet(u, idx, err);
          for (int k = 0; k < len; k++) {
            layers[l].ipairs[seqnum] = std::make_pair(dat[k], seqnum); // {val, seqnum}
            seqnum++;
          }
        }
        assert(seqnum == layers[l].totlen);
        sort(layers[l].ipairs.begin(), layers[l].ipairs.end()); // C++ sorts pairs in first-major order
      }

      // Usually, sorting iparis will give us a set of indices we need to access at layer l-1. But if
      // layer l-1 has index clones, we need to expand the index set accordingly. One
      // exeception is when the hops of the index clone is 0, i.e., users explicitly tell
      // us not to expand the index set, simply because users know the set itself is already self-contained w.r.t the clone.
      if (!indexclones[l-1].empty()) {
        IPM_ArrayIndex iclone = indexclones[l-1].front();
        assert(indexclones[l-1].size() == 1); // currently IPM only supports one index clone per layer

        // iset is gonna hold indices accessing layer l-1. Init it with unqiue indices in ipairs of layer l.
        std::set<local_int_t> iset;
        local_int_t preidx = IPM_INDEX_NULL;
        for (int j = 0; j < layers[l].totlen; j++) {
          local_int_t curidx = layers[l].ipairs[j].first;
          if (curidx != preidx) {
            iset.insert(curidx);
            preidx = curidx;
          }
        }

        std::set<local_int_t> curring, nextring;
        curring = iset;
        do {
          // Expand from current ring, fill next ring
          for (std::set<local_int_t>::iterator iter = curring.begin(); iter != curring.end(); ++iter) {
            local_int_t idx = *iter;
            int len = IPM_ArrayGetLength(iclone, idx, err);
            local_int_t *dat = IPMK_ArrayIndexGet(iclone, idx, err);
            for (int k = 0; k < len; k++) {
              // TODO: If we further require iclone to be hierachical, then !iset.count() can be replaced with !curring.count().
              // The latter is a much smaller set than the former and thus provides faster lookup
              if (dat[k] != IPM_INDEX_NULL && !iset.count(dat[k])) {
                nextring.insert(dat[k]);
              }
            }
          }
          if (nextring.empty()) break;
          iset.insert(nextring.begin(), nextring.end());
          curring = nextring;
          nextring.clear();
        } while (1);

        // With iset, we can now set up layer l-1's indices, imap etc.
        layers[l-1].indices.resize(iset.size());
        local_int_t seqnum = 0;
        for (std::set<local_int_t>::iterator iter = iset.begin(); iter != iset.end(); ++iter) {
          local_int_t idx = *iter;
          layers[l-1].totpts++;
          layers[l-1].totlen += IPM_ArrayGetLength(arrays[l-1], idx, err);
          layers[l-1].imap[idx] = seqnum;
          layers[l-1].indices[seqnum++] = idx;
        }
      }
      else
      {
        // layer l's sorted ipairs alone can give us indices we need to access at layer l-1
        local_int_t preidx = IPM_INDEX_NULL;
        for (local_int_t j = 0; j < layers[l].totlen; j++) {
          local_int_t curidx = layers[l].ipairs[j].first;
          if (curidx != preidx) {
            layers[l-1].totpts++;
            layers[l-1].totlen += IPM_ArrayGetLength(arrays[l-1], curidx, err);
            layers[l-1].indices.push_back(curidx);
            preidx = curidx;
          }
        }
      }
    }

    // With indices of entries to be packed at all layers, we can calculate
    // storage sizes of layers for packing.
    for (int l = 0; l < n; l++) {
      //totpts and gids are applicable to both arrays[l] and its clones
      layers[l].bufsize = sizeof(int) // totpts
        + sizeof(global_int_t)*layers[l].totpts; // gids of points

      // + storage for arrays[l]
      layers[l].bufsize += sizeof(int)*layers[l].totpts // lens for arrays[l]
        + (arrays[l]->type == IPM_ARRAY_INDEX ? sizeof(IPM_Index) : sizeof(IPM_Real))*layers[l].totlen; // data

      // + storage for clones of arrays[l]
      for (int k = 0; k < clones[l].size(); k++) {
        IPM_Array clone = clones[l][k];
        int sumlen = 0;
        int bytes = (clone->type == IPM_ARRAY_INDEX) ? sizeof(local_int_t) : sizeof(IPM_Real);
        for (int j = 0; j < layers[l].totpts; j++) sumlen += IPM_ArrayGetLength(clone, layers[l].indices[j], err);
        layers[l].bufsize += sizeof(int)*layers[l].totpts // lens of this clone
          + bytes*sumlen; // data of this clone
      }
    }

    // Once we know all layers' bufsize, we can set up senddisp[]
    senddisp[i+1] = senddisp[i]; // Shift senddisp by 1 for CSR format.
    for (int l = 0; l < n; l++) { senddisp[i+1]  += layers[l].bufsize; }
  }// for all tgts

  *sendbuf = new char[senddisp[tgts]]; if (senddisp[tgts]) assert(*sendbuf);

  //============================================================================
  // Pack points (from bottom array to top array) to be sent to each target
  //============================================================================
  for (int i = 0; i < tgts; i++) {
    PackDS *layers = layers_for_tgts[i];
    char *curptr = *sendbuf + senddisp[i];

    for (int l = 0; l < n; l++) {
      IPM_Array u = arrays[l];
      *(int*)curptr = layers[l].totpts; // At offset 0, put totpts

      // then put gids
      global_int_t *gidptr = (global_int_t *)((char*)curptr + sizeof(int));
      for (int j = 0; j < layers[l].totpts; j++) {
        global_int_t gid = u->layout->gids[layers[l].indices[j]];
        *gidptr++ = gid;
      }

      // then put lens of arrays[l]
      IPM_Index *lenptr = (IPM_Index *)gidptr;
      for (int j = 0; j < layers[l].totpts; j++) {
        *lenptr++ = IPM_ArrayGetLength(u, layers[l].indices[j], err);
      }
      curptr = (char*)lenptr;

      // then put data of arrays[l], which might be renumbered indices or original numeric data.
      // For indices, we may have two renumering schemes. If layer l-1 provides a non-empty imap,
      // we use that to map old indices to new indices. Otherwise, we just count from zero.
      if (u->type == IPM_ARRAY_INDEX)
      {
        IPM_Index curidx, preidx, newidx, *datptr;
        newidx = preidx = IPM_INDEX_NULL;
        datptr = (IPM_Index *)curptr; // Init index datptr

        if (layers[l-1].imap.empty()) {
          for (int j = 0; j < layers[l].totlen; j++) {
            curidx = layers[l].ipairs[j].first; // If curidx is IPM_INDEX_NULL (-1)
            if (curidx != preidx) { newidx++; preidx = curidx;}// newidx will be -1 too,
            datptr[layers[l].ipairs[j].second] = newidx; // otherwise newidx will be 0 for the first non -1 curidx
          }
        } else {
          for (int j = 0; j < layers[l].totlen; j++) {
            curidx = layers[l].ipairs[j].first;
            if (curidx != preidx) { newidx = layers[l-1].imap[curidx]; preidx = curidx;}
            datptr[layers[l].ipairs[j].second] = newidx; // newidx could be IPM_INDEX_NULL
          }
        }
        curptr = (char*)(&datptr[layers[l].totlen]); // Update curptr
      }
      else // u->type == IPM_ARRAY_NUMERIC
      {
        IPM_ArrayNumeric v = static_cast<IPM_ArrayNumeric>(u);
        IPM_Real *datptr = (IPM_Real *)curptr; // Init numeric datpr
        for (int j = 0; j < layers[l].totpts; j++) {
          IPM_Index idx = layers[l].indices[j];
          int len = IPM_ArrayGetLength(v, idx, err);
          IPM_Real *rptr = IPMK_ArrayNumericGet(v, idx, err);
          for (int k = 0; k < len; k++) *datptr++ = *rptr++;
        }
        curptr = (char*)datptr; // Update curptr
      }

      // then put lens and data of clones of arrays[l] one by one.
      for (int k = 0; k < clones[l].size(); k++) {
        lenptr = (IPM_Index *)curptr;
        for (int j = 0; j < layers[l].totpts; j++) { // lens
          *lenptr++ = IPM_ArrayGetLength(clones[l][k], layers[l].indices[j], err);
        }

        // Clones can be Index or Numeric. For Index, we use layer l's imap
        // to map old index values to new ones.
        if (clones[l][k]->type == IPM_ARRAY_INDEX)
        {
          IPM_ArrayIndex clone = static_cast<IPM_ArrayIndex>(clones[l][k]);
          local_int_t *datptr = (local_int_t *)lenptr; // data
          for (int j = 0; j < layers[l].totpts; j++) {
            IPM_Index idx = layers[l].indices[j];
            int len = IPM_ArrayGetLength(clone, idx, err);
            local_int_t *iptr = IPMK_ArrayIndexGet(clone, idx, err);
            for (int m = 0; m < len; m++) {
              // We may choose not to move entry iptr[m] to tgt
              if (layers[l].imap.find(iptr[m]) != layers[l].imap.end())
                *datptr++ = layers[l].imap[iptr[m]];
              else
                *datptr++ = IPM_INDEX_NULL;
            }
          }
          curptr = (char*)datptr; // Update curptr for next clone
        }
        else // clones[l][k]->type == IPM_ARRAY_NUMERIC
        {
          IPM_ArrayNumeric clone = static_cast<IPM_ArrayNumeric>(clones[l][k]);
          IPM_Real *datptr = (IPM_Real *)lenptr; // data
          for (int j = 0; j < layers[l].totpts; j++) {
            IPM_Index idx = layers[l].indices[j];
            int len = IPM_ArrayGetLength(clone, idx, err);
            IPM_Real *rptr = IPMK_ArrayNumericGet(clone, idx, err);
            for (int m = 0; m < len; m++) { *datptr++ = *rptr++; }
          }
          curptr = (char*)datptr; // Update curptr for next clone
        }
      }
    }
  }

  // Free data structures used in packing
  delete [] all_layers;

#ifdef IPM_DEBUG
  // Decode the sendbuf
  if (1) {
    int myrank;
    MPI_Comm_rank(top->layout->comm, &myrank);
    for (int i = 0; i < tgts; i++) {
      size_t cursize = 0;
      char *buf = *sendbuf + senddisp[i];
      char *curptr = buf;
      int sendlen = senddisp[i+1] - senddisp[i];
      int totpts, totlen;
      int level = 0;
      printf("\n===%d sends to rank %d===\n", myrank, sendto[i]);
      while (curptr < buf + sendlen) {
        printf("\nlevel = %d\n", level);
        totpts = *(int*)curptr;
        printf("totpts = %d\n", totpts);
        curptr += sizeof(int);

        printf("gids = ");
        IPM_Index *gidptr = (IPM_Index *)curptr;
        for (int j = 0; j < totpts; j++) {
          printf("%lld,",  *gidptr++);
        }
        printf("\n");

        printf("lens = ");
        int *lenptr = (int*)gidptr;
        totlen = 0;
        for (int j = 0; j < totpts; j++) {
          totlen += *lenptr;
          printf("%d,",  *lenptr++);
        }
        printf("\n");

        printf("data = ");
        IPM_Index *iptr = (IPM_Index *)lenptr;
        IPM_Real  *dptr = (IPM_Real  *)lenptr;

        for (int j = 0; j < totlen; j++) {
          if (level == 0)  { printf("%.2f,", *dptr++); } else  { printf("%d, ",  *iptr++); }
        }
        printf("\n");
        curptr = (level == 0) ? (char*)dptr : (char *)iptr;

        printf("clones at this level\n");

        for (int j = 0; j < clones[level].size(); j++) {
          printf("lens = ");
          int *lenptr = (int*)curptr;
          totlen = 0;
          for (int j = 0; j < totpts; j++) {
            totlen += *lenptr;
            printf("%d,",  *lenptr++);
          }
          printf("\n");

          printf("data = ");
          IPM_Real  *dptr = (IPM_Real  *)lenptr;
          for (int j = 0; j < totlen; j++) printf("%.2f,", *dptr++);
          printf("\n");
          curptr = (char*)dptr;
          printf("\n");
        }
        level++;
      }
    }
  }
#endif
}

/**
 * \brief Unpack data points sent from other processors. If a data point already
 * exists locally, we will use the received one to overwrite the local one.
 *
 * Given an index array \p top, we are receiving points of the array from \p srcs
 * processes. Specifically, for the i-th process, the received data will be
 * stored at address recvbuf + recvdisp[i], with length recvdisp[i+1] - recvdisp[i]
 * in bytes. The \p srcs nonblocking receive requests are given at \p recvreqs.
 *
 * Note: recvfrom[] contains ranks of source processes, solely for debugging.
 *
 */
void IPMR_ArrayUnpackOverwrite(IPM_ArrayIndex top, int srcs, const int *recvfrom, char *recvbuf, int *recvdisp,
  MPI_Request *recvreqs, int *err)
{
  typedef std::unordered_map<global_int_t, local_int_t>::const_iterator gid2lid_map_const_iter_t;

  //============================================================================
  // Discover the array hierachy along with their clones below the top
  //============================================================================
  int top_is_index_clone = (top->type == IPM_ARRAY_INDEX) && (top->attr & IPM_ARRAYATTR_ISCLONE);
  int n = 0;
  IPM_Array cur = top;
  while (cur) { n++; cur = cur->indexto; }

  std::vector<IPM_Array> arrays(n);
  std::vector<std::vector<IPM_Array> > clones(n);

  cur = top;
  for (int l = n -1; l >= 0; l--) { arrays[l] = cur; cur = cur->indexto; }

  // Find out clones at each level
  for (int l = n -1; l >= 0; l--) {
    if (l == n -1 && top_is_index_clone) continue; // itself is a clone, so skip it.
    IPMR_Layout layout = arrays[l]->layout;
    for (int i = 0; i < layout->clones.size(); i++) {
      IPM_Array clone = layout->clones[i]; assert(clone->attr & IPM_ARRAYATTR_ISCLONE);
      if (clone != top) clones[l].push_back(clone);
    }
  }

  // Allocate the unpacking data structure, of which gidhash, map etc must be init'ed to NULL
  UnpackDS *layers = new UnpackDS[n]; assert(layers);

  // Build a gid-to-lid map for exisiting points at each layer below the top
  // TODO: The operation is expensive. Need optimizations. For example,
  // only build the map for boundary points

#if 1
  // In Index rewriting, the lbdry is still valid
  for (int l = 0; l < n; l++) {
    IPMR_Layout layout = arrays[l]->layout;
    for (std::set<local_int_t>::iterator iter=layout->lbdry.begin(); iter != layout->lbdry.end(); iter++) {
      local_int_t lid = *iter;
      global_int_t gid = layout->gids[lid];
      layers[l].gid2lid_map[gid] = lid;
    }
  }
#else
  for (int l = 0; l < n; l++) {
    IPM_Array u = arrays[l];
    for (IPM_Index lid = 0; lid < u->layout->nvisible; lid++) {
      IPM_Index gid = layout->gids[lid];
	  layers[l].gid2lid_map[gid] = lid;
    }
  }
#endif

#ifdef IPM_CHECK
  for (int l = 0; l < n; l++) {
    IPM_Array u = arrays[l];
    for (IPM_Index lid = 0; lid < u->layout->nvisible; lid++) {
      IPM_Index gid = layout->gids[lid];
	  layers[l].gid2lid_map2[gid] = lid;
    }
  }

  for (int l = 0; l < n; l++) {
    for (gid2lid_map_const_iter_t iter = layers[l].gid2lid_map.begin(); iter != layers[l].gid2lid_map.end(); iter++) {
      gid2lid_map_const_iter_t got = layers[l].gid2lid_map2.find(iter->first);
      assert(got->second == iter->second);
    }
  }
#endif

  //============================================================================
  // Merge received data from srcs one by one
  //============================================================================
  for (int i = 0; i < srcs; i++) {
    int idx;
    MPI_Waitany(srcs, recvreqs, &idx, MPI_STATUS_IGNORE); assert(idx != MPI_UNDEFINED);
    void *curpos = recvbuf + recvdisp[idx];

    // Merge points bottom-up, since upper layers will use lower layers' newly set indices
    for (int l = 0; l < n; l++) {
      int totpts = *(int*)curpos;
      IPM_Index *gidptr = (IPM_Index *)((char*)curpos + sizeof(int)); // gids
      int *lenptr = (int *)(gidptr + totpts); // lens
      IPM_Index *iptr = (IPM_Index *)(lenptr + totpts); // indices or
      IPM_Real *fptr = (IPM_Real *)(lenptr + totpts); // floating points
      int len;
      global_int_t gid;
      local_int_t lid;

      layers[l].lid2lid_map.resize(totpts); // To record where the totpts points go locally

      // Seperate IPM_ARRAY_INDEX and IPM_ARRAY_NUMERIC, since for indices, we
      // always need to renumber them before putting to local arrays. For numeric,
      // we just need to overwrite old values with new ones.
      if (arrays[l]->type == IPM_ARRAY_INDEX)
      {
        IPM_ArrayIndex u = dynamic_cast<IPM_ArrayIndex>(arrays[l]);
        for (int j = 0; j < totpts; j++) {
          // Find where is the local one, and record it in order to replay on clones
          gid = gidptr[j];
          len = lenptr[j];
          gid2lid_map_const_iter_t got = layers[l].gid2lid_map.find(gid);
          if (got != layers[l].gid2lid_map.end()) { // The point already exists but need to be updated
            // Overwrite old value with help of layers[l-1].lid2lid_map
            lid = got->second;
            local_int_t *data = &u->data[u->disp[lid]];
            for (int m = 0; m < len; m++) {
              data[m] = (iptr[m] == IPM_INDEX_NULL) ? iptr[m] : layers[l-1].lid2lid_map[iptr[m]];
            }
          } else { // This point is new. Renumber it and push it back
            lid = layers[l].gid2lid_map[gid] = u->layout->nvisible;
            for (int m = 0; m < len; m++) {
              u->data.push_back(iptr[m] == IPM_INDEX_NULL ? iptr[m] : layers[l-1].lid2lid_map[iptr[m]]);
            }
            u->disp.push_back(u->disp.back() + len);
            u->layout->gids.push_back(gid);
            u->layout->nvisible++; // A new point was just pushed back

            u->layout->lbdry.insert(lid);
            u->layout->gbdry.insert(gid);
          }
          layers[l].lid2lid_map[j] = lid;
          iptr += len;
        }
        curpos = iptr;
      }
      else
      {
        assert(arrays[l]->type == IPM_ARRAY_NUMERIC);
        IPM_ArrayNumeric u = dynamic_cast<IPM_ArrayNumeric>(arrays[l]);
        for (int j = 0; j < totpts; j++) {
          // Find where is the local one, and record it in order to replay on clones
          gid = gidptr[j];
          len = lenptr[j];
          gid2lid_map_const_iter_t got = layers[l].gid2lid_map.find(gid);
          if (got != layers[l].gid2lid_map.end()) { // The point already exists but need to be updated
            lid = got->second;
            IPM_Real *data = &u->data[u->disp[lid]];
            for (int m = 0; m < len; m++) data[m] = fptr[m];
          } else { // This numeric point is new. Simply push it back
            lid = layers[l].gid2lid_map[gid] = u->layout->nvisible;
            u->data.insert(u->data.end(), fptr, fptr+len);
            u->disp.push_back(u->disp.back()+len);
            u->layout->gids.push_back(gid);
            u->layout->nvisible++; // A new point was just pushed back

            u->layout->lbdry.insert(lid);
            u->layout->gbdry.insert(gid);
          }
          layers[l].lid2lid_map[j] = lid;
          fptr += len;
        }
        curpos = fptr;
      }

      // Replay the writting on clones
      for (int k = 0; k < clones[l].size(); k++) {
        if (clones[l][k]->type == IPM_ARRAY_INDEX)
        {
          IPM_ArrayIndex clone = static_cast<IPM_ArrayIndex>(clones[l][k]);
          lenptr = (int *)curpos;
          iptr = (local_int_t *)(lenptr + totpts);
          for (int j = 0; j < totpts; j++) {
            len = lenptr[j];
            if (layers[l].lid2lid_map[j] < clone->disp.size()-1) { // Point j already exists
              local_int_t *data = &clone->data[clone->disp[layers[l].lid2lid_map[j]]];
              assert(len == IPM_ArrayGetLength(clone, layers[l].lid2lid_map[j], err)); // local & external lens should match
              for (int m = 0; m < len; m++) {
                data[m] = (iptr[m] == IPM_INDEX_NULL ? iptr[m] : layers[l].lid2lid_map[iptr[m]]);
              }
            } else { // Point j is new data
              for (int m = 0; m < len; m++) {
                clone->data.push_back(iptr[m] == IPM_INDEX_NULL ? iptr[m] : layers[l].lid2lid_map[iptr[m]]);
              }
              clone->disp.push_back(clone->disp.back()+len);
            }
            iptr += len;
          }
          curpos = iptr;
        }
        else // clone is IPM_ARRAY_NUMERIC
        {
          IPM_ArrayNumeric clone = dynamic_cast<IPM_ArrayNumeric>(clones[l][k]);
          lenptr = (int *)curpos;
          fptr = (IPM_Real *)(lenptr + totpts);

          for (int j = 0; j < totpts; j++) {
            len = lenptr[j];
            if (layers[l].lid2lid_map[j] < clone->disp.size()-1) { // Point j already exists
              IPM_Real *data = &clone->data[clone->disp[layers[l].lid2lid_map[j]]];
              for (int m = 0; m < len; m++) data[m] = fptr[m];
            } else { // New data
              clone->data.insert(clone->data.end(), fptr, fptr+len);
              clone->disp.push_back(clone->disp.back()+len);
            }
            fptr += len;
          }
          curpos = fptr;
        }


      }
    }
  }

  delete[] layers;
}

/**
 * \brief Unpack data points sent from other processes. If a data point already
 * exists locally, we will skip receiving this data point. In other words, we only
 * receive new data points.
 *
 * Given an index array <top>, we will be receiving points of the array from <srcs>
 * processes. Specifically, for the i-th process, the received data will be
 * stored at address recvbuf + recvdisp[i], with length recvdisp[i+1] - recvdisp[i]
 * in bytes. The srcs nonblocking receive requests are given at <recvreqs>.
 *
 * Note: recvfrom[] contains ranks of source processes, solely for debugging.
 *
 */
void IPMR_ArrayUnpackExpand(IPM_ArrayIndex top, int srcs, const int *recvfrom, char *recvbuf, int *recvdisp,
  MPI_Request *recvreqs, int *err)
{
  typedef std::unordered_map<global_int_t, local_int_t>::const_iterator gid2lid_map_const_iter_t;

  //============================================================================
  // Discover the array hierachy along with their clones below the top
  //============================================================================
  int top_is_index_clone = (top->type == IPM_ARRAY_INDEX) && (top->attr & IPM_ARRAYATTR_ISCLONE);
  int n = 0;
  IPM_Array cur = top;
  while (cur) { n++; cur = cur->indexto; }

  std::vector<IPM_Array> arrays(n);
  std::vector<std::vector<IPM_Array> > clones(n);

  cur = top;
  for (int l = n -1; l >= 0; l--) { arrays[l] = cur; cur = cur->indexto; }

  // Find out clones at each level
  for (int l = n -1; l >= 0; l--) {
    if (l == n -1 && top_is_index_clone) continue; // itself is a clone, so skip it.
    IPMR_Layout layout = arrays[l]->layout;
    for (int i = 0; i < layout->clones.size(); i++) {
      IPM_Array clone = layout->clones[i]; assert(clone->attr & IPM_ARRAYATTR_ISCLONE);
      if (clone != top) clones[l].push_back(clone);
    }
  }

  // Allocate the unpacking data structure, of which gidhash, map etc must be init'ed to NULL
  UnpackDS *layers = new UnpackDS[n]; assert(layers);

  // Build a gid-to-lid map for exisiting points at each layer below the top
  for (int l = 0; l < n; l++) {
    IPMR_Layout layout = arrays[l]->layout;
    for (local_int_t lid = 0; lid < layout->nvisible; lid++) {
      global_int_t gid = IPMR_ArrayGetGid(arrays[l], lid);
      set<global_int_t>::iterator iter = layout->gbdry.find(gid);
      if (iter != layout->gbdry.end()) layers[l].gid2lid_map[gid] = lid;
    }
  }

  //============================================================================
  // Merge received data from srcs one by one
  //============================================================================
  for (int i = 0; i < srcs; i++) {
    int idx;
    MPI_Waitany(srcs, recvreqs, &idx, MPI_STATUS_IGNORE); assert(idx != MPI_UNDEFINED);
    void *curpos = recvbuf + recvdisp[idx];

    // Merge points bottom-up, since upper layers will use lower layers' newly renumbered indices
    for (int l = 0; l < n; l++) {
      int totpts = *(int*)curpos;
      global_int_t *gidptr = (global_int_t *)((char*)curpos + sizeof(int)); // gids
      int *lenptr = (int *)(gidptr + totpts); // lens
      IPM_Index *iptr = (IPM_Index *)(lenptr + totpts); // indices or
      IPM_Real *fptr = (IPM_Real *)(lenptr + totpts); // floating points
      int len;
      global_int_t gid;
      local_int_t lid;

      // lid2lid_map records where the totpts points go locally. Skip the top layer since
      // no one uses this map when expanding arrays
      if (l < n -1) layers[l].lid2lid_map.resize(totpts);

      if (arrays[l]->type == IPM_ARRAY_INDEX)
      { // Indices need to be renumbered
        IPM_ArrayIndex u = dynamic_cast<IPM_ArrayIndex>(arrays[l]); assert(u);
        for (int j = 0; j < totpts; j++) {
          // Find where is the local one, and record it in order to replay on clones
          gid = gidptr[j];
          len = lenptr[j];
          gid2lid_map_const_iter_t got = layers[l].gid2lid_map.find(gid);
          if (l < n-1 && got != layers[l].gid2lid_map.end()) { // TODO: never checking gids for top since we do not maintain it due to Index removing
            lid = got->second; // Already exist.
          } else {
            // Meet a new index point. Renumber it and then push it back
            lid = layers[l].gid2lid_map[gid] = u->layout->nvisible;
            // Renumber the value of this data point using l-1's lid2lid_map
            for (int m = 0; m < len; m++) {
              u->data.push_back(iptr[m] == IPM_INDEX_NULL ? iptr[m] : layers[l-1].lid2lid_map[iptr[m]]);
            }
            u->disp.push_back(u->disp.back() + len);
            u->layout->gids.push_back(gid);
            u->layout->nvisible++; // A new point was just pushed back
          }
          if (l < n -1) layers[l].lid2lid_map[j] = lid;
          iptr += len;
        }
        curpos = iptr;
      }
      else // arrays[l] is IPM_ARRAY_NUMERIC
      {
        IPM_ArrayNumeric u = dynamic_cast<IPM_ArrayNumeric>(arrays[l]); assert(u);
        for (int j = 0; j < totpts; j++) {
          // Find where is the local one, and record it in order to replay on clones
          gid = gidptr[j];
          len = lenptr[j];
          gid2lid_map_const_iter_t got = layers[l].gid2lid_map.find(gid);
          if (got != layers[l].gid2lid_map.end()) { // If the point already exist.
            lid = got->second;
          } else {  // This is a new numeric point. Simply push it back
            lid = layers[l].gid2lid_map[gid] = u->layout->nvisible;
            u->data.insert(u->data.end(), fptr, fptr+len);
            u->disp.push_back(u->disp.back()+len);
            u->layout->gids.push_back(gid);
            u->layout->nvisible++; // A new point was just pushed back
          }
          if (l < n -1) layers[l].lid2lid_map[j] = lid;
          fptr += len;
        }
        curpos = fptr;
      }

      // Replay the expanding on clones. Clones do not update layout.
      for (int k = 0; k < clones[l].size(); k++) {
        if (clones[l][k]->type == IPM_ARRAY_INDEX)
        {
          IPM_ArrayIndex clone = dynamic_cast<IPM_ArrayIndex>(clones[l][k]);
          lenptr = (int *)curpos;
          iptr = (local_int_t *)(lenptr + totpts);
          for (int j = 0; j < totpts; j++) {
            len = lenptr[j];
            // lid >= current # of entries means point j is a new entry
            if (l == n -1 || layers[l].lid2lid_map[j] >= clone->disp.size() -1) {
              // iptr[] points to layer l itself, so we rewrite with lid2lid_map at layer l
              for (int m = 0; m < len; m++) {
                clone->data.push_back(iptr[m] == IPM_INDEX_NULL ? iptr[m] : layers[l].lid2lid_map[iptr[m]]);
              }
              clone->disp.push_back(clone->disp.back() + len);
            }
            iptr += len;
          }
          curpos = iptr;
        }
        else // clone is IPM_ARRAY_NUMERIC
        {
          IPM_ArrayNumeric clone = dynamic_cast<IPM_ArrayNumeric>(clones[l][k]);
          lenptr = (int *)curpos;
          fptr = (IPM_Real *)(lenptr + totpts);

          for (int j = 0; j < totpts; j++) {
            len = lenptr[j];
            // lid >= current # of entries means point j is a new entry
            if (l == n -1 || layers[l].lid2lid_map[j] >= clone->disp.size() -1) {
              clone->data.insert(clone->data.end(), fptr, fptr+len);
              clone->disp.push_back(clone->disp.back() + len);
            }
            fptr += len;
          }
          curpos = fptr;
        }
      }
    }
  }

  delete[] layers;
}

//=============================================================================
//
// IPMR_FunctionPartition:
// Given a function func, partition its array arguments. We assume the last
// array, i.e., func->array[func->n-1], is the top layer array. The top array
// has no shared points between processes. We partition it based on connectivity
// built at a certain lower layer array. ParMETIS is called for partitioning.
// After that, we will move array pieces according to the partition result.
//
//=============================================================================
void IPMR_FunctionPartition(IPM_Function func, IPM_Error *err)
{
  int rc;
  int MPI_MY_TAG = 99;
  int nprocs;
  int myrank;
  int tgts = 0; // Stuff for target processes I will send data to
  int external_tgts = 0; // Targets excluding myself
  int external_tgts_start_at; // 0 or 1, which is the index of 1st external tgt
  int *sendto = NULL;
  char *sendbuf = NULL;
  int *senddisp = NULL;
  int send_to_self;
  MPI_Request *sendreqs = NULL;

  int srcs = 0; // Stuff for source processes I will receive data from
  int *recvfrom = NULL;
  char *recvbuf = NULL;
  int *recvdisp = NULL;
  MPI_Request *recvreqs = NULL;
  int tot_recvlen;
  MPI_Comm comm;
  MPI_Status status;
  IPM_Index topntotal, topnvisible, topnlocal;

  int n = 0;
  IPM_ArrayIndex top;
  IPM_Array arrays[16] = {0};
  IPM_Array top2;
  IPM_Array cur;

  *err = IPM_ERROR_NONE;

  // Store the array hierachy in an easy to access manner.
  top = dynamic_cast<IPM_ArrayIndex>(func->array[func->n-1]);
  assert(top);
  assert(top->attr & IPM_ARRAYATTR_TOP);

  cur = top;
  while (cur) { n++; cur = cur->indexto; }
  cur = top;
  for (int i = n-1; i >= 0; i--) { arrays[i] = cur; cur = cur->indexto; }
  assert(n <= 16);
  assert(top->connect_depth < n);
  top2 = arrays[n-2];

  comm = top->layout->comm;
  topntotal = top->layout->ntotal;
  topnlocal = top->layout->nlocal;
  topnvisible = top->layout->nvisible;

  assert(topnlocal == topnvisible);

  MPI_Comm_size(comm, &nprocs);
  MPI_Comm_rank(comm, &myrank);

  //===========================================================================
  // Partition the top index array based on connnectivity at a lower layer array
  //===========================================================================

  // TODO: need a workaround for the limitation of ParMETIS: it requires each process
  // has at least one element before partitioning. In the initial stage, or after
  // continuous coarsening, some processes might have no elements.

  // Do not do partitioning when array size < nprocs or only one process exists
  //if (topntotal < nprocs || nprocs < 2) {
  if (topntotal < nprocs) {
    *err = IPM_ERROR_NONE;
    return;
  }

  // Do naive partitioning for the first time, when only rank 0 has data

  // The paritition array returned by ParMETIS
  std::vector<global_int_t> part(topnlocal);

  if (topnlocal == topntotal) {
    assert(myrank == 0); // rank 0 scatters data points to others
    for (int i = 0; i < nprocs; i++) {
      int lb = BLOCK_LOW(i, nprocs, topntotal); // [lower, upper) bound for rank i
      int ub = BLOCK_LOW(i+1, nprocs, topntotal);
      for (int j = lb; j < ub; j++) part[j] = i;
    }
  } else if (skip_parmetis) { // Don't move the mesh to ease debugging.
    for (int i = 0; i < topnlocal; i++) part[i] = myrank;
  } else if (topnlocal != 0) {
    // Since ParMETIS uses idx_t for both local integers and global ids, we
    // use global_int_t for all of its idx_t arguments
    std::vector<global_int_t> eptr(1,0), eind, elmdist(nprocs+1);
    std::set<local_int_t> curset, nextset;
    global_int_t wgtflag, numflag, ncon, ncommonnodes, nparts, edgecut;
    global_int_t *elmwgt;
    global_int_t options[5] = {0};
    IPM_Real ubvec[10] = {0.0};

    assert(topnlocal == topnvisible);
    for (int i = 0; i < top->layout->nlocal; i++) {
      int len =   top->disp[i+1] - top->disp[i];
      local_int_t *idat = &top->data[top->disp[i]];
      curset.insert(idat, idat+len);
      // Search down the array hierarchy to find out unique low level indices this entry references
      for (int k = 0; k < top->connect_depth -1; k++) {
        IPM_ArrayIndex nextu = dynamic_cast<IPM_ArrayIndex>(arrays[n-2-k]); assert(nextu);
        for (std::set<local_int_t>::const_iterator iter = curset.begin(); iter != curset.end(); ++iter) {
          local_int_t p = *iter;
          int len =   nextu->disp[p+1] - nextu->disp[p];
          local_int_t *idat = &nextu->data[nextu->disp[p]];
          nextset.insert(idat, idat+len); // use set since edges of an element may reference the same vertices
        }
        curset.swap(nextset);
        nextset.clear();
      }

      // Use the unique indices to access the lowest array we build connectivity on
      for (std::set<local_int_t>::const_iterator iter = curset.begin(); iter != curset.end(); ++iter) {
        eind.push_back(arrays[n-1-top->connect_depth]->layout->gids[*iter]);
      }
      eptr.push_back(eptr.back() + curset.size());
      curset.clear();
    }

    elmdist[0] = 0;
    global_int_t mynlocal = topnlocal;
    MPI_Allgather(&mynlocal, 1, GINT_MPI_DTYPE, &elmdist[1], 1, GINT_MPI_DTYPE, comm);
    for (int i = 1; i < nprocs+1; i++) elmdist[i] += elmdist[i-1];

    elmwgt = NULL;
    wgtflag = 0;
    numflag = 0; // C style
    ncon = 1;
    ncommonnodes = 1;
    nparts = nprocs;
    std::vector<IPM_Real> tpwgts(nparts*ncon);
    for (IPM_Index i=0; i<nparts*ncon; i++) tpwgts[i] = 1.0/(IPM_Real)(nparts);

    for (IPM_Index i=0; i< ncon; i++) ubvec[i] = 1.05;
    options[0] = 1;
    options[1] = 0;
    options[2] = 99;

    //printf("Calling ParMETIS\n");
    rc = ParMETIS_V3_PartMeshKway(&elmdist[0], &eptr[0], &eind[0], elmwgt, &wgtflag, &numflag, &ncon,
      &ncommonnodes, &nparts, &tpwgts[0], ubvec, options, &edgecut, &part[0], &comm);
    assert(rc == METIS_OK);
  }

  //===========================================================================
  // Group points according to their target procs.
  // Number of target procs, i.e., tgts is calculated. Target ranks are stored
  // at sendto[0..tgts). If the proc itself is also a target, move it to sendto[0].
  // For target sendto[i], we will send ptdisp[i+1] - ptdisp[i] points with indices
  // stored at ptlist[ptdisp[i]..ptdisp[i+1])
  //===========================================================================
  IPM_Index *ptdisp; // Point displacement
  std::vector<local_int_t> ptlist(topnlocal);

  // Scan part[] and inverse myrank (if any) to make my part be first after sorting
  for (IPM_Index i = 0; i < topnlocal; i++) {
    ptlist[i] = i;
    if (part[i] == myrank) part[i] = -myrank;
  }

  // Sort ptlist[] addcording to part[]. Note that part[] keeps unchanged.
  aux_cmp_vec = &part;
  stable_sort(ptlist.begin(), ptlist.end(), aux_cmp); // stable_sort is not required but faster here

  // Scan part[] again to know number of target procs, i.e., tgts, for malloc
  int currank = -myrank-1; // Make up a rank different from all in part[]
  for (IPM_Index i = 0; i < topnlocal; i++) {
    if (part[ptlist[i]] != currank) { tgts++; currank = part[ptlist[i]]; }
  }

  sendto = (int *)malloc(sizeof(int)*tgts); if (tgts) assert(sendto);
  ptdisp = (IPM_Index *)malloc(sizeof(IPM_Index)*(tgts+1)); assert(ptdisp);
  senddisp = (int *)malloc(sizeof(int)*(tgts+1)); assert(senddisp);

  // Scan part[] again to set up sendto[], ptdisp[]
  send_to_self = 0;
  if (tgts > 0) {
    int k = 0;
    ptdisp[0] = 0; ptdisp[tgts] = topnlocal;
    sendto[0] = part[ptlist[0]];
    for (IPM_Index i = 0; i < topnlocal; i++) {
      if (part[ptlist[i]] != sendto[k]) {
        k++;
        sendto[k] = part[ptlist[i]];
        ptdisp[k] = i;
      }
    }
    assert(k == tgts-1);
    if (sendto[0] == -myrank) sendto[0] = myrank; // Recover sendto[0]
    send_to_self = (sendto[0] == myrank) ? 1 : 0;
  }
  external_tgts_start_at = send_to_self ? 1 : 0;
  external_tgts = tgts - external_tgts_start_at;
  part = std::vector<global_int_t>(); // free memory of part early

  //============================================================================
  // Pack parts of the mesh for their target procs.
  // Data to be sent to target i will be stored at address sendbuf + senddisp[i],
  // with length senddisp[i+1]-senddisp[i] in bytes.
  //============================================================================
  IPMR_ArrayPack(static_cast<IPM_ArrayIndex>(top), tgts, sendto, &ptlist[0], ptdisp, &sendbuf, senddisp, 0, err);
  free(ptdisp);

  //============================================================================
  // With the send info, compute the reverse receive info
  //============================================================================
  int *tmpv = (int *)calloc(nprocs*2, sizeof(int)); assert(tmpv);
  for (int i = external_tgts_start_at; i < tgts; i++) {
    tmpv[2*sendto[i]] = 1;
    tmpv[2*sendto[i]+1] = senddisp[i+1] - senddisp[i];
  }
  MPI_Allreduce(MPI_IN_PLACE, tmpv, nprocs*2, MPI_INT, MPI_SUM, comm);
  srcs = tmpv[2*myrank];
  tot_recvlen = tmpv[2*myrank + 1];
  free(tmpv);

  recvdisp = (int *)malloc(sizeof(int)*(srcs+1)); assert(recvdisp);
  recvfrom = (int *)malloc(sizeof(int)*srcs); if (srcs) assert(recvfrom);
  recvbuf = (char *)malloc(tot_recvlen); if (tot_recvlen) assert(recvbuf);
  recvreqs = (MPI_Request *)malloc(sizeof(MPI_Request)*srcs); if(srcs) assert(recvreqs);

  // Find out who will send data to me and how much they send
  for (int i = 0; i < srcs; i++) {
    // Shift one and we will adjust recvdisp[] to CSR format later
    MPI_Irecv(&recvdisp[i+1], 1, MPI_INT, MPI_ANY_SOURCE, MPI_MY_TAG, comm, &recvreqs[i]);
  }

  for (int i = external_tgts_start_at; i < tgts; i++) {
    int sendlen = senddisp[i+1] - senddisp[i];
    MPI_Send(&sendlen, 1, MPI_INT, sendto[i], MPI_MY_TAG, comm);
  }

  for (int i = 0; i < srcs; i++) {
    if (MPI_Wait(&recvreqs[i], &status) != MPI_SUCCESS) {
      fprintf(stderr, "MPI_Wait error!\n");
      MPI_Abort(comm, -1);
    }
    recvfrom[i] = status.MPI_SOURCE;
  }

  // Adjust recvdisp[] to CSR format
  recvdisp[0] = 0;
  for (int i = 1; i < srcs+1; i++) {
    recvdisp[i] += recvdisp[i-1];
  }

  //============================================================================
  // Start nonblocking communications
  //============================================================================
  MPI_MY_TAG++;
  for (int i = 0; i < srcs; i++) {
    int recvlen = recvdisp[i+1] - recvdisp[i];
    MPI_Irecv(recvbuf + recvdisp[i], recvlen, MPI_CHAR, recvfrom[i], MPI_MY_TAG, comm, &recvreqs[i]);
  }

  sendreqs = (MPI_Request *)malloc(sizeof(MPI_Request)*tgts); assert(sendreqs);
  for (int i = external_tgts_start_at; i < tgts; i++) {
    int sendlen = senddisp[i+1] - senddisp[i];
    MPI_Isend(sendbuf + senddisp[i], sendlen, MPI_CHAR, sendto[i], MPI_MY_TAG, comm, &sendreqs[i]);
  }

  //============================================================================
  // Clean up local buffer to prepare for receiving data sent to me.
  // ATTENTION: relinquished points, i.e., points not referenced in lower arrays,
  // will be discarded because of this operation!
  //============================================================================
  for (int l = 0; l < n; l++) {
    IPM_Array u = arrays[l];
    u->clearData();
    u->layout->gids.clear();
    u->layout->nvisible = 0;

    for (int k = 0; k < u->layout->clones.size(); k++) {
      IPM_Array clone = u->layout->clones[k];
      clone->clearData();
    }
  }

  //============================================================================
  // Receive data the process sends to itself.
  // With good data locality, that should be a large piece. We treat it specially
  // and assemble this piece first, to enjoy the following benefits:
  // 1) Avoid communcation to self; 2) Because it is the first part, we are
  // assured there is no duplicated data points. So we can avoid lookups in each
  // data point insertion; 3) It might be overlapped with previous nonblocking comm.
  //============================================================================
  // TODO: call MPI_Test on recvreqs for better comm/comp overlapping
  if (send_to_self) {
    char *curptr = sendbuf;
    for (int l = 0; l < n; l++) {
      IPM_Array u = arrays[l];
      int totpts = *(int*)curptr;
      IPM_Index *gidptr = (IPM_Index *)(curptr + sizeof(int));
      int *lenptr = (int *)(gidptr + totpts);
      char *dptr = (char *)(lenptr + totpts);
      int totlen = 0;
      size_t datlen;
      for (int i = 0; i < totpts; i++) totlen += lenptr[i];

      u->layout->nvisible = totpts;

      if (u->type == IPM_ARRAY_NUMERIC)
      {
        datlen = sizeof(IPM_Real) * totlen;
        IPM_ArrayNumeric v = static_cast<IPM_ArrayNumeric>(u);
        v->data.resize(totlen);
        memcpy(v->getData(), dptr, datlen);
      }
      else // u is IPM_ARRAY_INDEX
      {
        datlen = sizeof(local_int_t) * totlen;
        IPM_ArrayIndex v = static_cast<IPM_ArrayIndex>(u);
        v->data.resize(totlen);
        memcpy(v->getData(), dptr, datlen);
      }

      u->layout->gids.resize(totpts);
      memcpy(&(u->layout->gids[0]), gidptr, sizeof(IPM_Index)*totpts);
      u->disp.reserve(totpts);
      for (int i = 0; i < totpts; i++) {
        IPM_Index disp = u->disp.back() + lenptr[i];
        u->disp.push_back(disp);
      }

      curptr = dptr + datlen;

      // Receive clones
      for (int k = 0; k < u->layout->clones.size(); k++) {
        IPM_Array clone = u->layout->clones[k];
        lenptr = (int*)curptr;
        dptr = (char *)(lenptr + totpts);
        totlen = 0;
        for (int i = 0; i < totpts; i++) totlen += lenptr[i];

        if (clone->type == IPM_ARRAY_NUMERIC)
        {
          IPM_ArrayNumeric v = dynamic_cast<IPM_ArrayNumeric>(clone); assert(v);
          datlen = sizeof(IPM_Real)*totlen;
          v->data.resize(totlen);
          memcpy(v->getData(), dptr, datlen);
        }
        else // clone is IPM_ARRAY_INDEX
        {
          IPM_ArrayIndex v = dynamic_cast<IPM_ArrayIndex>(clone); assert(v);
          datlen = sizeof(local_int_t)*totlen;
          v->data.resize(totlen);
          memcpy(v->getData(), dptr, datlen);
        }

        // Common part for both Numeric and Index
        clone->disp.reserve(totpts);
        for (int i = 0; i < totpts; i++) {
          IPM_Index disp = clone->disp.back() + lenptr[i];
          clone->disp.push_back(disp);
        }
        curptr = dptr + datlen;
      }

      assert(u->layout->nvisible == u->layout->gids.size());
    }
  }

  //============================================================================
  // Receive data sent from other processes (one bye one to overlap comp/comm)
  //============================================================================
  IPMR_ArrayUnpackExpand(static_cast<IPM_ArrayIndex>(top), srcs, recvfrom, recvbuf, recvdisp, recvreqs, err);

  //============================================================================
  // Set top array's layout->nlocal, because we know there is not overlap.
  // For lower layer arrays, their nlocal is set in IPMR_FindSharedPoints.
  //============================================================================
  top->layout->nlocal = top->layout->nvisible;

  // Wait for all isends to be completed
  MPI_Waitall(external_tgts, &sendreqs[external_tgts_start_at], MPI_STATUSES_IGNORE);

  //============================================================================
  // Free data structures used in send/recv and unpacking
  //============================================================================
  free(sendto);
  delete [] sendbuf;
  free(senddisp);
  free(sendreqs);

  free(recvfrom);
  free(recvbuf);
  free(recvdisp);
  free(recvreqs);
}

/**
 * Create a communication context
 */
void IPM_ArrayNumericCreateAddContext(IPM_ArrayNumeric u, IPM_Error *err)
{
  int MY_MPI_TAG = 99;
  IPMR_Layout layout = u->layout;
  NumericAddContext *ctx = new NumericAddContext();

  std::vector<bool> is_shmnbr(layout->nbrs.size(), false); // Not use shm by default
  int cnt;

  *err = IPM_ERROR_NONE;
  ctx->comm = layout->comm;

  /* Build on-node shared memory communication context if it is turned on
   */
#ifdef BUILD_ON_NODE_CONTEXT
  int origrank; // rank of a process in ctx->comm
  int shmrank; // rank of the same process in ctx->shmcomm
  int shmsize; // size of ctx->shmcomm

  MPI_Comm_rank(ctx->comm, &origrank);
  MPI_Comm_split_type(ctx->comm, MPI_COMM_TYPE_SHARED, origrank, MPI_INFO_NULL, &ctx->shmcomm);

  MPI_Comm_size(ctx->shmcomm, &shmsize);
  MPI_Comm_rank(ctx->shmcomm, &shmrank);

  std::vector<int> origidx; // Holds indices of shm nbrs in nbrs[]. If ctx->shmnbrs[i] = shmrank,
                            // then nbrs[origidx[i]] = origrank = rankmap[shmrank]
  std::vector<int> rankmap(shmsize); // map ranks in ctx->shmcomm to ranks in ctx->comm

  rankmap[shmrank] = origrank;

  MPI_Allgather(MPI_IN_PLACE, 0, MPI_DATATYPE_NULL, &rankmap[0], 1, MPI_INT, ctx->shmcomm);

  /* Set up ctx->{nbrs, nowners, shmnbrs, nshmowners}
   */

  // rankmap should already be sorted since we did Comm_split_type using origrank as key.
  // So even we will do binary-search on rankmap, we still can save this sort.
  // std::sort(rankmap.begin(), rankmap.end());

  ctx->nowners = 0;
  ctx->nshmowners = 0;
  for (int i = 0; i < layout->nbrs.size(); i++) {
    int origrank = layout->nbrs[i];
    std::vector<int>::iterator low = std::lower_bound(rankmap.begin(), rankmap.end(), origrank);
    if (low != rankmap.end() && *low == origrank) { // found origrank on node
      is_shmnbr[i] = true;
      ctx->shmnbrs.push_back(low - rankmap.begin());  // the distance = the shm rank
      origidx.push_back(i);
      if (i < layout->nowners) ctx->nshmowners++;
    } else { // origrank is an off-node neighbor
      // is_shmnbr[i] is false by default
      ctx->nbrs.push_back(origrank);
      if (i < layout->nowners) ctx->nowners++;
    }
  }

  /* Set up ctx->shmdisp[]
   */
  ctx->shmdisp.resize(ctx->shmnbrs.size()+1, 0);

  for (int i = 0; i < ctx->shmnbrs.size(); i++) {
    int shmrank = ctx->shmnbrs[i];
    int origrank = rankmap[shmrank];
    for (local_int_t j = layout->disp[origidx[i]]; j < layout->disp[origidx[i]+1]; j++) {
      ctx->shmdisp[i+1] += IPM_ArrayGetLength(u, layout->lids[j], err);
    }
  }

  for (int i = 1; i < ctx->shmdisp.size(); i++) {
    ctx->shmdisp[i] += ctx->shmdisp[i-1];
  }

  /* Set up ctx->shmoffset[]
   */
  ctx->shmoffset.resize(ctx->shmdisp.back());
  cnt = 0;
  for (int i = 0; i < ctx->shmnbrs.size(); i++) {
    int shmrank = ctx->shmnbrs[i];
    int origrank = rankmap[shmrank];
    for (local_int_t j = layout->disp[origidx[i]]; j < layout->disp[origidx[i]+1]; j++) {
      local_int_t idx = layout->lids[j];
      local_int_t ofst = u->disp[idx]; // offset of the first value of the entry
      for (int k = 0; k < IPM_ArrayGetLength(u, idx, err); k++) {
        ctx->shmoffset[cnt++] = ofst + k;
      }
    }
  }

  /* Set up cxt->{shmbuf, shmwin}
   */
  MPI_Aint shmbufsz = sizeof(IPM_Real)*ctx->shmdisp[ctx->nshmowners];
  int disp_unit = sizeof(IPM_Real);
  MPI_Info info;
  MPI_Info_create(&info);
  MPI_Info_set(info, "alloc_shared_noncontig", "true");
  //printf("on rank %d, nvisible =%d, shmsize = %d, shmbuf size = %lld bytes\n", origrank, layout->nvisible, shmsize, shmbufsz);
  int ret = MPI_Win_allocate_shared(shmbufsz, disp_unit, info, ctx->shmcomm, &ctx->shmbuf, &ctx->shmwin); assert(!ret);
  MPI_Info_free(&info);

  /* Set up ctx->shmsharerbptrs
   */
  ctx->shmsharerbptrs.resize(ctx->shmnbrs.size() - ctx->nshmowners); // need a baseptr for each sharer to read from it
  for (int i = 0; i < ctx->shmsharerbptrs.size(); i++) {
    MPI_Win_shared_query(ctx->shmwin, ctx->shmnbrs[ctx->nshmowners+i], &shmbufsz, &disp_unit, &ctx->shmsharerbptrs[i]);
  }

  // Also need to know a disp to access the specified region exposed to me
  std::vector<int> anotherdisp(ctx->shmnbrs.size());
  std::vector<MPI_Request> reqs(ctx->shmnbrs.size());

  for (int i = ctx->nshmowners; i < ctx->shmnbrs.size(); i++) { // Recv from shm sharers
    MPI_Irecv(&anotherdisp[i], 1, MPI_INT, ctx->shmnbrs[i], MY_MPI_TAG, ctx->shmcomm, &reqs[i]);
  }

  for (int i = 0; i < ctx->nshmowners; i++) { // Send to shm owners
    anotherdisp[i] = ctx->shmdisp[i];
    MPI_Isend(&anotherdisp[i], 1, MPI_INT, ctx->shmnbrs[i], MY_MPI_TAG, ctx->shmcomm, &reqs[i]);
  }

  MPI_Waitall(ctx->shmnbrs.size(), &reqs[0], MPI_STATUSES_IGNORE);

  for (int i = 0; i < ctx->shmsharerbptrs.size(); i++) {
    ctx->shmsharerbptrs[i] += anotherdisp[ctx->nshmowners + i];
  }
#else // !BUILD_ON_NODE_CONTEXT
  ctx->nbrs = layout->nbrs; // vector copying
  ctx->nowners = layout->nowners;
  // All entries of is_shmnbr[] ware set false at init
#endif

  /* Build off-node communication context
   *
   * Whether BUILD_ON_NODE_CONTEXT is on or off, at this moment
   * we have ctx->{nbrs, nowners} and is_shmnbr[] set up.
   */

  /* Set up ctx->disp[] by computing total length of data shared with neighbors
   */
  cnt = 1; // counts off-node neighbors. Starting from 1 to let ctx->disp[0] be 0
  ctx->disp.resize(ctx->nbrs.size()+1, 0);
  for (int i = 0; i < layout->nbrs.size(); i++) {
    if (!is_shmnbr[i]) {
      for (local_int_t j = layout->disp[i]; j < layout->disp[i+1]; j++) {
        ctx->disp[cnt] += IPM_ArrayGetLength(u, layout->lids[j], err);
      }
      cnt++;
    }
  }

  assert(cnt == ctx->disp.size());

  /* Do prefix sum to finalize disp[]
   */
  for (int i = 1; i < ctx->disp.size(); i++) ctx->disp[i] += ctx->disp[i-1];

  ctx->offset.resize(ctx->disp.back());
  ctx->buf.resize(ctx->disp.back());
  ctx->reqs.resize(ctx->nbrs.size());

  /* Set up ctx->offset[] by filling in offsets of reals shared with neighbors
   */
  cnt = 0; // counts individual reals
  for (int i = 0; i < layout->nbrs.size(); i++) {
    if (!is_shmnbr[i]) {
      for (local_int_t j = layout->disp[i]; j < layout->disp[i+1]; j++) {
        local_int_t idx = layout->lids[j];
        local_int_t ofst = u->disp[idx]; // offset of the first value of the entry
        for (int k = 0; k < IPM_ArrayGetLength(u, idx, err); k++) {
          ctx->offset[cnt++] = ofst + k;
        }
      }
    }
  }

  assert(cnt == ctx->disp.back());

  u->addctx = ctx;
}

/**
 * Create a communication context
 */
void IPM_ArrayNumericDestroyAddContext(IPM_ArrayNumeric u, IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  delete u->addctx;
  u->addctx = NULL;
}

//=============================================================================
// IPMR_CommNumericReduceWithContext:
// Do an Allreduce operation \p op over shared points of a numeric array.
//=============================================================================
void IPMR_CommNumericReduceWithContext(IPM_ArrayNumeric u, IPM_MemoryType op, IPM_Error *err)
{
  int MY_MPI_TAG = 99;
  NumericAddContext *ctx = u->addctx;
  MPI_Comm comm = ctx->comm;
  int cnt;

  *err = IPM_ERROR_NONE;

  /* First, owners collect contributions from sharers and sum them up
   */

  // Send/Recv contributions
  for (int i = ctx->nowners; i < ctx->nbrs.size(); i++) { // Recv from sharers
    MPI_Irecv(&ctx->buf[ctx->disp[i]], ctx->disp[i+1]-ctx->disp[i], REAL_MPI_DTYPE, ctx->nbrs[i], MY_MPI_TAG, comm, &ctx->reqs[i]);
  }

  for (int i = 0; i < ctx->nowners; i++) { // Send to owners
    for (int j = 0; j < ctx->disp[i+1]-ctx->disp[i]; j++) {
      ctx->buf[ctx->disp[i]+j] = u->data[ctx->offset[ctx->disp[i]+j]];
    }
    MPI_Isend(&ctx->buf[ctx->disp[i]], ctx->disp[i+1]-ctx->disp[i], REAL_MPI_DTYPE, ctx->nbrs[i], MY_MPI_TAG, comm, &ctx->reqs[i]);
  }

#ifdef BUILD_ON_NODE_CONTEXT
  {
    //MPI_Win_lock_all(MPI_MODE_NOCHECK, ctx->shmwin); // Open an RMA epoch for using Win_sync

    // Write my contributions to shmbuf for owners to read
    int cnt;
    for (int i = 0, cnt = 0; i < ctx->shmdisp[ctx->nshmowners]; i++) {
      ctx->shmbuf[cnt++] = u->data[ctx->shmoffset[i]];
    }

    MPI_Barrier(ctx->shmcomm); // Make sure all shm nbrs finished write
    //MPI_Win_sync(ctx->shmwin); // Make sure the write now visible to others
    __asm__ __volatile__  ( "mfence" ::: "memory" );

    // Directly LOAD contributions from shmbuf's of shm sharers and add them locally!
    int k;
    for (int i = ctx->nshmowners, k = 0; i < ctx->shmnbrs.size(); i++, k++) {
      cnt = 0;
      for (int j = ctx->shmdisp[i]; j < ctx->shmdisp[i+1]; j++) {
        switch (op) { // An optimizing compiler should be able to hoist the switch out of the outer loop
          case IPM_MEMORY_ADD :
            u->data[ctx->shmoffset[j]] += ctx->shmsharerbptrs[k][cnt++];
            break;
          case IPM_MEMORY_PROD :
            u->data[ctx->shmoffset[j]] *= ctx->shmsharerbptrs[k][cnt++];
            break;
          case IPM_MEMORY_MAX :
            u->data[ctx->shmoffset[j]] = std::max(u->data[ctx->shmoffset[j]], ctx->shmsharerbptrs[k][cnt++]);
            break;
          case IPM_MEMORY_MIN :
            u->data[ctx->shmoffset[j]] = std::min(u->data[ctx->shmoffset[j]], ctx->shmsharerbptrs[k][cnt++]);
            break;
          default:
            abort(); break;
        }
      }
    }
  }
#endif

  MPI_Waitall(ctx->reqs.size(), &ctx->reqs[0], MPI_STATUSES_IGNORE);

  // For shpts owned by me, accumlate contributions from sharers and then write results back
  for (int i = ctx->disp[ctx->nowners]; i < ctx->disp.back(); i++) {
    switch (op) { // An optimizing compiler should be able to hoist the switch out of the outer loop
      case IPM_MEMORY_ADD :
        u->data[ctx->offset[i]] += ctx->buf[i];
        break;
      case IPM_MEMORY_PROD :
        u->data[ctx->offset[i]] *= ctx->buf[i];
        break;
      case IPM_MEMORY_MAX :
        u->data[ctx->offset[i]] = std::max(u->data[ctx->offset[i]], ctx->buf[i]);
        break;
      case IPM_MEMORY_MIN :
        u->data[ctx->offset[i]] = std::min(u->data[ctx->offset[i]], ctx->buf[i]);
        break;
      default:
        abort(); break;
    }
  }

  for (int i = ctx->disp[ctx->nowners]; i < ctx->disp.back(); i++) {
      ctx->buf[i] = u->data[ctx->offset[i]];
  }

  /* Second, send/recv the results in a reverse communication
   */
  MY_MPI_TAG++;
  for (int i = 0; i < ctx->nowners; i++) { // receive from owners
    MPI_Irecv(&ctx->buf[ctx->disp[i]], ctx->disp[i+1]-ctx->disp[i], REAL_MPI_DTYPE, ctx->nbrs[i], MY_MPI_TAG, comm, &ctx->reqs[i]);
  }

  for (int i = ctx->nowners; i < ctx->nbrs.size(); i++) { // send to sharers
    MPI_Isend(&ctx->buf[ctx->disp[i]], ctx->disp[i+1]-ctx->disp[i], REAL_MPI_DTYPE, ctx->nbrs[i], MY_MPI_TAG, comm, &ctx->reqs[i]);
  }

#ifdef BUILD_ON_NODE_CONTEXT
  {
    // Directly STORE results to shmbuf's of on-node sharers
    int cnt;
    int k = 0;
    for (int i = ctx->nshmowners; i < ctx->shmnbrs.size(); i++, k++) {
      cnt = 0;
      for (int j = ctx->shmdisp[i]; j < ctx->shmdisp[i+1]; j++) {
        ctx->shmsharerbptrs[k][cnt++] = u->data[ctx->shmoffset[j]];
      }
    }

    MPI_Barrier(ctx->shmcomm);
    //MPI_Win_sync(ctx->shmwin);
    __asm__ __volatile__  ( "mfence" ::: "memory" );

    cnt = 0;
    for (int i = 0; i < ctx->shmdisp[ctx->nshmowners]; i++) {
      u->data[ctx->shmoffset[i]] = ctx->shmbuf[cnt++];
    }

    //MPI_Win_unlock_all(ctx->shmwin);
  }
#endif

  MPI_Waitall(ctx->reqs.size(), &ctx->reqs[0], MPI_STATUSES_IGNORE);

  /* Third, write the results received from owners to my local array
   */
  for (int i = 0; i < ctx->disp[ctx->nowners]; i++) {
      u->data[ctx->offset[i]] = ctx->buf[i];
  }
}

//=============================================================================
// IPMR_CommNumericReduceWithoutContext:
// Do an Allreduce operation \p op over shared points of a numeric array.
//=============================================================================
void IPMR_CommNumericReduceWithoutContext(IPM_ArrayNumeric u, const IPM_MemoryType op, IPM_Error *err)
{
  int MY_MPI_TAG = 99;
  IPMR_Layout layout = u->layout;
  MPI_Comm comm = u->layout->comm;
  std::vector<IPM_Real> buf; // buf for send/recv
  std::vector<int> bufdisp(layout->nbrs.size()+1, 0); // disp for each neighbor
  std::vector<MPI_Request> reqs(layout->nbrs.size());

  /* Set up bufdisp[] and also get length of buf
   */
  for (int i = 0; i < layout->nbrs.size(); i++) {
    int shptcnt = layout->disp[i+1] - layout->disp[i];
    for (int j = 0; j < shptcnt; j++) {
      local_int_t idx = layout->lids[layout->disp[i] + j];
      bufdisp[i+1] += IPM_ArrayGetLength(u, idx, err);
    }
  }

  for (int i = 1; i < layout->nbrs.size()+1; i++) {
    bufdisp[i] += bufdisp[i-1];
  }

  buf.resize(bufdisp.back());

  /* Send/recv contributions of shared points
   */
  for (int i = layout->nowners; i < layout->nbrs.size(); i++) { // recv from sharers
    MPI_Irecv(&buf[bufdisp[i]], bufdisp[i+1] - bufdisp[i], REAL_MPI_DTYPE, layout->nbrs[i], MY_MPI_TAG, comm, &reqs[i]);
  }

  for (int i = 0; i < layout->nowners; i++) { // send to owners
    int shptcnt = layout->disp[i+1] - layout->disp[i];
    IPM_Real *curptr = &buf[bufdisp[i]]; // Start address
    for (int j = 0; j < shptcnt; j++) {
      IPM_Index idx = layout->lids[layout->disp[i]+j];
      int len = IPM_ArrayGetLength(u, idx, err);
      IPM_Real *data = IPMK_ArrayNumericGet(u, idx, err);
      for (int k = 0; k < len; k++) {
        *curptr++ = data[k];
      }
    }
    MPI_Isend(&buf[bufdisp[i]], bufdisp[i+1] - bufdisp[i], REAL_MPI_DTYPE, layout->nbrs[i], MY_MPI_TAG, comm, &reqs[i]);
  }

  MPI_Waitall(layout->nbrs.size(), &reqs[0], MPI_STATUSES_IGNORE);

  /* Merge received contributions from sharers with local data
   */
  int cnt = bufdisp[layout->nowners]; // iter for IPM_Reals in buf
  for (int i = layout->disp[layout->nowners]; i < layout->disp.back(); i++) {
    local_int_t idx = layout->lids[i];
    int len = IPM_ArrayGetLength(u, idx, err);
    IPM_Real *data = IPMK_ArrayNumericGet(u, idx, err);
    for (int j = 0; j < len; j++) {
      switch (op) { // An optimizing compiler should be able to hoist the switch out of the outer loop
        case IPM_MEMORY_ADD :
          data[j] += buf[cnt++]; break;
        case IPM_MEMORY_PROD :
          data[j] *= buf[cnt++]; break;
        case IPM_MEMORY_MAX :
          data[j] = std::max(data[j], buf[cnt++]); break;
        case IPM_MEMORY_MIN :
          data[j] = std::min(data[j], buf[cnt++]); break;
        default:
          abort(); break;
      }
    }
  }

  /* Do reverse communication to send final results back to sharers
   */

  // Write final results back into buf
  cnt = bufdisp[layout->nowners]; // iter for IPM_Reals in buf
  for (int i = layout->disp[layout->nowners]; i < layout->disp.back(); i++) {
    local_int_t idx = layout->lids[i];
    int len = IPM_ArrayGetLength(u, idx, err);
    IPM_Real *data = IPMK_ArrayNumericGet(u, idx, err);
    for (int j = 0; j < len; j++) {
      buf[cnt++] = data[j];
    }
  }

  MY_MPI_TAG++;
  for (int i = layout->nowners; i < layout->nbrs.size(); i++) { // Send final results to sharers
    MPI_Isend(&buf[bufdisp[i]], bufdisp[i+1] - bufdisp[i], REAL_MPI_DTYPE, layout->nbrs[i], MY_MPI_TAG, comm, &reqs[i]);
  }

  for (int i = 0; i < layout->nowners; i++) { // Recv final results from owners
    MPI_Irecv(&buf[bufdisp[i]], bufdisp[i+1] - bufdisp[i], REAL_MPI_DTYPE, layout->nbrs[i], MY_MPI_TAG, comm, &reqs[i]);
  }

  MPI_Waitall(layout->nbrs.size(), &reqs[0], MPI_STATUSES_IGNORE);

  /* Write the results got from owners into my local array
   */
  cnt = 0;
  IPM_Real *resbuf = &buf[0];
  for (int i = 0; i < layout->disp[layout->nowners]; i++) {
    local_int_t idx = layout->lids[i];
    int len = IPM_ArrayGetLength(u, idx, err);
    IPM_Real *data = IPMK_ArrayNumericGet(u, idx, err);
    for (int j = 0; j < len; j++) {
      data[j] = resbuf[cnt++];
    }
  }

}

void IPMR_CommNumericReduce(IPM_ArrayNumeric u, IPM_MemoryType op, IPM_Error *err)
{
  *err = IPM_ERROR_NONE;
  if (!u->layout->nbrs.empty()) { // Communicate if there are neighbors
    if (u->addctx != NULL) IPMR_CommNumericReduceWithContext(u, op, err);
    else IPMR_CommNumericReduceWithoutContext(u, op, err);
  }
}

//==============================================================================
// An index array has been written. Assume for shared points in the array, only
// owners of these points have written valid data to them. This routine
// does communication to pass the so called valid data to sharers of the
// points. The data includes index values and everything they point to recursively.
// Sharers will replace their corresponding (invalid) local
// data with the received data.
//
//==============================================================================
void IPMR_CommIndexWrite(IPM_ArrayIndex u, IPM_Error *err)
{
  assert(u->type == IPM_ARRAY_INDEX);

  int MY_MPI_TAG = 199;
  IPMR_Layout layout = u->layout;
  MPI_Comm comm = u->layout->comm;
  int nowners = layout->nowners;
  int nsharers = layout->nbrs.size() - layout->nowners;
  char *sendbuf, *recvbuf;
  std::vector<int> senddisp(nsharers+1), recvdisp(nowners+1);
  std::vector<MPI_Request> sendreqs(nsharers), recvreqs(nowners);

  /* Add my newly added shared points to gbdry
   */
  {
    std::set<local_int_t> s, s2;
    if (nsharers) {
      for (local_int_t i = layout->disp[layout->nowners]; i < layout->disp.back(); i++) { s.insert(layout->lids[i]); }
    }

    IPM_Array cur = u;
    IPM_Array next = u->indexto;
    while (next) {
      IPMR_Layout layout = next->layout;
      s2.swap(s);
      s.clear();
      for (set<local_int_t>::iterator iter = s2.begin(); iter != s2.end(); iter++) {
        local_int_t lid = *iter;
        int len = IPM_ArrayGetLength(cur, lid, err);
        local_int_t *val = IPMK_ArrayIndexGet(static_cast<IPM_ArrayIndex>(cur), lid, err);
        for (int i = 0; i < len; i++) {
          if (val[i] != IPM_INDEX_NULL) {
            layout->lbdry.insert(val[i]);
            layout->gbdry.insert(IPMR_ArrayGetGid(next, val[i]));
            s.insert(val[i]);
          }
        }
      }
      cur = next;
      next = next->indexto;
    }
  }

  /* Pack shared points owned by me recursively, put the data into sendbuf,  which
   * is to be allocated by the callee.
   */
  // Pass &layout->lids[0] instead of &layout->lids[layout->disp[nowners], because layout->disp[]
  // is always &layout->lids[0] based
  IPMR_ArrayPack(u, nsharers, &layout->nbrs[nowners], &layout->lids[0], &layout->disp[nowners], &sendbuf, &senddisp[0], 1, err);

  /* Tell sharers lengths of data for them
   */
  for (int i = 0; i < nowners; i++) { // Recv from owners
    MPI_Irecv(&recvdisp[i+1], 1, MPI_INT, layout->nbrs[i], MY_MPI_TAG, comm, &recvreqs[i]);
  }

  for (int i = 0; i < nsharers; i++) { // Sent to sharers
    int sendlen = senddisp[i+1] - senddisp[i];
    MPI_Send(&sendlen, 1, MPI_INT, layout->nbrs[nowners+i], MY_MPI_TAG, comm);
  }
  MPI_Waitall(nowners, &recvreqs[0], MPI_STATUSES_IGNORE);

  recvdisp[0] = 0;
  for (int i = 1; i < nowners+1; i++) recvdisp[i] += recvdisp[i-1];

  /* Allocate recvbuf to receive data from owners, and start send/recv
   */
  recvbuf = new char[recvdisp.back()]; assert(recvbuf);
  MY_MPI_TAG++;
  for (int i = 0; i < nowners; i++) {
    MPI_Irecv(&recvbuf[recvdisp[i]], recvdisp[i+1] - recvdisp[i], MPI_CHAR, layout->nbrs[i], MY_MPI_TAG, comm, &recvreqs[i]);
  }

  for (int i = 0; i < nsharers; i++) {
    MPI_Isend(&sendbuf[senddisp[i]], senddisp[i+1] - senddisp[i], MPI_CHAR, layout->nbrs[nowners+i], MY_MPI_TAG, comm, &sendreqs[i]);
  }

  /* Unpack shared points received from owners
   */
  IPMR_ArrayUnpackOverwrite(u, nowners, &layout->nbrs[0], recvbuf, &recvdisp[0], &recvreqs[0], err);
  MPI_Waitall(nsharers, &sendreqs[0], MPI_STATUSES_IGNORE);

  delete [] sendbuf;
  delete [] recvbuf;
}

void IPMR_CommNumericWrite(IPM_ArrayNumeric u, IPM_Error *err)
{
}

void IPMR_FunctionPreLaunch(IPM_Function func,IPM_Error *err)
{
  *err = 0;
}

/*
  This is called on the arguments after the tasks are called, it should ensure
  that any contributions from tasks that need to be merged are merged
*/
void IPMR_FunctionPostLaunch(IPM_Function func,IPM_Error *err)
{
  //============================================================================
  // Remove and add data if an array is expanded
  //============================================================================
  for (int i=0; i < func->n; i++) {
    IPM_Array u = func->array[i];
    MPI_Comm comm = u->layout->comm;
    if (func->mtype[i] == IPM_MEMORY_EXPAND) {
      // Firstly, remove entries asked by users. Users can only remove entries in the top layer array.
      // To get rid of entries in lower layers, just remove entries referencing them in the top layer.
      // Unreferenced entries will automatically recycled during partitioning.
      if (i == func->n-1 && !u->layout->trashbin.empty()) {
        assert(u->type == IPM_ARRAY_INDEX && !(u->attr & IPM_ARRAYATTR_ISCLONE));

        std::vector<IPM_Array> family(u->layout->clones);
        family.push_back(u);

        // TODO: using C++ templates to avoid code duplication?
        for (int i = 0; i < family.size(); i++) {
          if (family[i]->type == IPM_ARRAY_INDEX) {
            IPM_ArrayIndex u2 = static_cast<IPM_ArrayIndex>(family[i]);
            int oldlen = u2->disp.back();
            std::vector<local_int_t> data, disp;
            disp.push_back(0);
            for (int i = 0; i < u2->layout->nlocal; i++) {
              if (!u2->layout->trashbin.count(i)) {
                int len = IPM_ArrayGetLength(u2, i, err);
                local_int_t *iptr = IPMK_ArrayIndexGet(u2, i, err);
                data.insert(data.end(), iptr, iptr+len);
                disp.push_back(len);
              }
            }

            for (int i = 1; i < disp.size(); i++) disp[i] += disp[i-1];
            u2->data.swap(data);
            u2->disp.swap(disp);

            // Adjust newdisp since we udpated disp
            int delta = oldlen - u2->disp.back(); assert(delta > 0);
            for (int j = 0; j < u2->newdisp.size(); j++) u2->newdisp[j] -= delta;
          } else {
            IPM_ArrayNumeric u2 = static_cast<IPM_ArrayNumeric>(family[i]);
            int oldlen = u2->disp.back();
            std::vector<IPM_Real> data;
            std::vector<local_int_t> disp;
            disp.push_back(0);
            for (int i = 0; i < u2->layout->nlocal; i++) {
              if (!u2->layout->trashbin.count(i)) {
                int len = IPM_ArrayGetLength(u2, i, err);
                IPM_Real *iptr = IPMK_ArrayNumericGet(u2, i, err);
                data.insert(data.end(), iptr, iptr+len);
                disp.push_back(len);
              }
            }

            for (int i = 1; i < disp.size(); i++) disp[i] += disp[i-1];
            u2->data.swap(data);
            u2->disp.swap(disp);

            // Adjust newdisp since we udpated disp
            int delta = oldlen - u2->disp.back(); assert(delta > 0);
            for (int j = 0; j < u2->newdisp.size(); j++) u2->newdisp[j] -= delta;

          }
        }
        // ntotal is out of date and we need to recompute it. But not here since
        // not all processes enter this if statement.
        u->layout->nvisible -= u->layout->trashbin.size();
        u->layout->nlocal = u->layout->nvisible;
        u->layout->gids.resize(u->layout->nvisible);
        u->layout->trashbin.clear();
      }

      // Update ntotal for the top array
      if (i == func->n -1) {
        global_int_t ntotal;
        global_int_t mynlocal = u->layout->nlocal;
        MPI_Allreduce(&mynlocal, &ntotal, 1, MPI_INT, MPI_SUM, comm);
        u->layout->ntotal = ntotal;
      }

      // Secondly, add newdata to data, newdisp to disp, and free the stash area.
      int newpts = u->newdisp.size();
      if (newpts) {
        u->commitNewData();
        u->clearNewData();
      }

      // Thirdly, update layout with the new points. Only non-clones can do it.
      // TODO: since we assume the top layer array has no overlaps,  we can avoid storing gids/locals for it.
      if (!(u->attr & IPM_ARRAYATTR_ISCLONE)) {
        if (newpts) {
          u->layout->gids.reserve(newpts);
          u->layout->locals.reserve(newpts);
        }

        // Compute globally unique ids for the new points, which are owned by this
        // process and are local & visible
        int mystart, totalnewpts, myrank;
        MPI_Comm_rank(comm, &myrank);
        MPI_Exscan(&newpts, &mystart, 1, MPI_INT, MPI_SUM, comm);
        MPI_Allreduce(&newpts, &totalnewpts, 1, MPI_INT, MPI_SUM, comm);
        if (myrank == 0) mystart = 0; // On rank 0, mystart is undefined after exscan
        global_int_t gbase = u->layout->ntotal + mystart;
        local_int_t lbase = u->layout->nvisible;
        for (int i = 0; i < newpts; i++, gbase++, lbase++) {
          u->layout->gids.push_back(gbase);
          u->layout->locals.push_back(lbase);
        }

        u->layout->ntotal += totalnewpts;
        u->layout->nlocal += newpts;
        u->layout->nvisible += newpts;
      }
    }
  }

  //============================================================================
  // Do communication at the same layer to maintain a consistent view of arrays
  //============================================================================
  for (int i = 0; i < func->n; i++) {
    if (func->array[i]->type == IPM_ARRAY_NUMERIC && func->mtype[i] < IPM_MEMORY_REDUCTION_END)
    {
      IPMR_CommNumericReduce(static_cast<IPM_ArrayNumeric>(func->array[i]), func->mtype[i], err);
    }
    else if (func->array[i]->type == IPM_ARRAY_REDUNDANT && func->mtype[i] < IPM_MEMORY_REDUCTION_END)
    {
      IPMR_RArrayReduce(static_cast<IPM_RArray>(func->array[i]), func->mtype[i], err);
    }
    else if (func->array[i]->type == IPM_ARRAY_NUMERIC && func->mtype[i] == IPM_MEMORY_WRITE)
    {
      IPMR_CommNumericWrite(static_cast<IPM_ArrayNumeric>(func->array[i]), err);
    }
  }

  //============================================================================
  // Align non-temporary clones to their updated layout
  //============================================================================
  for (int i = 0; i < func->n; i++) {
    IPM_Array u = func->array[i];
    if (u->type == IPM_ARRAY_REDUNDANT) continue; // Redundant arrays have no layout or clones

    for (int k = 0; k < u->layout->clones.size(); k++) {
      IPM_Array clone = u->layout->clones[k];
      int diff =  u->layout->nvisible - (clone->disp.size() - 1); // - current # of pts
      assert(diff >= 0);
      if (diff > 0) {
        if (clone->attr & IPM_ARRAYATTR_HARDCLONE) { // hardclone
          IPM_Index curpt = clone->disp.size();
          IPM_Index curdisp = clone->disp.back();
          for (; curpt < u->layout->nvisible; curpt++) {
            int len = IPM_ArrayGetLength(clone->original, curpt, err);
            if (clone->type == IPM_ARRAY_INDEX) {
              IPM_ArrayIndex clone2 = static_cast<IPM_ArrayIndex>(clone);
              for (int j = 0; j < len; j++) clone2->data.push_back(clone2->defval);
            } else if (clone->type == IPM_ARRAY_NUMERIC) {
              IPM_ArrayNumeric clone2 = static_cast<IPM_ArrayNumeric>(clone);
              for (int j = 0; j < len; j++) clone2->data.push_back(clone2->defval);
            }
            curdisp += len;
            clone->disp.push_back(curdisp);
          }
        } else {
          if (clone->type == IPM_ARRAY_INDEX) {
            IPM_ArrayIndex clone2 = static_cast<IPM_ArrayIndex>(clone);
            clone2->data.insert(clone2->data.end(), diff*clone2->deflen, clone2->defval);
          } else if (clone->type == IPM_ARRAY_NUMERIC) {
            IPM_ArrayNumeric clone2 = static_cast<IPM_ArrayNumeric>(clone);
            clone2->data.insert(clone2->data.end(), diff*clone2->deflen, clone2->defval);
          }

          clone->disp.reserve(diff);
          local_int_t curdisp = clone->disp.back();
          for (int k = 0; k < diff; k++) {
              curdisp += clone->deflen;
              clone->disp.push_back(curdisp);
          }
        }
      }
    }
  }

  //============================================================================
  // Do deep communication at multi-layers if an index array was just written
  //============================================================================
  for (int i = 0; i < func->n; i++) {
    if (func->array[i]->type == IPM_ARRAY_INDEX && func->mtype[i] == IPM_MEMORY_WRITE) {
      IPMR_CommIndexWrite(static_cast<IPM_ArrayIndex>(func->array[i]), err);
    }
  }

  //============================================================================
  // Repartition the mesh if ordered by users.
  // If repartitioning happens, rebuild sharing info of every array affected,
  // and renumber global ids of their data (since during the repartition, those
  // delinquished data points will get discarded, causing holes in gids).
  //============================================================================
  if (func->opts & IPM_FUNCTION_REPARTITION) {
    // ATTENTION:
    // Side-effect: nvisible of each array is updated. nlocal /ntotal becomes out-of-date (and will be set in FindSharedPoints)
    // but the out-of-date ntotal is >= the actual ntotal
    IPMR_FunctionPartition(func,err);

    IPM_ArrayIndex top = static_cast<IPM_ArrayIndex>(func->array[func->n-1]);
    IPM_Array cur;

    // Free contexts attached to arrays
    cur = top;
    while (cur) {
      // Free context attached to the array if it has one
      if (!cur->layout->ctxmap.empty()) {
        cur->layout->FreeAllContexts();
        cur->layout->ctxmap.clear();
      }
      cur = cur->indexto;
    }

    // Top layer array has no shpts.
    top->layout->nlocal = top->layout->nvisible;
    cur = top->indexto; // Top layer array has no shpts
    while (cur) {
      // TODO: an optimization is to find boundary of an upper array and use that to
      // quickly find shared points of lower arrays
      IPMR_FindSharedPoints(cur, err); // Build sharing info & update nlocal
      IPMR_RenumberGids(cur, err); // Assign new gids to points & update ntotal

      //Re-build boundary points
      IPMR_Layout layout = cur->layout;
      layout->gbdry.clear();
      layout->lbdry.clear();
      for (local_int_t i = 0; i < layout->disp.back(); i++) {
          layout->lbdry.insert(layout->lids[i]);
          layout->gbdry.insert(IPMR_ArrayGetGid(cur, layout->lids[i]));
      }
      cur = cur->indexto;
    }

  }

  *err = 0;
}

void IPM_FunctionLaunch(IPM_Function func,IPM_Error *err)
{
  *err = 0;
  int launch = 0;
  int rank;
  MPI_Comm comm = MPI_COMM_SELF;
  IPM_RArray r = NULL;

  IPMR_FunctionPreLaunch(func,err);

  // Run the function if any array argument has (local) data
  // If the mtype is a reduction, we need to always launch the kernel even no local data
  // exists, to contribute zero to the reduction
  for (int i = 0; i < func->n; i++) {
    if (func->array[i]->type == IPM_ARRAY_REDUNDANT || func->mtype[i] < IPM_MEMORY_REDUCTION_END) { launch = 1; continue; } // also test if memory type is MEMORY_ADD?
    if (func->array[i]->layout->nlocal != 0) { launch = 1; }
    if (i == func->n - 1) { comm = func->array[i]->layout->comm; }
  }

  if (func->opts & IPM_FUNCTION_SEQUENTIAL) {
    // IPMR_SequentialPhaseBegin(err);
  }

  MPI_Comm_rank(comm, &rank);

  if (launch || !rank) {
    switch (func->n) {
      case 1: {
        void (*f)(IPM_Array,IPM_Error*) = (void (*)(IPM_Array,IPM_Error*)) func->f;
	    (*f)(func->array[0],err);
        break; }
      case 2: {
        void (*f)(IPM_Array,IPM_Array,IPM_Error*) = (void (*)(IPM_Array,IPM_Array,IPM_Error*)) func->f;
        (*f)(func->array[0],func->array[1],err);
        break; }
      case 3: {
        void (*f)(IPM_Array,IPM_Array,IPM_Array,IPM_Error*) = (void (*)(IPM_Array,IPM_Array,IPM_Array,IPM_Error*)) func->f;
        (*f)(func->array[0],func->array[1],func->array[2],err);
        break; }
      case 4: {
        void (*f)(IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Error*) = (void (*)(IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Error*)) func->f;
        (*f)(func->array[0],func->array[1],func->array[2], func->array[3], err);
        break; }
      case 5: {
        void (*f)(IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Error*)
          = (void (*)(IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Error*)) func->f;
        (*f)(func->array[0],func->array[1],func->array[2], func->array[3], func->array[4],err);
        break; }
      case 6: {
        void (*f)(IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Error*)
          = (void (*)(IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Error*)) func->f;
        (*f)(func->array[0],func->array[1],func->array[2], func->array[3], func->array[4],func->array[5],err);
        break; }
      case 7: {
        void (*f)(IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Error*)
          = (void (*)(IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array, IPM_Error*)) func->f;
        (*f)(func->array[0],func->array[1],func->array[2], func->array[3], func->array[4],func->array[5], func->array[6], err);
        break; }
      case 8: {
        void (*f)(IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Error*)
          = (void (*)(IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Error*)) func->f;
        (*f)(func->array[0],func->array[1],func->array[2], func->array[3], func->array[4],func->array[5], func->array[6], func->array[7], err);
        break; }
      case 9: {
        void (*f)(IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Error*)
          = (void (*)(IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Error*)) func->f;
        (*f)(func->array[0],func->array[1],func->array[2], func->array[3], func->array[4],func->array[5], func->array[6], func->array[7], func->array[8], err);
        break; }
      case 10: {
        void (*f)(IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Error*)
          = (void (*)(IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Error*)) func->f;
        (*f)(func->array[0],func->array[1],func->array[2], func->array[3], func->array[4],func->array[5], func->array[6], func->array[7], func->array[8], func->array[9], err);
        break; }
      case 11: {
        void (*f)(IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array, IPM_Array, IPM_Error*)
          = (void (*)(IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array, IPM_Error*)) func->f;
        (*f)(func->array[0],func->array[1],func->array[2], func->array[3], func->array[4],func->array[5], func->array[6], func->array[7], func->array[8], func->array[9], func->array[10], err);
        break; }
      case 12: {
        void (*f)(IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Error*)
          = (void (*)(IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Error*)) func->f;
        (*f)(func->array[0],func->array[1],func->array[2], func->array[3], func->array[4],func->array[5], func->array[6], func->array[7], func->array[8], func->array[9], func->array[10], func->array[11], err);
        break; }
      case 13: {
        void (*f)(IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Error*)
          = (void (*)(IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Array,IPM_Error*)) func->f;
        (*f)(func->array[0],func->array[1],func->array[2], func->array[3], func->array[4],func->array[5], func->array[6], func->array[7], func->array[8], func->array[9], func->array[10], func->array[11], func->array[12], err);
        break; }
      default:
        IPM_SetErrorVoid(IPM_ERROR_WRONG_STATE);
        break;
    }
  }

  if (func->opts & IPM_FUNCTION_SEQUENTIAL) {
    // IPMR_SequentialPhaseEnd(err);
  }

  IPMR_FunctionPostLaunch(func,err);
}

void IPM_Launch(IPM_Function func,IPM_Array data, ...)
{
  va_list  Argp;
  func->array[0] = data;
  va_start(Argp,data);
  for (int i=1; i<func->n; i++) {
    func->array[i] = (IPM_Array) va_arg(Argp, IPM_Array);
  }
  IPM_Error *err = (IPM_Error*) va_arg(Argp, IPM_Error*);;
  va_end(Argp);
  IPM_FunctionLaunch(func,err);
}

IPM_Function IPM_FunctionCreate(IPM_FunctionOption opts, void* f, int n,...)
{
  IPM_Function func = (IPM_Function)malloc(sizeof(struct _p_IPM_Function));
  // no good way to handle failed malloc
  func->opts = opts;
  func->f    = (IPM_Error (*)(IPM_Array,...)) f;
  func->n    = n;
  va_list  Argp;
  va_start(Argp,n);
  int i = 0;
  for (i=0; i<n; i++) {
    func->mtype[i] = (IPM_MemoryType) va_arg(Argp, int);
  }
  IPM_Error *err = (IPM_Error*) va_arg(Argp, IPM_Error*);
  va_end(Argp);
  *err = 0;
  return func;
}

