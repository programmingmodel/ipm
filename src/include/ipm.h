/*
 Copyright (c) 2014 UChicago Argonne, LLC

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/
#if !defined(__IPM_H)
#define __IPM_H
#include <stdio.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#if IPM_INDEX_WIDTH == 32
  typedef int32_t IPM_Index;
#elif IPM_INDEX_WIDTH == 64
  typedef int64_t IPM_Index;
#else
  #error "Incorrect IPM_INDEX_WIDTH"
#endif

#if IPM_REAL_WIDTH == 32
  typedef float IPM_Real;
#elif IPM_REAL_WIDTH == 64
  typedef double IPM_Real;
#else
  #error "Incorrect IPM_REAL_WIDTH"
#endif

// For internal debugging
extern int gdebug;
extern int grank;
extern int skip_parmetis;

// A valid index, but point to nothing
#define IPM_INDEX_NULL -1 // Must be -1, since we relay on ++IPM_INDEX_NULL = 0

typedef int IPM_Error;
typedef struct _p_IPM_Array     *IPM_Array;
typedef struct _p_IPM_ArrayNumeric     *IPM_ArrayNumeric;
typedef struct _p_IPM_ArrayIndex     *IPM_ArrayIndex;
typedef struct _p_IPM_RArray     *IPM_RArray;
typedef struct _p_IPM_Function  *IPM_Function;

#define IPMK_Error        IPM_Error
#define IPMK_ArrayNumeric IPM_ArrayNumeric
#define IPMK_ArrayIndex   IPM_ArrayIndex
#define IPMK_RArray       IPM_RArray
#define IPMK_ArrayLocalIter IPM_ArrayLocalIter

typedef IPM_Index IPMK_Index;

typedef enum {
  IPM_MEMORY_ADD = 0,  // Sum
  IPM_MEMORY_PROD,  // Product
  IPM_MEMORY_MAX, // maximal
  IPM_MEMORY_MIN, // minimal
  IPM_MEMORY_REDUCTION_END, // reduction ops delimit
  IPM_MEMORY_READ,  // Read only
  IPM_MEMORY_WRITE,  // Rewrite existing entries
  IPM_MEMORY_EXPAND,  // Add new entries
} IPM_MemoryType;

typedef enum {
  IPM_ARRAY_INDEX,
  IPM_ARRAY_NUMERIC,
  IPM_ARRAY_REDUNDANT
} IPM_ArrayType;

typedef enum {
  IPM_ARRAYATTR_ISCLONE   = 0x1, // A clone of some array
  IPM_ARRAYATTR_HARDCLONE = 0x2, // A clone. Even lengths of entries are as same as its original
  IPM_ARRAYATTR_TOP = 0x4 // Is the top level array (without any overlap among processes)
} IPM_ArrayAttr;

typedef enum {
  IPM_FUNCTION_DEFAULT     = 0,
  IPM_FUNCTION_KERNEL      = 1,
  IPM_FUNCTION_SEQUENTIAL  = 2,
  IPM_FUNCTION_REPARTITION = 4
} IPM_FunctionOption;

#define IPM_ERROR_NONE               0
#define IPM_ERROR_OUT_OF_MEMORY      1
#define IPM_ERROR_WRONG_DATATYPE     2
#define IPM_ERROR_INDEX_OUT_OF_RANGE 3
#define IPM_ERROR_WRONG_STATE        4
#define IPM_ERROR_SYSTEM             5
#define IPM_ERROR_OUT_OF_SPACE       6

#define IPMK_ERROR_NONE               IPM_ERROR_NONE
#define IPMK_ERROR_WRONG_DATATYPE     IPM_ERROR_WRONG_DATATYPE
#define IPMK_ERROR_INDEX_OUT_OF_RANGE IPM_ERROR_INDEX_OUT_OF_RANGE
#define IPMK_ERROR_WRONG_STATE        IPM_ERROR_WRONG_STATE

/**
 * Create an empty numeric array.
 * \param name name of the array. Having a name improve debug messages.
 *    Can be NULL if you don't want to name it.
 * \sa IPM_ArrayIndexCreate, IPM_RArrayCreate
 */
extern IPM_ArrayNumeric IPM_ArrayNumericCreate(const char *name, IPM_Error *err);

/**
 * Create an empty index array.
 * \param name name of the array. Having a name improve debug messages.
 *    Can be NULL if you don't want to name it.
 * \sa IPM_ArrayNumericCreate, IPM_RArrayCreate
 */
extern IPM_ArrayIndex IPM_ArrayIndexCreate(const char *name, IPM_Array indexto, IPM_Error *err);

/**
 * Create an empty redundant array (RArray).
 * A RArray contains numeric values, however it is replicated on every process.
 * Its length is fixed after creation.
 * \param name name of the array. Having a name improve debug messages.
 *    Can be NULL if you don't want to name it.
 * \param length length of the array.
 * \sa IPM_ArrayNumericCreate, IPM_RArrayCreate
 */
extern IPM_RArray IPM_RArrayCreate(const char *name, int length, IPM_Error *err);

/**
 * Build a numeric array by cloning the array \p orig.
 * \param name name of the clone
 * \param orig the original of the clone
 * \param deflen the default length of elements in the clone
 * \param defval the default value of elements in the clone
 * \param temporary if true, the returned array is temporary
 */
extern IPM_ArrayNumeric IPM_ArrayNumericClone(const char *name, IPM_Array orig, int deflen, IPM_Real defval, int temporary, IPM_Error *err);

extern void IPM_ArrayNumericSet(IPM_ArrayNumeric u, IPM_Real alpha, IPM_Error *err);

/**
 * Build an index array by cloning the array \p orig.
 * \param name name of the clone
 * \param orig the original of the clone
 * \param deflen the default length of elements in the clone
 * \param defval the default value of elements in the clone
 * \param temporary if true, the returned array is temporary
 * \param hops number of hops to search in partitioning the index clone
 */
extern IPM_ArrayIndex IPM_ArrayIndexClone(const char *name, IPM_Array orig, int deflen, IPM_Index defval, int temporary, int hops, IPM_Error *err);

/**
 * Add an entry (aka, data point) to a numeric array.
 * \param u the numeric array
 * \param len length of this data point
 * \param values pointer to values of this data point
 */
extern IPMK_Index IPMK_ArrayNumericAdd(IPM_ArrayNumeric u, int len, IPM_Real *values,IPM_Error *err);

/**
 * x(i) = alpha * x(i) for all i
 */
void IPM_ArrayNumericScale(IPM_ArrayNumeric x, IPM_Real alpha, IPM_Error *err);

/**
 * Copy x to y, i.e., y = x
 * x and y must have the same size
 */
extern void IPM_ArrayNumericCopy(IPM_ArrayNumeric x, IPM_ArrayNumeric y, IPM_Error *err);

/**
 * w(i) = x(i) * y(i) for all i
 * w, x and y must have the same size
 */
extern void IPM_ArrayNumericPointwiseMult(IPM_ArrayNumeric w, IPM_ArrayNumeric x, IPM_ArrayNumeric y, IPM_Error *err);

/**
 * x(i) = 1/x(i) for all i
 */
extern void IPM_ArrayNumericReciprocal(IPM_ArrayNumeric x, IPM_Error *err);

/**
 * y = y + alpha*x
 */
extern void IPM_ArrayNumericAXPY(IPM_ArrayNumeric y, IPM_Real alpha, IPM_ArrayNumeric x, IPM_Error *err);

/**
 * y = x + alpha*y
 */
extern void IPM_ArrayNumericAYPX(IPM_ArrayNumeric y, IPM_Real alpha, IPM_ArrayNumeric x, IPM_Error *err);

/**
 * Compute y = alpha*x + beta*y
 * x and y must be clones of the same original, and have the same entry lengths.
 */
extern void IPM_ArrayNumericAXPBY(IPM_ArrayNumeric y, IPM_Real alpha, IPM_Real beta, IPM_ArrayNumeric x, IPM_Error *err);

extern IPM_Real IPM_ArrayNumericInnerProduct(IPM_ArrayNumeric x, IPM_ArrayNumeric y, IPM_Error *err);



/**
 * Add an entry (aka, data point) to an index array.
 * \param u the index array
 * \param len length of this data point
 * \param values pointer to values of this data point
 */
extern IPMK_Index IPMK_ArrayIndexAdd(IPM_ArrayIndex u, int len, IPMK_Index *values,IPM_Error *err);

/**
 * Remove an entry (aka, data point) from an index array.
 * \param u the index array
 * \param p index of the entry to be removed
 */
extern void IPMK_ArrayIndexRemove(IPM_ArrayIndex u, IPMK_Index p, IPM_Error *err);

/**
 * Write entry \p p of an index array \p u.
 * \param u the index array
 * \param p index of the entry to be overwritten
 * \param len length of \p values
 * \param values new values to be put to entry \p p
 * \param err return error code
 * \see IPMK_ArrayNumericChange
 */
extern void IPMK_ArrayIndexChange(IPM_ArrayIndex u, IPMK_Index p, int len, IPMK_Index *values,IPM_Error *err);

/**
 * Write entry \p p of a numeric array \p u.
 * \param u the numeric array
 * \param p index of the entry to be overwritten
 * \param len length of \p values
 * \param values new values to be put to entry \p p
 * \param err return error code
 * \see IPMK_ArrayIndexChange
 */
extern void IPMK_ArrayNumericChange(IPM_ArrayNumeric u, IPMK_Index p, int len, IPM_Real *values,IPM_Error *err);

/**
 * Zero out an IPM_RArray \p u
 */
extern void IPM_RArrayZero(IPM_RArray u, IPM_Error *err);

/**
 * Set all entries of IPM_ArrayNumeric \p u to alpha
 */
extern void IPM_ArrayNumericSet(IPM_ArrayNumeric u, IPM_Real alpha, IPM_Error *err);

/**
 * Destroy an index array \p u
 */
extern void IPM_ArrayIndexDestroy(IPM_ArrayIndex u, IPM_Error *err);

/**
 * Destroy a numeric array \p u
 */
extern void IPM_ArrayNumericDestroy(IPM_ArrayNumeric u, IPM_Error *err);

/**
 * Destroy a RArray \p u
 */
extern void IPM_RArrayDestroy(IPM_RArray u, IPM_Error *err);

/**
 * Destroy an abstract array \p u. The runtime will dispatche the call to
 * IPM_ArrayIndexDestroy, IPM_ArrayNumericDestroy or IPM_RArrayDestroy, based
 * on the actual type of the array.
 */
extern void IPM_ArrayDestroy(IPM_Array u, IPM_Error *err);

/**
 * Get length of an entry by its local index
 * TODO: rename it to IPM_ArrayEntryLength()
 */
extern int IPM_ArrayGetLength(void *u, IPM_Index p,IPM_Error *err);

/**
 * Fetch an entry of an index array by index
 * \param u the index array
 * \param p index of the entry
 * \return pointer to index values of the entry
 */
extern IPMK_Index* IPMK_ArrayIndexGet(IPM_ArrayIndex u, IPMK_Index p, IPM_Error *err);

/**
 * Fetch an entry of a numeric array by index
 * \param u the numeric array
 * \param p index of the entry
 * \return pointer to numeric values of the entry
 */
extern IPM_Real * IPMK_ArrayNumericGet(IPM_ArrayNumeric u, IPMK_Index p, IPM_Error *err);

/**
 * Fetch values of a RArray
 * \param u the RAarray
 * \return pointer to the whole numeric values of the RArray
 */
extern IPM_Real * IPMK_RArrayGet(IPM_RArray u, IPM_Error *err);

// This subroutine appears in both user functions and kernels
#define IPM_RArrayGet IPMK_RArrayGet

/**
 * Return total number of data points of array \p u across all processes.
 * Question: should it return size_t since it many be larger than int?
 */
extern int IPM_ArrayTotalSize(IPM_Array u, IPM_Error *err);

/**
 * Return number of visible data points of array \p u on process calling this routine
 */
extern IPM_Index IPM_ArrayVisibleSize(IPM_Array u, IPM_Error *err);

/**
 * Return number of local data points of array \p u on process calling this routine
 */
extern IPM_Index IPM_ArrayLocalSize(IPM_Array u, IPM_Error *err);

extern void IPM_ArrayIndexSetTop(IPM_ArrayIndex u, int connect_depth, IPM_Error *err);


/**
 * An iterator type to loop over all local points of an array.
 * Local points are not necessarily contigous in memory such that an iterator
 * is provide to enumerate them.
 */
typedef struct {
  IPM_Index cur;
  IPM_Index end; // copy of u->layout->nlocal;
  int full; // Are all visiable points local? If yes, we can optimize this case.
  IPM_Index *locals;
} IPM_ArrayLocalIter;

/**
 * Initialize a local data point iterator \p iter for array \p u
 */
extern void IPMK_ArrayLocalIterInit(void *u, IPMK_ArrayLocalIter *iter);

/**
 * Return true if and only if iterator \p iter has passed its end.
 */
static inline int IPMK_ArrayLocalIterDone(IPMK_ArrayLocalIter *iter)
{
  return iter->cur >= iter->end;
}

/**
 * Move the iterator \p iter to the next local data point.
 */
static inline void IPMK_ArrayLocalIterNext(IPMK_ArrayLocalIter *iter) { iter->cur++; }

/**
 * Return index of the local data point pointed by \p iter.
 * The index can be used to access the array over which the iterator was created.
 * The index is only meaningfull on the calling process.
 */
static inline IPM_Index IPMK_ArrayLocalIterToIndex(IPMK_ArrayLocalIter *iter)
{
  return iter->full ? iter->cur : iter->locals[iter->cur];
}

/**
 * An iterator type to loop over visible data points of an array.
 *
 * Visible data points are usually contigous in memroy. Nevertheless, we design
 * this iterator as a counterpart for IPM_ArrayLocalIter. This iterator type is
 * lightweighted and has the same performance as direct indexing.
 */
typedef struct {
  IPM_Index cur;
  IPM_Index end;
} IPM_ArrayVisibleIter;

#define IPMK_ArrayVisibleIter IPM_ArrayVisibleIter

/**
 * Initialize a visible data point iterator \p iter for array \p u
 */
extern void IPMK_ArrayVisibleIterInit(void *u, IPMK_ArrayVisibleIter *iter);

/**
 * Return true if and only if iterator \p iter has passed its end.
 */
static inline int IPMK_ArrayVisibleIterDone(IPMK_ArrayVisibleIter *iter)
{
  return iter->cur >= iter->end;
}

/**
 * Move the iterator \p iter to the next visible data point.
 */
static inline void IPMK_ArrayVisibleIterNext(IPMK_ArrayVisibleIter *iter) { iter->cur++; }

/**
 * Get index from an IPMK_ArrayVisibleIter
 */
static inline IPM_Index IPMK_ArrayVisibleIterToIndex(IPMK_ArrayVisibleIter *iter) { return iter->cur; }

/**
 * Return index of the visible data point pointed by \p iter.
 * The index can be used to access the array over which the iterator was created.
 * The index is only meaningfull on the calling process.
 */
extern IPM_Index IPM_ArrayGetGid(IPM_Array u, IPMK_Index p,IPM_Error *err);

extern int IPMR_AbortOnError;
#define IPM_SetError(flg,rtn) {*err = flg; printf("Error value %d line %d\n",*err,__LINE__); if (IPMR_AbortOnError) abort();return rtn;}
#define IPMK_SetError IPM_SetError
#define IPM_SetErrorVoid(flg) {*err = flg;printf("Error value %d line %d\n",*err,__LINE__);if (IPMR_AbortOnError) abort();return;}
#define IPMK_SetErrorVoid IPM_SetErrorVoid

extern void IPM_LaunchWait(IPM_Function func,IPM_Error *err);

extern IPM_Array IPMK_ArrayIndexTo(IPM_Array u, IPM_Error *err);

extern void IPM_Initialize(int*,char***,IPM_Error*);
extern void IPM_Finalize(IPM_Error*);
extern void IPM_Printf(const char[],...);

extern IPM_Function IPM_FunctionCreate(IPM_FunctionOption, void *f, int, ...);
extern void IPM_Launch(IPM_Function, IPM_Array, ...);
extern void IPM_FunctionLaunch(IPM_Function func, IPM_Error *err);

extern void IPM_FunctionDestroy(IPM_Function func, IPM_Error *err);

/**
 * Swap value of two numerica arrays \p u and \p v
 */
extern void IPM_ArrayNumericSwap(IPM_ArrayNumeric u, IPM_ArrayNumeric v, IPM_Error *err);

/**
 * Attach an user defined context \p ctx to an array \p u
 */
extern void IPM_ArrayAttachContext(IPM_Array u, const char* name, void *ctxdata, void *freefunc, IPM_Error *err);

/**
 * Detach the user defined context from array \p u and return it to user
 */
extern void IPM_ArrayDetachContext(IPM_Array u, IPM_Error *err);

/**
 * Lookup the user defined context attached to array \p u with name \p name
 */
extern void* IPM_ArrayLookupContext(IPM_Array u, const char* name, IPM_Error *err);

/**
 * Create a communication plan for summation for a numeric array \p u.
 * Use it when you know the layout of the array will be fixed for a while and you
 * pass IPM_MEMTYPE_ADD to a kernel as the array's memory type. The runtime
 * will create an optimized communication plan for \p u so that shared data points
 * in the array are efficiently and correctly summed up between processes.
 */
extern void IPM_ArrayNumericCreateAddContext(IPM_ArrayNumeric u, IPM_Error *err);

/**
 * Destroy the communication plan for summation once created for numeric array \p u.
 * It should be called once you know the layout of the array is going to be changed,
 * for example, when you call a kernel with IPM_FUNCTION_REPARTITION.
 */
extern void IPM_ArrayNumericDestroyAddContext(IPM_ArrayNumeric u, IPM_Error *err);

extern void IPM_ArrayNumericPrint(IPMK_ArrayNumeric u, IPMK_Error *err);
extern void IPM_ArrayIndexPrint(IPMK_ArrayIndex u, IPMK_Error *err);
extern void IPM_RArrayPrint(IPMK_RArray u, IPMK_Error *err);
extern void IPM_ArrayPrint(IPM_Array u, IPMK_Error *err);

extern IPM_Function ipm_arraynumericprint,ipm_arrayindexprint;


struct _p_IPM_Function {
  int n;  // number of arguments
  IPM_MemoryType   mtype[20];
  IPM_Array        array[20];
  IPM_FunctionOption opts;
  IPM_Error        (*f)(IPM_Array,...);
};

// Implements RK4
typedef struct {
  IPM_ArrayNumeric Q,k1,k2,k3,k4,W;
  IPM_RArray       scale;

  IPM_Function     myrhs;
  IPM_Function     myCompute;
  IPM_Function     myCompute5;
  IPM_Function     myPrint;

  double time, dt, FinalTime;
  int    tstep;
} RK4;

extern void RK4Init(RK4*, IPM_ArrayNumeric, IPM_Function);
extern void RK4Solve(RK4*);

#ifdef __cplusplus
}
#endif

#endif
