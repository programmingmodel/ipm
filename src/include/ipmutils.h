#if !defined(__IPMUTILS_H)
#define __IPMUTILS_H

// Give an array of size n, distributed it on nproc processes in blocks.
// For a proc with rank 'rank', compute the [low, high] boundary and size of the
// segment assigned to this proc.  Also, for an element idx, compute its owner proc.
// Since we use integer multiplication, we promote integers to 64-bit to avoid
// integer overflow.
#define BLOCK_LOW(rank, nproc, n) (((int64_t)(rank))*(n)/(nproc))
#define BLOCK_HIGH(rank, nproc, n) (BLOCK_LOW((rank)+1, nproc, n) - 1)
#define BLOCK_SIZE(rank, nproc, n) (BLOCK_LOW((rank)+1, nproc, n) - BLOCK_LOW(rank, nproc, n))
#define BLOCK_OWNER(idx, nproc, n) ((((int64_t)(nproc))*((idx)+1)-1)/(nproc))

#endif // !defined(__IPMUTILS_H)
