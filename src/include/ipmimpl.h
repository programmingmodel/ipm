#if !defined(__IPMIMPL_H)
#define __IPMIMPL_H

#include <mpi.h>
#include <set>
#include <vector>
#include <string>
#include <map>
#include "ipm.h"

#if IPM_INDEX_WIDTH == 32
  #define INDEX_DTYPE MPI_INT32_T
#elif IPM_INDEX_WIDTH == 64
  #define INDEX_DTYPE MPI_INT64_T
#else
  #error "Incorrect IPM_INDEX_WIDTH"
#endif


#if IPM_REAL_WIDTH == 32
  #define REAL_DTYPE MPI_FLOAT
#elif IPM_REAL_WIDTH == 64
  #define REAL_DTYPE MPI_DOUBLE
#else
  #error "Incorrect IPM_REAL_WIDTH"
#endif

#define LINT_MPI_DTYPE MPI_INT32_T
#define GINT_MPI_DTYPE MPI_INT32_T
#define REAL_MPI_DTYPE MPI_DOUBLE

typedef IPM_Index local_int_t;
typedef IPM_Index global_int_t;

typedef struct _p_IPMR_Layout *IPMR_Layout;

struct _p_IPM_Array {
  IPM_ArrayType type;
  std::string name;   //Name of the array to help debugging
  int attr; // attributes in bit mask format
  int deflen; // Default length of elements in the array

  // Store displacements of elements in the array such that disp[i+1] - disp[i]
  // is the length of element i. disp is for committed data and newdisp is
  // for stashed data.
  std::vector<local_int_t> disp, newdisp;

  // If it is an INDEX array, it points to array 'indexto'. Otherwise, indexto is NULL
  IPM_Array indexto;

  // If the array is a clone, its original is <original>
  IPM_Array original;

  // Parallel distribution of the array. Multiple arrays may share a layout
  IPMR_Layout layout;

  _p_IPM_Array(const char* name);
  _p_IPM_Array(const char *name, IPM_Array ref, int deflen, int tempclone);
  _p_IPM_Array(const char* name, IPM_Array ref, int temporary);
  ~_p_IPM_Array();

  virtual void* getData() = 0;
  virtual void clearData() {};
  virtual void commitNewData() {};
  virtual void clearNewData() {};
};


struct _p_IPM_ArrayIndex : public _p_IPM_Array {
  local_int_t defval;
  int hops; // If it is an index clone, how many hops shall we search in partitioning
  std::vector<local_int_t> data, newdata;

  // How many levels of arrays below the top array are used to build connectivity 
  // when partitioning the top array. For example, for a 2D mesh consisting of meshes, edges and vertices, users can
  // partition meshes based on their connectivity on edges (connect_depth = 1) or vertices (connect_depth = 2).
  // This field is only meaningful for the top level index array.
  int connect_depth; 

  _p_IPM_ArrayIndex(const char* name, IPM_Array indexto);
 _p_IPM_ArrayIndex(const char *name, IPM_Array ref, int deflen, local_int_t defval, int tempclone, int hops);

  void* getData() { return &data[0]; }
  void clearData() {
    data.clear();
    disp.resize(1); // disp[0] is always 0
  }
  void commitNewData() {
    data.insert(data.end(), newdata.begin(), newdata.end());
    disp.insert(disp.end(), newdisp.begin(), newdisp.end());
  }
  void clearNewData() {
    newdata.clear();
    newdisp.clear();
  }

};

/**
 * Context of a communication plan to add numeric arrays
 */
struct NumericAddContext {
  MPI_Comm comm;
  std::vector<int> nbrs; // Store all neighbors (or off-node neighbors)
  int nowners; // # of owners, which are in the front of nbrs[]
  std::vector<IPM_Real> buf; // send/recv buf
  std::vector<local_int_t> disp; // I share with nbrs[i] these values: buf[disp[i]..disp[i+1]),
  std::vector<local_int_t> offset; // whose offsets are offset[disp[i]..disp[i+1]). Here
  std::vector<MPI_Request> reqs; // offsets are for INDIVIDUAL IPM_Reals

#ifdef BUILD_ON_NODE_CONTEXT
  // On-node context
  MPI_Comm shmcomm; // The shared-memory communicator I am in
  MPI_Win shmwin; // MPI window for shmbuf's
  int nshmowners; // # of shm owners, which are in the front of shmnbrs[]
  std::vector<int> shmnbrs; // Store my shm nbrs in shmcomm
  std::vector<IPM_Real*> shmsharerbptrs; // Base pointers to bufs on my shm sharers
  IPM_Real *shmbuf; // shm buf. Put my contributions here and let owners read
  std::vector<local_int_t> shmdisp;
  std::vector<local_int_t> shmoffset;

  ~NumericAddContext() {
    MPI_Win_free(&shmwin);
    MPI_Comm_free(&shmcomm);
  }
#endif
};

struct _p_IPM_ArrayNumeric : public _p_IPM_Array {
  IPM_Real defval;
  std::vector<IPM_Real> data, newdata;

  NumericAddContext *addctx;

  _p_IPM_ArrayNumeric(const char* name);
  _p_IPM_ArrayNumeric(const char *name, IPM_Array ref, int deflen, IPM_Real defval, int tempclone);
  _p_IPM_ArrayNumeric(const char *name, IPM_ArrayNumeric ref, int tempclone);

  void* getData() { return &data[0]; }
  void clearData() {
    data.clear();
    disp.resize(1); // disp[0] is always 0
  }
  void commitNewData() {
    data.insert(data.end(), newdata.begin(), newdata.end());
    disp.insert(disp.end(), newdisp.begin(), newdisp.end());
  }
  void clearNewData() {
    newdata.clear();
    newdisp.clear();
  }
};

struct _p_IPM_RArray : public _p_IPM_Array {
  std::vector<IPM_Real> data;

  _p_IPM_RArray(const char* name, int length);
  void* getData() { return &data[0]; }
};

struct _p_IPMR_Layout {
  // Communicator over which arrays with this layout are distributed
  MPI_Comm comm;

  // Over all procs, there are ntotal points in this array. Of them, this proc
  // owns nlocal points, which are not overlapped with other procs. However,
  // this proc can view nvisible points. For example, two neighboring triangles
  // are distributed to two processes. The boundary edge is owned by one of
  // them, but visible to the other. So, nvisilbe always >= nlocal
  global_int_t ntotal;
  local_int_t nlocal;
  local_int_t nvisible;

  std::vector<global_int_t> gids; // Global ids of visible points
  std::vector<local_int_t> locals; // Local indices of locally owned points

  std::set<local_int_t> trashbin; // Store indices of entries to be removed

  // Log (persistent, or non-temp) clones of the array which created this
  // layout.
  //
  // Question: Clones stored here must be numeric arrays???
  // We need this info so that when we repartition an array, whose clones are
  // also moved along.  Assume temp arrays need not to be moved during
  // repartitioning, so temp clones will not be logged here.
  std::vector<IPM_Array> clones;

  // A data point may be shared by multiple processes, of which we specify the
  // process with the smallest rank as the owner of the point and say the others
  // as sharers of the point. From a process' point of view, it may be the
  // owner of some points and at the same time the sharer of some other points.

  // If two processes have at least one common point and one of them is
  // the owner of the point, then the two processes are said to be neighbors.  Note
  // that sharing one point does not necessarily make them neighbor. One of
  // them must be owner.

  // With above definitions, a process knows a bunch of owners and a bunch of
  // sharers. Each owner owns some points which are shared with the
  // process. Each sharer shares some points which are owned by the process.
  // For each process, we store its neighbors in vector nbrs[], with
  // owners in the front and sharers at the back. For each neigbhor, we store
  // the points the process shares with it using a CSR data structure lids[] and
  // disp[].

  // Suppose two processes P and Q share a set of points S, and S is owned by P,
  // then Q is in P's nbrs[], S is in P's nbrs[].  P, Q should have a consistent
  // view of S.  For example, S can be sorted by their gids.

  int nowners; // # of owners in nbrs[]
  std::vector<int> nbrs; // Store neighbors, with owners in the front.
  std::vector<local_int_t> lids; // I share points with local indices in
  std::vector<local_int_t> disp; // lids[disp[i]..disp[i+1]) with neighbor nbrs[i]

  // Keep track of boundary points visible to this proc. This info helps hash
  // table lookup when gluing incoming mesh pieces during mesh repartitioning.
  // We only need to look up boundary points instead of looking up all points.
  std::set<global_int_t> gbdry; // boundary points' gids
  std::set<local_int_t> lbdry; // boundary points' lids

  // Reference count of this Layout object
  int refcount;

  // Map a context using name as key, and <ctxdata, freefunc> as value
  std::map<std::string, std::pair<void*, void*> > ctxmap;

  _p_IPMR_Layout() {
    comm = MPI_COMM_WORLD;
    ntotal = 0;
    nvisible = nlocal = 0;
    nowners = 0;
    refcount = 1;
  }

  void FreeAllContexts() {
    typedef void (*ctx_freefunc_t)(void* ctxdata);
    for (std::map<std::string, std::pair<void*, void*> >::iterator iter = ctxmap.begin(); iter != ctxmap.end(); iter++) {
      ((ctx_freefunc_t)(iter->second.second))(iter->second.first);
    }
  }

  // Free all contexts before destroy the map
  ~_p_IPMR_Layout() { FreeAllContexts(); }

};


#endif

