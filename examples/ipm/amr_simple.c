/*
 Copyright (c) 2015 UChicago Argonne, LLC

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

/*
 * This sample IPM program demonstrates use of IPM_MEMORY_ADD, and (bad) mesh refinement
 * without breaking boundaries (i.e., shared data of processors).
 *
 * It starts with a mesh with four triangles. It refines triangles by adding a
 * centroid to a triangle and then splits it into three child triangles.
 *
 * Each element will contribute some value to its vertices. If a vertex is shared
 * by multiple processes, then those values should be summed up (hence IPM_MEMORY_ADD
 * comes to help) and be the same on all processes. Also, we want to know the total
 * sum of the values on vertices, so we use an IPM_RArray with IPM_MEMORY_ADD.
 *
 * Usage:
 *   ./prog -i | --niter <niter>   Number of refinements
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <unistd.h>
#include <getopt.h>
#include <ipm.h>
#include <mpi.h>
#ifdef HAVE_CAIRO
#include <cairo-pdf.h>
cairo_t *cr;
#endif

/**
 * Create a mesh with 4 elements and 5 vertices
 *
 * \verbatim
 *          2
 *          o
 *         /|\
 *        / | \
 *       /  |  \
 *      /(2)|(1)\
 *     /    |    \
 *  3 o-----o-----o 1
 *     \   4|    /
 *      \(3)|(0)/
 *       \  |  /
 *        \ | /
 *         \|/
 *          o
 *          0
 * \endverbatim
 */
void kCreate(IPMK_ArrayNumeric heat, IPMK_ArrayNumeric vertices, IPMK_ArrayIndex elements, IPMK_Error *err)
{
  if (IPM_ArrayTotalSize(vertices, err) > 0) IPMK_SetErrorVoid(IPMK_ERROR_WRONG_STATE);
  if (IPM_ArrayTotalSize(elements, err) > 0) IPMK_SetErrorVoid(IPMK_ERROR_WRONG_STATE);

  IPM_Real xy[5][2] = {{.5,0},{1,.5},{.5,1},{0,.5},{.5,.5}}; // 5 vertices
  IPM_Index id[4][3] = {{0,1,4},{1,2,4},{2,3,4},{3,0,4}}; // 4 elements
  IPM_Real h[4] = {1.0, 2.0, 3.0, 4.0}; // heat of elements
  IPMK_Index indices[5];

  // Add vertices
  for (int i = 0; i < 5; i++) indices[i] = IPMK_ArrayNumericAdd(vertices, 2, xy[i], err);

  // Add elements using indices returned above
  for (int i = 0; i < 4; i++) {
    IPMK_Index ps[3] = {indices[id[i][0]],indices[id[i][1]],indices[id[i][2]]};
    IPMK_ArrayIndexAdd(elements, 3, ps, err);
    IPMK_ArrayNumericAdd(heat, 1, &h[i], err); // heat is a clone of elements. They grow in lock step
  }
}

/**
 * Draw the mesh element by element
 */
void kDraw(IPMK_ArrayNumeric vertices, IPMK_ArrayIndex elements, IPMK_Error *err)
{
  IPMK_ArrayLocalIter iter;
  for (IPMK_ArrayLocalIterInit(elements, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPMK_Index p = IPMK_ArrayLocalIterToIndex(&iter);
    const IPMK_Index *ps = IPMK_ArrayIndexGet(elements, p, err); // vertices of the element
#ifdef HAVE_CAIRO
    const IPM_Real *xy0 = IPMK_ArrayNumericGet(vertices, ps[0], err); // coors of vertex
    const IPM_Real *xy1 = IPMK_ArrayNumericGet(vertices, ps[1], err);
    const IPM_Real *xy2 = IPMK_ArrayNumericGet(vertices, ps[2], err);
    cairo_move_to(cr,xy0[0], xy0[1]);
    cairo_line_to(cr,xy1[0], xy1[1]);
    cairo_line_to(cr,xy2[0], xy2[1]);
    cairo_close_path(cr);
    cairo_stroke (cr);
#endif
  }
}

/**
 * This kernel splits every element into three elements by introducing
 * a new vertice at its centroid. The heat (enery) of an element is evenly
 * distributed to its three children.
 */
void kRefine(IPMK_ArrayNumeric heat, IPMK_ArrayNumeric vertices, IPMK_ArrayIndex elements, IPMK_Error *err)
{
  if (IPMK_ArrayIndexTo(elements, err) != vertices) IPMK_SetErrorVoid(IPMK_ERROR_WRONG_STATE);

  IPMK_ArrayLocalIter iter;
  for (IPMK_ArrayLocalIterInit(elements, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPMK_Index p = IPMK_ArrayLocalIterToIndex(&iter);
    const IPMK_Index *ps = IPMK_ArrayIndexGet(elements, p, err); // vertices of the element
    const IPM_Real h = *IPMK_ArrayNumericGet(heat, p, err); // heat of the element
    const IPM_Real kidheat = h/3;

    const IPM_Real *xy0 = IPMK_ArrayNumericGet(vertices, ps[0], err); // coors of vertex
    const IPM_Real *xy1 = IPMK_ArrayNumericGet(vertices, ps[1], err);
    const IPM_Real *xy2 = IPMK_ArrayNumericGet(vertices, ps[2], err);
    IPMK_Index indices[3];

    // Compute the centroid and add it to vertices
    IPM_Real xynew[2] = {(xy0[0]+xy1[0]+xy2[0])/3.0, (xy0[1]+xy1[1]+xy2[1])/3.0};
    IPMK_Index newindex = IPMK_ArrayNumericAdd(vertices, 2, xynew, err);

    // The 1st kid
    indices[0] = newindex;
    indices[1] = ps[1];
    indices[2] = ps[2];
    IPMK_ArrayIndexAdd(elements, 3, indices, err);
    IPMK_ArrayNumericAdd(heat, 1, &kidheat, err);

    // The 2nd kid
    indices[0] = newindex;
    indices[1] = ps[2];
    indices[2] = ps[0];
    IPMK_ArrayIndexAdd(elements, 3, indices, err);
    IPMK_ArrayNumericAdd(heat, 1, &kidheat, err);

    // The 3rd kid replaces the parent
    indices[0] = ps[0];
    indices[1] = ps[1];
    indices[2] = newindex;
    IPMK_ArrayIndexChange(elements, p, 3, indices, err);
    IPMK_ArrayNumericChange(heat, p, 1, &kidheat, err);
  }
}

/**
 * This kernel computes radiation of each vertex. Let us suppose each element
 * contributes 1/3 of its heat to each of its three vertices.
 */
void kContribute(IPMK_ArrayNumeric radiation, IPMK_ArrayNumeric heat, IPMK_ArrayIndex elements, IPMK_Error *err)
{
  IPMK_ArrayLocalIter iter;
  for (IPMK_ArrayLocalIterInit(elements, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPMK_Index p = IPMK_ArrayLocalIterToIndex(&iter);
    const IPMK_Index *ps = IPMK_ArrayIndexGet(elements, p, err); // vertices of the element
    const IPM_Real h = *IPMK_ArrayNumericGet(heat, p, err); // heat of the element
    const IPM_Real kidheat = h/3;
    *IPMK_ArrayNumericGet(radiation, ps[0], err) += kidheat;
    *IPMK_ArrayNumericGet(radiation, ps[1], err) += kidheat;
    *IPMK_ArrayNumericGet(radiation, ps[2], err) += kidheat;
  }
}

/**
 * This kernel loops over all vertices and sums up their radiation.
 * Note that \p total should already be zeroed out before calling this kernel
 */
void kReduce(IPMK_RArray total, IPMK_ArrayNumeric radiation, IPMK_Error *err)
{
  IPMK_ArrayLocalIter iter;
  IPM_Real *tot = IPMK_RArrayGet(total, err);
  for (IPMK_ArrayLocalIterInit(radiation, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPMK_Index p = IPMK_ArrayLocalIterToIndex(&iter);
    *tot += *IPMK_ArrayNumericGet(radiation, p, err);
  }
  //printf("On processor %d, tot = %f\n", grank, *tot);
}

int main(int argc, char **argv)
{
  IPM_Error err;
  int niter, rank;
  char filename[128];
  IPM_ArrayNumeric vertices, radiation;
  IPM_ArrayIndex elements, heat;
  IPM_RArray total;
  IPM_Function myDraw, myCreate, myRefine, myContribute, myReduce;

  IPM_Initialize(&argc, &argv, &err);

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  if (!rank) {
    while (1) {
      static struct option long_options[] = {
        {"niter",  required_argument, 0, 'i'},
        {"help",   no_argument,       0, 'h'},
        {0, 0, 0, 0}
      };

      int option_index = 0;
      int c = getopt_long(argc, argv, "i:h",long_options, &option_index);

      if (c == -1) break; // End of the options
      switch (c) {
        case 'i':
          niter = atoi(optarg); break;
        case 'h':
        default:
          printf("Usage: %s -i | --niter <int>  Number of refinements\n", argv[0]);
          MPI_Abort(MPI_COMM_WORLD, -1);
          return 0;
      }
    }
  }
  MPI_Bcast(&niter, 1, MPI_INT, 0, MPI_COMM_WORLD);

#if defined(HAVE_CAIRO)
  snprintf(filename,128,"pdffile_%d.pdf", rank);
  cairo_surface_t *surface = cairo_pdf_surface_create(filename, 400, 400);
  cr = cairo_create (surface);
  cairo_scale (cr, 400, 400);
  cairo_set_line_width (cr, 1.0/400);
  cairo_set_source_rgb (cr, 0, 0, 0);
#endif

  vertices = IPM_ArrayNumericCreate("vertices", &err);
  elements = IPM_ArrayIndexCreate("elements", vertices, &err);

  heat = IPM_ArrayNumericClone("heat", elements, 1, 0.0, 0, &err);
  total= IPM_RArrayCreate("total", 1, &err);

  myDraw = IPM_FunctionCreate(IPM_FUNCTION_KERNEL, kDraw, 2,
    IPM_MEMORY_READ, // vertices
    IPM_MEMORY_READ, // elements
    &err);

  myCreate = IPM_FunctionCreate(IPM_FUNCTION_REPARTITION, kCreate, 3,
    IPM_MEMORY_EXPAND, // heat
    IPM_MEMORY_EXPAND, // vertices
    IPM_MEMORY_EXPAND, // elements
    &err);

  myRefine = IPM_FunctionCreate(IPM_FUNCTION_REPARTITION, kRefine, 3,
    IPM_MEMORY_EXPAND, // heat
    IPM_MEMORY_EXPAND, // vertices
    IPM_MEMORY_EXPAND, // elements
    &err);

  myContribute = IPM_FunctionCreate(IPM_FUNCTION_KERNEL, kContribute, 3,
    IPM_MEMORY_ADD, // rediation
    IPM_MEMORY_READ, // heat
    IPM_MEMORY_READ, // elements
    &err);

  myReduce = IPM_FunctionCreate(IPM_FUNCTION_KERNEL, kReduce, 2,
    IPM_MEMORY_ADD, // total
    IPM_MEMORY_READ, // radiation
    &err);

  // Initialize the mesh
  IPM_Launch(myCreate, heat, vertices, elements, &err);

  // Do multiple refinements
  for (int i = 0; i < niter; i++) {
    IPM_Launch(myRefine, heat, vertices, elements, &err);
    radiation = IPM_ArrayNumericClone("radiation", vertices, 1, 0.0, 1/*tempclone*/, &err);
    IPM_Launch(myContribute, radiation, heat, elements, &err);
    IPM_RArrayZero(total, &err);
    IPM_Launch(myReduce, total, radiation, &err);
    IPM_ArrayDestroy(radiation, &err);
  }

  // Draw the result
  IPM_Launch(myDraw, vertices, elements, &err);

  if (rank == 0) {
    IPM_Real *tot = IPMK_RArrayGet(total, &err);
    printf("Total radiation = %f\n", *tot);
  }

  // Destroy everthing
  IPM_ArrayDestroy(vertices, &err);
  IPM_ArrayDestroy(elements, &err);
  IPM_FunctionDestroy(myCreate, &err);
  IPM_FunctionDestroy(myRefine, &err);
  IPM_FunctionDestroy(myDraw, &err);

#if defined(HAVE_CAIRO)
  cairo_destroy(cr);
  cairo_surface_destroy (surface);
#endif

  IPM_Finalize(&err);
  return 0;
}

