/*
 Copyright (c) 2016 UChicago Argonne, LLC

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

/*
 * This example implements the libmesh example adapativity_ex2.C, which solves
 * a transient system with adaptive mesh refinement. The underlying equation is
 * the convection-diffusion equation. It uses the Crank-Nicolson semi-implicit method
 * to discretize the time. We use the unit diagonal term method to impose boundary
 * condition instead of the penalty method as in the original example.
 *
 * Usage:
 *  mpirun -n <nproc> ./prog  <# of init refinements> <# of time-steps> <# of refinements per time-step>
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>
#include <assert.h>
#include <ipm.h>
#include <mpi.h>
#include <float.h> // to get DECIMAL_DIG and print doubles in maximal precision
#include "utarray.h"
#include "uthash.h"

#ifdef HAVE_HPCTOOLKIT
#include <hpctoolkit.h>
#endif

#define USE_CSRCONTEXT

typedef struct {
  int nphi; // size of phi
  int nqp; // num. of quadrature points
  int ndim; // Dimensions, hardcoded to 2 in this example

  // These parts only depends on the reference element and are independant of the real element
  IPM_Real qp[24][2]; // qp[nqp][ndim]: coords of quadrature points. Max: 6 per edge, 4 edges
  IPM_Real qw[6]; // qw[nqp]: Gauss quadrature weights

  IPM_Real *phi; // phi[nphi][nqp]
  IPM_Real phi_storage[96]; // max: 4 phi's, 6 qpoints on a side (including parent & children), 4 sides

  IPM_Real *dphidxi, *dphideta;
  IPM_Real dphidxi_storage[96]; // dphi[nphi][nqp]
  IPM_Real dphideta_storage[96]; // dphi[nphi][nqp]

  int npsi;
  IPM_Real psi[2][6]; // psi[npsi][nqp]. Face EDGE2 has 2 shape functions and two quadrature points
  IPM_Real dpsidxi[2][6]; // dpsidxi[npsi][nqp]

  // This part is dependant on the real element
  IPM_Real xy[6][2]; // Actual coords of quadrature points on an EDGE2
  IPM_Real dphi[48]; // dphi[nphi][nqp][ndim]. dphi/dx, dphi/dy
  IPM_Real JxW[6]; // JxW[nqp]
  IPM_Real normals[6][2];
} FE;

// Return a[i][j] of a 1D array as if it is declared as a[isize][jsize]
#define VEC2(a, isize, jsize, i, j) ((a)[(jsize)*(i) + (j)])
// Return a[i][j][k] of a 1D array as if it is declared as a[isize][jsize][ksize]
#define VEC3(a, isize, jsize, ksize, i, j, k) ((a)[(jsize)*(ksize)*(i) + (ksize)*(j) + (k)])
#define DOT(a, b) (((a)[0])*((b)[0]) + ((a)[1])*((b)[1]))
#define SQ(x) ((x)*(x))

// Return distance between two points a and b
#define DIST(a, b) (sqrt(SQ(b[0]-a[0]) + SQ(b[1]-a[1])))

#define PHI(i, j) (VEC2(fe->phi, fe->nphi, fe->nqp, i, j))
#define DPHI(i, j, k) (VEC3(fe->dphi, fe->nphi, fe->nqp, fe->ndim, i, j, k))
#define DPHIDXI(i, j) (VEC2(fe->dphidxi, fe->nphi, fe->nqp, i, j))
#define DPHIDETA(i, j) (VEC2(fe->dphideta, fe->nphi, fe->nqp, i, j))


#if 0
#define vx 0
#define vy 0
#else
#define vx 0.8
#define vy 0.8
#endif

FE trife, quadfe, triface, quadface;
IPM_Real curtime;
const IPM_Real dt = 0.025;
const IPM_Real ksp_rtol = 1.0e-6;
const IPM_Real ksp_atol = 1.0e-50;
const int ksp_max_it = 5000;
const int ksp_gmres_restart = 30;

// Any element within 30% of the maximum error on any element will be refined,
// and any element within 7% of the minimum error on any element might be coarsened.
// In other words,
// error_delta = (error_max - error_min);
// refine_cutoff  = (1 - refine_fraction)*error_max;
// coarsen_cutoff = _coarsen_fraction*error_delta + error_min;

const IPM_Real refine_fraction = 0.7;
const IPM_Real coarsen_fraction = 0.07;

//const IPM_Real refine_fraction = 1; // uniformly refine
//const IPM_Real refine_fraction = -1; // no refine

//const IPM_Real coarsen_fraction = 2; // uniformly coarsen
//const IPM_Real coarsen_fraction = -1; // no coarsen

const IPM_Real diffusivity = 0.01;
const IPM_Real velocity[2] = {vx, vy};
int step;

char mesh_filename[128];

IPM_Real exact_solution (const IPM_Real x, const IPM_Real y, const IPM_Real t)
{
  const IPM_Real xo = 0.2;
  const IPM_Real yo = 0.2;
  const IPM_Real num = pow(x - vx*t - xo, 2.) + pow(y - vy*t - yo, 2.);

  const IPM_Real den = 0.01*(4.*t + 1.);
  return exp(-num/den)/(4.*t + 1.);
}

void exact_derivative(const IPM_Real x, const IPM_Real y, const IPM_Real t, IPM_Real *grad)
{
  const IPM_Real xo = 0.2;
  const IPM_Real yo = 0.2;

  IPM_Real uex = exact_solution(x, y, t);
  IPM_Real tmpx = -2*(x-vx*t-xo)/(0.01*(4*t+1));
  IPM_Real tmpy = -2*(y-vy*t-yo)/(0.01*(4*t+1));

  grad[0] = uex*tmpx;
  grad[1] = uex*tmpy;
}


// Compute the Givens rotation matrix parameters for a and b.
void rotmat(IPM_Real a, IPM_Real b, IPM_Real *c, IPM_Real *s)
{
  IPM_Real temp;
   if ( b == 0.0 ) {
      *c = 1.0;
      *s = 0.0;
  } else if (fabs(b) > fabs(a)) {
      temp = a/b;
      *s = 1.0 / sqrt(1.0 + SQ(temp));
      *c = temp * (*s);
   } else {
      temp = b / a;
      *c = 1.0 / sqrt(1.0 + SQ(temp));
      *s = temp * (*c);
    }
}

/**
 * Init the real element independant part of the FE data structure.
 */
void InitFEOnReference()
{
  FE *fe;

  /////////////////////////////////////////////////////////////////////////////
  // Init a triangle FE, including qpoints, weights, phi, dphi/dxi, dphi/deta
  /////////////////////////////////////////////////////////////////////////////
  fe = &trife;
  fe->nphi = 3;
  fe->nqp  = 4;
  fe->ndim = 2;

  // 4 quadrature points. See libmesh quadrature_gauss_2D.C for TRI3-THIRD
  fe->qp[0][0] = 1.5505102572168219018027159252941e-01L;
  fe->qp[0][0] = 1.5505102572168219018027159252941e-01L;
  fe->qp[0][1] = 1.7855872826361642311703513337422e-01L;
  fe->qp[1][0] = 6.4494897427831780981972840747059e-01L;
  fe->qp[1][1] = 7.5031110222608118177475598324603e-02L;
  fe->qp[2][0] = 1.5505102572168219018027159252941e-01L;
  fe->qp[2][1] = 6.6639024601470138670269327409637e-01L;
  fe->qp[3][0] = 6.4494897427831780981972840747059e-01L;
  fe->qp[3][1] = 2.8001991549907407200279599420481e-01L;

  fe->qw[0] = 1.5902069087198858469718450103758e-01L;
  fe->qw[1] = 9.0979309128011415302815498962418e-02L;
  fe->qw[2] = 1.5902069087198858469718450103758e-01L;
  fe->qw[3] = 9.0979309128011415302815498962418e-02L;

  // phi[nphi][nqp] = phi[3][4] = (1-xi-eta,  xi,  eta)[4]
  //                       | -1  -1 |
  //<dphi/dxi, dphi/deta>= |  1   0 |
  //                       |  0   1 |
  //
  // dphi/dxi[nphi][nqp][ndim] and dphi/deta[nphi][nqp][ndim]
  fe->phi = fe->phi_storage;
  fe->dphidxi = fe->dphidxi_storage;
  fe->dphideta = fe->dphideta_storage;
  for (int j = 0; j < fe->nqp; j++) {
    PHI(0, j) = 1 - fe->qp[j][0] - fe->qp[j][1];
    PHI(1, j) = fe->qp[j][0];
    PHI(2, j) = fe->qp[j][1];

    DPHIDXI(0, j) = -1;
    DPHIDXI(1, j) = 1;
    DPHIDXI(2, j) = 0;

    DPHIDETA(0, j) = -1;
    DPHIDETA(1, j) = 0;
    DPHIDETA(2, j) = 1;
  }

  /////////////////////////////////////////////////////////////////////////////
  // Init a face FE of triangles
  /////////////////////////////////////////////////////////////////////////////

  //  |\
  //  | \
  //  4  3
  //  |   \
  //  5    2
  //  |_0__1\

  fe = &triface;
  fe->nphi = 3;
  fe->nqp  = 6;
  fe->ndim = 2;
  fe->npsi = 2;

  IPM_Real xi[6];
  xi[0] = -sqrt(3)/3; // qpoints on parent edge
  xi[1] = sqrt(3)/3;
  xi[2] = (-3-sqrt(3))/6; // qpoints on 1st child edge
  xi[3] = (-3+sqrt(3))/6;
  xi[4] = (3-sqrt(3))/6; // qpoints on 2nd child edge
  xi[5] = (3+sqrt(3))/6;

  // Evaluate face shape functions (1-xi)/2, (1+xi)/2 at quadrature points
  for (int j = 0; j < fe->nqp; j++) {
    fe->psi[0][j] = (1-xi[j])/2; //1st shape function
    fe->psi[1][j] = (1+xi[j])/2; //2nd shape function
  }

  // Evaluate face shape derivatives -1/2, 1/2 at quadrature points
  for (int j = 0; j < fe->nqp; j++) {
    fe->dpsidxi[0][j] = -1/2.0; //1st shape function
    fe->dpsidxi[1][j] = 1/2.0; //2nd shape function
  }

  for (int j = 0; j < fe->nqp; j++) fe->qw[j] = 1.0;

  // Evaluate element shape functions {1-xi-eta, xi, eta} at quadrature points
  // on each edge of the element
  for (int k = 0; k < 3 /*edges*/; k++) {
    int base = fe->nqp*k;
    const IPM_Real v[3][2] = {{.0, .0}, {1.0, .0}, {.0, 1.0}};
    IPM_Real *ends[2];

    // Get two ends on this reference edge
    ends[0] = v[k];
    ends[1] = v[(k+1)%3];

    // Get reference quadrature points on this edge using fe->psi
    // x(xi) = sum { psi_i(xi)*x_i for i}
    for (int j = 0; j < fe->nqp; j++) {
      int curqp = base + j;
      fe->qp[curqp][0] = fe->qp[curqp][1] = .0;
      for (int i = 0; i < fe->npsi; i++) {
        fe->qp[curqp][0] += fe->psi[i][j]*ends[i][0]; // x of the j-th qpoint on this edge
        fe->qp[curqp][1] += fe->psi[i][j]*ends[i][1]; // y of the j-th qpoint on this edge
      }
    }

    // Compute elemental shape functions and derivatives on edge k at qpoints
    fe->phi = &fe->phi_storage[fe->nphi*fe->nqp*k]; // Move to storage for edge k
    fe->dphidxi = &fe->dphidxi_storage[fe->nphi*fe->nqp*k];
    fe->dphideta = &fe->dphideta_storage[fe->nphi*fe->nqp*k];
    for (int j = 0; j < fe->nqp; j++) {
      int curqp = base + j;
      PHI(0, j) = 1 - fe->qp[curqp][0] - fe->qp[curqp][1];
      PHI(1, j) = fe->qp[curqp][0];
      PHI(2, j) = fe->qp[curqp][1];

      DPHIDXI(0, j) = -1;
      DPHIDXI(1, j) = 1;
      DPHIDXI(2, j) = 0;

      DPHIDETA(0, j) = -1;
      DPHIDETA(1, j) = 0;
      DPHIDETA(2, j) = 1;
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // Init quadfe
  /////////////////////////////////////////////////////////////////////////////
  fe = &quadfe;
  fe->nphi = 4;
  fe->nqp  = 4;
  fe->ndim = 2;

  // Four quadrature points as a tensor product of 1D (-sqrt(3)/3, sqrt(3)/3)
  fe->qp[0][0] = -sqrt(3)/3; fe->qp[0][1] = -sqrt(3)/3;
  fe->qp[1][0] =  sqrt(3)/3; fe->qp[1][1] = -sqrt(3)/3;
  fe->qp[2][0] = -sqrt(3)/3; fe->qp[2][1] =  sqrt(3)/3;
  fe->qp[3][0] =  sqrt(3)/3; fe->qp[3][1] =  sqrt(3)/3;

  for (int i = 0; i < fe->nqp; i++) fe->qw[i] = 1.0;

  // phi[nphi][nqp] = phi[4][4] = 1/4.0* {(1-xi)(1-eta); (1+xi)(1-eta); (1+xi)(1+eta); (1-xi)(1+eta)}[4]
  //                             | -1+eta  -1+xi |
  // <dphi/dxi, dphi/deta>= 1/4 *|  1-eta  -1-xi |
  //                             |  1+eta   1+xi |
  //                             | -1-eta   1-xi |
  //
  // dphi[nphi][nqp][ndim], i.e., dphi[4]4][2]
  fe->phi = fe->phi_storage;
  fe->dphidxi = fe->dphidxi_storage;
  fe->dphideta = fe->dphideta_storage;
  for (int j = 0; j < fe->nqp; j++) {
    PHI(0, j) = 1/4.0*(1 - fe->qp[j][0])*(1 - fe->qp[j][1]);
    PHI(1, j) = 1/4.0*(1 + fe->qp[j][0])*(1 - fe->qp[j][1]);
    PHI(2, j) = 1/4.0*(1 + fe->qp[j][0])*(1 + fe->qp[j][1]);
    PHI(3, j) = 1/4.0*(1 - fe->qp[j][0])*(1 + fe->qp[j][1]);

    DPHIDXI(0, j) = (-1 + fe->qp[j][1])/4.0;
    DPHIDXI(1, j) = ( 1 - fe->qp[j][1])/4.0;
    DPHIDXI(2, j) = ( 1 + fe->qp[j][1])/4.0;
    DPHIDXI(3, j) = (-1 - fe->qp[j][1])/4.0;

    DPHIDETA(0, j) = (-1 + fe->qp[j][0])/4.0;
    DPHIDETA(1, j) = (-1 - fe->qp[j][0])/4.0;
    DPHIDETA(2, j) = ( 1 + fe->qp[j][0])/4.0;
    DPHIDETA(3, j) = ( 1 - fe->qp[j][0])/4.0;
  }

  /////////////////////////////////////////////////////////////////////////////
  // Init quadface
  /////////////////////////////////////////////////////////////////////////////
  fe = &quadface;

  fe->nphi = 4;
  fe->nqp  = 6; // parent edge and its two child edges, have 6 qpoints in total
  fe->ndim = 2;
  fe->npsi = 2;

  // Use the same 6 qpoints as in triface
  // xi[0] = ...; xi[1] = ...

  // Evaluate the two face shape functions (1-xi)/2, (1+xi)/2 at qpoints
  for (int j = 0; j < fe->nqp; j++) {
    fe->psi[0][j] = (1-xi[j])/2; //1st shape function
    fe->psi[1][j] = (1+xi[j])/2; //2nd shape function
  }

  // Evaluate the two face shape derivatives -1/2, 1/2 at quadrature points
  for (int j = 0; j < fe->nqp; j++) {
    fe->dpsidxi[0][j] = -1/2.0; //1st shape function
    fe->dpsidxi[1][j] = 1/2.0; //2nd shape function
  }

  // Set weights for quadrature points
  for (int j = 0; j < fe->nqp; j++) fe->qw[j] = 1.0;

  // Evaluate element shape functions 1/4.0* {(1-xi)(1-eta); (1+xi)(1-eta);
  // (1+xi)(1+eta); (1-xi)(1+eta)} at quadrature points of each side of the element
  for (int k = 0; k < 4 /*edges*/; k++) {
    int base = fe->nqp*k;
    IPM_Real v[4][2] = {{-1.0, -1.0}, {1.0, -1.0}, {1.0, 1.0}, {-1.0, 1.0}};
    IPM_Real *ends[2];

    // Get two ends of this reference edge
    ends[0] = v[k];
    ends[1] = v[(k+1)%4];

    // Get reference quadrature points on this edge using fe->psi
    // x(xi) = sum { psi_i(xi)*x_i for i}
    for (int j = 0; j < fe->nqp; j++) {
      int curqp = base + j;
      fe->qp[curqp][0] = fe->qp[curqp][1] = .0;
      for (int i = 0; i < fe->npsi; i++) {
        fe->qp[curqp][0] += fe->psi[i][j]*ends[i][0]; // x of the j-th qpoint on this edge
        fe->qp[curqp][1] += fe->psi[i][j]*ends[i][1]; // y of the j-th qpoint on this edge
      }
    }

    // Now we can compute element shape functions at qpoints on edge k
    fe->phi = &fe->phi_storage[fe->nphi*fe->nqp*k]; // Move to storage for edge k
    fe->dphidxi = &fe->dphidxi_storage[fe->nphi*fe->nqp*k];
    fe->dphideta = &fe->dphideta_storage[fe->nphi*fe->nqp*k];
    for (int j = 0; j < fe->nqp; j++) {
      int curqp = base + j;
      PHI(0, j) = 1/4.0*(1 - fe->qp[curqp][0])*(1 - fe->qp[curqp][1]);
      PHI(1, j) = 1/4.0*(1 + fe->qp[curqp][0])*(1 - fe->qp[curqp][1]);
      PHI(2, j) = 1/4.0*(1 + fe->qp[curqp][0])*(1 + fe->qp[curqp][1]);
      PHI(3, j) = 1/4.0*(1 - fe->qp[curqp][0])*(1 + fe->qp[curqp][1]);

      DPHIDXI(0, j) = (-1 + fe->qp[curqp][1])/4.0;
      DPHIDXI(1, j) = ( 1 - fe->qp[curqp][1])/4.0;
      DPHIDXI(2, j) = ( 1 + fe->qp[curqp][1])/4.0;
      DPHIDXI(3, j) = (-1 - fe->qp[curqp][1])/4.0;

      DPHIDETA(0, j) = (-1 + fe->qp[curqp][0])/4.0;
      DPHIDETA(1, j) = (-1 - fe->qp[curqp][0])/4.0;
      DPHIDETA(2, j) = ( 1 + fe->qp[curqp][0])/4.0;
      DPHIDETA(3, j) = ( 1 - fe->qp[curqp][0])/4.0;
    }
  }

}

/**
 * Init JxW, dphi/dx and dphi/dy at quadrature points of a triangle with vertices v[0~3]
 */
void InitFEOnReal(FE* fe, const IPM_Real **v)
{
  // <dphi/dxi>  = 1/4*(-1+eta, 1-eta, 1+eta, -1-eta)
  // <dphi/deta> = 1/4*(-1+xi, -1-xi,  1+xi,   1-xi)
  //
  // dx/dxi  = <dphi/dxi> * (x1, x2, x3, x4)
  // dy/dxi  = <dphi/dxi> * (y1, y2, y3, y4)

  // dx/deta = <dphi/deta>* (x1, x2, x3, x4)
  // dy/deta = <dphi/deta>* (y1, y2, y3, y4)

  // J = | dx/dxi  dx/deta |
  //     | dy/dxi  dy/deta |
  //
  // det(Jacobian) = dx/dxi * dy/deta - dx/deta * dy/dxi

  //
  //     |dxi/dx   dxi/dy|                  | dy/deta -dx/deta|
  // j = |               | = J^-1 = 1/det(J)|                 |
  //     |deta/dx deta/dy|                  |-dy/dxi   dx/dxi |
  IPM_Real dx_dxi, dy_dxi, dx_deta, dy_deta;
  IPM_Real dxi_dx, dxi_dy, deta_dx, deta_dy;
  IPM_Real det, det_inv;

  for (int j = 0; j < fe->nqp; j++) {
    dx_dxi = dy_dxi = dx_deta = dy_deta = 0.0;
    fe->xy[j][0] = fe->xy[j][1] = .0;
    for (int i = 0; i < fe->nphi; i++) {
      dx_dxi  += DPHIDXI(i, j)*v[i][0];
      dy_dxi  += DPHIDXI(i, j)*v[i][1];
      dx_deta += DPHIDETA(i, j)*v[i][0];
      dy_deta += DPHIDETA(i, j)*v[i][1];
      fe->xy[j][0] += PHI(i, j)*v[i][0];
      fe->xy[j][1] += PHI(i, j)*v[i][1];
    }

    det = dx_dxi * dy_deta - dx_deta * dy_dxi; assert(det > 0.0);
    det_inv = 1.0/det;

    dxi_dx  =  det_inv*dy_deta;
    dxi_dy  = -det_inv*dx_deta;
    deta_dx = -det_inv*dy_dxi;
    deta_dy =  det_inv*dx_dxi;

    fe->JxW[j] = det*fe->qw[j];
    for (int i = 0; i < fe->nphi; i++) {
      // dphi/dx = dphi/dxi*dxi/dx + dphi/deta*deta/dx
      DPHI(i, j, 0) = DPHIDXI(i, j)*dxi_dx + DPHIDETA(i, j)*deta_dx;
      // dphi/dy = dphi/dxi*dxi/dy + dphi/deta*deta/dy
      DPHI(i, j, 1) = DPHIDXI(i, j)*dxi_dy + DPHIDETA(i, j)*deta_dy;
    }
  }
}

/**
 * Init real element dependant part of a face FE of edge \p side of a triangle given by vertices v[0~2].
 * Here v[i] is a pointer to coordinates of vertex i, in other words, the x, y coordinates are v[i][0], v[i][1] resp.
 * This FE is initialized for purpose of estimating errors using Kelly flux jumps.
 */
void InitFaceKelly(FE *fe, int num_verts, const IPM_Real **v, int side, int pcnum)
{
  const int base = pcnum*2; // base can be 0, 2, 4
  IPM_Real ends[2][2];

  ends[0][0] = v[side][0]; // x
  ends[0][1] = v[side][1];  // y
  ends[1][0] = v[(side+1)%num_verts][0]; // x
  ends[1][1] = v[(side+1)%num_verts][1]; // y

  for (int k = 0; k < fe->ndim; k++) {
    // First qpoint on this edge (parent or child)
    fe->xy[base][k] = fe->psi[0][base]*ends[0][k] + fe->psi[1][base]*ends[1][k];

    // Second qpoint on this edge (parent or child)
    fe->xy[base+1][k] = fe->psi[0][base+1]*ends[0][k] + fe->psi[1][base+1]*ends[1][k];
  }

  // Compute JxW on this side, which is an EDGE2 element
  // phi = <(1-xi)/2  (1+xi)/2>
  // dphi/dxi = <-0.5, 0.5>
  // So, given an edge (p, q), the 1x1 Jacobian matrix is (q-p)/2
  for (int j = base; j < base+2; j++) {
    IPM_Real scale = (pcnum == 0? 1.0 : 0.5);
    fe->JxW[j] = sqrt(SQ(ends[1][0]-ends[0][0]) + SQ(ends[1][1]-ends[0][1]))/2.0*scale*fe->qw[j];
  }

  // Select the right phi's for this edge
  fe->phi = &fe->phi_storage[fe->nphi*fe->nqp*side];
  fe->dphidxi = &fe->dphidxi_storage[fe->nphi*fe->nqp*side];
  fe->dphideta = &fe->dphideta_storage[fe->nphi*fe->nqp*side];

  // Compute dphi/dx dphi/dy
  IPM_Real dx_dxi, dy_dxi, dx_deta, dy_deta;
  IPM_Real dxi_dx, dxi_dy, deta_dx, deta_dy;
  IPM_Real det, det_inv;

  for (int j = base; j < base+2; j++) {
    dx_dxi = dy_dxi = dx_deta = dy_deta = 0.0;
    for (int i = 0; i < fe->nphi; i++) {
        dx_dxi  += DPHIDXI(i, j)*v[i][0];
        dy_dxi  += DPHIDXI(i, j)*v[i][1];
        dx_deta += DPHIDETA(i, j)*v[i][0];
        dy_deta += DPHIDETA(i, j)*v[i][1];
    }

    det = dx_dxi * dy_deta - dx_deta * dy_dxi; assert(det > 0.0);
    det_inv = 1.0/det;

    dxi_dx  =  det_inv*dy_deta;
    dxi_dy  = -det_inv*dx_deta;
    deta_dx = -det_inv*dy_dxi;
    deta_dy =  det_inv*dx_dxi;

    for (int i = 0; i < fe->nphi; i++) {
      // dphi/dx = dphi/dxi*dxi/dx + dphi/deta*deta/dx
      DPHI(i, j, 0) = DPHIDXI(i, j)*dxi_dx + DPHIDETA(i, j)*deta_dx;
      // dphi/dy = dphi/dxi*dxi/dy + dphi/deta*deta/dy
      DPHI(i, j, 1) = DPHIDXI(i, j)*dxi_dy + DPHIDETA(i, j)*deta_dy;
    }
  }

  // Compute normals on the quadrature points
  for (int j = base; j < base+2; j++) {
    dx_dxi = dy_dxi = 0.0;
    for (int i = 0; i < fe->npsi; i++) {
      dx_dxi += fe->dpsidxi[i][j]*ends[i][0];
      dy_dxi += fe->dpsidxi[i][j]*ends[i][1];
    }

    IPM_Real r = sqrt(SQ(dx_dxi) + SQ(dy_dxi));
    fe->normals[j][0] = dy_dxi/r;
    fe->normals[j][1] = -dx_dxi/r;
  }
}


void ArrayNumericPrint(IPM_ArrayNumeric u)
{
  int err;
  IPMK_ArrayVisibleIter iter;
  for (IPMK_ArrayVisibleIterInit(u, &iter); !IPMK_ArrayVisibleIterDone(&iter); IPMK_ArrayVisibleIterNext(&iter)) {
    IPM_Index i = IPMK_ArrayVisibleIterToIndex(&iter); // index of the edge
    printf("%d: %20.6e\n", i, *IPMK_ArrayNumericGet(u, i, &err));
  }
}

void VTKOutput(char *suffix, IPMK_ArrayNumeric u, IPMK_ArrayNumeric vertices,IPMK_ArrayIndex edges,
  IPMK_ArrayIndex elements, IPMK_ArrayIndex elemerrs, IPMK_ArrayIndex activeelems, IPMK_Error *err)
{
  IPMK_ArrayLocalIter liter;
  IPMK_ArrayVisibleIter viter;

  // VTK Header
  FILE *vtkfile;
  char filename[128];
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  snprintf(filename,128,"p%02d-%s", rank, suffix);

  vtkfile = fopen(filename, "w");
  fprintf(vtkfile,
  "# vtk DataFile Version 3.0\n"
  "This file was generated by the IPM library\n"
  "ASCII\n"
  "DATASET UNSTRUCTURED_GRID\n\n");

  // POINTS
  int n_visible_vertices = IPM_ArrayVisibleSize(vertices, err);
  fprintf(vtkfile, "POINTS %d FLOAT\n", n_visible_vertices);
  for (IPMK_ArrayVisibleIterInit(vertices, &viter); !IPMK_ArrayVisibleIterDone(&viter); IPMK_ArrayVisibleIterNext(&viter)) {
    IPM_Index i = IPMK_ArrayVisibleIterToIndex(&viter);
    IPM_Real *xy = IPMK_ArrayNumericGet(vertices, i, err); // edges of the element
    IPM_Index gi = IPM_ArrayGetGid(vertices, i, err);
    fprintf(vtkfile, "%f %f 0\n", xy[0], xy[1]);
  }
  fprintf(vtkfile, "\n");

  // CELLS
  int n_visible_activeelems = IPM_ArrayVisibleSize(activeelems, err);
  int size = 0;

  // CELL TYPES
  fprintf(vtkfile, "CELL_TYPES %d\n", n_visible_activeelems);
  for (IPMK_ArrayVisibleIterInit(activeelems, &viter); !IPMK_ArrayVisibleIterDone(&viter); IPMK_ArrayVisibleIterNext(&viter)) {
    IPM_Index ep = IPMK_ArrayVisibleIterToIndex(&viter); // e' is index to activeelems
    IPM_Index e = *IPMK_ArrayIndexGet(activeelems, ep, err); // value at e' is index to elements
    int num_edges = IPM_ArrayGetLength(elements, e, err);
    size += num_edges;
    if (num_edges == 3) fprintf(vtkfile, " 5");
    else if (num_edges == 4) fprintf(vtkfile, " 9");
    else abort();
  }

   fprintf(vtkfile, "\n\n");

  fprintf(vtkfile, "CELLS %d %d\n", n_visible_activeelems, n_visible_activeelems+size);
  for (IPMK_ArrayVisibleIterInit(activeelems, &viter); !IPMK_ArrayVisibleIterDone(&viter); IPMK_ArrayVisibleIterNext(&viter)) {
    IPM_Index ep = IPMK_ArrayVisibleIterToIndex(&viter); // e' is index to activeelems
    IPM_Index e = *IPMK_ArrayIndexGet(activeelems, ep, err); // value at e' is index to elements
    int num_edges = IPM_ArrayGetLength(elements, e, err);
    const IPMK_Index *myedges = IPMK_ArrayIndexGet(elements, e, err);
    IPMK_Index vertidx[4] = {IPM_INDEX_NULL}; // Vertices of the element in a proper order
    IPMK_Index edgevert[8]; // Store vertices of edge 0~2 or 0~3
    int onbdary[4] = {0}; // Is this vertex on boundary?

    const int num_verts = num_edges; // True for triangles and quads

    assert(num_edges == 3 || num_edges == 4);

    // Store all vertices in a temp array to facilitate later accessing
    for (int i = 0; i < num_edges; i++) {
      IPMK_Index *ends = IPMK_ArrayIndexGet(edges, myedges[i], err);
      edgevert[2*i] = ends[0];
      edgevert[2*i+1] = ends[1];
    }

    // Go over vertices of the element and order them in a ring
    vertidx[0] = edgevert[0];
    vertidx[1] = edgevert[1];

    // If edge 0 is opposite to the direction of the element, we need to swap vertidx[0] and vertidx[1]
    if (edgevert[2] != vertidx[1] && edgevert[3] != vertidx[1]) {
      // It implies edge 1 must be connected to the tail of edge 0, i.e., verts[0]!
      IPMK_Index tmp = vertidx[0];
      vertidx[0] = vertidx[1];
      vertidx[1] = tmp;
    }

    // Found other vertices in the direction of vertidx[0]->vertidx[1]
    for (int i = 1; i < num_verts - 1; i++) {
      if (edgevert[2*i] != vertidx[i]) {
        vertidx[i+1] = edgevert[2*i]; assert(edgevert[2*i+1] == vertidx[i]);
      } else {
        vertidx[i+1] = edgevert[2*i+1]; assert(edgevert[2*i] == vertidx[i]);
      }
    }

    fprintf(vtkfile, "%d", num_verts);

    for (int i = 0; i < num_verts; i++) {
      fprintf(vtkfile, " %d", vertidx[i]); // GMV uses 1-based indices
    }
    fprintf(vtkfile, "\n");
  }
  fprintf(vtkfile, "\n");


  // POINT DATA
  fprintf(vtkfile, "POINT_DATA %d\n", n_visible_vertices);
  fprintf(vtkfile, "SCALARS u DOUBLE\n");
  fprintf(vtkfile, "LOOKUP_TABLE default\n");
  for (IPMK_ArrayVisibleIterInit(vertices, &viter); !IPMK_ArrayVisibleIterDone(&viter); IPMK_ArrayVisibleIterNext(&viter)) {
    IPM_Index i = IPMK_ArrayVisibleIterToIndex(&viter);
    IPM_Real val = *IPMK_ArrayNumericGet(u, i, err); // edges of the element
    fprintf(vtkfile, "%e\n", val);
  }
  fprintf(vtkfile, "\n");

  // CELL DATA
  fprintf(vtkfile, "CELL_DATA %d\n", n_visible_activeelems);

  // Alternative cell ids
  fprintf(vtkfile, "\n");
  fprintf(vtkfile, "SCALARS element_lids INT\n");
  fprintf(vtkfile, "LOOKUP_TABLE default\n");

  for (IPMK_ArrayVisibleIterInit(activeelems, &viter); !IPMK_ArrayVisibleIterDone(&viter); IPMK_ArrayVisibleIterNext(&viter)) {
    IPM_Index ep = IPMK_ArrayVisibleIterToIndex(&viter); // e' is index to activeelems
    IPM_Index e = *IPMK_ArrayIndexGet(activeelems, ep, err); // value at e' is index to elements
    fprintf(vtkfile, "%d\n", e); // GMV uses 1-based indices
  }

  // Element errors
#if 0
  fprintf(vtkfile, "SCALARS elemerrs double\n");
  fprintf(vtkfile, "LOOKUP_TABLE default\n");
  if (elemerrs) {
    for (IPMK_ArrayVisibleIterInit(elemerrs, &viter); !IPMK_ArrayVisibleIterDone(&viter); IPMK_ArrayVisibleIterNext(&viter)) {
      IPM_Index i = IPMK_ArrayVisibleIterToIndex(&viter);
      IPM_Real val = *IPMK_ArrayNumericGet(elemerrs, i, err); // edges of the element
      fprintf(vtkfile, " %e\n", val);
    }
  } else {
    for (IPMK_ArrayVisibleIterInit(activeelems, &viter); !IPMK_ArrayVisibleIterDone(&viter); IPMK_ArrayVisibleIterNext(&viter)) {
      fprintf(vtkfile, "0\n"); // GMV uses 1-based indices
    }
  }
#endif

  fprintf(vtkfile, "\n\n");
  fclose(vtkfile);
}

void PrintElements(IPM_ArrayIndex edges, IPM_ArrayIndex elements)
{
  IPMK_ArrayLocalIter iter;
  int err;
  for (IPMK_ArrayLocalIterInit(elements, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPM_Index e = IPMK_ArrayLocalIterToIndex(&iter);
    int num_edges = IPM_ArrayGetLength(elements, e, &err);
    const IPMK_Index *myedges = IPMK_ArrayIndexGet(elements, e, &err); // edges of the element
    printf("Element %3d :", e);
    for (int i = 0; i < num_edges; i++) {
      const IPMK_Index *ends = IPMK_ArrayIndexGet(edges, myedges[i], &err);
      printf(" (%d, %d)", ends[0], ends[1]);
    }
    printf("\n");
  }
}

/**
 * The kernel dump's the matrix A in Matrix Market format.
 * Also dump vector x and y. Here y = Ax.
*/
void kDumpAxy(IPMK_ArrayNumeric Kvert, IPMK_ArrayNumeric Kedge, IPMK_ArrayNumeric Kelem,
  IPMK_ArrayNumeric vertices, IPMK_ArrayIndex edges, IPMK_ArrayIndex elements,
  IPMK_ArrayNumeric x, IPM_ArrayNumeric y, IPMK_Error *err)
{
  FILE *pfile;
  char name[128] = {0};
  sprintf(name, "K%d.txt", grank);
  pfile = fopen(name, "w");

  int m, n, nnz;
  m = n = IPM_ArrayTotalSize(vertices, err);
  nnz = IPM_ArrayTotalSize(edges, err)*2 + m + 8; // Kedge, Kvert plus Kelem

  //Print out A
  if (!grank) { // rank 0 prints MatrixMarket banner
    fprintf(pfile, "%%%%MatrixMarket matrix coordinate real general\n");
    fprintf(pfile, "%8d %8d %8d\n", m, n, nnz);
  }

  int digs = DECIMAL_DIG;

  // Print matrix entries living on edges
  IPMK_ArrayLocalIter iter;
  for (IPMK_ArrayLocalIterInit(edges, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPMK_Index e = IPMK_ArrayLocalIterToIndex(&iter); // index of the edge
    const IPMK_Index *ends = IPMK_ArrayIndexGet(edges, e, err); // indices of vertices of the edge
    IPM_Index i = ends[0];
    IPM_Index j = ends[1];
    IPM_Index gi = IPM_ArrayGetGid(vertices, i, err) + 1; // MatrixMarket uses 1-based indices
    IPM_Index gj = IPM_ArrayGetGid(vertices, j, err) + 1;
    IPM_Real *fp = IPMK_ArrayNumericGet(Kedge, e, err);
    IPM_Real aij = fp[0];
    IPM_Real aji = fp[1];

    fprintf(pfile, "%8d %8d   %.*e\n", gi, gj, digs, aij);
    fprintf(pfile, "%8d %8d   %.*e\n", gj, gi, digs, aji);
  }

  // Print matrix entries living on vertices
  for (IPMK_ArrayLocalIterInit(vertices, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPMK_Index i = IPMK_ArrayLocalIterToIndex(&iter); // index of the edge
    IPM_Index gi = IPM_ArrayGetGid(vertices, i, err)+ 1; // MatrixMarket uses 1-based indices
    IPM_Real aii = *IPMK_ArrayNumericGet(Kvert, i, err);
    fprintf(pfile, "%8d %8d   %.*e\n", gi, gi, digs, aii);
  }

  // Print matrix entries living on quadrilateral elements (this is a boring part)
  for (IPMK_ArrayLocalIterInit(elements, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPM_Index e = IPMK_ArrayLocalIterToIndex(&iter);
    const int num_edges = IPM_ArrayGetLength(elements, e, err);

    assert(num_edges == 3 || num_edges == 4);
    if (num_edges == 3) continue; // Only quadrilaterals have internal edges

    const IPMK_Index *edgeidx = IPMK_ArrayIndexGet(elements, e, err);
    IPMK_Index vertidx[4] = {IPM_INDEX_NULL}; // Vertices of the element in a proper order
    IPMK_Index edgevert[8]; // Store vertices of edge 0~2 or 0~3
    const int num_verts = num_edges; // True for triangles and quads

    // Store all vertices in a temp array to facilitate later accessing
    for (int i = 0; i < num_edges; i++) {
      IPMK_Index *ends = IPMK_ArrayIndexGet(edges, edgeidx[i], err);
      edgevert[2*i] = ends[0];
      edgevert[2*i+1] = ends[1];
    }

    // Go over vertices of the element and order them in a ring (significant for quadrilaterals).
    vertidx[0] = edgevert[0];
    vertidx[1] = edgevert[1];

    // If edge 0 is opposite to the direction of the element, we need to swap vertidx[0] and vertidx[1]
    if (edgevert[2] != vertidx[1] && edgevert[3] != vertidx[1]) {
      // It implies edge 1 must be connected to the tail of edge 0, i.e., verts[0]!
      IPMK_Index tmp = vertidx[0];
      vertidx[0] = vertidx[1];
      vertidx[1] = tmp;
    }

    // Found other vertices in the direction of vertidx[0]->vertidx[1]
    for (int i = 1; i < num_verts - 1; i++) {
      if (edgevert[2*i] != vertidx[i]) {
        vertidx[i+1] = edgevert[2*i]; assert(edgevert[2*i+1] == vertidx[i]);
      } else {
        vertidx[i+1] = edgevert[2*i+1]; assert(edgevert[2*i] == vertidx[i]);
      }
    }

    // Do contribution on internal edges
    IPM_Real *ke = IPMK_ArrayNumericGet(Kelem, e, err);
    for (int i = 0; i < num_verts; i++) {
      int j = (i + 2)%4;
      IPM_Index gi = IPM_ArrayGetGid(vertices, vertidx[i], err) + 1; // MatrixMarket uses 1-based indices
      IPM_Index gj = IPM_ArrayGetGid(vertices, vertidx[j], err) + 1;
      fprintf(pfile, "%8d %8d   %.*e\n", gi, gj, digs, ke[i]);
    }
  }
  fclose(pfile);

  // Print out x
  name[0] = 0;
  sprintf(name, "x%d.txt", grank);
  pfile = fopen(name, "w");
  for (IPMK_ArrayLocalIterInit(x, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPMK_Index i = IPMK_ArrayLocalIterToIndex(&iter); // 0-based index
    IPM_Index gi = IPM_ArrayGetGid(x, i, err);
    IPM_Real xi = *IPMK_ArrayNumericGet(x, i, err);
    fprintf(pfile, "%8d   %.*e\n", gi, digs, xi);
  }
  fclose(pfile);

  // Print out y
  name[0] = 0;
  sprintf(name, "y%d.txt", grank);
  pfile = fopen(name, "w");
  for (IPMK_ArrayLocalIterInit(y, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPMK_Index i = IPMK_ArrayLocalIterToIndex(&iter); // 0-based index
    IPM_Index gi = IPM_ArrayGetGid(y, i, err);
    IPM_Real yi = *IPMK_ArrayNumericGet(y, i, err);
    fprintf(pfile, "%8d   %.*e\n", gi, digs, yi);
  }
  fclose(pfile);
}

/**
 * This kernel is called on a completely empty set of IPM objects.
 * It creates a mesh with 10 elements, 20 edges and 11 vertices.
 * Note that the elements must be in counterclockwise orientation.
 *
 *
 * \verbatim
 *
 *    Vertex view           Edge view             Element view
 *
 *              (2,2)
 *   8-----6-----5         +--0--+--1--+          +-----+-----+
 *   |\   /|     |         |\   /|     |          |\ 3 /|     |
 *   | \ / |     |         | 3 5 |     |          | \ / |     |
 *   |  7  |     |         2  X  7     8          |2 X 4|  1  |
 *   | / \ |     |         | 4 6 |     |          | / \ |     |
 *   |/   \|     |         |/   \|     |          |/ 5 \|     |
 *   3-----2-----4         +--9--+--10-+          +-----+-----+
 *   |     |\   /|         |     |\   /|          |     |\ 7 /|
 *   |     | \ / |         |     |13 15|          |     | \ / |
 *   |     |  9  |        11    12  X  17         |  0  |6 X 8|
 *   |     | / \ |         |     |14 16|          |     | / \ |
 *   |     |/   \|         |     |/   \|          |     |/ 9 \|
 *   0-----1-----10        +--18-+--19-+          +-----+-----+
 *  (0,0)
 *
 *
 * \endverbatim
 */

void kCreate(IPMK_ArrayNumeric vboundary, IPMK_ArrayNumeric vertices,
  IPMK_ArrayNumeric edgelevels, IPMK_ArrayIndex childedges, IPMK_ArrayIndex edges,
  IPMK_ArrayIndex parentelems, IPMK_ArrayIndex elements,
  IPMK_ArrayIndex activeelems, IPMK_Error *err)
{
  if (IPM_ArrayTotalSize(vertices, err) > 0) IPMK_SetErrorVoid(IPMK_ERROR_WRONG_STATE);
  if (IPM_ArrayTotalSize(edges, err) > 0) IPMK_SetErrorVoid(IPMK_ERROR_WRONG_STATE);
  if (IPM_ArrayTotalSize(elements, err) > 0) IPMK_SetErrorVoid(IPMK_ERROR_WRONG_STATE);

#if 1 // The default input mesh used by the libmesh example
  IPM_Real xy[11][2] = {{0, 0}, {1, 0}, {1, 1}, // 0, 1, 2
                        {0, 1},                 // 3
                        {2, 1}, {2, 2}, {1, 2}, // 4, 5, 6
                        {0.5, 1.5},             // 7
                        {0, 2}, {1.5, 0.5}, {2, 0}};// 8, 9, 10

  IPM_Real vboundarytags[11] = {1, 1, 0, 1, // 0, 1, 2, 3
                                1, 1, 1, 0, // 4, 5, 6, 7
                                1, 0, 1}; // 8, 9, 10

  int edgeid[20][2] = {{8, 6}, {6, 5}, // 0, 1
                       {8, 3}, {8, 7}, {3, 7}, {6, 7}, {2, 7}, {6, 2}, {4, 5}, // 2, 3, 4, 5, 6, 7, 8
                       {3, 2}, {2, 4}, // 9, 10
                       {3, 0}, {1, 2}, {2, 9}, {1, 9}, {4, 9}, {10, 9}, {4, 10}, // 11, 12, 13, 14, 15, 16, 17
                       {0, 1}, {1, 10}}; // 18, 19

  int quadid[2][4] = {{18, 12, 9, 11}, {10, 8, 1, 7}}; // 0, 1

  int triangleid[8][3] = {{4, 3, 2}, {3, 5, 0}, {5, 6, 7}, {6, 4, 9}, // 2, 3, 4, 5
                      {14, 13, 12}, {13, 15, 10}, {15, 16, 17}, {16, 14, 19}}; // 6, 7, 8, 9

  // Add vertices, u & vboundary
  IPMK_Index vertexindices[11];
  for (int i = 0; i < 11; i++) {
    vertexindices[i] = IPMK_ArrayNumericAdd(vertices, 2, xy[i],err);
    IPMK_ArrayNumericAdd(vboundary, 1, &vboundarytags[i], err);
  }

  // Add edges & childedges
  IPMK_Index edgeindices[20];
  for (int i = 0; i < 20; i++) {
    const IPM_Real zero = 0.0;
    IPMK_Index ps[2] = {vertexindices[edgeid[i][0]],vertexindices[edgeid[i][1]]};
    edgeindices[i] = IPMK_ArrayIndexAdd(edges, 2, ps, err);

    IPMK_Index kids[2] = {IPM_INDEX_NULL, IPM_INDEX_NULL};
    IPMK_ArrayIndexAdd(childedges, 2, kids, err);
    IPMK_ArrayNumericAdd(edgelevels, 1, &zero, err);
  }

  // Add quad elements & parentelems
  for (int i = 0; i < 2; i++) {
    IPMK_Index ps[4] = {edgeindices[quadid[i][0]], edgeindices[quadid[i][1]], edgeindices[quadid[i][2]], edgeindices[quadid[i][3]]};
    IPMK_Index leaf = IPMK_ArrayIndexAdd(elements, 4, ps, err);
    IPMK_ArrayIndexAdd(activeelems, 1, &leaf, err);

    IPMK_Index parent[1] = {IPM_INDEX_NULL};
    IPMK_ArrayIndexAdd(parentelems, 1, parent, err);
  }

  // Add triangle elements & parentelems
  for (int i = 0; i < 8; i++) {
    IPMK_Index ps[3] = {edgeindices[triangleid[i][0]], edgeindices[triangleid[i][1]], edgeindices[triangleid[i][2]]};
    IPMK_Index leaf = IPMK_ArrayIndexAdd(elements, 3, ps, err);
    IPMK_ArrayIndexAdd(activeelems, 1, &leaf, err);

    IPMK_Index parent [1]= {IPM_INDEX_NULL};
    IPMK_ArrayIndexAdd(parentelems, 1, parent, err);
  }
#else

  //   3               2
  //   +-------2-------+
  //   |               |
  //   |               |
  //   |               |
  //   3       0       1
  //   |               |
  //   |               |
  //   |               |
  //   +-------0-------+
  //   0               1

  IPM_Real xy[4][2] = {{0, 0}, {2, 0}, {2, 2}, {0, 2}};// 0, 1, 2, 3

  IPM_Real vboundarytags[4] = {1, 1, 1, 1};// 0, 1, 2, 3

  int edgeid[4][2] = {{0, 1}, {1, 2}, {2, 3}, {3, 0}}; // 0, 1, 2, 3

  int quadid[1][4] = {{0, 1, 2, 3}}; // 0

  // Add vertices, u & vboundary
  IPMK_Index vertexindices[4];
  for (int i = 0; i < 4; i++) {
    vertexindices[i] = IPMK_ArrayNumericAdd(vertices, 2, xy[i],err);
    IPMK_ArrayNumericAdd(vboundary, 1, &vboundarytags[i], err);
  }

  // Add edges & childedges
  IPMK_Index edgeindices[4];
  for (int i = 0; i < 4; i++) {
    const IPM_Real zero = 0.0;
    IPMK_Index ps[2] = {vertexindices[edgeid[i][0]],vertexindices[edgeid[i][1]]};
    edgeindices[i] = IPMK_ArrayIndexAdd(edges, 2, ps, err);

    IPMK_Index kids[2] = {IPM_INDEX_NULL, IPM_INDEX_NULL};
    IPMK_ArrayIndexAdd(childedges, 2, kids, err);
    IPMK_ArrayNumericAdd(edgelevels, 1, &zero, err);
  }

  // Add quad elements & parentelems
  for (int i = 0; i < 1; i++) {
    IPMK_Index ps[4] = {edgeindices[quadid[i][0]], edgeindices[quadid[i][1]], edgeindices[quadid[i][2]], edgeindices[quadid[i][3]]};
    IPMK_Index leaf = IPMK_ArrayIndexAdd(elements, 4, ps, err);
    IPMK_ArrayIndexAdd(activeelems, 1, &leaf, err);

    IPMK_Index parent[1] = {IPM_INDEX_NULL};
    IPMK_ArrayIndexAdd(parentelems, 1, parent, err);
  }

#endif

}

/**
 * Init u and distribute the mesh (hopefully the mesh is now big enough compared to nprocs)
 */
void kInitMesh(IPMK_ArrayNumeric u, IPMK_ArrayNumeric vertices, IPMK_ArrayIndex edges,
  IPMK_ArrayIndex elements, IPMK_ArrayIndex activeelems, IPMK_Error *err)
{
  IPMK_ArrayLocalIter iter;
  for (IPMK_ArrayLocalIterInit(vertices, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPM_Index i = IPMK_ArrayLocalIterToIndex(&iter);
    IPM_Real *xy = IPMK_ArrayNumericGet(vertices, i, err);
    IPM_Real uexact = exact_solution(xy[0], xy[1], curtime);
    *IPMK_ArrayNumericGet(u, i, err) = uexact;
  }
}


/**
 * The kernel examines every active element and decides whether we need to
 * refine the element and its edges by setting flags on them.
 *
 * elemrefineflags and edgerefineflags are for active elements and edges
 * respectively, with a default value 0. We test each active element. If its
 * error is bigger than a cut off, set its elemrefineflags to 1 and also set
 * edgerefineflags of its edges to 1.
 *
 * edgerefineflags are OR'ed between processes for this kernel. Though we set
 * IPM_MEMORY_ADD for elemrefineflags, it is not OR'ed between processes since
 * elemrefineflags is a clone of activeelems, which has no overlapping between
 * processes.
 *
 * The elements and edges selected for refinement by this kernel are rough. We
 * need to further smooth the flags to select MORE elements and edges for
 * refinement.
 *
 */

void kSetRefineFlags(IPMK_ArrayNumeric elemrefineflags, IPMK_ArrayNumeric edgerefineflags, IPMK_RArray maxerr,
  IPMK_ArrayIndex elements, IPMK_ArrayNumeric elemerrs, IPMK_ArrayIndex activeelems, IPMK_Error *err)
{
  IPM_Real globalmaxerr = *IPMK_RArrayGet(maxerr, err);
  IPM_Real refine_cutoff = (1-refine_fraction) * globalmaxerr;

  IPMK_ArrayLocalIter iter;
  for (IPMK_ArrayLocalIterInit(activeelems, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPM_Index ep = IPMK_ArrayLocalIterToIndex(&iter); // ep is an index of activeelems
    IPM_Index e = *IPMK_ArrayIndexGet(activeelems, ep, err); // value at ep is an index of elements
    const IPMK_Index *edgeidx = IPMK_ArrayIndexGet(elements, e, err);
    int num_edges = IPM_ArrayGetLength(elements, e, err);
    IPM_Real myerr = *IPMK_ArrayNumericGet(elemerrs, ep, err);

    if (myerr >= refine_cutoff) {
      *IPMK_ArrayNumericGet(elemrefineflags, ep, err) = 1.0;
      for (int i = 0; i < num_edges; i++) *IPMK_ArrayNumericGet(edgerefineflags, edgeidx[i], err) = 1.0;
    }
  }
}


/*
 * Suppose an active element e was not set for refinement in kSetRefineFlags and
 * e has a remote neighbor f which was set for refinement in kSetRefineFlags.
 * After communication in post lauch of kSetRefineFlags, e should know it needs
 * to be refined, otherwise there will be multiple hanging nodes along the edge
 * between e and f.
 *
 *    +----------+-----+-----+
 *    |          |     |     |
 *    |          |     |     |
 *    |          |     |     |
 *    |    e     o-----+-----+
 *    |          |     |     |
 *    |          |  f  |     |
 *    |          |     |     |
 *    +----------+-----+-----+
 *
 * If e turns refinable, this status change can further affect e's other
 * neighbors, remote or not.  Therefore, we need to repeat until no change to
 * smooth flags on elements and edges.  We do so by propagating elemrefineflags
 * through edgerefineflags.
 *
 */

void kSmoothRefineFlags(IPMK_RArray flagchanged, IPMK_ArrayNumeric elemrefineflags, IPMK_ArrayNumeric edgerefineflags,
  IPMK_ArrayIndex childedges, IPMK_ArrayIndex elements, IPMK_ArrayIndex activeelems, IPMK_Error *err)
{
  int global_changed = 0;
  int changed;

  *IPMK_RArrayGet(flagchanged, err) = 0; // clear the RArray first

  do {
    changed = 0;

    IPMK_ArrayLocalIter iter;
    for (IPMK_ArrayLocalIterInit(activeelems, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
      IPM_Index ep = IPMK_ArrayLocalIterToIndex(&iter); // e' indexes activeelems
      // If this active element was not set for refinement, check again to see if we have to change mind
      if (*IPMK_ArrayNumericGet(elemrefineflags, ep, err) < 0.5) {
        IPM_Index e = *IPMK_ArrayIndexGet(activeelems, ep, err); // e indexes elements
        const IPMK_Index *edgeidx = IPMK_ArrayIndexGet(elements, e, err);
        const int num_edges = IPM_ArrayGetLength(elements, e, err);
        int need_to_refine = 0;

        // Examine child edges of e's edges (if any) to see if they are set for refinement
        for (int i = 0; i < num_edges; i++) {
          IPMK_Index *mychildedges = IPMK_ArrayIndexGet(childedges, edgeidx[i], err);
          if (*mychildedges != IPM_INDEX_NULL) {
            if (*IPMK_ArrayNumericGet(edgerefineflags, mychildedges[0], err) > 0.5 // If either child edge needs to be refined
             || *IPMK_ArrayNumericGet(edgerefineflags, mychildedges[1], err) > 0.5)
            {
              need_to_refine = 1;
              break;
            }
          }
        }

        // If yes, tag the element and its edges
        if (need_to_refine) {
          changed = 1;
          global_changed = 1;
          *IPMK_ArrayNumericGet(elemrefineflags, ep, err) = 1.0;
          for (int i = 0; i < num_edges; i++)
            *IPMK_ArrayNumericGet(edgerefineflags, edgeidx[i], err) += 1.0;
        }
      }
    }
  } while (changed);

  if (global_changed) *IPMK_RArrayGet(flagchanged, err) = 1;
}

/*
 * This kernel initially decides which elements are coarsenable.
 *
 * An element is coarsenable means we want to remove its child elements from
 * the mesh. Of course, only elements having children could be coarsenable.
 *
 * We use a flag array elemcoarsenflags with default value 1 to indicate
 * whether elements are coarsenable. This default value of 1 implies we need to
 * screen out elements not eligible for coarsening by clearing their flags.
 *
 * For each element, in the following cases, we clear its elemcoarsenflags.
 *
 * 1. The element is active (has no children to remove);
 *
 * 2. The element has inactive children (not its turn yet);
 *
 * 3. The element has an active child with a big error (illegal to coarsen);
 *
 * 4. The element has an active child with edges need to be refined (due to
 * neighbor elements required to be refined).  Coarsening the element would
 * results in multiple hanging nodes along the edge.
 *
 * elemcoarsenflags are AND'ed between processes after this kernel call. The
 * elements selected for coarsening by this kernel are rough. We need to
 * further smooth the flags to REMOVE more elements from coarsening.
 *
 */

void kClearElementCoarsenFlags(IPMK_ArrayNumeric elemcoarsenflags, IPMK_RArray maxerr, IPMK_RArray minerr,
  IPMK_ArrayNumeric edgerefineflags, IPMK_ArrayIndex parentelems, IPMK_ArrayIndex elements,
  IPMK_ArrayNumeric elemerrs, IPMK_ArrayIndex activeelems, IPMK_Error *err)
{
  IPM_Real globalmaxerr = *IPMK_RArrayGet(maxerr, err);
  IPM_Real globalminerr = *IPMK_RArrayGet(minerr, err);
//  IPM_Real coarsen_cutoff = globalminerr + (globalmaxerr - globalminerr)*coarsen_fraction;
 IPM_Real coarsen_cutoff = (globalmaxerr - 0)*coarsen_fraction;

  int visibleelems = IPM_ArrayVisibleSize(elements, err);
  int *elem_is_active = calloc(visibleelems, sizeof(int)); // flag to say if an element is active.

  // Test parents of active elements (if any) first
  IPMK_ArrayLocalIter iter;
  for (IPMK_ArrayLocalIterInit(activeelems, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPMK_Index ep = IPMK_ArrayLocalIterToIndex(&iter); // e' indexes activeelems
    IPMK_Index e = *IPMK_ArrayIndexGet(activeelems, ep, err); // value at e' indexes elements
    IPMK_Index parent = *IPMK_ArrayIndexGet(parentelems, e, err);

    elem_is_active[e] = 1; // For later use

    if (parent != IPM_INDEX_NULL) {
      IPM_Real *flag = IPMK_ArrayNumericGet(elemcoarsenflags, parent, err);

      if (*flag != 0) { // If flag is 1, test further to see if we need to clear it
        if (*IPMK_ArrayNumericGet(elemerrs, ep, err) > coarsen_cutoff) {
          // Case 3: don't coarsen parent if ep's error is too big
          *flag = 0;
        } else {
          const int num_edges = IPM_ArrayGetLength(elements, e, err);
          const IPMK_Index *myedges = IPMK_ArrayIndexGet(elements, e, err);
          int do_coarsen = 1;

          for (int i = 0; i < num_edges; i++) {
            if (*IPMK_ArrayNumericGet(edgerefineflags, myedges[i], err) > 0.5) { do_coarsen = 0; break; }
          }

          // Case 4: don't coarsen parent if a neighbor of e wants to refine its edges
          if (!do_coarsen) *flag = 0;
        }
      }
    }
  }

  // Test all elements to screen out elements that never should be considered for coarsening
  for (IPMK_ArrayLocalIterInit(elements, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPM_Index e = IPMK_ArrayLocalIterToIndex(&iter);
    IPM_Index parent = *IPMK_ArrayIndexGet(parentelems, e, err);
    if (elem_is_active[e]) {
      // Case 1: e is active
      *IPMK_ArrayNumericGet(elemcoarsenflags, e, err) = 0;
    } else if (parent != IPM_INDEX_NULL) {
      // Case 2: parent has an inactive child
      *IPMK_ArrayNumericGet(elemcoarsenflags, parent, err) = 0;
    }
  }

  free(elem_is_active);
}

/*
 * This kernel initially decides which edges are coarsenable.
 *
 * An edge is coarsenable means we want to remove its child edges from the
 * mesh. Again, only edges having children could be coarsenable.
 *
 * We use a flag array edgecoarsenflags with default value 1 to indicate
 * whether edges are coarsenable. This default value of 1 implies we need to
 * screen out edges not eligible for coarsening by clearing their flags.
 *
 * If an inactive element is not coarsenable, we change its edges'
 * edgecoarsenflags to 0.
 *
 * edgecoarsenflags are AND'ed between processes after this kernel call. The
 * edges selected for coarsening by this kernel are not final. We need to
 * further smooth the flags to REMOVE more edges from coarsening.
 *
 * We separate kClearEdgeCoarsenFlags from kClearElementCoarsenFlags since some
 * elements' flag is not cleared until post launch of kClearElementCoarsenFlags.
 *
 * Suppose element e has children f1~f4. f1~f3 are on the same process P1 and
 * have a small error. P1 decides e is coarsenable. However, f4 is on process
 * P2 and has a big error. P2 decides e is not coarsenable. Only after
 * kClearElementCoarsenFlags, P1 gets to know e is not coarsenable.
 *
 *    +-----+-----+
 *    |     |     |
 *    |  f1 |  f2 |
 *    |     |     |
 *    |-----e-----+
 *    |     |     |
 *    |  f3 |  f4 |
 *    |     |     |
 *    +-----+-----+
 */

void kClearEdgeCoarsenFlags(IPMK_ArrayNumeric edgecoarsenflags, IPMK_ArrayNumeric elemcoarsenflags,
  IPMK_ArrayIndex elements, IPMK_ArrayIndex activeelems, IPMK_Error *err)
{
  int visibleelems = IPM_ArrayVisibleSize(elements, err);
  int *elem_is_active = calloc(visibleelems, sizeof(int)); // flag to say if an element is active.

  IPMK_ArrayLocalIter iter;
  for (IPMK_ArrayLocalIterInit(activeelems, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPMK_Index ep = IPMK_ArrayLocalIterToIndex(&iter); // e' indexes activeelems
    IPMK_Index e = *IPMK_ArrayIndexGet(activeelems, ep, err); // value at e' indexes elements
    elem_is_active[e] = 1; // For later use
  }

  for (IPMK_ArrayLocalIterInit(elements, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPM_Index e = IPMK_ArrayLocalIterToIndex(&iter);
    IPM_Real *flag = IPMK_ArrayNumericGet(elemcoarsenflags, e, err);

    // if e is active, though e is not coarsenable, e's edges having chidren
    // are coarsenable from the view of e, since coarsening those edges won't
    // make e invalid. Therefore we won't clear edgecoarsenflags of e's edges.
    if (!elem_is_active[e] && *flag < 0.5) {
      int num_edges = IPM_ArrayGetLength(elements, e, err);
      IPMK_Index *myedges = IPMK_ArrayIndexGet(elements, e, err);
      for (int i = 0; i < num_edges; i++) *IPMK_ArrayNumericGet(edgecoarsenflags, myedges[i], err) = 0;
    }
  }

  free(elem_is_active);
}


/*
 * Suppose an inactive element e was set for coarsening in kSetCoarsenFlags. e
 * has a remote neighbor f and f was not set for coarsening in
 * kSetCoarsenFlags. After communication in post launch of kSetCoarsenFlags, e
 * should know it can not be coarsened, otherwise there will be multiple
 * hanging nodes along the edge between e and f.
 *
 *    +-----------+-----+-----+
 *    |     |     |     |     |
 *    |     |     |     |     |
 *    |     |     |     |     |
 *    |-----e-----+-----+-----+
 *    |     |     |  |  |     |
 *    |     |     o--f--|     |
 *    |     |     |  |  |     |
 *    +-----------+-----+-----+
 *
 * If e turns non-coarsenable, this status change might further affect e's
 * other neighbors, remote or not. Therefore, we need to repeat until no
 * change to smooth the flags on elements. We propagate elemcoarsenflags
 * through edgecoarsenflags.
 *
 * elemcoarsenflags and edgecoarsenflags are AND'ed between processes.
 *
 */

void kSmoothCoarsenFlags(IPMK_RArray flagchanged, IPMK_ArrayNumeric elemcoarsenflags, IPMK_ArrayNumeric edgecoarsenflags,
  IPMK_ArrayIndex childedges, IPMK_ArrayIndex elements, IPMK_Error *err)
{
  int changed;
  int globally_changed = 0;

  *IPMK_RArrayGet(flagchanged, err) = 0; // clear the RArray first

  do {
    changed = 0;

    IPMK_ArrayLocalIter iter;
    for (IPMK_ArrayLocalIterInit(elements, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
      IPM_Index e = IPMK_ArrayLocalIterToIndex(&iter);

      // If this element was set for coarsening, check child edges of its edges
      if (*IPMK_ArrayNumericGet(elemcoarsenflags, e, err) > 0.5) {
        const IPMK_Index *myedges = IPMK_ArrayIndexGet(elements, e, err);
        int num_edges = IPM_ArrayGetLength(elements, e, err);
        int do_not_coarsen = 0;
        // Examine e's edges to see ...
        for (int i = 0; i < num_edges; i++) {
          IPMK_Index *mychildedges = IPMK_ArrayIndexGet(childedges, myedges[i], err);
          assert(*mychildedges != IPM_INDEX_NULL); // Edge i should have children since e is said to be coarsenable
          // ... if either child edge has children but not coarsenable, we can not coarsen e
          if (*IPMK_ArrayIndexGet(childedges, mychildedges[0], err) != IPM_INDEX_NULL &&
              *IPMK_ArrayNumericGet(edgecoarsenflags, mychildedges[0], err) < 0.5 ||
              *IPMK_ArrayIndexGet(childedges, mychildedges[1], err) != IPM_INDEX_NULL  &&
              *IPMK_ArrayNumericGet(edgecoarsenflags, mychildedges[1], err) < 0.5)
          {
            do_not_coarsen = 1;
            break;
          }
        }

        if (do_not_coarsen) {
          changed = 1;
          globally_changed = 1;
          *IPMK_ArrayNumericGet(elemcoarsenflags, e, err) = 0;
          for (int i = 0; i < num_edges; i++) *IPMK_ArrayNumericGet(edgecoarsenflags, myedges[i], err) = 0;
        }
      }
    }
  } while (changed);

  if (globally_changed) *IPMK_RArrayGet(flagchanged, err) = 1;
}


/**
 * The kernel goes over edges of parent to active elements, if they are coarsenable, just clear its two children.
 */
void kCoarsenEdges(IPMK_ArrayNumeric edgecoarsenflags, IPMK_ArrayIndex childedges, IPMK_ArrayIndex edges, IPMK_Error *err)
{
  IPMK_ArrayLocalIter iter;
  for (IPMK_ArrayLocalIterInit(edges, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPMK_Index eidx = IPMK_ArrayLocalIterToIndex(&iter); // index of the edge
    IPM_Real edgecoarsenflag = *IPMK_ArrayNumericGet(edgecoarsenflags, eidx, err);
    IPMK_Index *children = IPMK_ArrayIndexGet(childedges, eidx, err);

    // If the edge is coarsenable and does have children, clear them!
    // The child edges are still in \p edges. They will be recycled when the runtime repartitions the mesh.
    if (edgecoarsenflag > 0.5 && children[0] != IPM_INDEX_NULL) {
      children[0] = children[1] = IPM_INDEX_NULL;
    }
  }
}

/**
 * The kernel removes children of and activiate coarsenable elements.
 */
void kCoarsenElements(IPMK_ArrayNumeric elemcoarsenflags,
  IPMK_ArrayIndex parentelems, IPMK_ArrayIndex elements, IPMK_ArrayIndex activeelems, IPMK_Error *err)
{
  IPMK_ArrayLocalIter iter;
  // Remove active elements with coarsenable parents
  for (IPMK_ArrayLocalIterInit(activeelems, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPM_Index ep = IPMK_ArrayLocalIterToIndex(&iter); // ep indexes activeelems
    IPM_Index e = *IPMK_ArrayIndexGet(activeelems, ep, err); // value at p indexes to elements
    IPM_Index parent = *IPMK_ArrayIndexGet(parentelems, e, err);

    if (parent == IPM_INDEX_NULL) continue; // e has no parents and is a level 0 element

    IPM_Real elemcoarsenflag = *IPMK_ArrayNumericGet(elemcoarsenflags, parent, err);
    if (elemcoarsenflag > 0.5) IPMK_ArrayIndexRemove(activeelems, ep, err);
  }

 // ... and also add their parent to activeelems
  for (IPMK_ArrayLocalIterInit(elements, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPM_Index e = IPMK_ArrayLocalIterToIndex(&iter);
    IPM_Real elemcoarsenflag = *IPMK_ArrayNumericGet(elemcoarsenflags, e, err);
    if (elemcoarsenflag > 0.5) IPMK_ArrayIndexAdd(activeelems, 1, &e, err);
  }
}


/**
 * Estimate errors of elements and also get the maximal error.
 *
 * \param[out] maxerr The maximal error
 * \param[out] elemerrs Errors for elements
 * \param[in] hmax Diameters of elements
 * \param[in] kellyjump Kellyjump of elements
 * \param[in] elements The element array, used to get indices to edges.
 */
void kEstimateErrors(IPMK_RArray maxerr, IPMK_RArray minerr, IPMK_ArrayNumeric elemerrs,
  IPMK_ArrayNumeric hmax, IPMK_ArrayNumeric kellyjump, IPMK_ArrayIndex childedges,
  IPMK_ArrayIndex elements, IPMK_ArrayIndex activeelems, IPMK_Error *err)
{
  IPMK_ArrayLocalIter iter;
  IPM_Real *maxptr = IPMK_RArrayGet(maxerr, err);
  IPM_Real *minptr = IPMK_RArrayGet(minerr, err);
  IPM_Real themaxerr = 0; // The max err
  IPM_Real theminerr = 1e50; // The min err

  for (IPMK_ArrayLocalIterInit(activeelems, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPM_Index ep = IPMK_ArrayLocalIterToIndex(&iter); // e' indexes activeelems
    IPM_Index e = *IPMK_ArrayIndexGet(activeelems, ep, err); // value at p indexes elements
    const int num_edges = IPM_ArrayGetLength(elements, e, err);
    const IPMK_Index *myedges = IPMK_ArrayIndexGet(elements, e, err);
    IPM_Real myhmax = *IPMK_ArrayNumericGet(hmax, ep, err);
    IPM_Real *errptr = IPMK_ArrayNumericGet(elemerrs, ep, err);
    IPM_Real myerr = .0;

    for (int i = 0; i < num_edges; i++) {
      IPMK_Index *children = IPMK_ArrayIndexGet(childedges, myedges[i], err);
      IPM_Real *jump;
      if (children[0] == IPM_INDEX_NULL) {
        jump = IPMK_ArrayNumericGet(kellyjump, myedges[i], err);
        // jump[0] == 0 means the edge is on boundary
        if (jump[0] != 0 && jump[1] != 0) myerr += SQ(jump[0]) + SQ(jump[1]);// + 1e-30; // Mimic libMesh, add 1e-30 as minimal
      } else {
        for (int j = 0; j < 2; j++) {
          jump = IPMK_ArrayNumericGet(kellyjump, children[j], err);
          myerr += SQ(jump[0]) + SQ(jump[1]);// + 1e-30;
        }
      }
    }
    myerr *= myhmax/24;
    myerr = sqrt(myerr);
    *errptr = myerr;
    if (myerr > themaxerr) themaxerr = myerr;
    if (myerr < theminerr) theminerr = myerr;
  }

  *maxptr = themaxerr;
  *minptr = theminerr;
}

void kComputeKellyJump(IPMK_ArrayNumeric hmax, IPMK_ArrayNumeric kellyjump, /*OUT*/
  IPMK_ArrayNumeric u, IPMK_ArrayNumeric vboundary, IPMK_ArrayNumeric vertices,  /*IN*/
  IPMK_ArrayIndex childedges, IPMK_ArrayIndex edges, IPMK_ArrayIndex elements, IPMK_ArrayIndex activeelems, IPMK_Error *err)
{
  IPMK_ArrayLocalIter iter;
  for (IPMK_ArrayLocalIterInit(activeelems, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPM_Index ep = IPMK_ArrayLocalIterToIndex(&iter); // e' indexes activeelems
    IPM_Index e = *IPMK_ArrayIndexGet(activeelems, ep, err); // value at p indexes elements
    const int num_edges = IPM_ArrayGetLength(elements, e, err);
    const IPMK_Index *myedges = IPMK_ArrayIndexGet(elements, e, err);
    IPMK_Index vertidx[4] = {IPM_INDEX_NULL}; // Vertices of the element in a proper order
    IPM_Real *vertcoord[4] = {NULL}; // Pointers to coordinates of the vertices
    IPMK_Index edgevert[8]; // Store vertices of edges

    const int num_verts = num_edges; // True for triangles and quads

    assert(num_edges == 3 || num_edges == 4);

    // Store all vertices in a temp array to facilitate later accessing
    for (int i = 0; i < num_edges; i++) {
      IPMK_Index *ends = IPMK_ArrayIndexGet(edges, myedges[i], err);
      edgevert[2*i] = ends[0];
      edgevert[2*i+1] = ends[1];
    }

    // Go over vertices of the element and order them in a ring (significant for quadrilaterals).
    vertidx[0] = edgevert[0];
    vertidx[1] = edgevert[1];

    // If edge 0 is opposite to the direction of the element, we need to swap vertidx[0] and vertidx[1]
    if (edgevert[2] != vertidx[1] && edgevert[3] != vertidx[1]) {
      // It implies edge 1 must be connected to the tail of edge 0, i.e., verts[0]!
      IPMK_Index tmp = vertidx[0];
      vertidx[0] = vertidx[1];
      vertidx[1] = tmp;
    }

    // Find other vertices in the direction of vertidx[0]->vertidx[1]
    for (int i = 1; i < num_verts - 1; i++) {
      if (edgevert[2*i] != vertidx[i]) {
        vertidx[i+1] = edgevert[2*i]; assert(edgevert[2*i+1] == vertidx[i]);
      } else {
        vertidx[i+1] = edgevert[2*i+1]; assert(edgevert[2*i] == vertidx[i]);
      }
    }

    IPM_Real nodes[4][2] = {{0}}; // nodes[][] is for debugging purpose
    for (int i = 0; i < num_verts; i++) {
      vertcoord[i] = IPMK_ArrayNumericGet(vertices, vertidx[i], err);
      nodes[i][0] = vertcoord[i][0];
      nodes[i][1] = vertcoord[i][1];
    }

    // Compute diameter of this element
    IPM_Real maxh = .0;
    for (int i = 0; i < num_verts; i++) {
      for (int j = i+1; j< num_verts; j++) {
        IPM_Real h = DIST(vertcoord[i], vertcoord[j]);
        if (h > maxh) maxh = h;
      }
    }
    *IPMK_ArrayNumericGet(hmax, ep, err) = maxh;

    // Accumulate grad*norm at qpoints of edges of the element.
    for (int k = 0; k < num_edges; k++) {
      int ebdary = (*IPMK_ArrayNumericGet(vboundary, edgevert[2*k], err) > 0.5
                    && *IPMK_ArrayNumericGet(vboundary, edgevert[2*k+1], err) > 0.5) ? 1 : 0;
      if (!ebdary) { // On boundary, diff[] is already initialized to be zero
        IPMK_Index *children = IPMK_ArrayIndexGet(childedges, myedges[k], err);

        IPMK_Index pcedges[3]; // Put parent and the two child edges together
        pcedges[0] = myedges[k];
        if (vertidx[k] == edgevert[2*k]) { // Be carefulll about the order of child edges
          pcedges[1] = children[0];
          pcedges[2] = children[1];
        } else {
          pcedges[1] = children[1];
          pcedges[2] = children[0];
        }

        int mbegin, mend;
        if (children[0] == IPM_INDEX_NULL) { mbegin = 0; mend = 1; } // parent only
        else { mbegin = 1; mend = 3; } // children only

        for (int pcnum = mbegin; pcnum < mend; pcnum++) { // parent-children num
          int cnt;
          IPM_Real tmps[2];
          FE *fe = (num_verts == 3? &triface : &quadface);

          InitFaceKelly(fe, num_verts, vertcoord, k, pcnum);

          for (int j = 2*pcnum, cnt=0; j < 2*(pcnum+1); j++, cnt++) {
            IPM_Real grad[2] = {.0};
            for (int i = 0; i < fe->nphi; i++) {
              IPM_Real ui = *IPMK_ArrayNumericGet(u, vertidx[i],  err);
              grad[0] += ui*DPHI(i, j, 0);
              grad[1] += ui*DPHI(i, j, 1);
            }
            tmps[cnt] = DOT(grad, fe->normals[j])*sqrt(fe->JxW[j]);
          }

          IPM_Real *jump = IPMK_ArrayNumericGet(kellyjump, pcedges[pcnum], err);
          if (vertidx[k] == edgevert[2*k]) { // This edge is in the same orientation as the element
            jump[0] += tmps[0];
            jump[1] += tmps[1];
          } else {
            jump[1] += tmps[0];
            jump[0] += tmps[1];
          }
        }
      }
    }
  }
}

/**
 * The kernel splits edges tagged by \p edgeflags.
 *
 * To split an edge, it introduces a point in the middle and creates two child edges.
 * \p edgeflags and \p edgechildren have the same layout as \p edges. \p edgechildren
 * is already initialized with default IPM_INDEX_NULL.
 */
void kRefineEdges(IPMK_ArrayNumeric u_old, IPMK_ArrayNumeric vboundary, IPMK_ArrayNumeric vertices,
  IPMK_ArrayNumeric edgelevels, IPMK_ArrayIndex edgerefineflags, IPMK_ArrayIndex childedges, IPMK_ArrayIndex edges, IPMK_Error *err)
{
  IPMK_Index i, j, k, *verts, newedge[2], child[2];
  IPM_Real *xyi, *xyj, xyk[2];
  IPM_Real vbdary;

  // Loop over local edges
  IPMK_ArrayLocalIter iter;
  for (IPMK_ArrayLocalIterInit(edges, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPMK_Index e = IPMK_ArrayLocalIterToIndex(&iter); // index of the edge
    IPMK_Index *children = IPMK_ArrayIndexGet(childedges, e, err);

    if (children[0] != IPM_INDEX_NULL) continue; // do not refine edges already split

    IPM_Real *flag = IPMK_ArrayNumericGet(edgerefineflags, e, err);
    IPM_Real u_old_i, u_old_j, u_old_k;

    if (*flag > 0.5) { // safer than flag >= 1.0
      verts = IPMK_ArrayIndexGet(edges, e, err); // vertex indices of the edge
      i = verts[0];
      j = verts[1];

      xyi = IPMK_ArrayNumericGet(vertices, i, err); // coord of vertex i
      xyj = IPMK_ArrayNumericGet(vertices, j, err); // coord of vertex j
      u_old_i = *IPMK_ArrayNumericGet(u_old, i, err);
      u_old_j = *IPMK_ArrayNumericGet(u_old, j, err);

      // Add a middle point k to i, j to \p vertices
      xyk[0] = (xyi[0]+xyj[0])/2.0;
      xyk[1] = (xyi[1]+xyj[1])/2.0;

      // k is on boundary if i and j are
      vbdary = (*IPMK_ArrayNumericGet(vboundary, i, err) > 0.5 && *IPMK_ArrayNumericGet(vboundary, j, err) > 0.5) ? 1.0 : 0.0;

      // Linearly interpolate the new vertex
      u_old_k = vbdary? exact_solution(xyk[0], xyk[1], curtime - dt) : (u_old_i + u_old_j)/2.0;

      k = IPMK_ArrayNumericAdd(vertices, 2, xyk, err);
      IPMK_ArrayNumericAdd(u_old, 1, &u_old_k, err);
      IPMK_ArrayNumericAdd(vboundary, 1, &vbdary, err);

      // Add two child edges to \p edges, which must be added in i-k, k-j or
      // j-k, k-i format, so that we can easily recover k in element refinement.
      newedge[0] = i; newedge[1] = k; child[0] = IPMK_ArrayIndexAdd(edges, 2, newedge, err);
      newedge[0] = k; newedge[1] = j; child[1] = IPMK_ArrayIndexAdd(edges, 2, newedge, err);

      // Set level for new-born child edges
      IPM_Real level = *IPMK_ArrayNumericGet(edgelevels, e, err);
      level += 1.0;
      IPMK_ArrayNumericAdd(edgelevels, 1, &level, err);
      IPMK_ArrayNumericAdd(edgelevels, 1, &level, err);

      // Direct the NULL indices to point to the children
      IPMK_ArrayIndexChange(childedges, e, 2, child, err);
    }
  }
}

/**
 * The subroutine refines elements if they have split edge(s).
 * If an element is refined, it may have up to 4 child elements.
 */

void RefineTriangle(IPMK_Index activeidx, IPMK_Index elemidx, IPMK_Index *parentedges,
  IPMK_Index *mychildedges, IPMK_Index *midpoints, IPMK_Index *verts,
  IPMK_ArrayNumeric vertices, IPMK_ArrayNumeric edgelevels, IPMK_ArrayIndex edges,
  IPMK_ArrayIndex parentelems, IPMK_ArrayIndex elements,
  IPMK_ArrayIndex activeelems, IPMK_Error *err)
{
  const IPMK_Index *myedges = IPMK_ArrayIndexGet(elements, elemidx, err); // edges of the element
  IPMK_Index e[9] = {IPM_INDEX_NULL}; // A triangle has at most 9 sub-edges
  IPMK_Index ab, bc, ac; // Three edges of the triangle
  IPMK_Index a, b, c, x, y, z; // Three vertices and middle points of the triangle
  IPMK_Index newedge[2]; // Temp variable for a new interior edge

  // Get the structure of the triangle. a, b, c are its three vertices.
  // ab, bc, ac are its edges. x, y, z are middle points of edge ab, bc, ac
  // respectively. e0, e1, ..., e5 are child edges of ab, bc, ac in the exact
  // order shown in the diagram below. Note that the middle points and child
  // edges do not nessesarily all exist. If one does not exist, its value
  // will be IPM_INDEX_NULL.
  //
  //                       a
  //                      / \
  //                  e0 /   \ e5
  //                    /     \
  //              ab   x       z    ac
  //                  /         \
  //              e1 /           \ e4
  //                /             \
  //               b-------y-------c
  //                   e2     e3
  //                      bc
  a = verts[0];
  b = verts[1];
  c = verts[2];
  ab = parentedges[0];
  bc = parentedges[1];
  ac = parentedges[2];
  x = midpoints[0];
  y = midpoints[1];
  z = midpoints[2];
  for (int i = 0; i < 6; i++) e[i] = mychildedges[i];


  // Refine the triangle into four children.
  // New elements must be created with edges ordered in counterclockwise direction.
  // For example, if an element has edges e0, e1, e2, then e0 -> e1 -> e2 must be
  // in counterclockwise direction. Note that that does not necessarily mean each
  // edge is also in counterclockwise direction, e.g., if e0 = (a, b), a->b might
  // be in clockwise direction. We need to correctly handle these variations.

  IPMK_Index child[4]; // Indices of (at most) the four children after refinement
  IPMK_Index newtri[4][3]; // Temp variable for a new child element
  int edges_split = (x == IPM_INDEX_NULL ? 0 : 1) +
                    (y == IPM_INDEX_NULL ? 0 : 1) +
                    (z == IPM_INDEX_NULL ? 0 : 1);

  assert(edges_split == 3);

  // All three edges have benn split, so refine the element as follows
  //                       a
  //                      / \
  //                  e0 /   \ e5
  //                    /     \
  //                   x---e8--z
  //                  / \     / \
  //              e1 /  e6   e7  \ e4
  //                /     \ /     \
  //               b-------y-------c
  //                   e2     e3
  newedge[0] = x; newedge[1] = y; e[6] = IPMK_ArrayIndexAdd(edges, 2, newedge, err);
  newedge[0] = y; newedge[1] = z; e[7] = IPMK_ArrayIndexAdd(edges, 2, newedge, err);
  newedge[0] = z; newedge[1] = x; e[8] = IPMK_ArrayIndexAdd(edges, 2, newedge, err);

  // Set level for new-born child edges
  IPM_Real level = *IPMK_ArrayNumericGet(edgelevels, ab, err);
  level += 1.0;
  IPMK_ArrayNumericAdd(edgelevels, 1, &level, err);
  IPMK_ArrayNumericAdd(edgelevels, 1, &level, err);
  IPMK_ArrayNumericAdd(edgelevels, 1, &level, err);

  newtri[0][0] = e[0];  newtri[0][1] = e[8];  newtri[0][2] = e[5]; // First vertex is a
  newtri[1][0] = e[2];  newtri[1][1] = e[6];  newtri[1][2] = e[1]; // First vertex is b
  newtri[2][0] = e[4];  newtri[2][1] = e[7];  newtri[2][2] = e[3]; // First vertex is c
  newtri[3][0] = e[6];  newtri[3][1] = e[7];  newtri[3][2] = e[8]; // First vertex is x

  child[0] = IPMK_ArrayIndexAdd(elements, 3, newtri[0], err);
  IPMK_ArrayIndexAdd(parentelems, 1, &elemidx, err);
  IPMK_ArrayIndexAdd(activeelems, 1, &child[0], err);

  child[1] = IPMK_ArrayIndexAdd(elements, 3, newtri[1], err);
  IPMK_ArrayIndexAdd(parentelems, 1, &elemidx, err);
  IPMK_ArrayIndexAdd(activeelems, 1, &child[1], err);

  child[2] = IPMK_ArrayIndexAdd(elements, 3, newtri[2], err);
  IPMK_ArrayIndexAdd(parentelems, 1, &elemidx, err);
  IPMK_ArrayIndexAdd(activeelems, 1, &child[2], err);

  child[3] = IPMK_ArrayIndexAdd(elements, 3, newtri[3], err);
  IPMK_ArrayIndexAdd(parentelems, 1, &elemidx, err);
  IPMK_ArrayIndexAdd(activeelems, 1, &child[3], err);

  IPMK_ArrayIndexRemove(activeelems, activeidx, err);
}

/**
 * The subroutine refines a quadlilateral element with index i in the elements array
 */
void RefineQuad(IPMK_Index activeidx, IPMK_Index elemidx,  IPMK_Index *parentedges,
  IPMK_Index *mychildedges, IPMK_Index *midpoints, IPMK_Index *verts,
  IPMK_ArrayNumeric u_old, IPMK_ArrayNumeric vertices,
  IPMK_ArrayNumeric edgelevels, IPMK_ArrayIndex edges,
  IPMK_ArrayIndex parentelems, IPMK_ArrayIndex elements,
  IPMK_ArrayIndex activeelems, IPMK_Error *err)
{
  IPMK_ArrayLocalIter iter;
  const IPMK_Index *myedges = IPMK_ArrayIndexGet(elements, elemidx, err); // edges of the element
  IPMK_Index e[22] = {IPM_INDEX_NULL}; // A quad can have up to 22 different subedges
  IPMK_Index ab, bc, cd, da; // Four edges of the triangle
  IPMK_Index a, b, c, d, w, x, y, z; // Four vertices and four potential middle points

  // Get the structure of the element. a, b, c d are its four vertices.  ab, bc,
  // cd, da are its edges. w, x, y, z are middle points of edge ab, bc, cd, da
  // respectively. e[0..21] are sub-edge that might by generated during
  // refinement. Note that the midpoints and child edges do not nessesarily
  // all exist. If one does not exist, its value will be IPM_INDEX_NULL.
  //
  //        e7        e6
  //   a---------z---------d
  //   |                   |
  //   |                   |
  // e0|                   |e5
  //   |                   |
  //   w                   y
  //   |                   |
  //   |                   |
  // e1|                   |e4
  //   |                   |
  //   b---------x---------c
  //        e2       e3
  a = verts[0];
  b = verts[1];
  c = verts[2];
  d = verts[3];
  ab = parentedges[0];
  bc = parentedges[1];
  cd = parentedges[2];
  da = parentedges[3];
  w = midpoints[0];
  x = midpoints[1];
  y = midpoints[2];
  z = midpoints[3];
  for (int i = 0; i < 8; i++) e[i] = mychildedges[i];

  // New elements must be created with edges ordered in counterclockwise direction.
  // For example, if an element has edges e0, e1, e2, then e0 -> e1 -> e2 must be
  // in counterclockwise direction. Note that that does not necessarily mean each
  // edge is also in counterclockwise direction, e.g., if e0 = (a, b), a->b might
  // be in clockwise direction. We need to correctly handle these variations.
  int edges_split = (w == IPM_INDEX_NULL ? 0 : 1) +
                    (x == IPM_INDEX_NULL ? 0 : 1) +
                    (y == IPM_INDEX_NULL ? 0 : 1) +
                    (z == IPM_INDEX_NULL ? 0 : 1);

  IPM_Index v;
  IPM_Real x1, y1, x2, y2, x3, y3, x4, y4, xy[2], *coord;
  IPMK_Index newedge[2]; // Temp variable for a new interior edge
  IPMK_Index newtri[4][3], newquad[4][4]; // Temp variable for possible new triangles or quads
  IPMK_Index child[4]; // Indices of (at most) the four children after refinement
  int got_2tri1quad = 0;
  IPM_Real ua, ub, uc, ud, uv;

  assert(edges_split == 4);

  // All four edges have been split. Refine the quad as follows
  //
  //        e7        e6
  //   a---------z---------d
  //   |         |         |
  //   |         e21       |
  // e0|         |         |e5
  //   |         |         |
  //   w----e18--v---e20---y
  //   |         |         |
  //   |         e19       |
  // e1|         |         |e4
  //   |         |         |
  //   b---------x---------c
  //        e2       e3
  coord = IPMK_ArrayNumericGet(vertices, a, err); x1 = coord[0]; y1 = coord[1];
  coord = IPMK_ArrayNumericGet(vertices, b, err); x2 = coord[0]; y2 = coord[1];
  coord = IPMK_ArrayNumericGet(vertices, c, err); x3 = coord[0]; y3 = coord[1];
  coord = IPMK_ArrayNumericGet(vertices, d, err); x4 = coord[0]; y4 = coord[1];
  xy[0] = (x1 + x2 + x3 + x4)/4.0;
  xy[1] = (y1 + y2 + y3 + y4)/4.0;

  ua = *IPMK_ArrayNumericGet(u_old, a, err);
  ub = *IPMK_ArrayNumericGet(u_old, b, err);
  uc = *IPMK_ArrayNumericGet(u_old, c, err);
  ud = *IPMK_ArrayNumericGet(u_old, d, err);
  uv = (ua + ub + uc + ud)/4.0;
  //uv = exact_solution(xy[0], xy[1], curtime - dt); // : (ua + ub + uc + ud)/4.0;

  v = IPMK_ArrayNumericAdd(vertices, 2, xy, err);
  IPMK_ArrayNumericAdd(u_old, 1, &uv, err);

  newedge[0] = w; newedge[1] = v; e[18] = IPMK_ArrayIndexAdd(edges, 2, newedge, err);
  newedge[0] = x; newedge[1] = v; e[19] = IPMK_ArrayIndexAdd(edges, 2, newedge, err);
  newedge[0] = y; newedge[1] = v; e[20] = IPMK_ArrayIndexAdd(edges, 2, newedge, err);
  newedge[0] = z; newedge[1] = v; e[21] = IPMK_ArrayIndexAdd(edges, 2, newedge, err);

  // Set level for new-born child edges
  IPM_Real level = *IPMK_ArrayNumericGet(edgelevels, ab, err);
  level += 1.0;
  IPMK_ArrayNumericAdd(edgelevels, 1, &level, err);
  IPMK_ArrayNumericAdd(edgelevels, 1, &level, err);
  IPMK_ArrayNumericAdd(edgelevels, 1, &level, err);
  IPMK_ArrayNumericAdd(edgelevels, 1, &level, err);

  newquad[0][0] = e[0]; newquad[0][1] = e[18]; newquad[0][2] = e[21]; newquad[0][3] = e[7];
  newquad[1][0] = e[1]; newquad[1][1] = e[2];  newquad[1][2] = e[19]; newquad[1][3] = e[18];
  newquad[2][0] = e[6]; newquad[2][1] = e[21];  newquad[2][2] = e[20]; newquad[2][3] = e[5];
  newquad[3][0] = e[4]; newquad[3][1] = e[20];  newquad[3][2] = e[19]; newquad[3][3] = e[3];

  child[0] = IPMK_ArrayIndexAdd(elements, 4, newquad[0], err);
  IPMK_ArrayIndexAdd(parentelems, 1, &elemidx, err);
  IPMK_ArrayIndexAdd(activeelems, 1, &child[0], err);

  child[1] = IPMK_ArrayIndexAdd(elements, 4, newquad[1], err);
  IPMK_ArrayIndexAdd(parentelems, 1, &elemidx, err);
  IPMK_ArrayIndexAdd(activeelems, 1, &child[1], err);

  child[2] = IPMK_ArrayIndexAdd(elements, 4, newquad[2], err);
  IPMK_ArrayIndexAdd(parentelems, 1, &elemidx, err);
  IPMK_ArrayIndexAdd(activeelems, 1, &child[2], err);

  child[3] = IPMK_ArrayIndexAdd(elements, 4, newquad[3], err);
  IPMK_ArrayIndexAdd(parentelems, 1, &elemidx, err);
  IPMK_ArrayIndexAdd(activeelems, 1, &child[3], err);

  IPMK_ArrayIndexRemove(activeelems, activeidx, err);

}

void kRefineElements(IPMK_ArrayNumeric u_old, IPMK_ArrayNumeric vertices,
  IPMK_ArrayNumeric edgelevels, IPMK_ArrayIndex childedges, IPMK_ArrayIndex edges,
  IPMK_ArrayIndex parentelems, IPMK_ArrayIndex elements,
  IPMK_ArrayNumeric elemrefineflags, IPMK_ArrayIndex activeelems, IPMK_Error *err)
{
  IPMK_ArrayLocalIter iter;
  for (IPMK_ArrayLocalIterInit(activeelems, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPM_Index p = IPMK_ArrayLocalIterToIndex(&iter); // p is index to activeelems

    if (*IPMK_ArrayNumericGet(elemrefineflags, p, err) < 0.5) continue;

    IPM_Index elemidx = *IPMK_ArrayIndexGet(activeelems, p, err); // value at p is index to elements
    const int num_edges = IPM_ArrayGetLength(elements, elemidx, err);
    const IPMK_Index *myedges = IPMK_ArrayIndexGet(elements, elemidx, err);
    IPMK_Index edgevert[4][2]; // Store the two vertices of each edge
    IPMK_Index parentedges[4]; // the element's edges if they are not split
    IPMK_Index mychildedges[4][2]; // child edges of my edges if they are split
    IPMK_Index verts[4]; // vertices of the element in counterclockwise direction
    IPMK_Index midpoints[4]; // mid-points of edges
    IPMK_Index tmpidx;

    assert(num_edges == 3 || num_edges == 4);

    // Get vertices, childedges and midpoints of the element in its original direction
    for (int i = 0; i < num_edges; i++) {
      IPMK_Index *children, *ends;
      children = IPMK_ArrayIndexGet(childedges, myedges[i], err);
      parentedges[i] = myedges[i];
      mychildedges[i][0] = children[0];
      mychildedges[i][1] = children[1];

      ends = IPMK_ArrayIndexGet(edges, mychildedges[i][0], err);
      edgevert[i][0] = ends[0];
      midpoints[i] = ends[1];
      ends = IPMK_ArrayIndexGet(edges, mychildedges[i][1], err);
      assert(midpoints[i] == ends[0]);
      edgevert[i][1] = ends[1];
    }

    // Order vertices, childedges in the same (counterclockwise) direction as the edges

    // Firstly, if edge 0's direction is opposite to the element's direction, reverse edge 0
    if (edgevert[0][1] != edgevert[1][0] && edgevert[0][1] != edgevert[1][1]) {
      tmpidx = edgevert[0][0]; // swap edgevert[0][0] and edgevert[0][1]
      edgevert[0][0] = edgevert[0][1];
      edgevert[0][1] = tmpidx;
      tmpidx = mychildedges[0][0]; // swap childedges[0][0] and childedges[0][1]
      mychildedges[0][0] = mychildedges[0][1];
      mychildedges[0][1] = tmpidx;
    }

    // Secondly, reverse remaining edges if they are opposite to the element's direction
    for (int i = 1; i < num_edges; i++) {
      if (edgevert[i][0] != edgevert[i-1][1]) {
        tmpidx = edgevert[i][0]; // swap edgevert[i][0] and edgevert[i][1]
        edgevert[i][0] = edgevert[i][1];
        edgevert[i][1] = tmpidx;
        tmpidx = mychildedges[i][0]; // swap childedges[i][0] and childedges[i][1]
        mychildedges[i][0] = mychildedges[i][1];
        mychildedges[i][1] = tmpidx;
      }
    }

    // Now, we can easily get vertices of the element in counterclockwise direction
    for (int i = 0; i < num_edges; i++) verts[i] = edgevert[i][0];

    if (num_edges == 3) RefineTriangle(p, elemidx, parentedges, mychildedges, midpoints, verts, vertices, edgelevels, edges, parentelems, elements, activeelems, err);
    else if (num_edges == 4) RefineQuad(p, elemidx, parentedges, mychildedges, midpoints, verts, u_old, vertices, edgelevels, edges, parentelems, elements, activeelems, err);
    else { abort(); }
  }
}


void kAssemble(IPMK_ArrayNumeric Kvert,  IPMK_ArrayNumeric Kedge,
  IPMK_ArrayNumeric Kelem, IPMK_ArrayNumeric f,
  IPMK_ArrayNumeric vboundary, IPMK_ArrayNumeric u_old, IPMK_ArrayNumeric vertices,
  IPMK_ArrayIndex childedges, IPMK_ArrayIndex edges, IPMK_ArrayIndex elements, IPMK_ArrayIndex activeelems, IPMK_Error *err)
{
  FE *fe, *fe_face;
  IPMK_ArrayLocalIter iter;

  for (IPMK_ArrayLocalIterInit(activeelems, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPM_Index p = IPMK_ArrayLocalIterToIndex(&iter); // p is index to activeelems
    IPM_Index e = *IPMK_ArrayIndexGet(activeelems, p, err); // value at p is index to elements
    const int num_edges = IPM_ArrayGetLength(elements, e, err);
    const IPMK_Index *edgeidx = IPMK_ArrayIndexGet(elements, e, err);
    IPMK_Index vertidx[4] = {IPM_INDEX_NULL}; // Vertices of the element in a proper order
    IPM_Real *vertcoord[4] = {NULL}; // Pointers to coordinates of the vertices
    IPMK_Index edgevert[8]; // Store vertices of edge 0~2 or 0~3
    int edgevertord[8]; // Order of the vertices according to the order in vertidx[]
    IPM_Real Ke[4][4] = {.0}; // elemental matrix
    IPM_Real Fe[4] = {.0}; // RHS
    int onbdary[4] = {0}; // Is this vertex on boundary?
    IPM_Real u_old_val[4];

    const int num_verts = num_edges; // True for triangles and quads

    assert(num_edges == 3 || num_edges == 4);

    // Store all vertices in a temp array to facilitate later accessing
    for (int i = 0; i < num_edges; i++) {
      IPMK_Index *ends = IPMK_ArrayIndexGet(edges, edgeidx[i], err);
      edgevert[2*i] = ends[0];
      edgevert[2*i+1] = ends[1];
    }

    // Go over vertices of the element and order them in a ring
    vertidx[0] = edgevert[0];
    vertidx[1] = edgevert[1];

    // If edge 0 is opposite to the direction of the element, we need to swap vertidx[0] and vertidx[1]
    if (edgevert[2] != vertidx[1] && edgevert[3] != vertidx[1]) {
      // It implies edge 1 must be connected to the tail of edge 0, i.e., verts[0]!
      IPMK_Index tmp = vertidx[0];
      vertidx[0] = vertidx[1];
      vertidx[1] = tmp;
    }

    // Found other vertices in the direction of vertidx[0]->vertidx[1]
    for (int i = 1; i < num_verts - 1; i++) {
      if (edgevert[2*i] != vertidx[i]) {
        vertidx[i+1] = edgevert[2*i]; assert(edgevert[2*i+1] == vertidx[i]);
      } else {
        vertidx[i+1] = edgevert[2*i+1]; assert(edgevert[2*i] == vertidx[i]);
      }
    }

    // Look up and store order of vertices of each edge
    for (int i = 0; i < num_edges*2; i++) { // Loop over edgevert[]
      for (int j = 0; j < num_verts; j++) {
        if (edgevert[i] == vertidx[j]) {
          edgevertord[i] =j;
          break;
        }
      }
    }

    for (int i = 0; i < num_verts; i++) {
      IPM_Real vbdary = *IPMK_ArrayNumericGet(vboundary, vertidx[i], err);
      if (vbdary > 0.5) onbdary[i] = 1;
    }

    IPM_Real nodes[4][2] = {{0}}; // nodes[][] is for debugging purpose
    for (int i = 0; i < num_verts; i++) {
      vertcoord[i] = IPMK_ArrayNumericGet(vertices, vertidx[i], err);
      u_old_val[i] = *IPMK_ArrayNumericGet(u_old, vertidx[i], err);
      nodes[i][0] = vertcoord[i][0];
      nodes[i][1] = vertcoord[i][1];
    }

    fe = (num_verts == 3? &trife : &quadfe);
    InitFEOnReal(fe, vertcoord);

    // Assemble the elemental matrix and RHS
    for (int qp = 0; qp < fe->nqp; qp++) {
      // Get old values
      IPM_Real my_u_old = 0.0;
      IPM_Real grad_u_old[2] = {0.0};
      for (int i = 0; i < num_verts; i++) {
        my_u_old += PHI(i, qp)*u_old_val[i];
        grad_u_old[0] += DPHI(i, qp, 0)*u_old_val[i];
        grad_u_old[1] += DPHI(i, qp, 1)*u_old_val[i];
      }

      for (int i = 0; i < fe->nphi; i++) {
        // The RHS contribution
        if (!onbdary[i]) {
          Fe[i] += fe->JxW[qp]*(my_u_old*PHI(i, qp)
                                - 0.5*dt*(
                                    DOT(grad_u_old, velocity)*PHI(i, qp)
                                    + diffusivity*DOT(grad_u_old, &DPHI(i, qp, 0))));
        }

        // The matrix contribution
        for (int j = 0; j < fe->nphi; j++) {
          //if (!onbdary[i] && !onbdary[j]) {
            Ke[i][j] += fe->JxW[qp]*(PHI(i, qp)*PHI(j, qp) +
                                     + 0.5*dt*(
                                        DOT(velocity, &DPHI(j, qp, 0))*PHI(i, qp)
                                        + diffusivity*DOT(&DPHI(i, qp, 0), &DPHI(j, qp, 0))));
          //}
        }
      }
    }

#if 0
    // Impose boundary conditions with the penalty method
    const IPM_Real penalty = 1.e10;
    for (int k = 0; k < num_edges; k++) {
      int ebdary = *IPMK_ArrayNumericGet(eboundary, edgeidx[k], err);
      if (ebdary) {
        if (num_edges == 3) {
          InitTriFaceBC(k, vertcoord);
          fe = &triface;
        } else if (num_edges == 4) {
          InitQuadFaceBC(k, vertcoord);
          fe = &quadface;
        }

        for (int qp = 0; qp < fe->nqp; qp++) {
          IPM_Real value = exact_solution(fe->xy[qp][0], fe->xy[qp][1], curtime);

          // RHS contribution
          for (int i = 0; i < fe->nphi; i++) {
            Fe[i] += penalty*fe->JxW[qp]*value*PHI(i, qp);
          }

          // Matrix contribution
          for (int i = 0; i < fe->nphi; i++) {
            for (int j = 0; j < fe->nphi; j++) {
              Ke[i][j] += penalty*fe->JxW[qp]*PHI(i, qp)*PHI(j, qp);
            }
          }
        }
      }
    }
//#else
    // Impose Dierichlet BC with the unit diagonal term mothod
    // Set RHS of boundary DOFs as exact values
    for (int i = 0; i < fe->nphi; i++) {
      if (onbdary[i]) Fe[i] = exact_solution(vertcoord[i][0], vertcoord[i][1], curtime);
    }

    // Revise RHS of interior DOFs accordingly
    for (int i = 0; i < fe->nphi; i++) {
      if (!onbdary[i]) {
        for (int j = 0; j < fe->nphi; j++) if (onbdary[j]) Fe[i] -= Ke[i][j]*Fe[j];
      }
    }

    // Revise Ke, making a boundary vertex's row/column all zero except the diagonal be 1
    for (int i = 0; i < fe->nphi; i++) {
      if (onbdary[i]) Ke[i][i] = 1.0;
      for (int j = i+1; j < fe->nphi; j++) {
        if (onbdary[i] || onbdary[j]) { Ke[i][j] = Ke[j][i] = 0; }
      }
    }
#endif

    // Distribute Ke back to Kvert, Kedge and Kelem
    for (int i = 0; i < num_verts; i++) {
      IPM_Real *val = IPMK_ArrayNumericGet(Kvert, vertidx[i], err);
      val[0] += Ke[i][i];
    }

    for (int k = 0; k < num_edges; k++) {
      int i = edgevertord[2*k];
      int j = edgevertord[2*k+1];
      IPM_Real *val = IPMK_ArrayNumericGet(Kedge, edgeidx[k], err);
      val[0] += Ke[i][j];
      val[1] += Ke[j][i];
    }

    if (num_edges == 4) {
      IPM_Real val[4] = {.0};
      for (int i = 0; i < num_verts; i++) val[i] = Ke[i][(i+2)%4]; // Diagonals of a quad.
      IPMK_ArrayNumericAdd(Kelem, 4, val, err);
    } else {
      IPMK_ArrayNumericAdd(Kelem, 0, NULL, err); // ATTENTION: add a 0-sized entry
    }

    // Accumlate Fe to f
    for (int i = 0; i < num_verts; i++) {
      IPM_Real *val = IPMK_ArrayNumericGet(f, vertidx[i], err);
      val[0] += Fe[i];
    }
  }

}

/**
 * Compute dot product of two array x, y, assuming x, y are clones of the same
 * original and their entry length is fixed as 1.
 */
void kDot(IPMK_RArray product, IPMK_ArrayNumeric x, IPMK_ArrayNumeric y, IPMK_Error *err)
{
  IPMK_ArrayLocalIter iter;
  IPM_Real sum = 0.0;

#if 0
  for (IPMK_ArrayLocalIterInit(x, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPMK_Index i = IPMK_ArrayLocalIterToIndex(&iter); // index of the entry
    IPM_Real xval = *IPMK_ArrayNumericGet(x, i, err);
    IPM_Real yval = *IPMK_ArrayNumericGet(y, i, err);
    sum += xval*yval;
  }
#else
  //IPM_Real *xp = IPMK_ArrayNumericGet(x, 0, err);
  //IPM_Real *yp = IPMK_ArrayNumericGet(y, 0, err);
  //int sz = IPM_ArrayLocalSize(x, err);

  //for (int i = 0; i < sz; i++) sum += xp[i]*yp[i];
  sum = IPM_ArrayNumericInnerProduct(x, y, err);
#endif

  IPM_Real *p = IPMK_RArrayGet(product, err);
  p[0] = sum;
}

void kComputeNodalErrors(IPMK_ArrayNumeric e, IPMK_ArrayNumeric vertices, IPMK_ArrayNumeric u, IPMK_Error *err)
{
  IPMK_ArrayVisibleIter iter;
  IPM_Real sum = 0.0;
  for (IPMK_ArrayVisibleIterInit(vertices, &iter); !IPMK_ArrayVisibleIterDone(&iter); IPMK_ArrayVisibleIterNext(&iter)) {
    IPMK_Index i = IPMK_ArrayVisibleIterToIndex(&iter); // index of the entry
    IPM_Real *xy = IPMK_ArrayNumericGet(vertices, i, err);
    IPM_Real uapprox = *IPMK_ArrayNumericGet(u, i, err);
    IPM_Real *eptr = IPMK_ArrayNumericGet(e, i, err);
    IPM_Real uexact = exact_solution(xy[0], xy[1], curtime);
    *eptr = uapprox - uexact;
  }
}

// Comment out since we should always create context for SpMV
/**
 * The kernel computes y = Kx
 * y will be overwritten.
 *
 */
void kSpMV(IPMK_ArrayNumeric y, IPMK_ArrayNumeric x, IPMK_ArrayNumeric Kvert, IPMK_ArrayIndex vertices,
  IPMK_ArrayNumeric Kedge, IPMK_ArrayIndex childedges, IPMK_ArrayIndex edges,
  IPMK_ArrayNumeric Kelem, IPMK_ArrayIndex elements, IPMK_ArrayIndex activeelems, IPMK_Error *err)
{
  IPM_ArrayNumericSet(y, 0.0, err);

  // Loop over local vertices
  IPMK_ArrayLocalIter iter;
  for (IPMK_ArrayLocalIterInit(vertices, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPMK_Index i = IPMK_ArrayLocalIterToIndex(&iter); // index of the entry
    IPM_Real *xi = IPMK_ArrayNumericGet(x, i, err);
    IPM_Real *yi = IPMK_ArrayNumericGet(y, i, err);
    IPM_Real kii = *IPMK_ArrayNumericGet(Kvert, i, err);
    yi[0] += kii*xi[0];
  }

  // Loop over local edges (shall we skip edges other than of active elements?)
  for (IPMK_ArrayLocalIterInit(edges, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPMK_Index e = IPMK_ArrayLocalIterToIndex(&iter); // index of the entry
    IPMK_Index *children = IPMK_ArrayIndexGet(childedges, e, err);
    if (children[0] != IPM_INDEX_NULL) continue; // not an edge of active elements

    const IPMK_Index *ends = IPMK_ArrayIndexGet(edges, e, err); // indices of vertices of the edge
    IPM_Index i = ends[0];
    IPM_Index j = ends[1];
    IPM_Real *xi = IPMK_ArrayNumericGet(x, i, err);
    IPM_Real *xj = IPMK_ArrayNumericGet(x, j, err);
    IPM_Real *yi = IPMK_ArrayNumericGet(y, i, err);
    IPM_Real *yj = IPMK_ArrayNumericGet(y, j, err);
    IPM_Real *fp = IPMK_ArrayNumericGet(Kedge, e, err);
    IPM_Real kij = fp[0];
    IPM_Real kji = fp[1];

    yi[0] += kij*xj[0];
    yj[0] += kji*xi[0];
  }

  // Loop over local quad elements to accumlate contributions on internal edges
  for (IPMK_ArrayLocalIterInit(activeelems, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPM_Index p = IPMK_ArrayLocalIterToIndex(&iter); // p is an index to activeelems
    IPM_Index e = *IPMK_ArrayIndexGet(activeelems, p, err); // value at p
    const int num_edges = IPM_ArrayGetLength(elements, e, err);

    assert(num_edges == 3 || num_edges == 4);
    if (num_edges == 3) continue; // Only quadrilaterals have internal edges

    const IPMK_Index *edgeidx = IPMK_ArrayIndexGet(elements, e, err);
    IPMK_Index vertidx[4] = {IPM_INDEX_NULL}; // Vertices of the element in a proper order
    IPMK_Index edgevert[8]; // Store vertices of edge 0~2 or 0~3
    const int num_verts = num_edges; // True for triangles and quads

    // Store all vertices in a temp array to facilitate later accessing
    for (int i = 0; i < num_edges; i++) {
      IPMK_Index *ends = IPMK_ArrayIndexGet(edges, edgeidx[i], err);
      edgevert[2*i] = ends[0];
      edgevert[2*i+1] = ends[1];
    }

    // Go over vertices of the element and order them in a ring (significant for quadrilaterals).
    vertidx[0] = edgevert[0];
    vertidx[1] = edgevert[1];

    // If edge 0 is opposite to the direction of the element, we need to swap vertidx[0] and vertidx[1]
    if (edgevert[2] != vertidx[1] && edgevert[3] != vertidx[1]) {
      // It implies edge 1 must be connected to the tail of edge 0, i.e., verts[0]!
      IPMK_Index tmp = vertidx[0];
      vertidx[0] = vertidx[1];
      vertidx[1] = tmp;
    }

    // Found other vertices in the direction of vertidx[0]->vertidx[1]
    for (int i = 1; i < num_verts - 1; i++) {
      if (edgevert[2*i] != vertidx[i]) {
        vertidx[i+1] = edgevert[2*i]; assert(edgevert[2*i+1] == vertidx[i]);
      } else {
        vertidx[i+1] = edgevert[2*i+1]; assert(edgevert[2*i] == vertidx[i]);
      }
    }

    // Do contribution on internal edges
    IPM_Real *ke = IPMK_ArrayNumericGet(Kelem, p, err);
    for (int i = 0; i < num_verts; i++) {
      int j = (i + 2)%4;
      IPM_Real *yi = IPMK_ArrayNumericGet(y, vertidx[i], err);
      IPM_Real *xj = IPMK_ArrayNumericGet(x, vertidx[j], err);
      yi[0] += ke[i]*xj[0];
    }
  }
}

typedef struct {
  int rows;
  IPMK_Index *rowptr;
  IPMK_Index *colidx;
  IPM_Real *val;
} CSR;

static void DestructCSR(CSR* csr)
{
  assert(csr);
  free(csr->rowptr);
  free(csr->colidx);
  free(csr->val);
}

static void DumpCSR(CSR *csr)
{
  int n = csr->rows;
  IPM_Real *k = (IPM_Real *)calloc(n*n, sizeof(IPM_Real));

  for (int i = 0; i < csr->rows; i++) {
    //printf("%d :", i);
    for (int j = csr->rowptr[i]; j < csr->rowptr[i+1]; j++) {
      //printf(" (%d, %f)", csr->colidx[j], csr->val[j]);
      k[i*n + csr->colidx[j]] = csr->val[j];
    }
    //printf("\n");
  }

  for (int i = 0; i < csr->rows; i++) {
    printf("%3d :", i);
    for (int j = 0; j < csr->rows; j++) {
      if (k[i*n+j] == 0) printf(" 0");
      else printf(" %f", k[i*n+j]);
    }
    printf("\n");
  }
}

typedef struct {
  IPMK_Index node; // node is the hanging nodes
  IPMK_Index ends[2]; // node is the middle point of the edge with ends[0~1]
  IPM_Real level; // level of the edge
} hanging_node_t;

static UT_icd hanging_node_icd = {sizeof(hanging_node_t), NULL, NULL, NULL};

typedef struct {
  IPMK_Index unconstrained_node;
  IPM_Real coeff;
} constraint_t;

static UT_icd constraint_icd = {sizeof(constraint_t), NULL, NULL, NULL};

// Map a constrained node to its constraints
typedef struct {
  IPMK_Index constrained_node;
  UT_array *constraints; // array of constraint_t
  UT_hash_handle hh;
} constraints_map_t;

typedef struct {
  IPMK_Index first;
  IPMK_Index second;
} index_pair_t;

// Map (i, j) to kij
typedef struct {
  index_pair_t ij;
  IPM_Real kij;
  UT_hash_handle hh;
} kij_map_t;

static int hanging_node_cmp(void *_n1, void *_n2)
{
  hanging_node_t *n1 = (hanging_node_t *)_n1;
  hanging_node_t *n2 = (hanging_node_t *)_n2;
  IPM_Real diff = n1->level - n2->level;
  return (diff < -0.5) ? -1 : (diff > 0.5) ? 1 : 0;
}

static int kij_map_cmp(void *_n1, void *_n2)
{
  kij_map_t *n1 = (kij_map_t *)_n1;
  kij_map_t *n2 = (kij_map_t *)_n2;

  if (n1->ij.first != n2->ij.first) return n1->ij.first - n2->ij.first;
  else return n1->ij.second - n2->ij.second;
}


// Insert Kij to the sparse matrix if you are sure Kji does not exist in the map
static void InsertEntryToMatrix(IPMK_Index i, IPMK_Index j, IPM_Real kij, kij_map_t **kij_map)
{
    index_pair_t ij;
    ij.first = i;
    ij.second = j;

    kij_map_t *entry = (kij_map_t *)malloc(sizeof(kij_map_t));
    entry->ij = ij;
    entry->kij = kij;
    HASH_ADD(hh, *kij_map, ij /* field name */, sizeof(index_pair_t), entry);
}


// Add Kij to the sparse matrix. If Kij already exists, add the value to it. Otherwise create a new entry
static void AddEntryToMatrix(IPMK_Index i, IPMK_Index j, IPM_Real kij, kij_map_t **kij_map)
{
    kij_map_t *out;
    index_pair_t ij;

    ij.first = i;
    ij.second = j;
    HASH_FIND(hh, *kij_map, &ij, sizeof(index_pair_t), out);
    if (out == NULL) {
      kij_map_t *entry = (kij_map_t *)malloc(sizeof(kij_map_t));
      entry->ij = ij;
      entry->kij = kij;
      HASH_ADD(hh, *kij_map, ij /* field name */, sizeof(index_pair_t), entry);
    } else {
      out->kij += kij;
    }
}

// Apply constraints to Kii
void ApplyConstraintsToKii(IPMK_Index i, IPM_Real kii, kij_map_t **kij_map, const constraints_map_t *constraints_map)
{
  constraints_map_t *i_constraints_out;

  HASH_FIND(hh, constraints_map, &i, sizeof(IPMK_Index), i_constraints_out);
  if (i_constraints_out == NULL) {
    AddEntryToMatrix(i, i, kii, kij_map);
  } else {
    for (int p = 0; p < utarray_len(i_constraints_out->constraints); p++) {
      constraint_t *p_constraint = (constraint_t *)utarray_eltptr(i_constraints_out->constraints, p);
      for (int q = 0; q < utarray_len(i_constraints_out->constraints); q++) {
        constraint_t *q_constraint = (constraint_t *)utarray_eltptr(i_constraints_out->constraints, q);
        AddEntryToMatrix(p_constraint->unconstrained_node, q_constraint->unconstrained_node, p_constraint->coeff*q_constraint->coeff*kii, kij_map);
      }
    }
  }
}

// Apply constraints to Kii and Kji
void ApplyConstraintsToKijKji(IPMK_Index i, IPMK_Index j, IPM_Real kij, IPM_Real kji, kij_map_t **kij_map, const constraints_map_t *constraints_map)
{
  constraints_map_t *i_constraints_out, *j_constraints_out;

  HASH_FIND(hh, constraints_map, &i, sizeof(IPMK_Index), i_constraints_out);
  HASH_FIND(hh, constraints_map, &j, sizeof(IPMK_Index), j_constraints_out);

  if (i_constraints_out == NULL && j_constraints_out == NULL) {
    AddEntryToMatrix(i, j, kij, kij_map);
    AddEntryToMatrix(j, i, kji, kij_map);
  } else if (i_constraints_out != NULL && j_constraints_out == NULL) { // i is constrained
    for (int p = 0; p < utarray_len(i_constraints_out->constraints); p++) {
      constraint_t *constraint = (constraint_t *)utarray_eltptr(i_constraints_out->constraints, p);
      AddEntryToMatrix(constraint->unconstrained_node, j, constraint->coeff*kij, kij_map);
      AddEntryToMatrix(j, constraint->unconstrained_node, constraint->coeff*kji, kij_map);
    }
  } else if (i_constraints_out == NULL && j_constraints_out != NULL) { // j is constrained
    for (int p = 0; p < utarray_len(j_constraints_out->constraints); p++) {
      constraint_t *constraint = (constraint_t *)utarray_eltptr(j_constraints_out->constraints, p);
      AddEntryToMatrix(i, constraint->unconstrained_node, constraint->coeff*kij, kij_map);
      AddEntryToMatrix(constraint->unconstrained_node, i, constraint->coeff*kji, kij_map);
    }
  } else { // both i and j are constrained
    for (int p = 0; p < utarray_len(i_constraints_out->constraints); p++) {
      constraint_t *i_constraint = (constraint_t *)utarray_eltptr(i_constraints_out->constraints, p);
      for (int q = 0; q < utarray_len(j_constraints_out->constraints); q++) {
        constraint_t *j_constraint = (constraint_t *)utarray_eltptr(j_constraints_out->constraints, q);
        AddEntryToMatrix(i_constraint->unconstrained_node, j_constraint->unconstrained_node, i_constraint->coeff*j_constraint->coeff*kij, kij_map);
        AddEntryToMatrix(j_constraint->unconstrained_node, i_constraint->unconstrained_node, i_constraint->coeff*j_constraint->coeff*kji, kij_map);
      }
    }
  }
}

// Build constraints into a context and generate a diagonal with constraints considered.
//
//
static CSR * BuildContextWithHangingNodes(UT_array *hanging_nodes, IPMK_ArrayNumeric edgeactiveflags, IPMK_ArrayNumeric edgehangingflags,
  IPMK_ArrayNumeric vboundary,
  IPMK_ArrayNumeric Kvert,  IPMK_ArrayNumeric Kedge, IPMK_ArrayNumeric Kelem, IPMK_ArrayNumeric f,
  IPMK_ArrayNumeric vertices, IPMK_ArrayIndex childedges, IPMK_ArrayNumeric edgelevels,
  IPMK_ArrayIndex edges, IPMK_ArrayIndex elements, IPMK_ArrayIndex activeelems, IPMK_Error *err)
{
  IPMK_ArrayVisibleIter viter;
  IPMK_ArrayLocalIter liter;

  /* Build the constraint map, which maps a constrained node to
      a set of unconstrained nodes
   */
  constraints_map_t *constraints_map = NULL;

  // Sort hanging nodes by their level to easily handle recursive constraints
  utarray_sort(hanging_nodes, hanging_node_cmp);

  // Loop over the sorted hanging nodes to build a constraint map
  for (int i = 0; i < utarray_len(hanging_nodes); i++) {
    hanging_node_t *hanging_node = (hanging_node_t *)utarray_eltptr(hanging_nodes, i);
    constraints_map_t *constraints_row = (constraints_map_t *)malloc(sizeof(constraints_map_t));

    constraints_row->constrained_node = hanging_node->node;
    utarray_new(constraints_row->constraints, &constraint_icd); // alloc a constraints-row for this node
    for (int j = 0; j < 2; j++) { // Check the two ends to see if they are constrained too
      IPMK_Index this_end = hanging_node->ends[j];
      constraints_map_t *result;
      constraint_t tmp;

      // this_end has a smaller level than the current hanging node, so if it is constrained,
      // it should already be in the hash table.
      HASH_FIND(hh, constraints_map, &this_end, sizeof(IPMK_Index), result);

      if (result) { // Found it, so this end is constrained
        for (int k = 0; k < utarray_len(result->constraints); k++) {
          constraint_t *p = (constraint_t *)utarray_eltptr(result->constraints, k);
          tmp.unconstrained_node = p->unconstrained_node;
          tmp.coeff = 0.5 * p->coeff;
          utarray_push_back(constraints_row->constraints, &tmp);
        }
      } else { // this end is unconstrained, so we get 0.5 of it
          tmp.unconstrained_node = this_end;
          tmp.coeff = 0.5;
          utarray_push_back(constraints_row->constraints, &tmp);
      }
    }

    HASH_ADD(hh, constraints_map, constrained_node/* field name*/, sizeof(IPMK_Index), constraints_row);
  }

  /* Build a map for the sparse matrix which maps (i,j) to Kij with constraints considered.
      We loop over Kij's OWNED by this process. If i or/add j is constrained, we scatter value of Kij to Kpq's,
      where p's constrain i, and q's constrain j.
   */
  kij_map_t *kij_map = NULL;

  // Loop over active elements to access Kij's internal to elements
  for (IPMK_ArrayLocalIterInit(activeelems, &liter); !IPMK_ArrayLocalIterDone(&liter); IPMK_ArrayLocalIterNext(&liter)) {
    IPM_Index p = IPMK_ArrayLocalIterToIndex(&liter); // p is index to activeelems
    IPM_Index e = *IPMK_ArrayIndexGet(activeelems, p, err); // value at p is index to elements
    const int num_edges = IPM_ArrayGetLength(elements, e, err);

    assert(num_edges == 3 || num_edges == 4);
    if (num_edges == 3) continue; // Only quadrilaterals have internal edges

    const IPMK_Index *edgeidx = IPMK_ArrayIndexGet(elements, e, err);
    IPMK_Index vertidx[4] = {IPM_INDEX_NULL}; // Vertices of the element in a proper order
    IPMK_Index edgevert[8]; // Store vertices of edge 0~2 or 0~3
    const int num_verts = num_edges; // True for triangles and quads

    // Store all vertices in a temp array to facilitate later accessing
    for (int i = 0; i < num_edges; i++) {
      IPMK_Index *ends = IPMK_ArrayIndexGet(edges, edgeidx[i], err);
      edgevert[2*i] = ends[0];
      edgevert[2*i+1] = ends[1];
    }

    // Go over vertices of the element and order them in a ring (significant for quadrilaterals).
    vertidx[0] = edgevert[0];
    vertidx[1] = edgevert[1];

    // If edge 0 is opposite to the direction of the element, we need to swap vertidx[0] and vertidx[1]
    if (edgevert[2] != vertidx[1] && edgevert[3] != vertidx[1]) {
      // It implies edge 1 must be connected to the tail of edge 0, i.e., verts[0]!
      IPMK_Index tmp = vertidx[0];
      vertidx[0] = vertidx[1];
      vertidx[1] = tmp;
    }

    // Found other vertices in the direction of vertidx[0]->vertidx[1]
    for (int i = 1; i < num_verts - 1; i++) {
      if (edgevert[2*i] != vertidx[i]) {
        vertidx[i+1] = edgevert[2*i]; assert(edgevert[2*i+1] == vertidx[i]);
      } else {
        vertidx[i+1] = edgevert[2*i+1]; assert(edgevert[2*i] == vertidx[i]);
      }
    }

    IPM_Real *val = IPMK_ArrayNumericGet(Kelem, p, err);

    for (int k = 0; k < 2; k++) {
      IPM_Index i = vertidx[k];
      IPM_Index j = vertidx[k+2];
      ApplyConstraintsToKijKji(i, j, val[k], val[k+2], &kij_map, constraints_map);
    }
  }

  // Loop over active edges to access Kij on active edges (edges belonging to active elements)
  for (IPMK_ArrayLocalIterInit(edges, &liter); !IPMK_ArrayLocalIterDone(&liter); IPMK_ArrayLocalIterNext(&liter)) {
     IPM_Index e = IPMK_ArrayLocalIterToIndex(&liter); // index of the edge
     IPM_Real flag = *IPMK_ArrayNumericGet(edgeactiveflags, e, err);
     if (flag > 0.5) {
       const IPM_Index *ends = IPMK_ArrayIndexGet(edges, e, err); // indices of vertices of the edge
       IPM_Index i = ends[0];
       IPM_Index j = ends[1];
       IPM_Real *val = IPMK_ArrayNumericGet(Kedge, e, err);
       ApplyConstraintsToKijKji(i, j, val[0], val[1], &kij_map, constraints_map);
    }
  }

  // Loop over vertices to access Kii on vertices
  //
  // Also copy owned entries out of RHS f and put it in f2.
  // Other entries in f2 are zero, since we have no contributions to them
  int num_verts = IPM_ArrayVisibleSize(f, err);
  IPM_Real *f2 = (IPM_Real *)calloc(num_verts, sizeof(IPM_Real));

  for (IPMK_ArrayLocalIterInit(vertices, &liter); !IPMK_ArrayLocalIterDone(&liter); IPMK_ArrayLocalIterNext(&liter)) {
     IPM_Index i = IPMK_ArrayLocalIterToIndex(&liter); // index of the edge
     IPM_Real *val = IPMK_ArrayNumericGet(Kvert, i, err);
     ApplyConstraintsToKii(i, val[0], &kij_map, constraints_map);
     f2[i] = *IPMK_ArrayNumericGet(f, i, err);
  }

  // Adjust RHS to absorb contributions from hanging nodes
  for (IPMK_ArrayLocalIterInit(vertices, &liter); !IPMK_ArrayLocalIterDone(&liter); IPMK_ArrayLocalIterNext(&liter)) {
    IPM_Index i = IPMK_ArrayLocalIterToIndex(&liter); // index of the edge
    constraints_map_t *i_constraints_out;
    HASH_FIND(hh, constraints_map, &i, sizeof(IPMK_Index), i_constraints_out);
    if (i_constraints_out != NULL)  { // i is hanging
      // Scatter f[i] for hanging nodes
      for (int p = 0; p < utarray_len(i_constraints_out->constraints); p++) {
        constraint_t *i_constraint = (constraint_t *)utarray_eltptr(i_constraints_out->constraints, p);
        f2[i_constraint->unconstrained_node] += i_constraint->coeff * f2[i];
      }

      // Add a row in the matrix for the hanging node
      InsertEntryToMatrix(i, i, 1.0, &kij_map);
      for (int p = 0; p < utarray_len(i_constraints_out->constraints); p++) {
        constraint_t *i_constraint = (constraint_t *)utarray_eltptr(i_constraints_out->constraints, p);
        InsertEntryToMatrix(i, i_constraint->unconstrained_node, -i_constraint->coeff, &kij_map);
      }
      // Set RHS of the hanging node to 0
      f2[i] = 0;
    }
  }

  /* Now put the sparse matrix in CSR format
  */
  CSR *csr;
  {
    // Sort entries of kij_map in i-major and j-minor order
    HASH_SORT(kij_map, kij_map_cmp);

    csr = (CSR *)malloc(sizeof(CSR));
    IPM_Index rows = IPM_ArrayVisibleSize(vertices, err);
    IPM_Index nnz = HASH_COUNT(kij_map);

    csr->rows = rows;
    csr->rowptr = (IPMK_Index *)calloc(rows+1, sizeof(IPM_Index)); // Initially, it is an array of counters for nonzeros per row
    csr->colidx = (IPMK_Index *)malloc(sizeof(IPMK_Index)*nnz);
    csr->val= (IPM_Real *)malloc(sizeof(IPM_Real)*nnz);

    IPM_Index cnt = 0;
    for (kij_map_t *iter = kij_map; iter != NULL; iter = iter->hh.next) {
      IPM_Index i, j;
      i = iter->ij.first;
      j = iter->ij.second;

      int ibdary = *IPMK_ArrayNumericGet(vboundary, i, err) > 0.5 ? 1 : 0;
      int jbdary = *IPMK_ArrayNumericGet(vboundary, j, err) > 0.5 ? 1 : 0;

      if (ibdary && i != j) {
        // ignore kij, in effect set kij to 0
      } else if (ibdary && i == j) { // Set kii to 1 for DOFs on boundary and fi to exact
        csr->rowptr[i+1]++;
        csr->colidx[cnt] = j;
        csr->val[cnt] = 1.0;
        cnt++;
        iter->kij = 1;
        IPM_Real *xy = IPMK_ArrayNumericGet(vertices, i, err);
        f2[i] = exact_solution(xy[0], xy[1], curtime);
      } else if (jbdary) {
        IPM_Real *xy = IPMK_ArrayNumericGet(vertices, j, err);
        f2[i] -= iter->kij * exact_solution(xy[0], xy[1], curtime);
      } else {
        csr->rowptr[i+1]++; // currently rowptr[] only counts nonzeros per row
        csr->colidx[cnt] = j;
        csr->val[cnt] = iter->kij;
        cnt++;
      }
    }

    // Adjust rowptr[] to CSR
    for (int i = 1; i < rows+1; i++) csr->rowptr[i] += csr->rowptr[i-1];
  }


  /* Copy diagonal values to Kvert. Also, restore f with f2
  */
  for (IPMK_ArrayVisibleIterInit(vertices, &viter); !IPMK_ArrayVisibleIterDone(&viter); IPMK_ArrayVisibleIterNext(&viter)) {
    IPM_Index i = IPMK_ArrayVisibleIterToIndex(&viter); // index of the edge
    index_pair_t ii = (index_pair_t){i, i};
    kij_map_t *entry_out;
    HASH_FIND(hh, kij_map, &ii, sizeof(index_pair_t), entry_out);
    IPM_Real mycontribution = (entry_out != NULL ? entry_out->kij : 0);
    *IPMK_ArrayNumericGet(Kvert, i, err) = mycontribution;
    *IPMK_ArrayNumericGet(f, i, err) = f2[i];
  }

  // Free temp data structures
  free(f2);

  {
    kij_map_t *current_kij, *tmp;
    HASH_ITER(hh, kij_map, current_kij, tmp) {
      HASH_DEL(kij_map, current_kij); // delete & free
      free(current_kij);
    }
  }

  {
    constraints_map_t *current_constraints, *tmp;
    HASH_ITER(hh, constraints_map, current_constraints, tmp) {
      HASH_DEL(constraints_map, current_constraints); // delete & free
      utarray_free(current_constraints->constraints);
      free(current_constraints);
    }
  }

  //DumpCSR(csr);

  return csr;
}

static CSR *BuildContextWithoutHangingNodes(IPMK_ArrayNumeric f, IPMK_ArrayNumeric Kvert, IPMK_ArrayIndex vertices,
  IPMK_ArrayNumeric Kedge, IPMK_ArrayIndex childedges, IPMK_ArrayIndex edges,
  IPMK_ArrayNumeric Kelem, IPMK_ArrayIndex elements, IPMK_ArrayIndex activeelems, IPMK_Error *err)
{
  CSR *csr;
  IPM_Index rows;
  IPM_Index nnz;
  IPM_Index *rowptr;
  IPM_Index *colidx;
  IPM_Real *val;
  IPMK_ArrayLocalIter iter;

  /* Count number of nonzeros per row (one vertice corresponds to one row)
    */
  rows = IPM_ArrayVisibleSize(vertices, err);
  rowptr = calloc(rows+1, sizeof(IPM_Index)); // Initially, it is an array of counters for nonzeros per row

  // Count nonzeros living on elements
  for (IPMK_ArrayLocalIterInit(activeelems, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPM_Index p = IPMK_ArrayLocalIterToIndex(&iter); // p is index to activeelems
    IPM_Index e = *IPMK_ArrayIndexGet(activeelems, p, err); // value at p is index to elements
    const int num_edges = IPM_ArrayGetLength(elements, e, err);

    assert(num_edges == 3 || num_edges == 4);
    if (num_edges == 3) continue; // Only quadrilaterals have internal edges

    const IPMK_Index *edgeidx = IPMK_ArrayIndexGet(elements, e, err);
    IPMK_Index vertidx[4] = {IPM_INDEX_NULL}; // Vertices of the element in a proper order
    IPMK_Index edgevert[8]; // Store vertices of edge 0~2 or 0~3
    const int num_verts = num_edges; // True for triangles and quads

    // Store all vertices in a temp array to facilitate later accessing
    for (int i = 0; i < num_edges; i++) {
      IPMK_Index *ends = IPMK_ArrayIndexGet(edges, edgeidx[i], err);
      edgevert[2*i] = ends[0];
      edgevert[2*i+1] = ends[1];
    }

    // Go over vertices of the element and order them in a ring (significant for quadrilaterals).
    vertidx[0] = edgevert[0];
    vertidx[1] = edgevert[1];

    // If edge 0 is opposite to the direction of the element, we need to swap vertidx[0] and vertidx[1]
    if (edgevert[2] != vertidx[1] && edgevert[3] != vertidx[1]) {
      // It implies edge 1 must be connected to the tail of edge 0, i.e., verts[0]!
      IPMK_Index tmp = vertidx[0];
      vertidx[0] = vertidx[1];
      vertidx[1] = tmp;
    }

    // Found other vertices in the direction of vertidx[0]->vertidx[1]
    for (int i = 1; i < num_verts - 1; i++) {
      if (edgevert[2*i] != vertidx[i]) {
        vertidx[i+1] = edgevert[2*i]; assert(edgevert[2*i+1] == vertidx[i]);
      } else {
        vertidx[i+1] = edgevert[2*i+1]; assert(edgevert[2*i] == vertidx[i]);
      }
    }

    for (int i = 0; i < num_verts; i++) rowptr[vertidx[i]+1]++; // shift by 1 to facilitate later adjustment
  }

  // Count nonzeros living on edges
  for (IPMK_ArrayLocalIterInit(edges, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
     IPM_Index e = IPMK_ArrayLocalIterToIndex(&iter); // index of the edge
     IPM_Index *children = IPMK_ArrayIndexGet(childedges, e, err);
     if (children[0] == IPM_INDEX_NULL) { // without
       const IPM_Index *ends = IPMK_ArrayIndexGet(edges, e, err); // indices of vertices of the edge
       IPM_Index i = ends[0];
       IPM_Index j = ends[1];
       rowptr[i+1]++;
       rowptr[j+1]++;
    }
  }

  // Count nonzeros living on vertices
  for (IPMK_ArrayLocalIterInit(vertices, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
     IPM_Index i = IPMK_ArrayLocalIterToIndex(&iter); // index of the vertiex
     rowptr[i+1]++;
  }

  /* With rowptr[], we can set up other data structures of CSR
   */

  // Adjust rowtpr to the correct CSR format. Note that rowptr[0] was init'ed to 0
  for (int i = 1; i < rows + 1; i++) rowptr[i] += rowptr[i-1];

  nnz = rowptr[rows];

  // Once nnz is known, we can set up colidx[] and val[]
  colidx = malloc(sizeof(IPM_Index)*nnz);
  val = malloc(sizeof(IPM_Real)*nnz);

  /* Fill in val[] to be stored in CSR
   */
  IPM_Index *cur = calloc(rows, sizeof(IPM_Index)); // Current positions of nonzeros for each row in val[]

  // Put Kij's living on elements into val[]
  for (IPMK_ArrayLocalIterInit(activeelems, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPM_Index p = IPMK_ArrayLocalIterToIndex(&iter); // p is index to activeelems
    IPM_Index e = *IPMK_ArrayIndexGet(activeelems, p, err); // value at p is index to elements
    const int num_edges = IPM_ArrayGetLength(elements, e, err);

    assert(num_edges == 3 || num_edges == 4);
    if (num_edges == 3) continue; // Only quadrilaterals have internal edges

    const IPMK_Index *edgeidx = IPMK_ArrayIndexGet(elements, e, err);
    IPMK_Index vertidx[4] = {IPM_INDEX_NULL}; // Vertices of the element in a proper order
    IPMK_Index edgevert[8]; // Store vertices of edge 0~2 or 0~3
    const int num_verts = num_edges; // True for triangles and quads

    // Store all vertices in a temp array to facilitate later accessing
    for (int i = 0; i < num_edges; i++) {
      IPMK_Index *ends = IPMK_ArrayIndexGet(edges, edgeidx[i], err);
      edgevert[2*i] = ends[0];
      edgevert[2*i+1] = ends[1];
    }

    // Go over vertices of the element and order them in a ring (significant for quadrilaterals).
    vertidx[0] = edgevert[0];
    vertidx[1] = edgevert[1];

    // If edge 0 is opposite to the direction of the element, we need to swap vertidx[0] and vertidx[1]
    if (edgevert[2] != vertidx[1] && edgevert[3] != vertidx[1]) {
      // It implies edge 1 must be connected to the tail of edge 0, i.e., verts[0]!
      IPMK_Index tmp = vertidx[0];
      vertidx[0] = vertidx[1];
      vertidx[1] = tmp;
    }

    // Found other vertices in the direction of vertidx[0]->vertidx[1]
    for (int i = 1; i < num_verts - 1; i++) {
      if (edgevert[2*i] != vertidx[i]) {
        vertidx[i+1] = edgevert[2*i]; assert(edgevert[2*i+1] == vertidx[i]);
      } else {
        vertidx[i+1] = edgevert[2*i+1]; assert(edgevert[2*i] == vertidx[i]);
      }
    }

    IPM_Real *fp = IPMK_ArrayNumericGet(Kelem, p, err);

    for (int k = 0; k < num_verts; k++) {
      IPM_Index i = vertidx[k];
      IPM_Index j = vertidx[(k+2)%4];
      val[rowptr[i] + cur[i]] = fp[k]; // Kij
      colidx[rowptr[i] + cur[i]] = j;
      cur[i]++;
    }

  }

  // Put Kij's living on edges into val[]
  for (IPMK_ArrayLocalIterInit(edges, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
     IPM_Index e = IPMK_ArrayLocalIterToIndex(&iter); // index of the edge
     IPM_Index *children = IPMK_ArrayIndexGet(childedges, e, err);
     if (children[0] == IPM_INDEX_NULL) {
       const IPM_Index *ends = IPMK_ArrayIndexGet(edges, e, err); // indices of vertices of the edge
       IPM_Index i = ends[0];
       IPM_Index j = ends[1];
       IPM_Real *fp = IPMK_ArrayNumericGet(Kedge, e, err);
       val[rowptr[i] + cur[i]] = fp[0]; // Kij
       colidx[rowptr[i] + cur[i]] = j;
       cur[i]++;

       val[rowptr[j] + cur[j]] = fp[1]; // Kji
       colidx[rowptr[j] + cur[j]] = i;
       cur[j]++;
    }
  }

  // Put Kij's living on vertices into val[]
  for (IPMK_ArrayLocalIterInit(vertices, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
     IPM_Index i = IPMK_ArrayLocalIterToIndex(&iter); // index of the edge
     val[rowptr[i] + cur[i]] = *IPMK_ArrayNumericGet(Kvert, i, err);
     colidx[rowptr[i] + cur[i]] = i;
     cur[i]++;
  }
  free(cur);

  // Put non-local entries of f, Kvert to zero since their memory type is IPM_MEMORY_ADD
  int num_verts = IPM_ArrayVisibleSize(f, err);
  IPM_Real *f2 = (IPM_Real *)calloc(num_verts, sizeof(IPM_Real));
  IPM_Real *Kvert2 = (IPM_Real *)calloc(num_verts, sizeof(IPM_Real));

  IPMK_ArrayLocalIter liter;
  for (IPMK_ArrayLocalIterInit(vertices, &liter); !IPMK_ArrayLocalIterDone(&liter); IPMK_ArrayLocalIterNext(&liter)) {
    IPM_Index i = IPMK_ArrayLocalIterToIndex(&liter); // index of the edge
    f2[i] = *IPMK_ArrayNumericGet(f, i, err);
    Kvert2[i] = *IPMK_ArrayNumericGet(Kvert, i, err);
  }

  memcpy(IPMK_ArrayNumericGet(f, 0, err), f2, sizeof(IPM_Real)*num_verts);
  memcpy(IPMK_ArrayNumericGet(Kvert, 0, err), Kvert2, sizeof(IPM_Real)*num_verts);

  free(f2);
  free(Kvert2);

  // TODO: One may sort val according to colidx per row, though improvement is unknown
  csr = (CSR*)malloc(sizeof(CSR));
  csr->rows = rows;
  csr->rowptr = rowptr;
  csr->colidx = colidx;
  csr->val = val;
  return csr;
}

static void DestructHangingNodes(UT_array *hanging_nodes)
{
  utarray_free(hanging_nodes);
}

void kBuildContext(IPMK_ArrayNumeric edgeactiveflags, IPMK_ArrayNumeric edgehangingflags, IPMK_ArrayNumeric vboundary,
  IPMK_ArrayNumeric Kvert,  IPMK_ArrayNumeric Kedge, IPMK_ArrayNumeric Kelem, IPMK_ArrayNumeric f,
  IPMK_ArrayNumeric vertices, IPMK_ArrayIndex childedges, IPMK_ArrayNumeric edgelevels,
  IPMK_ArrayIndex edges, IPMK_ArrayIndex elements, IPMK_ArrayIndex activeelems, IPMK_Error *err)
{
  IPMK_ArrayVisibleIter viter;

  // Collect hanging nodes (if any) and store them in a utarray
  UT_array *hanging_nodes;
  utarray_new(hanging_nodes, &hanging_node_icd);

  for (IPMK_ArrayVisibleIterInit(edges, &viter); !IPMK_ArrayVisibleIterDone(&viter); IPMK_ArrayVisibleIterNext(&viter)) {
    IPMK_Index e = IPMK_ArrayVisibleIterToIndex(&viter);
    hanging_node_t hanging_node;
    if (*IPMK_ArrayNumericGet(edgehangingflags, e, err) > 0.5) { // The edge has a hanging node
      IPMK_Index *verts = IPMK_ArrayIndexGet(edges, e, err); // vertices of the edge
      IPMK_Index *children = IPMK_ArrayIndexGet(childedges, e, err); // children of the edge
      IPMK_Index *kidverts = IPMK_ArrayIndexGet(edges, children[0], err); // vertices if the first child
      IPM_Real level = *IPMK_ArrayNumericGet(edgelevels, e, err);
      hanging_node.node = kidverts[1];
      hanging_node.ends[0] = verts[0];
      hanging_node.ends[1] = verts[1];
      hanging_node.level = level;
      utarray_push_back(hanging_nodes, &hanging_node);
    }
  }

  CSR *csr;
  if (1 || utarray_len(hanging_nodes)) {
    csr = BuildContextWithHangingNodes(hanging_nodes, edgeactiveflags, edgehangingflags, vboundary,
        Kvert,  Kedge, Kelem, f, vertices, childedges, edgelevels, edges, elements, activeelems, &err);
  } else {
    csr = BuildContextWithoutHangingNodes(f, Kvert, vertices, Kedge, childedges, edges, Kelem, elements, activeelems, &err);
  }
  IPM_ArrayAttachContext(activeelems, "SpMVCSR", csr, DestructCSR, &err);
  IPM_ArrayAttachContext(activeelems, "HangingNodes", hanging_nodes, DestructHangingNodes, &err);
}

// Find out which edges are active (i.e., edges of active elements)
// and which edges are hanging (i.e., their middle points are hanging)
void kIdentifyActiveAndHangingEdges(IPMK_ArrayNumeric edgeactiveflags, IPMK_ArrayNumeric edgehangingflags,
  IPMK_ArrayIndex childedges, IPMK_ArrayIndex edges, IPMK_ArrayIndex elements, IPMK_ArrayIndex activeelems, IPMK_Error *err)
{
  IPMK_ArrayLocalIter iter;
  for (IPMK_ArrayLocalIterInit(activeelems, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPM_Index ep = IPMK_ArrayLocalIterToIndex(&iter); // e' is index to activeelems
    IPM_Index e = *IPMK_ArrayIndexGet(activeelems, ep, err); // value at p is index to elements
    const int num_edges = IPM_ArrayGetLength(elements, e, err);
    const IPMK_Index *edgeidx = IPMK_ArrayIndexGet(elements, e, err);

    for (int i = 0; i < num_edges; i++) {
      IPMK_Index *children = IPMK_ArrayIndexGet(childedges, edgeidx[i], err);
      *IPMK_ArrayNumericGet(edgeactiveflags, edgeidx[i], err) = 1.0;
      if (children[0] != IPM_INDEX_NULL) {
        *IPMK_ArrayNumericGet(edgehangingflags, edgeidx[i], err) = 1.0;
      }
    }
  }
}

/**
 * The kernel computes y = Kx with help of a CSR data structure.
 * y will be overwritten.
 *
 */
void kSpMVCSR(IPMK_ArrayNumeric y, IPMK_ArrayNumeric x, IPMK_ArrayNumeric Kvert, IPMK_ArrayIndex vertices,
  IPMK_ArrayNumeric Kedge, IPMK_ArrayIndex childedges, IPMK_ArrayIndex edges,
  IPMK_ArrayNumeric Kelem, IPMK_ArrayIndex elements, IPMK_ArrayIndex activeelems, IPMK_Error *err)
{
  CSR *csr = (CSR*)IPM_ArrayLookupContext((IPM_Array)activeelems, "SpMVCSR", err);

  assert(csr);

  IPM_Index rows = IPM_ArrayVisibleSize(vertices, err);
  IPM_Real *xdata = IPMK_ArrayNumericGet(x, 0, err);
  IPM_Real *ydata = IPMK_ArrayNumericGet(y, 0, err);

  for (int i = 0; i < rows; i++) {
    IPM_Real res = 0;
    for (int j = csr->rowptr[i]; j < csr->rowptr[i+1]; j++) {
      IPM_Real alpha = csr->val[j];
      res += alpha*xdata[csr->colidx[j]];
    }
    ydata[i] = res;
  }
}

void kComputeErrorvec(IPMK_RArray errorvec, IPMK_ArrayNumeric u, IPMK_ArrayNumeric vertices,
  IPMK_ArrayIndex edges, IPMK_ArrayIndex elements, IPMK_ArrayIndex activeelems, IPMK_Error *err)
{
  FE *fe;
  IPMK_ArrayLocalIter iter;

  IPM_Real myerrorvec[2] = {.0};
  IPM_Real *errorvec_ptr = IPMK_RArrayGet(errorvec, err);

  for (IPMK_ArrayLocalIterInit(activeelems, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPM_Index p = IPMK_ArrayLocalIterToIndex(&iter); // p is an index to activeelems
    IPM_Index e = *IPMK_ArrayIndexGet(activeelems, p, err); // value at p
    const int num_edges = IPM_ArrayGetLength(elements, e, err);
    const IPMK_Index *edgeidx = IPMK_ArrayIndexGet(elements, e, err);
    IPMK_Index vertidx[4] = {IPM_INDEX_NULL}; // Vertices of the element in a proper order
    IPM_Real *vertcoord[4] = {NULL}; // Pointers to coordinates of the vertices
    IPMK_Index edgevert[8]; // Store vertices of edge 0~2 or 0~3
    IPM_Real uval[4];

    const int num_verts = num_edges; // True for triangles and quads

    assert(num_edges == 3 || num_edges == 4);

    // Store all vertices in a temp array to facilitate later accessing
    for (int i = 0; i < num_edges; i++) {
      IPMK_Index *ends = IPMK_ArrayIndexGet(edges, edgeidx[i], err);
      edgevert[2*i] = ends[0];
      edgevert[2*i+1] = ends[1];
    }

    // Go over vertices of the element and order them in a ring
    vertidx[0] = edgevert[0];
    vertidx[1] = edgevert[1];

    // If edge 0 is opposite to the direction of the element, we need to swap vertidx[0] and vertidx[1]
    if (edgevert[2] != vertidx[1] && edgevert[3] != vertidx[1]) {
      // It implies edge 1 must be connected to the tail of edge 0, i.e., verts[0]!
      IPMK_Index tmp = vertidx[0];
      vertidx[0] = vertidx[1];
      vertidx[1] = tmp;
    }

    // Found other vertices in the direction of vertidx[0]->vertidx[1]
    for (int i = 1; i < num_verts - 1; i++) {
      if (edgevert[2*i] != vertidx[i]) {
        vertidx[i+1] = edgevert[2*i]; assert(edgevert[2*i+1] == vertidx[i]);
      } else {
        vertidx[i+1] = edgevert[2*i+1]; assert(edgevert[2*i] == vertidx[i]);
      }
    }

    IPM_Real nodes[4][2] = {{0}}; // nodes[][] is for debugging purpose
    for (int i = 0; i < num_verts; i++) {
      vertcoord[i] = IPMK_ArrayNumericGet(vertices, vertidx[i], err);
      uval[i] = *IPMK_ArrayNumericGet(u, vertidx[i], err);
      nodes[i][0] = vertcoord[i][0];
      nodes[i][1] = vertcoord[i][1];
    }

    fe = (num_verts == 3? &trife : &quadfe);
    InitFEOnReal(fe, vertcoord);

    for (int j = 0; j < fe->nqp; j++) {
      IPM_Real approx_u = .0;
      IPM_Real approx_grad_u[2] = {.0};
      IPM_Real exact_u, exact_grad_u[2];
      for (int i = 0; i < fe->nphi; i++) {
        approx_u += uval[i]*PHI(i, j);
        approx_grad_u[0] += uval[i]*DPHI(i, j, 0);
        approx_grad_u[1] += uval[i]*DPHI(i, j, 1);
      }

      exact_u = exact_solution(fe->xy[j][0], fe->xy[j][1], curtime);
      exact_derivative(fe->xy[j][0], fe->xy[j][1], curtime, exact_grad_u);

      myerrorvec[0] += SQ(approx_u - exact_u)*fe->JxW[j];
      myerrorvec[1] += (SQ(approx_grad_u[0] - exact_grad_u[0]) + SQ(approx_grad_u[1] - exact_grad_u[1]))*fe->JxW[j];
    }
  }

  errorvec_ptr[0] = myerrorvec[0];
  errorvec_ptr[1] = myerrorvec[1];
}

int main(int argc,char **args)
{
  IPM_Error err;
  int rank;
  char filename[128];
  double etime;

  int intargs[10];
  int n_refinements = 2;
  int n_timesteps = 2;
  int max_r_steps = 2;
  int init_timestep = 0;

  IPM_Initialize(&argc,&args,&err);

  MPI_Comm_rank(MPI_COMM_WORLD,&rank);

  if (!rank) {
    if (argc !=4) {
      printf("Usage: %s <n_refinements> <n_timesteps> <max_r_steps>\n", args[0]);
      MPI_Abort(MPI_COMM_WORLD, -1);
    }
    intargs[0] = atoi(args[1]);
    intargs[1] = atoi(args[2]);
    intargs[2] = atoi(args[3]);
    MPI_Bcast(intargs, 3, MPI_INT, 0, MPI_COMM_WORLD);
  } else {
    MPI_Bcast(intargs, 3, MPI_INT, 0, MPI_COMM_WORLD);
  }

  n_refinements = intargs[0];
  n_timesteps = intargs[1];
  max_r_steps = intargs[2];

#ifdef HAVE_HPCTOOLKIT
  hpctoolkit_sampling_stop();
#endif

  // Create kernel functions
  IPM_Function myCreate = IPM_FunctionCreate(IPM_FUNCTION_KERNEL, kCreate, 8,
            IPM_MEMORY_EXPAND, // vboundary
            IPM_MEMORY_EXPAND, // vertices
            IPM_MEMORY_EXPAND, // edgelevels
            IPM_MEMORY_EXPAND, // edgechildren
            IPM_MEMORY_EXPAND, // edges
            IPM_MEMORY_EXPAND, // parentelems
            IPM_MEMORY_EXPAND, // elements
            IPM_MEMORY_EXPAND, // activeelems
            &err);

  IPM_Function myDumpAxy = IPM_FunctionCreate(IPM_FUNCTION_KERNEL, kDumpAxy, 8,
            IPM_MEMORY_READ, // Kvert
            IPM_MEMORY_READ,  // Kedge
            IPM_MEMORY_READ, // Kelem
            IPM_MEMORY_READ, // vertices
            IPM_MEMORY_READ, // edges
            IPM_MEMORY_READ, // elements
            IPM_MEMORY_READ, // x
            IPM_MEMORY_READ, // y
            &err);

  IPM_Function myAssemble = IPM_FunctionCreate(IPM_FUNCTION_KERNEL, kAssemble, 11,
            IPM_MEMORY_ADD, // Kvert
            IPM_MEMORY_ADD, // Kedge
            IPM_MEMORY_EXPAND,  // Kelem. Maybe IPM_MEMORY_WRITE
            IPM_MEMORY_ADD,  // f
            IPM_MEMORY_READ, // vboundary
            IPM_MEMORY_READ, // u_old
            IPM_MEMORY_READ, // vertices
            IPM_MEMORY_READ, // childedges
            IPM_MEMORY_READ, // edges
            IPM_MEMORY_READ, // elements
            IPM_MEMORY_READ, // activeelems
            &err);

  IPM_Function myIdentifyActiveAndHangingEdges = IPM_FunctionCreate(IPM_FUNCTION_KERNEL, kIdentifyActiveAndHangingEdges, 6,
            IPM_MEMORY_ADD, // edgeactiveflags
            IPM_MEMORY_ADD, // edgehangingflags
            IPM_MEMORY_READ, // childedges
            IPM_MEMORY_READ, // edges
            IPM_MEMORY_READ, // elements
            IPM_MEMORY_READ, // activeelems
            &err);

  IPM_Function myBuildContext = IPM_FunctionCreate(IPM_FUNCTION_KERNEL, kBuildContext, 13,
            IPM_MEMORY_READ, // edgeactiveflags
            IPM_MEMORY_READ, // edgehangingflags
            IPM_MEMORY_READ, // vboundary
            IPM_MEMORY_ADD,  // Kvert, which might be adjusted
            IPM_MEMORY_READ, // Kedge
            IPM_MEMORY_READ, // Kelem
            IPM_MEMORY_ADD,  // f, which might be adjusted
            IPM_MEMORY_READ, // vertices
            IPM_MEMORY_READ, // childedges
            IPM_MEMORY_READ, // edgelevels
            IPM_MEMORY_READ, // edges
            IPM_MEMORY_READ, // elements
            IPM_MEMORY_READ, // activeelems
            &err);

  IPM_Function myComputeKellyJump = IPM_FunctionCreate(IPM_FUNCTION_KERNEL, kComputeKellyJump, 9,
            IPM_MEMORY_WRITE, // hmax
            IPM_MEMORY_ADD,  // kellyjump
            IPM_MEMORY_READ, // u
            IPM_MEMORY_READ, // vboundary
            IPM_MEMORY_READ, // vertices
            IPM_MEMORY_READ, // childedges
            IPM_MEMORY_READ, // edges
            IPM_MEMORY_READ, // elements
            IPM_MEMORY_READ, // activeelems
            &err);

  IPM_Function myEstimateErrors = IPM_FunctionCreate(IPM_FUNCTION_KERNEL, kEstimateErrors, 8,
            IPM_MEMORY_MAX,  // maxerr
            IPM_MEMORY_MIN,  // minerr
            IPM_MEMORY_WRITE,// elemerrs
            IPM_MEMORY_READ, // hmax
            IPM_MEMORY_READ, // kellyjump
            IPM_MEMORY_READ, // childedges
            IPM_MEMORY_READ, // elements
            IPM_MEMORY_READ, // activeelems
            &err);

  IPM_Function mySetRefineFlags = IPM_FunctionCreate(IPM_FUNCTION_KERNEL, kSetRefineFlags, 6,
            IPM_MEMORY_ADD, // elemrefineflags. MEMORY_WRITE is fine since it is a top array
            IPM_MEMORY_ADD,  // edgerefineflags
            IPM_MEMORY_READ, // maxerr
            IPM_MEMORY_READ, // elements
            IPM_MEMORY_READ, // elemerrs
            IPM_MEMORY_READ, // activeelems
            &err);

  IPM_Function mySmoothRefineFlags = IPM_FunctionCreate(IPM_FUNCTION_KERNEL, kSmoothRefineFlags, 6,
            IPM_MEMORY_ADD, // flagchanged
            IPM_MEMORY_ADD, // elemrefineflags
            IPM_MEMORY_ADD,  // edgerefineflags
            IPM_MEMORY_READ, // childedges
            IPM_MEMORY_READ, // elements
            IPM_MEMORY_READ, // activeelems
            &err);

  IPM_Function myClearElementCoarsenFlags = IPM_FunctionCreate(IPM_FUNCTION_KERNEL, kClearElementCoarsenFlags, 8,
            IPM_MEMORY_PROD, // elemcoarsenflags
            IPM_MEMORY_READ, // maxerr
            IPM_MEMORY_READ, // minerr
            IPM_MEMORY_READ, // edgerefineflags
            IPM_MEMORY_READ, // parentelems
            IPM_MEMORY_READ, // elements
            IPM_MEMORY_READ, // elemerrs
            IPM_MEMORY_READ, // activeelems
            &err);

  IPM_Function myClearEdgeCoarsenFlags = IPM_FunctionCreate(IPM_FUNCTION_KERNEL, kClearEdgeCoarsenFlags, 4,
            IPM_MEMORY_PROD, // edgecoarsenflags
            IPM_MEMORY_READ, // elemcoarsenflags
            IPM_MEMORY_READ, // elements
            IPM_MEMORY_READ, // activeelems
            &err);

  IPM_Function mySmoothCoarsenFlags = IPM_FunctionCreate(IPM_FUNCTION_KERNEL, kSmoothCoarsenFlags, 5,
            IPM_MEMORY_ADD, // flagchanged
            IPM_MEMORY_PROD, // elemcoarsenflags
            IPM_MEMORY_PROD, // edgecoarsenflags
            IPM_MEMORY_READ, // childedges
            IPM_MEMORY_READ, // elements
            &err);

  IPM_Function myCoarsenEdges = IPM_FunctionCreate(IPM_FUNCTION_KERNEL, kCoarsenEdges, 3,
            IPM_MEMORY_READ, // edgecoarsenflags
            IPM_MEMORY_WRITE,  // childedges
            IPM_MEMORY_READ, // edges
            &err);

  IPM_Function myRefineEdges = IPM_FunctionCreate(IPM_FUNCTION_KERNEL, kRefineEdges, 7,
            IPM_MEMORY_EXPAND, // u_old
            IPM_MEMORY_EXPAND, // vboundary
            IPM_MEMORY_EXPAND, // vertices
            IPM_MEMORY_EXPAND, // edgelevels
            IPM_MEMORY_READ,   // edgerefineflags
            IPM_MEMORY_WRITE,  // childedges
            IPM_MEMORY_EXPAND, // edges
            &err);

  IPM_Function myCoarsenElements = IPM_FunctionCreate(IPM_FUNCTION_KERNEL, kCoarsenElements, 4,
            IPM_MEMORY_READ, // elemcoarsenflags
            IPM_MEMORY_READ, // parentelems
            IPM_MEMORY_READ, // elements
            IPM_MEMORY_EXPAND, // activeelems
            &err);

  IPM_Function myRefineElements = IPM_FunctionCreate(IPM_FUNCTION_REPARTITION, kRefineElements, 9,
            IPM_MEMORY_EXPAND, // u_old
            IPM_MEMORY_EXPAND, // vertices
            IPM_MEMORY_EXPAND, // edgelevels
            IPM_MEMORY_EXPAND, // childedges
            IPM_MEMORY_EXPAND, // edges
            IPM_MEMORY_EXPAND, // parentelems
            IPM_MEMORY_EXPAND, // elements
            IPM_MEMORY_EXPAND, // elemrefineflags
            IPM_MEMORY_EXPAND, // activeelems
            &err);

  // The kernel is to refine the original mesh on rank 0
  IPM_Function myRefineElementsInCreation = IPM_FunctionCreate(IPM_FUNCTION_KERNEL, kRefineElements, 9,
            IPM_MEMORY_EXPAND, // u
            IPM_MEMORY_EXPAND, // vertices
            IPM_MEMORY_EXPAND, // edgelevels
            IPM_MEMORY_EXPAND, // childedges
            IPM_MEMORY_EXPAND, // edges
            IPM_MEMORY_EXPAND, // parentelems
            IPM_MEMORY_EXPAND, // elements
            IPM_MEMORY_EXPAND, // elemcoarsenflags
            IPM_MEMORY_EXPAND, // activeelems
            &err);

  IPM_Function myInitMesh = IPM_FunctionCreate(IPM_FUNCTION_REPARTITION, kInitMesh, 5,
            IPM_MEMORY_WRITE, // u
            IPM_MEMORY_READ, // vertices
            IPM_MEMORY_READ, // edges
            IPM_MEMORY_READ, // elements
            IPM_MEMORY_READ, // activeelems
            &err);

  IPM_Function mySpMV = IPM_FunctionCreate(IPM_FUNCTION_KERNEL, kSpMV, 10,
            IPM_MEMORY_ADD,  // y
            IPM_MEMORY_READ, // x
            IPM_MEMORY_READ, // Kvert
            IPM_MEMORY_READ, // vertices
            IPM_MEMORY_READ, // Kedge
            IPM_MEMORY_READ, // childedges
            IPM_MEMORY_READ, // edges
            IPM_MEMORY_READ, // Kelem
            IPM_MEMORY_READ, // elements
            IPM_MEMORY_READ, // activeelems
            &err);

  IPM_Function mySpMVCSR = IPM_FunctionCreate(IPM_FUNCTION_KERNEL, kSpMVCSR, 10,
            IPM_MEMORY_ADD,  // y
            IPM_MEMORY_READ, // x
            IPM_MEMORY_READ, // Kvert
            IPM_MEMORY_READ, // vertices
            IPM_MEMORY_READ, // Kedge
            IPM_MEMORY_READ, // childedges
            IPM_MEMORY_READ, // edges
            IPM_MEMORY_READ, // Kelem
            IPM_MEMORY_READ, // elements
            IPM_MEMORY_READ, // activeelems
            &err);

  IPM_Function myDot = IPM_FunctionCreate(IPM_FUNCTION_KERNEL, kDot, 3,
            IPM_MEMORY_ADD,  // product
            IPM_MEMORY_READ, // x
            IPM_MEMORY_READ, // y
            &err);

  IPM_Function myComputeNodalErrors = IPM_FunctionCreate(IPM_FUNCTION_KERNEL, kComputeNodalErrors, 3,
            IPM_MEMORY_WRITE, // e
            IPM_MEMORY_READ,  // vertices
            IPM_MEMORY_READ,  // u
            &err);

  IPM_Function myComputeErrorvec = IPM_FunctionCreate(IPM_FUNCTION_KERNEL, kComputeErrorvec, 6,
            IPM_MEMORY_ADD, // errorvec
            IPM_MEMORY_READ,  // u
            IPM_MEMORY_READ,  // vertices
            IPM_MEMORY_READ,  // edges
            IPM_MEMORY_READ,  // elements
            IPM_MEMORY_READ,  // activeelems
            &err);

  IPM_ArrayNumeric vertices = IPM_ArrayNumericCreate("vertices", &err);
  IPM_ArrayIndex edges = IPM_ArrayIndexCreate("edges", vertices, &err);
  IPM_ArrayIndex childedges = IPM_ArrayIndexClone("childedges", edges, 2/*deflen*/, IPM_INDEX_NULL/*defval*/, 0/*non-temp*/, 0, &err);
  IPM_ArrayIndex elements = IPM_ArrayIndexCreate("elements", edges, &err);
  IPM_ArrayIndex parentelems = IPM_ArrayIndexClone("parentelems", elements, 1, IPM_INDEX_NULL, 0/*non-temp*/, 1, &err);
  IPM_ArrayIndex activeelems = IPM_ArrayIndexCreate("activeelems", elements, &err);
  IPM_ArrayIndexSetTop(activeelems, 3, &err);

  IPM_ArrayNumeric vboundary = IPM_ArrayNumericClone("vboundary", vertices, 1/*deflen*/, 0/*defval*/, 0/*non-temp*/, &err);
  IPM_ArrayNumeric edgelevels = IPM_ArrayNumericClone("edgelevels", edges, 1/*deflen*/, 0/*defval*/, 0/*non-temp*/, &err);

  IPM_ArrayNumeric u = IPM_ArrayNumericClone("u", vertices, 1, 0, 0, &err); // It is Uvert in practice
  IPM_ArrayNumeric u_old = IPM_ArrayNumericClone("u_old", vertices, 1, 0, 0, &err); // It is Uvert in practice

  IPM_RArray errorvec = IPM_RArrayCreate("errorvec", 2, &err);
  IPM_RArray gamma = IPM_RArrayCreate("gamma", 1, &err);
  IPM_RArray sigma = IPM_RArrayCreate("sigma", 1, &err);
  IPM_RArray maxerr = IPM_RArrayCreate("maxerr", 1, &err);
  IPM_RArray minerr = IPM_RArrayCreate("minerr", 1, &err);
  IPM_RArray norm2 = IPM_RArrayCreate("norm2", 1, &err);
  IPM_RArray bnorm = IPM_RArrayCreate("fnorm", 1, &err);
  IPM_RArray rnorm = IPM_RArrayCreate("rnorm", 1, &err);
  IPM_RArray norm = IPM_RArrayCreate("norm", 1, &err);

  IPM_RArray flagchanged = IPM_RArrayCreate("flagchanged", 1, &err);

  curtime = init_timestep*dt;
  InitFEOnReference();

  IPM_Launch(myCreate, vboundary, vertices, edgelevels, childedges, edges, parentelems, elements, activeelems, &err);

  for (int i = 0; i < n_refinements; i++) {
    IPM_ArrayNumeric elemrefineflags = IPM_ArrayNumericClone("elemrefineflags", activeelems, 1/*deflen*/, 1.0/*defval*/, 1/*temp*/, &err);
    IPM_ArrayNumeric edgerefineflags = IPM_ArrayNumericClone("edgerefineflags", edges, 1/*deflen*/, 1.0/*defval*/, 1/*temp*/, &err);

    IPM_Launch(myRefineEdges, u, vboundary, vertices, edgelevels, edgerefineflags, childedges, edges, &err);
    IPM_Launch(myRefineElementsInCreation, u, vertices, edgelevels, childedges, edges, parentelems, elements, elemrefineflags, activeelems, &err);
    IPM_ArrayDestroy(edgerefineflags, &err);
    IPM_ArrayDestroy(elemrefineflags, &err);
  }

  IPM_Launch(myInitMesh, u, vertices, edges, elements, activeelems, &err);

#define MESH_OUTPUT

  // Draw the mesh after creation and uniform refinement
#ifdef MESH_OUTPUT
  mesh_filename[0] = 0;
  sprintf(mesh_filename, "mesh-000.vtk");
  VTKOutput(mesh_filename, u, vertices, edges, elements, NULL, activeelems, &err);
#endif

#ifdef HAVE_HPCTOOLKIT
  hpctoolkit_sampling_start();
#endif
  etime -= MPI_Wtime();
  for (step = init_timestep; step < init_timestep + n_timesteps; step++) {
    curtime += dt;

    IPM_ArrayNumericCopy(u, u_old, &err); // u_old = u

    for (int r_step = 0; r_step < max_r_steps; r_step++) {
      // Assemble the stiffness martix K and the load vector f
      IPM_ArrayNumeric Kvert = IPM_ArrayNumericClone("Kvert", vertices, 1/*deflen*/, .0, 1/*temp*/, &err);
      IPM_ArrayNumeric Kedge = IPM_ArrayNumericClone("Kedge", edges, 2/*deflen*/, .0, 1/*temp*/, &err);
      IPM_ArrayNumeric Kelem = IPM_ArrayNumericClone("Kelem", activeelems, 0/*deflen*/, .0, 1/*temp*/, &err);
      IPM_ArrayNumeric f = IPM_ArrayNumericClone("f", vertices, 1/*deflen*/, .0, 1/*temp*/, &err);
      IPM_ArrayNumeric edgeactiveflags = IPM_ArrayNumericClone("edgeactiveflags", edges, 1/*deflen*/, .0, 1/*temp*/, &err);
      IPM_ArrayNumeric edgehangingflags = IPM_ArrayNumericClone("edgehangingflags", edges, 1/*deflen*/, .0, 1/*temp*/, &err);

      IPM_Launch(myAssemble, Kvert, Kedge, Kelem, f, vboundary, u_old, vertices,
        childedges, edges, elements, activeelems, &err);

      IPM_Launch(myIdentifyActiveAndHangingEdges, edgeactiveflags, edgehangingflags, childedges, edges, elements, activeelems, &err);

      IPM_Launch(myBuildContext, edgeactiveflags, edgehangingflags, vboundary,
        Kvert,  Kedge, Kelem, f, vertices, childedges, edgelevels, edges, elements, activeelems, &err);

      int ksp_its = 0;
      // Solve a linear system Ax=b with GMRES. Here, A is k, x is u and b is f.
      {
        IPM_Real bnorm_val, rnorm_val, error, temp;
        int flag = 0;
        int m =  ksp_gmres_restart;

        IPM_ArrayNumeric Minv = IPM_ArrayNumericClone("Minv", vertices, 1, 0, 1/*temp*/, &err);
        IPM_ArrayNumeric r = IPM_ArrayNumericClone("r", vertices, 1, 0, 1/*temp*/, &err);

        IPM_ArrayNumericSet(u, 0, &err); // start from a zero guess

        IPM_ArrayNumericCopy(Kvert, Minv, &err); // Get the PC
        IPM_ArrayNumericReciprocal(Minv, &err);

        IPM_ArrayNumericPointwiseMult(r, Minv, f, &err); // r = M \ (b-A*x)
        IPM_Launch(myDot, rnorm, r, r, &err);
        bnorm_val = rnorm_val = sqrt(*IPMK_RArrayGet(rnorm, &err));
        if (bnorm_val == 0.0) bnorm_val = 1.0; // Use preconditioned bnorm

        // Allocate memory temp variables
        IPM_Real *_H = (IPM_Real *)calloc((m+1)*m, sizeof(IPM_Real)); // (m+1) x m matrix
        #define H(i, j) (_H[(i)*m+(j)])

        IPM_Real *cs = (IPM_Real *)calloc(m, sizeof(IPM_Real));
        IPM_Real *sn = (IPM_Real *)calloc(m, sizeof(IPM_Real));
        IPM_Real *e1 = (IPM_Real *)calloc(m+1, sizeof(IPM_Real));
        IPM_Real *s = (IPM_Real *)calloc(m+1, sizeof(IPM_Real));
        IPM_Real *y = (IPM_Real *)calloc(m, sizeof(IPM_Real));
        e1[0] = 1.0;

        // a group of vectors
        IPM_ArrayNumeric *V = (IPM_ArrayNumeric *)malloc(sizeof(IPM_ArrayNumeric)*(m+1));
        for (int k = 0; k < m+1; k++) V[k] = IPM_ArrayNumericClone("V[i]", vertices, 1, 0, 1/*temp*/, &err);

        IPM_ArrayNumeric z = IPM_ArrayNumericClone("z", vertices, 1/*deflen*/, 0, 1/*temp*/, &err); // temp vec
        IPM_ArrayNumeric w = IPM_ArrayNumericClone("w", vertices, 1/*deflen*/, 0, 1/*temp*/, &err); // temp vec

        // Let's get started
        for (ksp_its = 0; ksp_its < ksp_max_it; ) {
          // Compute r and its norm
          IPM_Launch(mySpMVCSR, z, u, Kvert, vertices, Kedge, childedges, edges, Kelem, elements, activeelems, &err); // z = A*x
          IPM_ArrayNumericAYPX(z, -1, f, &err);   // z = b - z
          IPM_ArrayNumericPointwiseMult(r, Minv, z, &err); // r = M \ z = M \ (b-A*x)
          IPM_Launch(myDot, rnorm, r, r, &err);
          rnorm_val = sqrt(*IPMK_RArrayGet(rnorm, &err));

          // Init V[0]
          IPM_ArrayNumericCopy(r, V[0], &err);
          IPM_ArrayNumericScale(V[0], 1/rnorm_val, &err); // V(:, 0) = r / norm(r)

          memset(s, 0, sizeof(IPM_Real)*(m+1));
          s[0] = rnorm_val; // s = norm(r)*e1

          int last;
          for (int i = 0; i < m; i++) {
            ksp_its++;
            last = i;
            IPM_Launch(mySpMVCSR, z, V[i], Kvert, vertices, Kedge, childedges, edges, Kelem, elements, activeelems, &err);
            IPM_ArrayNumericPointwiseMult(w, Minv, z, &err); // w = M \ (A*V(:,i));

            for (int k = 0; k < i+1; k++) { // Modified Gram-Schmidt
              IPM_Launch(myDot, norm, w, V[k], &err);
              H(k, i) = *IPMK_RArrayGet(norm, &err); // H(k, i) = w'*V[k]
              IPM_ArrayNumericAXPY(w, -H(k, i), V[k], &err); // w = w - H(k, i)*V[k]
            }

            IPM_Launch(myDot, norm, w, w, &err);
            H(i+1, i) = sqrt(*IPMK_RArrayGet(norm, &err)); // H(i+1, i)  = norm(w)

            IPM_ArrayNumericCopy(w, V[i+1], &err);
            IPM_ArrayNumericScale(V[i+1], 1/H(i+1, i), &err); // V(:, i+1) = w / H(i+1, i)

            for (int k = 0; k < i; k++) { // Apply Givens rotation
              temp = cs[k]*H(k, i) + sn[k]*H(k+1, i);
              H(k+1, i) = -sn[k]*H(k,i) + cs[k]*H(k+1, i);
              H(k, i) = temp;
            }
            rotmat(H(i, i), H(i+1, i), &cs[i], &sn[i]);
            temp = cs[i]*s[i];
            s[i+1] = -sn[i]*s[i];
            s[i] = temp;
            H(i, i) = cs[i]*H(i, i) + sn[i]*H(i+1, i);
            H(i+1, i) = 0.0;
            error = fabs(s[i+1])/bnorm_val;

            if (error <= ksp_rtol) break;
          }

          y[last] = s[last] / H(last, last);
          for (int i = last - 1; i >= 0; i--) {
            y[i] = s[i];
            for (int j = i+1; j < last + 1; j++) y[i] -= H(i, j) * y[j];
            y[i] = y[i] /H(i, i);
          }

          for (int i = 0; i < last + 1; i++) IPM_ArrayNumericAXPY(u, y[i], V[i], &err); // x += y[j]*V[j]

          if (error <= ksp_rtol) break;

          // Compute |r|
          IPM_Launch(mySpMVCSR, z, u, Kvert, vertices, Kedge, childedges, edges, Kelem, elements, activeelems, &err); // z = A*x
          IPM_ArrayNumericAYPX(z, -1, f, &err); // z = b - A*x
          IPM_ArrayNumericPointwiseMult(r, Minv, z, &err); // r = M \ (b-A*x)
          IPM_Launch(myDot, rnorm, r, r, &err);
          rnorm_val = sqrt(*IPMK_RArrayGet(rnorm, &err));

          s[last+1] = rnorm_val;
          error = s[last+1] / bnorm_val; // check convergence
          if ( error <= ksp_rtol ) break;
       }

        if ( error > ksp_rtol ) flag = 1;

        // Free temp variables
        free(_H);
        free(cs);
        free(sn);
        free(e1);
        free(s);
        free(y);
        IPM_ArrayDestroy(z, &err);
        IPM_ArrayDestroy(w, &err);
        for (int k = 0; k < m+1; k++)  IPM_ArrayDestroy(V[k], &err);
        free(V);
        IPM_ArrayDestroy(Minv, &err);
        IPM_ArrayDestroy(r, &err);
      } // end of solver

      IPM_ArrayNumericDestroy(Kvert, &err);
      IPM_ArrayNumericDestroy(Kedge, &err);
      IPM_ArrayNumericDestroy(Kelem, &err);
      IPM_ArrayNumericDestroy(f, &err);
      IPM_ArrayDestroy(edgehangingflags, &err);

      IPM_Launch(myComputeErrorvec, errorvec, u, vertices, edges, elements, activeelems, &err);
      IPM_Real *errorvec_ptr= IPMK_RArrayGet(errorvec, &err);

      //IPM_Launch(myDumpAxy, Kvert, Kedge, Kelem, vertices, edges, elements, u, f, &err);
      int total_vertices_before_refine = IPM_ArrayTotalSize(vertices, &err);
      int total_activeelems_before_refine = IPM_ArrayTotalSize(activeelems, &err);

      // Possibly refine/coarsen the mesh
      if (r_step+1 < max_r_steps) { // No refinement in the last r_step since the system won't be solved.
        // Estimate errors on elements and mark one to be refined

        // KellyJump error on edges of active elements
        IPM_ArrayNumeric kellyjump = IPM_ArrayNumericClone("kellyjump", edges, 2/*deflen*/, 0.0/*defval*/, 1/*temp*/, &err);

        // Diameters of active elements
        IPM_ArrayNumeric hmax = IPM_ArrayNumericClone("hmax", activeelems, 1/*deflen*/, 0.0/*defval*/, 1/*temp*/, &err);

         // Errors of active elements
        IPM_ArrayNumeric elemerrs = IPM_ArrayNumericClone("elemerrs", activeelems, 1/*deflen*/, 0.0/*defval*/, 1/*temp*/, &err);

        // Flags to indicate if active elements are refinable
        IPM_ArrayNumeric elemrefineflags = IPM_ArrayNumericClone("elemrefineflags", activeelems, 1/*deflen*/, 0.0/*defval*/, 0/*temp*/, &err);

        // Flags to indicate if edges are refinable
        IPM_ArrayNumeric edgerefineflags = IPM_ArrayNumericClone("edgerefineflags", edges, 1/*deflen*/, 0.0/*defval*/, 1/*temp*/, &err);

        // Flags to indicate if elements are coarsenable
        IPM_ArrayNumeric elemcoarsenflags = IPM_ArrayNumericClone("elemcoarsenflags", elements, 1/*deflen*/, 1/*defval*/, 1/*temp*/, &err);

        // Flags to indicate if edges are coarsenable
        IPM_ArrayNumeric edgecoarsenflags = IPM_ArrayNumericClone("edgecoarsenflags", edges, 1/*deflen*/, 1/*defval*/, 1/*temp*/, &err);

        IPM_Launch(myComputeKellyJump, hmax, kellyjump, u, vboundary, vertices, childedges, edges, elements, activeelems, &err);
        IPM_Launch(myEstimateErrors, maxerr, minerr, elemerrs, hmax, kellyjump, childedges, elements, activeelems, &err);

        IPM_Launch(mySetRefineFlags, elemrefineflags, edgerefineflags, maxerr, elements, elemerrs, activeelems, &err);

        do {
          IPM_Launch(mySmoothRefineFlags, flagchanged, elemrefineflags, edgerefineflags, childedges, elements, activeelems, &err);
        } while (*IPM_RArrayGet(flagchanged, &err) > 0.5);

        IPM_Launch(myClearElementCoarsenFlags, elemcoarsenflags, maxerr, minerr, edgerefineflags, parentelems, elements, elemerrs, activeelems, &err);
        IPM_Launch(myClearEdgeCoarsenFlags, edgecoarsenflags, elemcoarsenflags, elements, activeelems, &err);

        do {
          IPM_Launch(mySmoothCoarsenFlags, flagchanged, elemcoarsenflags, edgecoarsenflags, childedges, elements, &err);
        } while (*IPM_RArrayGet(flagchanged, &err) > 0.5);

        // Coarsen edges
        IPM_Launch(myCoarsenEdges, edgecoarsenflags, childedges, edges, &err);

        // Refine edges & project solution for new vertices
        IPM_Launch(myRefineEdges, u_old, vboundary, vertices, edgelevels, edgerefineflags, childedges, edges, &err);

        IPM_Launch(myCoarsenElements, elemcoarsenflags, parentelems, elements, activeelems, &err);

        // Refine elements & project solution for new vertices
        IPM_Launch(myRefineElements, u_old, vertices, edgelevels, childedges, edges, parentelems, elements, elemrefineflags, activeelems, &err);

        // Draw the final mesh before it is destroyed
#ifdef MESH_OUTPUT
        mesh_filename[0] = 0;
        sprintf(mesh_filename, "mesh-%03d.vtk", step+1);
        VTKOutput(mesh_filename, u_old, vertices, edges, elements, elemerrs, activeelems, &err);
#endif

        // Destroy temporary arrays like kvert, kedge, Kelem, f etc.
        IPM_ArrayNumericDestroy(kellyjump, &err);
        IPM_ArrayNumericDestroy(hmax, &err);
        IPM_ArrayNumericDestroy(elemerrs, &err);
        IPM_ArrayNumericDestroy(edgerefineflags, &err);
        IPM_ArrayNumericDestroy(elemcoarsenflags, &err);
        IPM_ArrayNumericDestroy(edgecoarsenflags, &err);
      }

      if (rank == 0) {
        printf("time = %.3f, refine_step = %d, ksp_iters = %3d, active_elems = %8d, vertices = %8d, l2_err = %f, h1_err = %f",
            curtime, r_step, ksp_its, total_activeelems_before_refine, total_vertices_before_refine,
            sqrt(errorvec_ptr[0]), sqrt(errorvec_ptr[0] + errorvec_ptr[1]));
        if (r_step+1 < max_r_steps) printf("\nminerr = %.3e, maxerr = %.3e", *IPMK_RArrayGet(minerr, &err), *IPMK_RArrayGet(maxerr, &err));
        printf("\n");
      }

    }// refinement step loop

    if (rank == 0) printf("\n");
  } // time step loop

  etime += MPI_Wtime();

  if (rank == 0) printf("Execution time = %f seconds\n", etime);
#ifdef HAVE_HPCTOOLKIT
  hpctoolkit_sampling_stop();
#endif

  // Destroy everything before exit
  IPM_ArrayDestroy(u, &err);
  IPM_ArrayDestroy(u_old, &err);
  IPM_ArrayDestroy(vboundary, &err);
  IPM_ArrayDestroy(vertices, &err);

  IPM_ArrayDestroy(edgelevels, &err);
  IPM_ArrayDestroy(childedges, &err);
  IPM_ArrayDestroy(edges, &err);

  IPM_ArrayDestroy(parentelems, &err);
  IPM_ArrayDestroy(elements, &err);
  IPM_ArrayDestroy(activeelems, &err);

  IPM_ArrayDestroy(errorvec, &err);
  IPM_ArrayDestroy(gamma, &err);
  IPM_ArrayDestroy(sigma, &err);
  IPM_ArrayDestroy(maxerr, &err);
  IPM_ArrayDestroy(minerr, &err);
  IPM_ArrayDestroy(norm2, &err);

  IPM_FunctionDestroy(myCreate, &err);
  IPM_FunctionDestroy(myRefineElementsInCreation, &err);
  IPM_FunctionDestroy(myDumpAxy, &err);

  IPM_FunctionDestroy(mySetRefineFlags, &err);
  IPM_FunctionDestroy(myClearElementCoarsenFlags, &err);
  IPM_FunctionDestroy(myClearEdgeCoarsenFlags, &err);
  IPM_FunctionDestroy(myCoarsenEdges, &err);
  IPM_FunctionDestroy(myCoarsenElements, &err);
  IPM_FunctionDestroy(myEstimateErrors, &err);
  IPM_FunctionDestroy(myComputeKellyJump, &err);

  IPM_FunctionDestroy(myRefineEdges, &err);
  IPM_FunctionDestroy(myRefineElements, &err);

  IPM_FunctionDestroy(myAssemble, &err);
  IPM_FunctionDestroy(myDot, &err);
  IPM_FunctionDestroy(myComputeNodalErrors, &err);

  IPM_FunctionDestroy(mySpMVCSR, &err);
  IPM_FunctionDestroy(myComputeErrorvec, &err);

  IPM_Finalize(&err);
  return 0;
}
