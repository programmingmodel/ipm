/*
 Copyright (c) 2015 UChicago Argonne, LLC

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

/*
 * This sample IPM program implements an AMR code.
 *
 * It starts with a mesh with four triangles. It tests triangles to see if they
 * need to be refined. If yes, it tags their edges, which means we are going to
 * add a midpoint on the edges and split them into two edges.
 * After the tagging process, for a triangle, it may have 0, 1, 2 or 3 tagged edges.
 * It says we do not refine the triangle, or refine it into 2, 3 or 4 child triangles

 * Usage:
 *   mpirun -n nproc ./prog <Number of refinements>
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>
#include <assert.h>
#include <ipm.h>
#include <mpi.h>

#if defined(HAVE_CAIRO)
#include <cairo-pdf.h>
cairo_t *cr;
#endif

#ifdef HAVE_HPCTOOLKIT
#include <hpctoolkit.h>
#endif

/*
   This code demonstrates the use of the IPM parallel programming model with finite element mesh refinement
   for triangular elements.
   Refines a circle.
   See ipm.h and IPM.pdf for more details on the model.
*/

void kDraw(IPMK_ArrayNumeric vertices,IPMK_ArrayIndex edges,IPMK_ArrayIndex elements,IPMK_Error *err)
{
  if (IPMK_ArrayIndexTo(edges, err) != vertices) IPMK_SetErrorVoid(IPMK_ERROR_WRONG_STATE);
  // To draw a complete picture of the part of the mesh on this proc, we
  // need to access all visible edges (instead of edges owned by this proc)

  IPMK_ArrayLocalIter iter;
  for (IPMK_ArrayLocalIterInit(elements, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPM_Index i = IPMK_ArrayLocalIterToIndex(&iter);
    const IPMK_Index *myedges = IPMK_ArrayIndexGet(elements, i, err); // edges of the element
    for (int i=0; i<3; i++) {
      const IPMK_Index *ps = IPMK_ArrayIndexGet(edges, myedges[i], err);

#if defined(HAVE_CAIRO)
      cairo_move_to(cr,IPMK_ArrayNumericGet(vertices,ps[0],err)[0]/2.0+.5, -1.0*IPMK_ArrayNumericGet(vertices,ps[0],err)[1]/2.0+.5);
      cairo_line_to(cr,IPMK_ArrayNumericGet(vertices,ps[1],err)[0]/2.0+.5, -1.0*IPMK_ArrayNumericGet(vertices,ps[1],err)[1]/2.0+.5);
      cairo_close_path(cr);
      cairo_stroke (cr);
#endif
    }
  }
}

/*
  This kernel/task is called on a completely empty set of IPM objects.

  It creates
    - 4 element objects that point to
    - 8 edge objects that ppoint to
    - 5 vertex objects

  Each element object points to 3 edge objects and each edge object points to 2 vertex objects

*/
void kCreate(IPMK_ArrayNumeric vertices,IPMK_ArrayIndex edges,IPMK_ArrayIndex elements,IPMK_Error *err)
{
  if (IPM_ArrayTotalSize(vertices, err) > 0) IPMK_SetErrorVoid(IPMK_ERROR_WRONG_STATE);
  if (IPM_ArrayTotalSize(edges, err) > 0) IPMK_SetErrorVoid(IPMK_ERROR_WRONG_STATE);
  if (IPM_ArrayTotalSize(elements, err) > 0) IPMK_SetErrorVoid(IPMK_ERROR_WRONG_STATE);

  double xy[5][2] = {{0,-1},{1,0},{0,1},{-1,0},{0,0}};
  int edgeid[8][2] = {{0,1},{1,4},{4,0},{2,1},{4,2},{2,3},{3,4},{0,3}};
  int elementid[4][3] = {{0,1,2},{1,4,3},{4,5,6},{7,6,2}};

  // Add vertices
  IPMK_Index vertexindices[5];
  for (int i=0; i<5; i++) vertexindices[i] = IPMK_ArrayNumericAdd(vertices,2,xy[i],err);

  // Add edges
  IPMK_Index edgeindices[8];
  for (int i=0; i<8; i++) {
    IPMK_Index ps[2] = {vertexindices[edgeid[i][0]],vertexindices[edgeid[i][1]]};
    edgeindices[i] = IPMK_ArrayIndexAdd(edges,2,ps,err);
  }

  // Add elements
  for (int i=0; i<4; i++) {
    IPMK_Index ps[3] = {edgeindices[elementid[i][0]],edgeindices[elementid[i][1]],edgeindices[elementid[i][2]]};
    IPMK_ArrayIndexAdd(elements,3,ps,err);
  }
}


/** The kernel examines every elements and decide whether to split its three edges
 *
 * Each element points to three edgeflags, if the element is refined then each of its three edgeflags is incremented
 * On entry edgeflags are 0.0
 *
 */
void kTestElements(IPMK_ArrayNumeric vertices,IPMK_ArrayNumeric edgeflags, IPMK_ArrayIndex edges, IPMK_ArrayIndex elements, IPMK_Error *err)
{
  IPMK_ArrayLocalIter iter;
  for (IPMK_ArrayLocalIterInit(elements, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPM_Index i = IPMK_ArrayLocalIterToIndex(&iter);
    const IPMK_Index *edgeidx = IPMK_ArrayIndexGet(elements, i, err);

    double xm = 0.0, ym = 0.0; // midpoint of the triangle
    double d = 0.0; // perimeter of the triangle
    for (int j=0; j < 3; j++) {
      const IPMK_Index *verts = IPMK_ArrayIndexGet(edges, edgeidx[j], err);
      const double *xyl = IPMK_ArrayNumericGet(vertices,verts[0],err);
      const double *xyr = IPMK_ArrayNumericGet(vertices,verts[1],err);
      xm += xyl[0] + xyr[0];
      ym += xyl[1] + xyr[1];
      d  += sqrt((xyl[0] - xyr[0])*(xyl[0] - xyr[0]) + (xyl[1] - xyr[1])*(xyl[1] - xyr[1]));
    }
    xm /= 6.0;
    ym /= 6.0;
    double d1 = sqrt((xm - 0.5)*(xm - 0.5) + (ym - 0.5)*(ym - 0.5));
    double d2 = sqrt((xm + 0.5)*(xm + 0.5) + (ym + 0.5)*(ym + 0.5));

    if (d1 < 0.5*d || d2 < 0.5*d) {
      for (int i = 0; i < 3; i++) {
        IPM_Real *flag = IPMK_ArrayNumericGet(edgeflags, edgeidx[i], err);
        *flag += 1.0; // Temporarily use double for boolean
      }
    }
  }
}

/*
  The kernel loops over each edge and tests if it is set to be refined (by edgeflags being nonzero).
  If so, it introduces a new point in the middle and creates two child edges.
  edgeflags and edgechildren have the same layout as edges. Edgechildren is already initialized
  with default IPM_INDEX_NULL.
  edgeflags and edgechildren have the same layout as edges. edgeflags indicates whether
  an edge should be split. Assume edgechildren is already initialized
  with default value IPM_INDEX_NULL.
*/
void kRefineEdges(IPMK_ArrayNumeric vertices,IPMK_ArrayIndex edgeflags, IPMK_ArrayIndex edgechildren, IPMK_ArrayIndex edges, IPMK_Error *err)
{
  IPMK_ArrayLocalIter iter;
  for (IPMK_ArrayLocalIterInit(edges, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPMK_Index i = IPMK_ArrayLocalIterToIndex(&iter);
    IPM_Real *flag = IPMK_ArrayNumericGet(edgeflags, i, err);
    if (*flag > 0.8) { // In theory, *flag >= 1.0. Relax it for rounding errors
      const IPMK_Index *ends = IPMK_ArrayIndexGet(edges, i, err);
      const double  *xy0 = IPMK_ArrayNumericGet(vertices, ends[0],err); // coors of vertex
      const double  *xy1 = IPMK_ArrayNumericGet(vertices, ends[1],err);

      // Add a new vertex into vertices
      double  xynew[2];
      double xm = (xy0[0]+xy1[0])/2.0;
      double ym = (xy0[1]+xy1[1])/2.0;
      if (((xy0[0]*xy0[0] + xy0[1]*xy0[1]) > 0.999)  && ((xy1[0]*xy1[0] + xy1[1]*xy1[1]) > 0.999)) {
        xynew[0] = ( (xm < 0.0) ?  -1.0 : 1.0) * 1.0/sqrt(1.0 + (ym*ym)/(xm*xm));
        xynew[1] = (ym/xm)*xynew[0];
      } else {
        xynew[0] = xm;
        xynew[1] = ym;
      }

      IPMK_Index newindex = IPMK_ArrayNumericAdd(vertices,2,xynew,err);
      // Add two child edges. Suppose the edge is (a,b) and the middle point is x,
      // the two child edges must be added in (a, x), (x, b) format, so that we
      // can easily recover x in element refinement.
      IPMK_Index indices[2];
      IPMK_Index child[2];

      indices[0] = ends[0];
      indices[1] = newindex;
      child[0] = IPMK_ArrayIndexAdd(edges,2,indices,err);

      indices[0] = newindex;
      indices[1] = ends[1];
      child[1] = IPMK_ArrayIndexAdd(edges,2,indices,err);

      IPMK_ArrayIndexChange(edgechildren, i, 2,child,err); // Change the NULL indices to point to the children
    }
  }
}

/*
  The kernel loops over elements. Based on whether its edges are refined,
  it introduces 0 to 3 new child elements.
*/
void kRefineElements(IPMK_ArrayNumeric vertices, IPMK_ArrayIndex edgechildren, IPMK_ArrayIndex edges, IPMK_ArrayIndex elements, IPMK_Error *err)
{
  IPMK_ArrayLocalIter iter;
  for (IPMK_ArrayLocalIterInit(elements, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    int i = IPMK_ArrayLocalIterToIndex(&iter);
    const IPMK_Index *myedges = IPMK_ArrayIndexGet(elements, i, err); // edges of the element
    IPMK_Index e[9] = {IPM_INDEX_NULL}; // A triangle has at most 9 sub-edges
    IPMK_Index ab, bc, ac; // Three edges of the triangle
    IPMK_Index a, b, c, x, y, z; // Tree vertices and middle points of the triangle
    IPMK_Index *childedges; // Child edges of an edge by reading edgechildren
    IPMK_Index *verts; // Two ends of an edge
    IPMK_Index newedge[2]; // Temp variable for a new interior edge
    IPMK_Index newelement[3]; // Temp variable for a new child element
    IPMK_Index child[4]; // Indices of (at most) the four children after refinement
    int found_bc, found_ac;
    int edges_split;

    x = y = z = IPM_INDEX_NULL;
    found_bc = found_ac = 0;

    // Discover structure of the triangle. a, b, c are its three vertices.
    // ab, bc, ac are its edges. x, y, z are middle points of edge ab, bc, ac
    // respectively. e0, e1, ..., e5 are child edges of ab, bc, ac in the exact
    // order shown in the diagram below. Note that the middle points and child
    // edges do not nessesarily all exist. If one does not exist, its value
    // will be IPM_INDEX_NULL.
    //
    //                       a
    //                      / \
    //                  e0 /   \ e5
    //                    /     \
    //              ab   x       z    ac
    //                  /         \
    //              e1 /           \ e4
    //                /             \
    //               b-------y-------c
    //                   e2     e3
    //                      bc

    // Assume the first edge is ab.  Set ab, a, b, x, e[0], e[1]
    ab = myedges[0];
    // The IPMK_ArrayIndexGets assume parent edges are not overwritten by its children
    verts = IPMK_ArrayIndexGet(edges, myedges[0], err);
    a = verts[0]; b = verts[1];
    childedges = IPMK_ArrayIndexGet(edgechildren, myedges[0], err);
    e[0] = childedges[0]; e[1] = childedges[1];
    if (e[0] != IPM_INDEX_NULL) { verts = IPMK_ArrayIndexGet(edges, e[0], err); x = verts[1]; }

    // The second edge is either bc or ac
    verts = IPMK_ArrayIndexGet(edges, myedges[1], err);
    childedges = IPMK_ArrayIndexGet(edgechildren, myedges[1], err);
    if (a == verts[0]) {
      found_ac = 1;
      c = verts[1];
      ac = myedges[1];
      e[5] = childedges[0]; e[4] = childedges[1];
      if (e[5] != IPM_INDEX_NULL) { verts = IPMK_ArrayIndexGet(edges, e[5], err); z = verts[1]; }
    } else if (a == verts[1]){
      found_ac = 1;
      c = verts[0];
      ac = myedges[1];
      e[4] = childedges[0]; e[5] = childedges[1];
      if (e[4] != IPM_INDEX_NULL) { verts = IPMK_ArrayIndexGet(edges, e[4], err); z = verts[1]; }
    } else if (b == verts[0]) {
      found_bc = 1;
      c = verts[1];
      bc = myedges[1];
      e[2] = childedges[0]; e[3] = childedges[1];
      if (e[2] != IPM_INDEX_NULL) { verts = IPMK_ArrayIndexGet(edges, e[2], err); y = verts[1]; }
    } else {
      assert(b == verts[1]);
      found_bc = 1;
      c = verts[0];
      bc = myedges[1];
      e[3] = childedges[0]; e[2] = childedges[1];
      if (e[3] != IPM_INDEX_NULL) { verts = IPMK_ArrayIndexGet(edges, e[3], err); y = verts[1]; }
    }

    // The third edge is the one not yet found
    verts = IPMK_ArrayIndexGet(edges, myedges[2], err);
    childedges = IPMK_ArrayIndexGet(edgechildren, myedges[2], err);
    if (!found_bc) {
      bc = myedges[2];
      if (b == verts[0]) {
        e[2] = childedges[0]; e[3] = childedges[1];
        if (e[2] != IPM_INDEX_NULL) { verts = IPMK_ArrayIndexGet(edges, e[2], err); y = verts[1]; }
      } else {
        assert(c ==verts[0]);
        e[3] = childedges[0]; e[2] = childedges[1];
        if (e[3] != IPM_INDEX_NULL) { verts = IPMK_ArrayIndexGet(edges, e[3], err); y = verts[1]; }
      }
    } else {
      assert(!found_ac);
      ac = myedges[2];
      if (c == verts[0]) {
        e[4] = childedges[0]; e[5] = childedges[1];
        if (e[4] != IPM_INDEX_NULL) { verts = IPMK_ArrayIndexGet(edges, e[4], err); z = verts[1]; }
      } else {
        assert(c == verts[1]);
        e[5] = childedges[0]; e[4] = childedges[1];
        if (e[5] != IPM_INDEX_NULL) { verts = IPMK_ArrayIndexGet(edges, e[5], err); z = verts[1]; }
      }
    }

    edges_split = (x == IPM_INDEX_NULL ? 0 : 1) +
                  (y == IPM_INDEX_NULL ? 0 : 1) +
                  (z == IPM_INDEX_NULL ? 0 : 1);

    switch (edges_split) {
    case 3:
      // All three edges have benn split, so refine the element as follows
      //                       a
      //                      / \
      //                  e0 /   \ e5
      //                    /     \
      //                   x---e8--z
      //                  / \     / \
      //              e1 /  e6   e7  \ e4
      //                /     \ /     \
      //               b-------y-------c
      //                   e2     e3
      newedge[0] = x; newedge[1] = y; e[6] = IPMK_ArrayIndexAdd(edges, 2, newedge, err);
      newedge[0] = y; newedge[1] = z; e[7] = IPMK_ArrayIndexAdd(edges, 2, newedge, err);
      newedge[0] = z; newedge[1] = x; e[8] = IPMK_ArrayIndexAdd(edges, 2, newedge, err);

      newelement[0] = e[0];  newelement[1] = e[8];  newelement[2] = e[5];
      child[0] = IPMK_ArrayIndexAdd(elements, 3, newelement, err);

      newelement[0] = e[1];  newelement[1] = e[2];  newelement[2] = e[6];
      child[1] = IPMK_ArrayIndexAdd(elements, 3, newelement, err);

      newelement[0] = e[6];  newelement[1] = e[7];  newelement[2] = e[8];
      child[2] = IPMK_ArrayIndexAdd(elements, 3, newelement, err);

      newelement[0] = e[7];  newelement[1] = e[3];  newelement[2] = e[4];
      //child[3] = IPMK_ArrayIndexAdd(elements, 3, newelement, err);
      IPMK_ArrayIndexChange(elements, i, 3, newelement, err);
      break;
    case 2:
      // Only two edges have been split. There are three cases shown below.
      //            a                     a                      a
      //           /|\                   /|\                    / \
      //       e0 / | \                 / | \ e5            e0 /   \e5
      //         /  |  \               /  |  \                /     \
      //        x   e7  \ac         ab/   e7  z              x---e6--z
      //       / \  |    \           /    |  / \            /     .`  \
      //   e1 /  e6 |     \         /     | e6  \e4     e1 /   e7      \e4
      //     /     \|      \       /      |/     \        /. `          \
      //    b-------y-------c     b-------y-------c      b---------------c
      //        e2     e3             e2     e3                 bc
      if (x != IPM_INDEX_NULL && y != IPM_INDEX_NULL) {
        // x, y exist
        newedge[0] = x; newedge[1] = y; e[6] = IPMK_ArrayIndexAdd(edges, 2, newedge, err);
        newedge[0] = a; newedge[1] = y; e[7] = IPMK_ArrayIndexAdd(edges, 2, newedge, err);

        newelement[0] = e[0];  newelement[1] = e[6];  newelement[2] = e[7];
        child[0] = IPMK_ArrayIndexAdd(elements, 3, newelement, err);

        newelement[0] = e[1];  newelement[1] = e[2];  newelement[2] = e[6];
        child[1] = IPMK_ArrayIndexAdd(elements, 3, newelement, err);

        newelement[0] = e[7];  newelement[1] = e[3];  newelement[2] = ac;
        //child[2] = IPMK_ArrayIndexAdd(elements, 3, newelement, err);
      } else if (y != IPM_INDEX_NULL && z != IPM_INDEX_NULL) {
        // y, z exist
        newedge[0] = y; newedge[1] = z; e[6] = IPMK_ArrayIndexAdd(edges, 2, newedge, err);
        newedge[0] = y; newedge[1] = a; e[7] = IPMK_ArrayIndexAdd(edges, 2, newedge, err);

        newelement[0] = ab;  newelement[1] = e[2];  newelement[2] = e[7];
        child[0] = IPMK_ArrayIndexAdd(elements, 3, newelement, err);

        newelement[0] = e[6];  newelement[1] = e[3];  newelement[2] = e[4];
        child[1] = IPMK_ArrayIndexAdd(elements, 3, newelement, err);

        newelement[0] = e[7];  newelement[1] = e[6];  newelement[2] = e[5];
        //child[2] = IPMK_ArrayIndexAdd(elements, 3, newelement, err);
      } else {
        // x, z exist
        newedge[0] = x; newedge[1] = z; e[6] = IPMK_ArrayIndexAdd(edges, 2, newedge, err);
        newedge[0] = b; newedge[1] = z; e[7] = IPMK_ArrayIndexAdd(edges, 2, newedge, err);

        newelement[0] = e[0];  newelement[1] = e[6];  newelement[2] = e[5];
        child[0] = IPMK_ArrayIndexAdd(elements, 3, newelement, err);

        newelement[0] = e[1];  newelement[1] = e[7];  newelement[2] = e[6];
        child[1] = IPMK_ArrayIndexAdd(elements, 3, newelement, err);

        newelement[0] = e[7];  newelement[1] = bc;  newelement[2] = e[4];
        //child[2] = IPMK_ArrayIndexAdd(elements, 3, newelement, err);
      }
      IPMK_ArrayIndexChange(elements, i, 3, newelement, err); // Use one child to overwrite parent
      break;
    case 1:
      // Only one edge has been split. There are three cases shown below.
      //            a                     a                      a
      //           / \                   /|\                    / \
      //       e0 /   \                 / | \                  /   \e5
      //         /     \               /  |  \                /     \
      //        x       \ac         ab/   e6  \ac          ab/       z
      //       /  ` .    \           /    |    \            /     .`  \
      //   e1 /      e6.  \         /     |     \          /   e6      \e4
      //     /          ` .\       /      |      \        /. `          \
      //    b---------------c     b-------y-------c      b---------------c
      //          bc                  e2     e3                 bc
      if (x != IPM_INDEX_NULL) {
        newedge[0] = x; newedge[1] = c; e[6] = IPMK_ArrayIndexAdd(edges, 2, newedge, err);

        newelement[0] = e[0];  newelement[1] = e[6];  newelement[2] = ac;
        child[0] = IPMK_ArrayIndexAdd(elements, 3, newelement, err);

        newelement[0] = e[1];  newelement[1] = bc;  newelement[2] = e[6];
        //child[1] = IPMK_ArrayIndexAdd(elements, 3, newelement, err);
      } else if (y != IPM_INDEX_NULL) {
        newedge[0] = a; newedge[1] = y; e[6] = IPMK_ArrayIndexAdd(edges, 2, newedge, err);

        newelement[0] = ab;  newelement[1] = e[2];  newelement[2] = e[6];
        child[0] = IPMK_ArrayIndexAdd(elements, 3, newelement, err);

        newelement[0] = e[6];  newelement[1] = e[3];  newelement[2] = ac;
        //child[1] = IPMK_ArrayIndexAdd(elements, 3, newelement, err);
      } else {
        newedge[0] = b; newedge[1] = z; e[6] = IPMK_ArrayIndexAdd(edges, 2, newedge, err);

        newelement[0] = ab;  newelement[1] = e[6];  newelement[2] = e[5];
        child[0] = IPMK_ArrayIndexAdd(elements, 3, newelement, err);

        newelement[0] = e[6];  newelement[1] = bc;  newelement[2] = e[4];
        //child[1] = IPMK_ArrayIndexAdd(elements, 3, newelement, err);
      }
      IPMK_ArrayIndexChange(elements, i, 3, newelement, err); // Use one child to overwrite parent
      break;
    case 0:
      // None of the edges has been split. The element won't be refined.
      break;
    }
  }
}

static void krhs(IPMK_ArrayNumeric Q,IPMK_ArrayNumeric W,IPMK_ArrayNumeric vertices, IPMK_ArrayIndex edges, IPMK_ArrayIndex elements,IPMK_Error *err)
{
  IPMK_ArrayLocalIter iter;
  for (IPMK_ArrayLocalIterInit(Q, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPM_Index i = IPMK_ArrayLocalIterToIndex(&iter);
    for (int j=0; j<IPM_ArrayGetLength(Q,i,err); j++) {
      IPMK_ArrayNumericGet(W,i,err)[j] = IPMK_ArrayNumericGet(Q,i,err)[j];
    }
  }
}

int main(int argc,char **args)
{
  char filename[128];
  int rank;
  double elapsed_time;
  IPM_Error err;
  int niter = 3;

  IPM_Initialize(&argc,&args,&err);

  MPI_Comm_rank(MPI_COMM_WORLD,&rank);

  if (!rank) {
    if (argc !=2) {
      printf("Usage: %s <niter>\n", args[0]);
      MPI_Abort(MPI_COMM_WORLD, -1);
    }
    niter = atoi(args[1]);
    MPI_Bcast(&niter, 1, MPI_INT, 0, MPI_COMM_WORLD);
  } else {
    MPI_Bcast(&niter, 1, MPI_INT, 0, MPI_COMM_WORLD);
  }

#ifdef HAVE_HPCTOOLKIT
  hpctoolkit_sampling_stop();
#endif

#if defined(HAVE_CAIRO)
  snprintf(filename,128,"pdffile_%d.pdf",rank);
  cairo_surface_t *surface = cairo_pdf_surface_create(filename,800,800);
  cr = cairo_create (surface);
  cairo_scale (cr, 800, 800);
  cairo_set_line_width (cr, 1.0/200.);
  cairo_set_source_rgb (cr, 0, 0, 0);
#endif

  IPM_ArrayNumeric vertices = IPM_ArrayNumericCreate("vertices", &err);
  IPM_ArrayIndex edges = IPM_ArrayIndexCreate("edges", vertices, &err);
  IPM_ArrayIndex elements = IPM_ArrayIndexCreate("elements", edges, &err);

  // Create kernel functions
  IPM_Function myCreate = IPM_FunctionCreate(IPM_FUNCTION_REPARTITION, kCreate, 3,
            IPM_MEMORY_EXPAND, // vertices
            IPM_MEMORY_EXPAND, // edges
            IPM_MEMORY_EXPAND, // elements
            &err);

  IPM_Function myDraw = IPM_FunctionCreate(IPM_FUNCTION_KERNEL, kDraw, 3,
            IPM_MEMORY_READ, // vertices
            IPM_MEMORY_READ, // edges
            IPM_MEMORY_READ, // elements
            &err);

  IPM_Function myTestElements = IPM_FunctionCreate(IPM_FUNCTION_KERNEL, kTestElements, 4,
            IPM_MEMORY_READ, // vertices
            IPM_MEMORY_ADD,  // edgeflags
            IPM_MEMORY_READ, // edges
            IPM_MEMORY_READ, // elements
            &err);

  IPM_Function myRefineEdges = IPM_FunctionCreate(IPM_FUNCTION_KERNEL, kRefineEdges, 4,
            IPM_MEMORY_EXPAND, // vertices
            IPM_MEMORY_READ,   // edgeflags
            IPM_MEMORY_WRITE,  // edgechildren
            IPM_MEMORY_EXPAND, // edges
            &err);

  IPM_Function myRefineElements = IPM_FunctionCreate(IPM_FUNCTION_REPARTITION, kRefineElements, 4,
            IPM_MEMORY_READ,   // vertices
            IPM_MEMORY_READ,   // edgechildren
            IPM_MEMORY_EXPAND, // edges
            IPM_MEMORY_EXPAND, // elements
            &err);

  IPM_Launch(myCreate, vertices, edges, elements, &err);

  // Do AMR multiple iterations!
  for (int i = 0; i < niter; i++) {

    // Skip timing the first few iterations until iteration 60,
    // when the mesh is big enough and ParMETIS will do partitioning
    // in parallel (ParMETIS's SMALLGRAPH is defined as 10000).
    if (i == 60) {
#ifdef HAVE_HPCTOOLKIT
        hpctoolkit_sampling_start();
#endif
        elapsed_time = -MPI_Wtime();
    }

    IPM_ArrayNumeric edgeflags = IPM_ArrayNumericClone("edgeflags", edges, 1/*deflen*/, 0.0/*defval*/, 1,&err);
    IPM_Launch(myTestElements, vertices, edgeflags, edges,elements, &err);

    IPM_ArrayIndex edgechildren= IPM_ArrayIndexClone("edgechildren", edges, 2/*deflen*/, IPM_INDEX_NULL/*defval*/, 1, 0, &err);
    IPM_Launch(myRefineEdges, vertices, edgeflags, edgechildren, edges, &err);

    //IPM_ArrayPrint(edgechildren, &err);
    //IPM_ArrayPrint(edges, &err);
    //IPM_ArrayPrint(vertices, &err);

    if (i == 73) gdebug = 1;

    IPM_Launch(myRefineElements, vertices, edgechildren, edges, elements, &err);

    IPM_ArrayNumericDestroy(edgeflags, &err);
    IPM_ArrayIndexDestroy(edgechildren, &err);
#if 0
    if (rank == 0) printf("After iteration %d : %d elements, %d edges, %d vertices\n", i,
        IPM_ArraySize(elements, &err), IPM_ArraySize(edges, &err), IPM_ArraySize(vertices, &err));
#endif
  }

#ifdef HAVE_HPCTOOLKIT
  hpctoolkit_sampling_stop();
#endif
  elapsed_time += MPI_Wtime();

  if (niter >= 60 && rank == 0) printf("Elapsed time = %.3f\n", elapsed_time);

  // Draw the final mesh
  IPM_Launch(myDraw, vertices, edges, elements, &err);

#if 0
  // contains the numerical solution at the vertex of each element
  // since this is dG each element has its own values that may not match those on neighboring elements
  IPM_ArrayNumeric elementvalues = IPM_ArrayNumericClone("elementvalues", elements, 3, 1.0, 0,&err);

  IPM_Function myrhs     = IPM_FunctionCreate(IPM_FUNCTION_DEFAULT,krhs,5,IPM_MEMORY_READ,IPM_MEMORY_WRITE,IPM_MEMORY_READ,IPM_MEMORY_READ,IPM_MEMORY_READ,&err);
  myrhs->array[2] = vertices;  // set the "user" arguments for the ODE right hand side function
  myrhs->array[3] = edges;
  myrhs->array[4] = elements;

  RK4 rk;
  RK4Init(&rk,elementvalues,myrhs);

  RK4Solve(&rk);

  //  IPM_Launch(myDoubleelementvalues,elementvalues, &err);
  // IPM_Launch(myDoubleelementvalues,elementvalues, &err);

  // vertex values for each edge; left and right
  //  IPM_ArrayNumeric edgevalues_left  = IPM_ArrayNumericClone("edgevalues_left", edges, 2, 0.0, &err);
  //IPM_ArrayNumeric edgevalues_right = IPM_ArrayNumericClone("edgevalues_right", edges, 2, 0.0, &err);

#endif

  // Destroy everything before exit
  IPM_ArrayNumericDestroy(vertices, &err);
  IPM_ArrayIndexDestroy(edges, &err);
  IPM_ArrayIndexDestroy(elements, &err);

  IPM_FunctionDestroy(myCreate, &err);
  IPM_FunctionDestroy(myDraw, &err);
  IPM_FunctionDestroy(myTestElements, &err);
  IPM_FunctionDestroy(myRefineEdges, &err);
  IPM_FunctionDestroy(myRefineElements, &err);

#if defined(HAVE_CAIRO)
  cairo_destroy(cr);
  cairo_surface_destroy (surface);
#endif

  IPM_Finalize(&err);
  return 0;
}
