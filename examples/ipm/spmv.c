/*
 Copyright (c) 2015 UChicago Argonne, LLC

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

/*
 * This code implements a sparse matrix vector (SpMV) multiplication.

 * A mesh is a graph, which can be represented by a matrix. We implicitly store
 * the matrix on both edges and vertices of the mesh, and store vectors on vertices
 * and perform a SpMV.
 *
 * The code is based on amr_ipm.c, however with a very important difference.
 * In the kRefineEdges kernel, if an edge is to be refined, we will use one
 * of its child edge to replace (change) the parent edge, instead of adding
 * the two child edges as new to the array. In this way, there will be no
 * relinquished edges as there are in amr_ipm.c. In IPM, relinquished data points
 * are cleared during mesh repartitioning. That means if run the program with one
 * process, the relinquished edges will hang there and result in wrong SpMv
 * result.
 *
 * kRefineElements is also updated for the new edgechildren, edges and vertices
 * relationship.
 *
 * Usage:
 *  mpirun -n <nproc> ./prog  <Number of refinements>
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>
#include <assert.h>
#include <ipm.h>
#include <mpi.h>
#include <float.h> // to get DECIMAL_DIG and print doubles in maximal precision

#if defined(HAVE_CAIRO)
#include <cairo-pdf.h>
cairo_t *cr;
#endif

#ifdef HAVE_HPCTOOLKIT
#include <hpctoolkit.h>
#endif

#define DOFS 1

/**
 * The kernel draw the mesh and print the SpMV v = Au on the mesh.
 * Aij, Aji are shown on edges. Aii (u, v) are show on vertices.
*/
void kDrawAuv(IPMK_ArrayNumeric u, IPMK_ArrayNumeric v, IPMK_ArrayNumeric Aii, IPMK_ArrayNumeric Aij,
  IPMK_ArrayNumeric vertices,IPMK_ArrayIndex edges,IPMK_ArrayIndex elements,IPMK_Error *err)
{
#if defined(HAVE_CAIRO)

  // Draw the mesh
  if (IPMK_ArrayIndexTo(edges, err) != vertices) IPMK_SetErrorVoid(IPMK_ERROR_WRONG_STATE);
  // To draw a complete picture of the part of the mesh on this proc, we
  // need to access all visible edges (instead of edges owned by this proc)

  IPM_Real *xy, *xy1, *xy2, x, y, x1, y1, x2, y2, dx, dy, alpha;
  IPM_Real aij, aji, aii, *fp;
  char buf[128];

  // Draw mesh and Aij, Aji
  IPMK_ArrayLocalIter iter;
  for (IPMK_ArrayLocalIterInit(elements, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPM_Index i = IPMK_ArrayLocalIterToIndex(&iter);
    const IPMK_Index *myedges = IPMK_ArrayIndexGet(elements, i, err); // edges of the element
    for (int i=0; i<3; i++) {
      cairo_set_source_rgb (cr, 0.75, 0.75, 0.75);

      const IPMK_Index *ps = IPMK_ArrayIndexGet(edges, myedges[i], err);
      xy1 = IPMK_ArrayNumericGet(vertices, ps[0], err);
      x1 = xy1[0];
      y1 = xy1[1];
      x1 = x1/2.0+.6; // shift and scale to [0, 1] square
      y1 = -y1/2.0+.6;
      cairo_move_to(cr, x1, y1);

      xy2 = IPMK_ArrayNumericGet(vertices, ps[1], err);
      x2 = xy2[0];
      y2 = xy2[1];
      x2 = x2/2.0+.6; // shift and scale to [0, 1] square
      y2 = -y2/2.0+.6;
      cairo_line_to(cr, x2, y2);
      cairo_close_path(cr);
      cairo_stroke (cr);

      cairo_set_source_rgb (cr, 1, 0, 0);

      // Draw aij along the line close to vertex i
      dx = x2 - x1;
      dy = y2 - y1;
      if (fabs(dx) < 1.0e-5) {
        y1 += 0.4*dy;
      } else {
        alpha = dy/dx;
        if (dx > 0) x1 += dx*0.5; else x1 += dx*0.4;
        y1 += dx*0.4*alpha;
      }

      fp = IPMK_ArrayNumericGet(Aij, myedges[i], err);
      aij = fp[0];
      sprintf(buf, "%.2f\n", aij);
      cairo_move_to(cr, x1, y1);
      cairo_show_text(cr, buf);

      // Draw aji along the line close to vertex j
      dx = x1 - x2;
      dy = y1 - y2;
      if (fabs(dx) < 1.0e-5) {
        y2 += 0.4*dy;
      } else {
        alpha = dy/dx;
        if (dx >0) x2 += dx*0.5; else x2 += dx*0.4;
        y2 += dx*0.4*alpha;
      }
      aji = fp[1];
      sprintf(buf, "%.2f\n", aji);
      cairo_move_to(cr, x2, y2);
      cairo_show_text(cr, buf);
    }
  }

  // Draw Aii (u, v)
  cairo_set_source_rgb (cr, 1, 0, 0);
  IPM_ArrayVisibleIter iter2;
  for (IPMK_ArrayVisibleIterInit(vertices, &iter2); !IPMK_ArrayVisibleIterDone(&iter2); IPMK_ArrayVisibleIterNext(&iter2)) {
    IPM_Index i = IPMK_ArrayVisibleIterToIndex(&iter2);
    IPM_Real ui = *IPMK_ArrayNumericGet(u, i, err);
    IPM_Real aii = *IPMK_ArrayNumericGet(Aii, i, err);
    IPM_Real vi = *IPMK_ArrayNumericGet(v, i, err);

    xy = IPMK_ArrayNumericGet(vertices, i, err); // edges of the element
    IPM_Index gi = IPM_ArrayGetGid(vertices, i, err);
    x = xy[0]/2.0 + 0.6;
    y = -xy[1]/2.0 + 0.6;
    //sprintf(buf, "%d: %.2f(%.2f, %.2f)\n", gi, aii, ui, vi);
    sprintf(buf, "%d, %d, %.2f\n", i, gi, vi);
    cairo_move_to(cr, x, y);
    cairo_show_text(cr, buf);
  }
  cairo_set_source_rgb (cr, 0, 0, 0);
#endif
}


/**
 * The kernel draw the mesh and also show each vertex's global id
*/
void kDrawMesh(IPMK_ArrayNumeric vertices,IPMK_ArrayIndex edges,IPMK_ArrayIndex elements,IPMK_Error *err)
{
  IPM_Real *xy, *xy1, *xy2, x, y, x1, y1, x2, y2;
  char buf[128];
#if defined(HAVE_CAIRO)
  // Draw mesh with grey lines to easily show text on it
  cairo_set_source_rgb (cr, 0.75, 0.75, 0.75);
  IPMK_ArrayLocalIter iter;
  for (IPMK_ArrayLocalIterInit(elements, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPM_Index i = IPMK_ArrayLocalIterToIndex(&iter);
    const IPMK_Index *myedges = IPMK_ArrayIndexGet(elements, i, err); // edges of the element
    for (int i=0; i<3; i++) {
      const IPMK_Index *ps = IPMK_ArrayIndexGet(edges, myedges[i], err);
      xy1 = IPMK_ArrayNumericGet(vertices, ps[0], err);
      x1 = xy1[0];
      y1 = xy1[1];
      x1 = x1/2.0+.6; // shift and scale to [0, 1] square
      y1 = -y1/2.0+.6;
      cairo_move_to(cr, x1, y1);

      xy2 = IPMK_ArrayNumericGet(vertices, ps[1], err);
      x2 = xy2[0];
      y2 = xy2[1];
      x2 = x2/2.0+.6; // shift and scale to [0, 1] square
      y2 = -y2/2.0+.6;
      cairo_line_to(cr, x2, y2);
      cairo_close_path(cr);
      cairo_stroke (cr);
    }
  }

  // Draw gid of each vertex in red
  cairo_set_source_rgb (cr, 1, 0, 0);
  IPM_ArrayVisibleIter iter2;
  for (IPMK_ArrayVisibleIterInit(vertices, &iter2); !IPMK_ArrayVisibleIterDone(&iter2); IPMK_ArrayVisibleIterNext(&iter2)) {
    IPM_Index i = IPMK_ArrayVisibleIterToIndex(&iter2);
    xy = IPMK_ArrayNumericGet(vertices, i, err); // edges of the element
    IPM_Index gi = IPM_ArrayGetGid(vertices, i, err);
    x = xy[0]/2.0 + 0.6;
    y = -xy[1]/2.0 + 0.6;
    sprintf(buf, "%d\n", gi);
    cairo_move_to(cr, x, y);
    cairo_show_text(cr, buf);
  }
#endif
}

/**
 * The kernel dump's the matrix A in Matrix Market format
*/
void kDumpAuv(IPMK_ArrayNumeric u, IPMK_ArrayNumeric v, IPMK_ArrayNumeric Aii, IPMK_ArrayNumeric Aij,
  IPMK_ArrayNumeric vertices, IPMK_ArrayIndex edges, IPMK_Error *err)
{
  FILE *pfile;
  char name[128] = {0};
  sprintf(name, "A%d.txt", grank);
  pfile = fopen(name, "w");

  int m, n, nnz;
  m = n = IPM_ArrayTotalSize(vertices, err);
  nnz = IPM_ArrayTotalSize(edges, err)*2 + m; // Aij, Aji plus Aii

  //Print out A
  if (!grank) { // rank 0 prints MatrixMarket banner
    fprintf(pfile, "%%%%MatrixMarket matrix coordinate real general\n");
    fprintf(pfile, "%8d %8d %8d\n", m, n, nnz);
  }

  int digs = DECIMAL_DIG;

  // Loop over local edges
  IPMK_ArrayLocalIter iter;
  for (IPMK_ArrayLocalIterInit(edges, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPMK_Index e = IPMK_ArrayLocalIterToIndex(&iter); // index of the edge
    const IPMK_Index *ends = IPMK_ArrayIndexGet(edges, e, err); // indices of vertices of the edge
    IPM_Index i = ends[0];
    IPM_Index j = ends[1];
    IPM_Index gi = IPM_ArrayGetGid(u, i, err) + 1; // MatrixMarket uses 1-based indices
    IPM_Index gj = IPM_ArrayGetGid(u, j, err) + 1;
    IPM_Real *fp = IPMK_ArrayNumericGet(Aij, e, err);
    IPM_Real aij = fp[0];
    IPM_Real aji = fp[1];

    fprintf(pfile, "%8d %8d   %.*e\n", gi, gj, digs, aij);
    fprintf(pfile, "%8d %8d   %.*e\n", gj, gi, digs, aji);
  }

  // Loop over local vertices
  for (IPMK_ArrayLocalIterInit(vertices, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPMK_Index i = IPMK_ArrayLocalIterToIndex(&iter); // index of the edge
    IPM_Index gi = IPM_ArrayGetGid(vertices, i, err)+ 1; // MatrixMarket uses 1-based indices
    IPM_Real aii = *IPMK_ArrayNumericGet(Aii, i, err);
    fprintf(pfile, "%8d %8d   %.*e\n", gi, gi, digs, aii);
  }

  fclose(pfile);

  // Print out u
  name[0] = 0;
  sprintf(name, "u%d.txt", grank);
  pfile = fopen(name, "w");
  for (IPMK_ArrayLocalIterInit(u, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPMK_Index i = IPMK_ArrayLocalIterToIndex(&iter); // 0-based index
    IPM_Index gi = IPM_ArrayGetGid(u, i, err);
    IPM_Real ui = *IPMK_ArrayNumericGet(u, i, err);
    fprintf(pfile, "%8d   %.*e\n", gi, digs, ui);
  }
  fclose(pfile);

  // Print out v
  name[0] = 0;
  sprintf(name, "v%d.txt", grank);
  pfile = fopen(name, "w");
  for (IPMK_ArrayLocalIterInit(v, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPMK_Index i = IPMK_ArrayLocalIterToIndex(&iter); // 0-based index
    IPM_Index gi = IPM_ArrayGetGid(v, i, err);
    IPM_Real vi = *IPMK_ArrayNumericGet(v, i, err);
    fprintf(pfile, "%8d   %.*e\n", gi, digs, vi);
  }
  fclose(pfile);

}


/**
 * This kernel/task is called on a completely empty set of IPM objects.
 *
 * It creates
 *  - 4 element objects that point to
 *  - 8 edge objects that ppoint to
 *  - 5 vertex objects
 *
 * \verbatim
 *          2
 *          o
 *         /|\
 *        / | \
 *       5  4  3
 *      / C | B \
 *     /    |    \
 *  3 o--6--o--1--o 1
 *     \   4|    /
 *      \ D | A /
 *       7  2  0
 *        \ | /
 *         \|/
 *          o
 *          0
 * \endverbatim
 */
void kCreate(IPMK_ArrayNumeric u, IPMK_ArrayNumeric Aii, IPMK_ArrayNumeric Aij,
  IPMK_ArrayNumeric vertices, IPMK_ArrayIndex edges, IPMK_ArrayIndex elements, IPMK_Error *err)
{
  if (IPM_ArrayTotalSize(vertices, err) > 0) IPMK_SetErrorVoid(IPMK_ERROR_WRONG_STATE);
  if (IPM_ArrayTotalSize(edges, err) > 0) IPMK_SetErrorVoid(IPMK_ERROR_WRONG_STATE);
  if (IPM_ArrayTotalSize(elements, err) > 0) IPMK_SetErrorVoid(IPMK_ERROR_WRONG_STATE);

  IPM_Real xy[5][2] = {{0,-1},{1,0},{0,1},{-1,0},{0,0}};
  int edgeid[8][2] = {{0,1},{1,4},{4,0},{2,1},{4,2},{2,3},{3,4},{0,3}};
  int elementid[4][3] = {{0,1,2},{1,4,3},{4,5,6},{7,6,2}}; // element A, B, C, D

  IPM_Real initu[5] = {0.1, 2.0, 0.3, 0.9, 1.5};
  IPM_Real pu[5][DOFS];
  for (int i = 0; i < 5; i++) {
    for (int j = 0; j < DOFS; j++) pu[i][j] = initu[i];
  }

  IPM_Real pAii[5] = {.9, .1, .2, .3, .4};
  IPM_Real pAij[16] = {.9, .2, .3, .3, 5, .6, 2, 2, .9, .1, 1, .12, .13, 3, 1.5, .16};

  // Add vertices, u, Aii
  IPMK_Index vertexindices[5];
  for (int i=0; i<5; i++) {
    vertexindices[i] = IPMK_ArrayNumericAdd(vertices, 2, xy[i],err);
    IPMK_ArrayNumericAdd(u, DOFS, pu[i], err);
    IPMK_ArrayNumericAdd(Aii, 1, &pAii[i], err);
  }

  // Add edges, Aij
  IPMK_Index edgeindices[8];
  for (int i=0; i<8; i++) {
    IPMK_Index ps[2] = {vertexindices[edgeid[i][0]],vertexindices[edgeid[i][1]]};
    edgeindices[i] = IPMK_ArrayIndexAdd(edges, 2, ps, err);
    IPMK_ArrayNumericAdd(Aij, 2, &pAij[i*2], err);
  }

  // Add elements
  for (int i=0; i<4; i++) {
    IPMK_Index ps[3] = {edgeindices[elementid[i][0]], edgeindices[elementid[i][1]], edgeindices[elementid[i][2]]};
    IPMK_ArrayIndexAdd(elements, 3, ps, err);
  }
}


/**
 * The kernel examines every element and decide whether to split its edges
 *
 * If edges are set to be split, the corresponding edgeflags are increased by 1.0
 * On entry edgeflags are 0.0
 *
 */
void kTestElements(IPMK_ArrayNumeric vertices,IPMK_ArrayNumeric edgeflags, IPMK_ArrayIndex edges, IPMK_ArrayIndex elements, IPMK_Error *err)
{
  IPMK_ArrayLocalIter iter;
  for (IPMK_ArrayLocalIterInit(elements, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPM_Index i = IPMK_ArrayLocalIterToIndex(&iter);
    const IPMK_Index *edgeidx = IPMK_ArrayIndexGet(elements, i, err);

    double xm = 0.0, ym = 0.0; // midpoint of the triangle
    double d = 0.0; // perimeter of the triangle
    for (int j=0; j < 3; j++) {
      const IPMK_Index *verts = IPMK_ArrayIndexGet(edges, edgeidx[j], err);
      const double *xyl = IPMK_ArrayNumericGet(vertices,verts[0],err);
      const double *xyr = IPMK_ArrayNumericGet(vertices,verts[1],err);
      xm += xyl[0] + xyr[0];
      ym += xyl[1] + xyr[1];
      d  += sqrt((xyl[0] - xyr[0])*(xyl[0] - xyr[0]) + (xyl[1] - xyr[1])*(xyl[1] - xyr[1]));
    }
    xm /= 6.0;
    ym /= 6.0;
    double d1 = sqrt((xm - 0.5)*(xm - 0.5) + (ym - 0.5)*(ym - 0.5));
    double d2 = sqrt((xm + 0.5)*(xm + 0.5) + (ym + 0.5)*(ym + 0.5));

    if (d1 < 0.5*d || d2 < 0.5*d) {
      for (int i = 0; i < 3; i++) {
        IPM_Real *flag = IPMK_ArrayNumericGet(edgeflags, edgeidx[i], err);
        *flag += 1.0; // Temporarily use double for boolean
      }
    }
  }
}

/**
 * The kernel splits edges tagged by \p edgeflags.
 *
 * To split an edge, it introduces a point in the middle and creates two child edges.
 * \p edgeflags and \p edgechildren have the same layout as \p edges. \p edgechildren
 * is already initialized with default IPM_INDEX_NULL.
 */
void kRefineEdges(IPMK_ArrayNumeric u, IPMK_ArrayNumeric Aii, IPMK_ArrayNumeric Aij,
  IPMK_ArrayNumeric vertices, IPMK_ArrayIndex edgeflags, IPMK_ArrayIndex edgechildren, IPMK_ArrayIndex edges, IPMK_Error *err)
{
  IPMK_ArrayLocalIter iter;

  // Loop over local edges
  for (IPMK_ArrayLocalIterInit(edges, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPMK_Index e = IPMK_ArrayLocalIterToIndex(&iter); // index of the edge
    IPM_Real *flag = IPMK_ArrayNumericGet(edgeflags, e, err);
    IPM_Real fa[2];

    if (*flag > 0.8) { // Without rounding errors, *flag >= 1.0 is enough
      const IPMK_Index *ends = IPMK_ArrayIndexGet(edges, e, err); // indices of vertices of the edge
      const IPM_Index i = ends[0];
      const IPM_Index j = ends[1];

      const IPM_Real *xy0 = IPMK_ArrayNumericGet(vertices, i, err); // coord of vertex i
      const IPM_Real *xy1 = IPMK_ArrayNumericGet(vertices, j, err); // coord of vertex j
      IPM_Real *ui = IPMK_ArrayNumericGet(u, i, err);
      IPM_Real *uj = IPMK_ArrayNumericGet(u, j, err);
      IPM_Real aii = *IPMK_ArrayNumericGet(Aii, i, err);
      IPM_Real ajj = *IPMK_ArrayNumericGet(Aii, j, err);
      IPM_Real *fp = IPMK_ArrayNumericGet(Aij, e, err);
      IPM_Real aij = fp[0];
      IPM_Real aji = fp[1];

      // Add a new vertex k to \p vertices, and simultaneously increase \p u, \p Aii
      IPM_Real xynew[2];
      IPM_Real xm = (xy0[0]+xy1[0])/2.0;
      IPM_Real ym = (xy0[1]+xy1[1])/2.0;
      if (((xy0[0]*xy0[0] + xy0[1]*xy0[1]) > 0.999) && ((xy1[0]*xy1[0] + xy1[1]*xy1[1]) > 0.999)) {
        xynew[0] = (xm < 0.0 ?  -1.0 : 1.0) * 1.0/sqrt(1.0 + (ym*ym)/(xm*xm));
        xynew[1] = (ym/xm)*xynew[0];
      } else {
        xynew[0] = xm;
        xynew[1] = ym;
      }

      IPMK_Index k = IPMK_ArrayNumericAdd(vertices, 2, xynew, err);

      IPM_Real uk[DOFS];
      for (int d = 0; d < DOFS; d++) uk[d]= ui[d]/3 + 2*uj[d]/3;
      IPMK_ArrayNumericAdd(u, DOFS, uk, err);

      IPM_Real akk = aii/3 + 2*ajj/3;
      IPMK_ArrayNumericAdd(Aii, 1, &akk, err);

      // Add two child edges to \p edges, and simultaneously increase \p Aij, \p Aji.
      // Suppose edge (i, j)'s middle point is k, the two edges must be added in
      // (i, k), (k, j) format, so that we can easily
      // recover k in element refinement.
      IPMK_Index indices[2], child[2];
      IPM_Real aik, aki, akj, ajk;

      indices[0] = i; // edge i-k
      indices[1] = k;
      aik = aij/3;
      aki = aji/3;
      child[0] = IPMK_ArrayIndexAdd(edges, 2, indices, err);
      fa[0] = aik; fa[1]= aki; IPMK_ArrayNumericAdd(Aij, 2, fa, err);

      indices[0] = k; // edge k-j
      indices[1] = j;
      akj = aij*2/3;
      ajk = aji*2/3;

      /* This is what amr_ipm.c does.
      child[1] = IPMK_ArrayIndexAdd(edges, 2, indices, err);
      IPMK_ArrayNumericAdd(Aij, 1, &akj, err);
      IPMK_ArrayNumericAdd(Aji, 1, &ajk, err);
      */

      IPMK_ArrayIndexChange(edges, e, 2, indices, err);
      fa[0] = akj; fa[1]= ajk; IPMK_ArrayNumericChange(Aij, e, 2, fa, err);
      child[1] = e;

      IPMK_ArrayIndexChange(edgechildren, e, 2, child, err); // Change the NULL indices to point to the children
    }
  }
}

/**
 * The kernel refines elements if they have split edge(s).
 * If an element is refined, it may have up to 4 child elements.
 */
void kRefineElements(IPMK_ArrayNumeric Aii, IPMK_ArrayNumeric Aij,
  IPMK_ArrayNumeric vertices, IPMK_ArrayIndex edgechildren, IPMK_ArrayIndex edges, IPMK_ArrayIndex elements, IPMK_Error *err)
{
  IPMK_ArrayLocalIter iter;
  for (IPMK_ArrayLocalIterInit(elements, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    int i = IPMK_ArrayLocalIterToIndex(&iter);
    const IPMK_Index *myedges = IPMK_ArrayIndexGet(elements, i, err); // edges of the element
    IPMK_Index e[9] = {IPM_INDEX_NULL}; // A triangle has at most 9 sub-edges
    IPMK_Index ab, bc, ac; // Three edges of the triangle
    IPMK_Index a, b, c, x, y, z; // Three vertices and middle points of the triangle
    IPMK_Index *childedges; // Child edges of an edge by reading edgechildren
    IPMK_Index *verts; // Two ends of an edge
    IPMK_Index *kids[2];
    IPMK_Index newedge[2]; // Temp variable for a new interior edge
    IPMK_Index newelement[3]; // Temp variable for a new child element
    IPMK_Index child[4]; // Indices of (at most) the four children after refinement
    IPMK_Index end0, end1;
    int found_bc, found_ac;
    int edges_split;
    IPM_Real Aaa, Abb, Acc, Axx, Ayy, Azz; // Aii values of a, b, c and possible x, y, z
    IPM_Real Axy, Ayx, Ayz, Azy, Azx, Axz;
    IPM_Real Aay, Aya, Abz, Azb, Acx, Axc;
    IPM_Real fa[2];

    // Discover structure of the triangle. a, b, c are its three vertices.
    // ab, bc, ac are its edges. x, y, z are middle points of edge ab, bc, ac
    // respectively. e0, e1, ..., e5 are child edges of ab, bc, ac in the exact
    // order shown in the diagram below. Note that the middle points and child
    // edges do not nessesarily all exist. If one does not exist, its value
    // will be IPM_INDEX_NULL.
    //
    //                       a
    //                      / \
    //                  e0 /   \ e5
    //                    /     \
    //              ab   x       z    ac
    //                  /         \
    //              e1 /           \ e4
    //                /             \
    //               b-------y-------c
    //                   e2     e3
    //                      bc

    x = y = z = IPM_INDEX_NULL;
    found_bc = found_ac = 0;
    ab = bc = ac = IPM_INDEX_NULL;
    // Note e[0..8] are already initialized to be IPM_INDEX_NULL;

    // Assume the first edge is along ab direction
    childedges = IPMK_ArrayIndexGet(edgechildren, myedges[0], err);
    if (childedges[0] == IPM_INDEX_NULL) { // Edge not split and we have ab
      ab = myedges[0];
      verts = IPMK_ArrayIndexGet(edges, myedges[0], err);
      a = verts[0];
      b = verts[1];
    } else { // We have ax, xb
      assert(childedges[1] != IPM_INDEX_NULL); // if one child exists, the other must also exist.
      kids[0] = IPMK_ArrayIndexGet(edges, childedges[0], err);
      kids[1] = IPMK_ArrayIndexGet(edges, childedges[1], err);
      end0 = kids[0][0];
      end1 = kids[1][1];

      a = end0;
      x = kids[0][1]; assert(x == kids[1][0]);
      b = end1;
      e[0] = childedges[0];
      e[1] = childedges[1];
    }

    // The second edge is either along bc direction or ac direction
    childedges = IPMK_ArrayIndexGet(edgechildren, myedges[1], err);

    if (childedges[0] == IPM_INDEX_NULL) { // Edge not split and we have ac or bc
      verts = IPMK_ArrayIndexGet(edges, myedges[1], err);
      if (a == verts[0]) { // We have ac
        c = verts[1];
        ac = myedges[1];
        found_ac = 1;
      } else if (a == verts[1]) { // We have ca
        c = verts[0];
        ac = myedges[1];
        found_ac = 1;
      } else if (b == verts[0]) {  // We have bc
        c = verts[1];
        bc = myedges[1];
        found_bc = 1;
      } else { // We have cb
        assert(b == verts[1]);
        c = verts[0];
        bc = myedges[1];
        found_bc = 1;
      }
    } else { // either ac or bc is split
      kids[0] = IPMK_ArrayIndexGet(edges, childedges[0], err);
      kids[1] = IPMK_ArrayIndexGet(edges, childedges[1], err);
      end0 = kids[0][0];
      end1 = kids[1][1];

      if (a == end0) { // We have az, zc
        c = end1;
        z = kids[0][1];
        e[5] = childedges[0];
        e[4] = childedges[1];
        found_ac = 1;
      } else if (a == end1) { // We have cz, za
        c = end0;
        z = kids[0][1];
        e[4] = childedges[0];
        e[5] = childedges[1];
        found_ac = 1;
      } else if (b == end0) { // We have by, yc
        c = end1;
        y = kids[0][1];
        e[2] = childedges[0];
        e[3] = childedges[1];
        found_bc = 1;
      } else if (b == end1) { // We have cy, yb
        c = end0;
        y = kids[0][1];
        e[3] = childedges[0];
        e[2] = childedges[1];
        found_bc = 1;
      }
    }

    // The third edge is the one not yet found
    verts = IPMK_ArrayIndexGet(edges, myedges[2], err);
    childedges = IPMK_ArrayIndexGet(edgechildren, myedges[2], err);
    if (!found_bc) {
      if (childedges[0] == IPM_INDEX_NULL) { // bc is not split
        bc = myedges[2];
      } else { // bc is split
          kids[0] = IPMK_ArrayIndexGet(edges, childedges[0], err);
          kids[1] = IPMK_ArrayIndexGet(edges, childedges[1], err);
          end0 = kids[0][0];
          end1 = kids[1][1];
          y = kids[0][1];
           if (b == end0) { // We have by, yc
            e[2] = childedges[0];
            e[3] = childedges[1];
          } else { // We have cy, yb
            e[3] = childedges[0];
            e[2] = childedges[1];
          }
      }
    } else {
      assert(!found_ac);
      if (childedges[0] == IPM_INDEX_NULL) { // ac is not split
        ac = myedges[2];
      } else { // ac is split
          kids[0] = IPMK_ArrayIndexGet(edges, childedges[0], err);
          kids[1] = IPMK_ArrayIndexGet(edges, childedges[1], err);
          end0 = kids[0][0];
          end1 = kids[1][1];
          z = kids[0][1];
           if (a == end0) { // We have az, zc
            e[5] = childedges[0];
            e[4] = childedges[1];
          } else { // We have cz, za
            assert(c == end0 && a == end1);
            e[4] = childedges[0];
            e[5] = childedges[1];
          }
        }
    }

    edges_split = (x == IPM_INDEX_NULL ? 0 : 1) +
                  (y == IPM_INDEX_NULL ? 0 : 1) +
                  (z == IPM_INDEX_NULL ? 0 : 1);

    Aaa = *IPMK_ArrayNumericGet(Aii, a, err);
    Abb = *IPMK_ArrayNumericGet(Aii, b, err);
    Acc = *IPMK_ArrayNumericGet(Aii, c, err);
    if (x != IPM_INDEX_NULL) Axx = *IPMK_ArrayNumericGet(Aii, x, err);
    if (y != IPM_INDEX_NULL) Ayy = *IPMK_ArrayNumericGet(Aii, y, err);
    if (z != IPM_INDEX_NULL) Azz = *IPMK_ArrayNumericGet(Aii, z, err);

    switch (edges_split) {
    case 3:
      // All three edges have benn split, so refine the element as follows
      //                       a
      //                      / \
      //                  e0 /   \ e5
      //                    /     \
      //                   x---e8--z
      //                  / \     / \
      //              e1 /  e6   e7  \ e4
      //                /     \ /     \
      //               b-------y-------c
      //                   e2     e3
      Axy = (Axx + Ayy)/2; Ayx = - Axy;
      Ayz = (Ayy + Azz)/2; Azy = - Ayz;
      Azx = (Azz + Axx)/2; Axz = - Azx;

      newedge[0] = x; newedge[1] = y; e[6] = IPMK_ArrayIndexAdd(edges, 2, newedge, err);
      fa[0] = Axy; fa[1] = Ayx; IPMK_ArrayNumericAdd(Aij, 2, fa, err);

      newedge[0] = y; newedge[1] = z; e[7] = IPMK_ArrayIndexAdd(edges, 2, newedge, err);
      fa[0] = Ayz; fa[1] = Azy; IPMK_ArrayNumericAdd(Aij, 2, fa, err);

      newedge[0] = z; newedge[1] = x; e[8] = IPMK_ArrayIndexAdd(edges, 2, newedge, err);
      fa[0] = Azx; fa[1] = Axz; IPMK_ArrayNumericAdd(Aij, 2, fa, err);

      newelement[0] = e[0];  newelement[1] = e[8];  newelement[2] = e[5];
      child[0] = IPMK_ArrayIndexAdd(elements, 3, newelement, err);

      newelement[0] = e[1];  newelement[1] = e[2];  newelement[2] = e[6];
      child[1] = IPMK_ArrayIndexAdd(elements, 3, newelement, err);

      newelement[0] = e[6];  newelement[1] = e[7];  newelement[2] = e[8];
      child[2] = IPMK_ArrayIndexAdd(elements, 3, newelement, err);

      newelement[0] = e[7];  newelement[1] = e[3];  newelement[2] = e[4];
      //child[3] = IPMK_ArrayIndexAdd(elements, 3, newelement, err);
      IPMK_ArrayIndexChange(elements, i, 3, newelement, err);
      break;
    case 2:
      // Only two edges have been split. There are three cases shown below.
      //            a                     a                      a
      //           /|\                   /|\                    / \
      //       e0 / | \                 / | \ e5            e0 /   \e5
      //         /  |  \               /  |  \                /     \
      //        x   e7  \ac         ab/   e7  z              x---e6--z
      //       / \  |    \           /    |  / \            /     .`  \
      //   e1 /  e6 |     \         /     | e6  \e4     e1 /   e7      \e4
      //     /     \|      \       /      |/     \        /. `          \
      //    b-------y-------c     b-------y-------c      b---------------c
      //        e2     e3             e2     e3                 bc
      if (x != IPM_INDEX_NULL && y != IPM_INDEX_NULL) {
        // x, y exist
        Axy = (Axx + Ayy)/2; Ayx = - Axy;
        Aay = (Aaa + Ayy)/2; Aya = - Aay;

        newedge[0] = x; newedge[1] = y; e[6] = IPMK_ArrayIndexAdd(edges, 2, newedge, err);
        fa[0] = Axy; fa[1] = Ayx; IPMK_ArrayNumericAdd(Aij, 2, fa, err);

        newedge[0] = a; newedge[1] = y; e[7] = IPMK_ArrayIndexAdd(edges, 2, newedge, err);
        fa[0] = Aay; fa[1] = Aya; IPMK_ArrayNumericAdd(Aij, 2, fa, err);

        newelement[0] = e[0];  newelement[1] = e[6];  newelement[2] = e[7];
        child[0] = IPMK_ArrayIndexAdd(elements, 3, newelement, err);

        newelement[0] = e[1];  newelement[1] = e[2];  newelement[2] = e[6];
        child[1] = IPMK_ArrayIndexAdd(elements, 3, newelement, err);

        newelement[0] = e[7];  newelement[1] = e[3];  newelement[2] = ac;
        //child[2] = IPMK_ArrayIndexAdd(elements, 3, newelement, err);
      } else if (y != IPM_INDEX_NULL && z != IPM_INDEX_NULL) {
        // y, z exist
        Ayz = (Ayy + Azz)/2; Azy = - Ayz;
        Aay = (Aaa + Ayy)/2; Aya = - Aay;

        newedge[0] = y; newedge[1] = z; e[6] = IPMK_ArrayIndexAdd(edges, 2, newedge, err);
        fa[0] = Ayz; fa[1] = Azy; IPMK_ArrayNumericAdd(Aij, 2, fa, err);

        newedge[0] = a; newedge[1] = y; e[7] = IPMK_ArrayIndexAdd(edges, 2, newedge, err);
        fa[0] = Aay; fa[1] = Aya; IPMK_ArrayNumericAdd(Aij, 2, fa, err);

        newelement[0] = ab;  newelement[1] = e[2];  newelement[2] = e[7];
        child[0] = IPMK_ArrayIndexAdd(elements, 3, newelement, err);

        newelement[0] = e[6];  newelement[1] = e[3];  newelement[2] = e[4];
        child[1] = IPMK_ArrayIndexAdd(elements, 3, newelement, err);

        newelement[0] = e[7];  newelement[1] = e[6];  newelement[2] = e[5];
        //child[2] = IPMK_ArrayIndexAdd(elements, 3, newelement, err);
      } else {
        // x, z exist
        Axz = (Axx + Azz)/2; Azx = - Axz;
        Abz = (Abb + Azz)/2; Azb = - Abz;

        newedge[0] = x; newedge[1] = z; e[6] = IPMK_ArrayIndexAdd(edges, 2, newedge, err);
        fa[0] = Axz; fa[1] = Azx; IPMK_ArrayNumericAdd(Aij, 2, fa, err);

        newedge[0] = b; newedge[1] = z; e[7] = IPMK_ArrayIndexAdd(edges, 2, newedge, err);
        fa[0] = Abz; fa[1] = Azb; IPMK_ArrayNumericAdd(Aij, 2, fa, err);

        newelement[0] = e[0];  newelement[1] = e[6];  newelement[2] = e[5];
        child[0] = IPMK_ArrayIndexAdd(elements, 3, newelement, err);

        newelement[0] = e[1];  newelement[1] = e[7];  newelement[2] = e[6];
        child[1] = IPMK_ArrayIndexAdd(elements, 3, newelement, err);

        newelement[0] = e[7];  newelement[1] = bc;  newelement[2] = e[4];
        //child[2] = IPMK_ArrayIndexAdd(elements, 3, newelement, err);
      }
      IPMK_ArrayIndexChange(elements, i, 3, newelement, err); // Use one child to overwrite parent
      break;
    case 1:
      // Only one edge has been split. There are three cases shown below.
      //            a                     a                      a
      //           / \                   /|\                    / \
      //       e0 /   \                 / | \                  /   \e5
      //         /     \               /  |  \                /     \
      //        x       \ac         ab/   e6  \ac          ab/       z
      //       /  ` .    \           /    |    \            /     .`  \
      //   e1 /      e6.  \         /     |     \          /   e6      \e4
      //     /          ` .\       /      |      \        /. `          \
      //    b---------------c     b-------y-------c      b---------------c
      //          bc                  e2     e3                 bc
      if (x != IPM_INDEX_NULL) {
        Acx = (Acc + Axx)/2; Axc = - Acx;

        newedge[0] = c; newedge[1] = x; e[6] = IPMK_ArrayIndexAdd(edges, 2, newedge, err);
        fa[0] = Acx; fa[1] = Axc; IPMK_ArrayNumericAdd(Aij, 2, fa, err);

        newelement[0] = e[0];  newelement[1] = e[6];  newelement[2] = ac;
        child[0] = IPMK_ArrayIndexAdd(elements, 3, newelement, err);

        newelement[0] = e[1];  newelement[1] = bc;  newelement[2] = e[6];
        //child[1] = IPMK_ArrayIndexAdd(elements, 3, newelement, err);
      } else if (y != IPM_INDEX_NULL) {
        Aay = (Aaa + Ayy)/2; Aya = - Aay;

        newedge[0] = a; newedge[1] = y; e[6] = IPMK_ArrayIndexAdd(edges, 2, newedge, err);
        fa[0] = Aay; fa[1] = Aya; IPMK_ArrayNumericAdd(Aij, 2, fa, err);

        newelement[0] = ab;  newelement[1] = e[2];  newelement[2] = e[6];
        child[0] = IPMK_ArrayIndexAdd(elements, 3, newelement, err);

        newelement[0] = e[6];  newelement[1] = e[3];  newelement[2] = ac;
        //child[1] = IPMK_ArrayIndexAdd(elements, 3, newelement, err);
      } else {
        Abz = (Abb + Azz)/2; Azb = - Abz;

        newedge[0] = b; newedge[1] = z; e[6] = IPMK_ArrayIndexAdd(edges, 2, newedge, err);
        fa[0] = Abz; fa[1] = Azb; IPMK_ArrayNumericAdd(Aij, 2, fa, err);

        newelement[0] = ab;  newelement[1] = e[6];  newelement[2] = e[5];
        child[0] = IPMK_ArrayIndexAdd(elements, 3, newelement, err);

        newelement[0] = e[6];  newelement[1] = bc;  newelement[2] = e[4];
        //child[1] = IPMK_ArrayIndexAdd(elements, 3, newelement, err);
      }
      IPMK_ArrayIndexChange(elements, i, 3, newelement, err); // Use one child to overwrite parent
      break;
    case 0:
      // None of the edges has been split. The element won't be refined.
      break;
    }
  }
}

/**
 * The kernel computes v = Au
 * On entry to the kernel, suppose v is initialized to zero.
 *
 */
void kSpMV(IPMK_ArrayNumeric v, IPMK_ArrayNumeric u, IPMK_ArrayNumeric Aii,
  IPMK_ArrayNumeric vertices, IPMK_ArrayNumeric Aij, IPMK_ArrayIndex edges, IPMK_Error *err)
{
  IPMK_ArrayLocalIter iter;

  // Loop over local edges
  for (IPMK_ArrayLocalIterInit(edges, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPMK_Index e = IPMK_ArrayLocalIterToIndex(&iter); // index of the edge
    const IPMK_Index *ends = IPMK_ArrayIndexGet(edges, e, err); // indices of vertices of the edge
    IPM_Index i = ends[0];
    IPM_Index j = ends[1];
    IPM_Real *ui = IPMK_ArrayNumericGet(u, i, err);
    IPM_Real *uj = IPMK_ArrayNumericGet(u, j, err);
    IPM_Real *vi = IPMK_ArrayNumericGet(v, i, err);
    IPM_Real *vj = IPMK_ArrayNumericGet(v, j, err);

    IPM_Real *fp = IPMK_ArrayNumericGet(Aij, e, err);
    IPM_Real aij = fp[0];
    IPM_Real aji = fp[1];

    for (int d = 0; d < DOFS; d++) {
        vi[d] += aij*uj[d];
        vj[d] += aji*ui[d];
    }
  }

  // Loop over local vertices
  for (IPMK_ArrayLocalIterInit(vertices, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPMK_Index i = IPMK_ArrayLocalIterToIndex(&iter); // index of the edge
    IPM_Real *ui = IPMK_ArrayNumericGet(u, i, err);
    IPM_Real *vi = IPMK_ArrayNumericGet(v, i, err);
    IPM_Real aii = *IPMK_ArrayNumericGet(Aii, i, err);

    for (int d = 0; d < DOFS; d++) vi[d] += aii*ui[d];
  }
}

typedef struct {
  IPM_Index *rowptr;
  IPM_Index *colidx;
  IPM_Real *val;
} CSR;

static CSR *ConstructCSR(IPMK_ArrayNumeric Aii, IPMK_ArrayNumeric vertices, IPMK_ArrayNumeric Aij, IPMK_ArrayIndex edges, IPMK_Error *err)
{
  CSR *csr;
  IPM_Index rows = IPM_ArrayVisibleSize(vertices, err);
  IPM_Index nnz;
  IPM_Index *rowptr = calloc(rows+1, sizeof(IPM_Index));
  IPM_Index *colidx;
  IPM_Real *val;
  IPMK_ArrayLocalIter iter;

  // Loop over local edges and local vertices to setup rowptr[], which initially stores # of neighbors of each vertex
  for (IPMK_ArrayLocalIterInit(edges, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
     IPM_Index e = IPMK_ArrayLocalIterToIndex(&iter); // index of the edge
     const IPM_Index *ends = IPMK_ArrayIndexGet(edges, e, err); // indices of vertices of the edge
     IPM_Index i = ends[0];
     IPM_Index j = ends[1];
     rowptr[i+1]++; // shift i, j by 1 to easy later adjustment
     rowptr[j+1]++;
  }

  for (IPMK_ArrayLocalIterInit(vertices, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
     IPM_Index i = IPMK_ArrayLocalIterToIndex(&iter); // index of the vertiex
     rowptr[i+1]++;
  }

  // Adjust rowtpr to the correct CSR format. Note that rowptr[0] was init'ed to 0
  for (int i = 1; i < rows + 1; i++) rowptr[i] += rowptr[i-1];

  nnz = rowptr[rows];
  //assert(nnz == edges->layout->nlocal*2 + vertices->layout->nlocal);

  // Once nnz is known, we can set up colidx[] and val[]
  colidx = malloc(sizeof(IPM_Index)*nnz);
  val = malloc(sizeof(IPM_Real)*nnz);

  // Fill in Aij and Aii
  IPM_Index *cur = calloc(rows, sizeof(IPM_Index)); // record current positions of non-zeros

  for (IPMK_ArrayLocalIterInit(edges, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
     IPM_Index e = IPMK_ArrayLocalIterToIndex(&iter); // index of the edge
     const IPM_Index *ends = IPMK_ArrayIndexGet(edges, e, err); // indices of vertices of the edge
     IPM_Index i = ends[0];
     IPM_Index j = ends[1];
     IPM_Real *fp = IPMK_ArrayNumericGet(Aij, e, err);
     val[rowptr[i] + cur[i]] = fp[0]; // aij
     colidx[rowptr[i] + cur[i]] = j;
     cur[i]++;

     val[rowptr[j] + cur[j]] = fp[1]; // aji
     colidx[rowptr[j] + cur[j]] = i;
     cur[j]++;
  }

  for (IPMK_ArrayLocalIterInit(vertices, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
     IPM_Index i = IPMK_ArrayLocalIterToIndex(&iter); // index of the edge
     val[rowptr[i] + cur[i]] = *IPMK_ArrayNumericGet(Aii, i, err);
     colidx[rowptr[i] + cur[i]] = i;
     cur[i]++;
  }

  // TODO: One may sort val according to colidx per row, though improvement is unknown
  free(cur);
  csr = (CSR *)malloc(sizeof(CSR));
  csr->rowptr = rowptr;
  csr->colidx = colidx;
  csr->val = val;

  return csr;
}

static void DestructCSR(CSR* csr)
{
  assert(csr);
  free(csr->rowptr);
  free(csr->colidx);
  free(csr->val);
}

// Compute v = Ku, with K stored on Aii and Aij
void kSpMVCSR(IPMK_ArrayNumeric v, IPMK_ArrayNumeric u, IPMK_ArrayNumeric Aii, IPMK_ArrayNumeric vertices,
  IPMK_ArrayNumeric Aij, IPMK_ArrayIndex edges, IPMK_Error *err)
{
  CSR *csr = (CSR*)IPM_ArrayLookupContext((IPM_Array)edges, "SpMVCSR", err);

  if (csr == NULL) {
    csr = ConstructCSR(Aii, vertices, Aij, edges, err);
    assert (csr);
    IPM_ArrayAttachContext(edges, "SpMVCSR", csr, DestructCSR, err);
  }

  IPM_Index rows = IPM_ArrayVisibleSize(vertices, err);
  IPM_Real *udata = IPMK_ArrayNumericGet(u, 0, err);
  IPM_Real *vdata = IPMK_ArrayNumericGet(v, 0, err);
  IPM_Real res[DOFS] = {0};

  for (int i = 0; i < rows; i++) {
    for (int j = csr->rowptr[i]; j < csr->rowptr[i+1]; j++) {
      IPM_Real alpha = csr->val[j];
      IPM_Real *uptr = &udata[DOFS*csr->colidx[j]];
      //for (int d = 0; d < DOFS; d++) { res[d] += alpha*uptr[d]; }
    }
    //for (int d = 0; d < DOFS; d++) { vdata[DOFS*i + d] = res[d]; res[d] = 0; }
  }
}

int main(int argc,char **args)
{
  char filename[128];
  int rank;
  double elapsed_time;
  IPM_Error err;
  int niter = 3;

  IPM_Initialize(&argc,&args,&err);

  MPI_Comm_rank(MPI_COMM_WORLD,&rank);

  if (!rank) {
    if (argc !=2) {
      printf("Usage: %s <niter>\n", args[0]);
      MPI_Abort(MPI_COMM_WORLD, -1);
    }
    niter = atoi(args[1]);
    MPI_Bcast(&niter, 1, MPI_INT, 0, MPI_COMM_WORLD);
  } else {
    MPI_Bcast(&niter, 1, MPI_INT, 0, MPI_COMM_WORLD);
  }

#ifdef HAVE_HPCTOOLKIT
  hpctoolkit_sampling_stop();
#endif

#if defined(HAVE_CAIRO)
  snprintf(filename,128,"pdffile_%d.pdf",rank);
  cairo_surface_t *surface = cairo_pdf_surface_create(filename,800,800);
  cr = cairo_create (surface);
  cairo_scale (cr, 700, 700);
  cairo_set_line_width (cr, 1.0/400.);
  cairo_set_source_rgb (cr, 0, 0, 0);
  cairo_set_font_size(cr, 1.0/80);

#endif

  IPM_ArrayNumeric vertices = IPM_ArrayNumericCreate("vertices", &err);
  IPM_ArrayIndex edges = IPM_ArrayIndexCreate("edges", vertices, &err);
  IPM_ArrayIndex elements = IPM_ArrayIndexCreate("elements", edges, &err);

  IPM_ArrayNumeric u = IPM_ArrayNumericClone("u", vertices, DOFS, 0, 0, &err);
  IPM_ArrayNumeric Aii = IPM_ArrayNumericClone("Aii", vertices, 1, 0, 0, &err);
  IPM_ArrayNumeric Aij = IPM_ArrayNumericClone("Aij", edges, 2, 0, 0, &err);

  // Create kernel functions
  IPM_Function myCreate = IPM_FunctionCreate(IPM_FUNCTION_REPARTITION, kCreate, 6,
            IPM_MEMORY_EXPAND, // u
            IPM_MEMORY_EXPAND, // Aii
            IPM_MEMORY_EXPAND, // Aij
            IPM_MEMORY_EXPAND, // vertices
            IPM_MEMORY_EXPAND, // edges
            IPM_MEMORY_EXPAND, // elements
            &err);

  IPM_Function myDrawAuv = IPM_FunctionCreate(IPM_FUNCTION_KERNEL, kDrawAuv, 7,
            IPM_MEMORY_READ, // u
            IPM_MEMORY_READ, // v
            IPM_MEMORY_READ, // Aii
            IPM_MEMORY_READ, // Aij
            IPM_MEMORY_READ, // vertices
            IPM_MEMORY_READ, // edges
            IPM_MEMORY_READ, // elements
            &err);

  IPM_Function myDrawMesh = IPM_FunctionCreate(IPM_FUNCTION_KERNEL, kDrawMesh, 3,
            IPM_MEMORY_READ, // vertices
            IPM_MEMORY_READ, // edges
            IPM_MEMORY_READ, // elements
            &err);

  IPM_Function myDumpAuv = IPM_FunctionCreate(IPM_FUNCTION_KERNEL, kDumpAuv, 7,
            IPM_MEMORY_READ, // u
            IPM_MEMORY_READ, // v
            IPM_MEMORY_READ, // Aii
            IPM_MEMORY_READ, // Aij
            IPM_MEMORY_READ, // vertices
            IPM_MEMORY_READ, // edges
            IPM_MEMORY_READ, // elements
            &err);

  IPM_Function myTestElements = IPM_FunctionCreate(IPM_FUNCTION_KERNEL, kTestElements, 4,
            IPM_MEMORY_READ, // vertices
            IPM_MEMORY_ADD,  // edgeflags
            IPM_MEMORY_READ, // edges
            IPM_MEMORY_READ, // elements
            &err);

  IPM_Function myRefineEdges = IPM_FunctionCreate(IPM_FUNCTION_KERNEL, kRefineEdges, 7,
            IPM_MEMORY_EXPAND, // u
            IPM_MEMORY_EXPAND, // Aii
            IPM_MEMORY_EXPAND, // Aij
            IPM_MEMORY_EXPAND, // vertices
            IPM_MEMORY_READ,   // edgeflags
            IPM_MEMORY_WRITE,  // edgechildren
            IPM_MEMORY_EXPAND, // edges
            &err);

  IPM_Function myRefineElements = IPM_FunctionCreate(IPM_FUNCTION_REPARTITION, kRefineElements, 6,
            IPM_MEMORY_READ,   // Aii
            IPM_MEMORY_EXPAND, // Aij
            IPM_MEMORY_READ,   // vertices
            IPM_MEMORY_READ,   // edgechildren
            IPM_MEMORY_EXPAND, // edges
            IPM_MEMORY_EXPAND, // elements
            &err);

  IPM_Function mySpMV = IPM_FunctionCreate(IPM_FUNCTION_KERNEL, kSpMV, 6,
            IPM_MEMORY_ADD,  // v
            IPM_MEMORY_READ, // u
            IPM_MEMORY_READ, // Aii
            IPM_MEMORY_READ, // vertices
            IPM_MEMORY_READ, // Aij
            IPM_MEMORY_READ, // edges
            &err);

  IPM_Function mySpMVCSR = IPM_FunctionCreate(IPM_FUNCTION_KERNEL, kSpMVCSR, 6,
            IPM_MEMORY_ADD,  // v
            IPM_MEMORY_READ, // u
            IPM_MEMORY_READ, // Aii
            IPM_MEMORY_READ, // vertices
            IPM_MEMORY_READ, // Aij
            IPM_MEMORY_READ, // edges
            &err);

  IPM_Launch(myCreate, u, Aii, Aij, vertices, edges, elements, &err);
  IPM_ArrayNumeric v;
  double etime = 0;

  int count = 500;

  for (int i = 0; i < niter; i++) {
    IPM_ArrayNumeric edgeflags = IPM_ArrayNumericClone("edgeflags", edges, 1/*deflen*/, 0.0/*defval*/, 1/*temp-clone*/, &err);
    IPM_Launch(myTestElements, vertices, edgeflags, edges, elements, &err);

    IPM_ArrayIndex edgechildren= IPM_ArrayIndexClone("edgechildren", edges, 2/*deflen*/, IPM_INDEX_NULL/*defval*/, 1, 0, &err);
    IPM_Launch(myRefineEdges, u, Aii, Aij, vertices, edgeflags, edgechildren, edges, &err);
    IPM_Launch(myRefineElements, Aii, Aij, vertices, edgechildren, edges, elements, &err);
    v = IPM_ArrayNumericClone("v", vertices, DOFS/*deflen*/, 0.0/*defval*/, 1/*temp-clone*/, &err);

    if (i == niter - 1) {
      IPM_ArrayNumericCreateAddContext(v, &err);
      for (int k = 0; k < 10; k++) IPM_Launch(mySpMVCSR, v, u, Aii, vertices, Aij, edges, &err);

//#ifdef HAVE_HPCTOOLKIT
//  hpctoolkit_sampling_start();
//#endif
      etime = -MPI_Wtime();
      for (int k = 0; k < count; k++) IPM_Launch(mySpMVCSR, v, u, Aii, vertices, Aij, edges, &err);
      etime +=MPI_Wtime();
//#ifdef HAVE_HPCTOOLKIT
//  hpctoolkit_sampling_stop();
//#endif

      IPM_ArrayNumericDestroyAddContext(v, &err);
    } else {
      IPM_Launch(mySpMV, v, u, Aii, vertices, Aij, edges, &err);
    }

    // Draw the final mesh before v is destroyed
    if (i == niter -1) {
      //IPM_Launch(myDrawAuv, u, v, Aii, Aij, vertices, edges, elements, &err);
      //IPM_Launch(myDrawMesh, vertices, edges, elements, &err);
      //IPM_Launch(myDumpAuv, u, v, Aii, Aij, vertices, edges, elements, &err);
    }

    IPM_ArrayNumericSwap(u, v, &err);

#if 1
    if (rank == 0) printf("After iteration %d : %d elements, %d edges, %d vertices\n", i,
        IPM_ArrayTotalSize(elements, &err), IPM_ArrayTotalSize(edges, &err), IPM_ArrayTotalSize(vertices, &err));
#endif

    IPM_ArrayNumericDestroy(edgeflags, &err);
    IPM_ArrayNumericDestroy(v, &err);
    IPM_ArrayIndexDestroy(edgechildren, &err);
  }// for

  if (!rank) printf("Time used on %d times SpMV (seconds): %.3f\n", count, etime);

  // Destroy everything before exit
  IPM_ArrayNumericDestroy(vertices, &err);
  IPM_ArrayNumericDestroy(Aii, &err);
  IPM_ArrayNumericDestroy(Aij, &err);
  IPM_ArrayNumericDestroy(u, &err);
  IPM_ArrayIndexDestroy(edges, &err);
  IPM_ArrayIndexDestroy(elements, &err);

  IPM_FunctionDestroy(myCreate, &err);
  IPM_FunctionDestroy(myDrawAuv, &err);
  IPM_FunctionDestroy(myDrawMesh, &err);
  IPM_FunctionDestroy(myDumpAuv, &err);
  IPM_FunctionDestroy(myTestElements, &err);
  IPM_FunctionDestroy(myRefineEdges, &err);
  IPM_FunctionDestroy(myRefineElements, &err);
  IPM_FunctionDestroy(mySpMV, &err);
  IPM_FunctionDestroy(mySpMVCSR, &err);

#if defined(HAVE_CAIRO)
  cairo_destroy(cr);
  cairo_surface_destroy (surface);
#endif

  IPM_Finalize(&err);
  return 0;
}
