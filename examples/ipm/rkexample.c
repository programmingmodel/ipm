/*
 Copyright (c) 2015 UChicago Argonne, LLC

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>
#include <ipm.h>
//#include <ipm_rk.h>


/*
  Initialize Q

*/
static void kCreate(IPMK_ArrayNumeric Q,IPMK_Error *err)
{
  double one = 1.0;
  IPMK_ArrayNumericAdd(Q,1,&one,err);
}

static void krhs(IPMK_ArrayNumeric Q,IPMK_ArrayNumeric W,IPMK_Error *err)
{
  IPMK_ArrayLocalIter iter;
  for (IPMK_ArrayLocalIterInit(Q, &iter); !IPMK_ArrayLocalIterDone(&iter); IPMK_ArrayLocalIterNext(&iter)) {
    IPM_Index i = IPMK_ArrayLocalIterToIndex(&iter);
    for (int j=0; j<IPM_ArrayGetLength(Q,i,err); j++) {
      IPMK_ArrayNumericGet(W,i,err)[j] = 2*IPMK_ArrayNumericGet(Q,i,err)[j];
    }
  }
}
int main(int argc,char **args)
{
  IPM_Error err;
  IPM_Initialize(&argc,&args,&err);

  RK4 rk;

  // Create the solution vector and initialize it with 1.0
  IPM_ArrayNumeric Q = IPM_ArrayNumericCreate("Q", &err);
  IPM_Function     myCreate = IPM_FunctionCreate(IPM_FUNCTION_REPARTITION, kCreate, 1,IPM_MEMORY_EXPAND,&err);
  IPM_Launch(myCreate, Q, &err);

  IPM_Function myrhs     = IPM_FunctionCreate(IPM_FUNCTION_DEFAULT,krhs,2,IPM_MEMORY_READ,IPM_MEMORY_WRITE,&err);


  RK4Init(&rk,Q,myrhs);

  RK4Solve(&rk);
  IPM_Finalize(&err);
  return 0;
}  



