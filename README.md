This is a library for the IPM programming model.

To build, type "CC=/path/to/mpicc cmake . -DPARMETIS_DIR=/path/to/parmetis -DCAIRO_DIR=/path/to/cairo".
The width of idx_t and real_t of ParMETIS should match that of IPM_Index and IPM_Real.
